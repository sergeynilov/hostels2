-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.26-0ubuntu0.18.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for Hostels2
CREATE DATABASE IF NOT EXISTS `Hostels2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `Hostels2`;


-- Dumping structure for table Hostels2.ad_categories
CREATE TABLE IF NOT EXISTS `ad_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_categories_name_unique` (`name`),
  KEY `categories_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_facilities
CREATE TABLE IF NOT EXISTS `ad_facilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_facilities_name_unique` (`name`),
  KEY `facilities_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_groups
CREATE TABLE IF NOT EXISTS `ad_groups` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_groups_name_unique` (`name`),
  UNIQUE KEY `ad_groups_description_unique` (`description`),
  KEY `groups_created_at_index` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_highlights
CREATE TABLE IF NOT EXISTS `ad_highlights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_hostels
CREATE TABLE IF NOT EXISTS `ad_hostels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `subregion_id` int(11) DEFAULT NULL,
  `state_id` int(11) NOT NULL,
  `town` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street_addr` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_descr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance` int(11) DEFAULT NULL,
  `website` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_priority_listing` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `is_homepage_spotlight` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `book_now_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` decimal(4,2) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `payment_log_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_expired_at` datetime DEFAULT NULL,
  `next_payment_at` date DEFAULT NULL,
  `subscription_id` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_gateway` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'E',
  `view_count` int(11) NOT NULL DEFAULT '0',
  `about_us` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancellation_policy` text COLLATE utf8mb4_unicode_ci,
  `must_read_information` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `fk_bp_status_postal_code_idx` (`status`,`postal_code`),
  KEY `fk_locations_idx` (`state_id`,`region_id`,`subregion_id`,`town`),
  KEY `fk_bp_hostel_1_idx` (`user_id`),
  KEY `fk_bp_hostel_1_idx2` (`region_id`),
  KEY `fk_bp_hostel_subregion` (`subregion_id`),
  KEY `fk_bp_hostel_2_idx` (`state_id`),
  KEY `fk_bp_hostel_is_priority_listing` (`is_priority_listing`),
  KEY `fk_bp_hostel_is_homepage_spotlight` (`is_homepage_spotlight`),
  KEY `fk_bp_status_idx` (`status`),
  KEY `fk_bp_hostel_subscription_id` (`subscription_id`),
  KEY `fk_bp_hostel_1_idx1` (`modified_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_hostel_extra_details
CREATE TABLE IF NOT EXISTS `ad_hostel_extra_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostel_id` int(11) NOT NULL,
  `num_beds` int(11) NOT NULL,
  `min_nights` tinyint(1) NOT NULL,
  `max_nights` tinyint(1) DEFAULT NULL,
  `bedsheets` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `towels` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `parking` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `airport_train` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `luggage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `coed_dorm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `bathroom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `credit_cards` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `nonsmoking_rooms` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `smoke_free` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `pets_allowed` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `service_animals` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `wheelchair` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `internet_computers` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `wireless_internet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `checkout` int(11) NOT NULL DEFAULT '0',
  `reception_hours_start` int(11) NOT NULL DEFAULT '0',
  `reception_hours_end` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bp_hostel_extra_details_hostel_id` (`hostel_id`),
  KEY `ind_bp_hostel_extra_details` (`bedsheets`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_hostel_facilities
CREATE TABLE IF NOT EXISTS `ad_hostel_facilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `facility_id` tinyint(1) NOT NULL,
  `hostel_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bp_hostel_facility_facility_id_idx` (`facility_id`),
  KEY `fk_bp_hostel_facility_hostel_id_idx` (`hostel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_hostel_images
CREATE TABLE IF NOT EXISTS `ad_hostel_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostel_id` int(10) unsigned NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  `is_video` tinyint(1) NOT NULL DEFAULT '0',
  `video_width` smallint(6) DEFAULT NULL,
  `video_height` smallint(6) DEFAULT NULL,
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostel_images_id_filename_unique` (`hostel_id`,`filename`),
  KEY `hostel_images_id_is_main` (`hostel_id`,`is_main`),
  KEY `hostel_images_id_is_video_filename` (`hostel_id`,`is_video`,`filename`),
  KEY `hostel_created_at_index` (`created_at`),
  CONSTRAINT `ad_hostel_images_hostel_id_foreign` FOREIGN KEY (`hostel_id`) REFERENCES `ad_hostels` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_hostel_inqueries
CREATE TABLE IF NOT EXISTS `ad_hostel_inqueries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostel_id` int(11) NOT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `request_callback` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' N=>New, A=>Accepted, O=> Completed, C => Cancelled ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bp_hostel_inquery_status_email_ixd` (`status`,`email`),
  KEY `fk_bp_hostel_inquery_hostel_id_idx` (`hostel_id`),
  KEY `hostel_inqueries_creator_id_foreign` (`creator_id`),
  CONSTRAINT `ad_hostel_inqueries_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `ad_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_hostel_reviews
CREATE TABLE IF NOT EXISTS `ad_hostel_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostel_id` int(11) NOT NULL,
  `email_inquiried` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `stars_rating_type_id` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bp_hostel_review_status_created_at_idx` (`status`,`created_at`),
  KEY `fk_bp_hostel_review_hostel_id_idx` (`hostel_id`),
  KEY `fk_bp_hostel_review_email_inquiried_idx` (`email_inquiried`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_hostel_rooms
CREATE TABLE IF NOT EXISTS `ad_hostel_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostel_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `price` decimal(7,2) NOT NULL,
  `is_dorm` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `is_private` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostel_room_all` (`hostel_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_location_data
CREATE TABLE IF NOT EXISTS `ad_location_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subregion` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suburb` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_location_data_state_ind` (`state`),
  KEY `fk_location_data_region_ind` (`region`),
  KEY `fk_location_data_subregion_ind` (`subregion`),
  KEY `fk_location_data_suburb_ind` (`suburb`),
  KEY `fk_location_data_postcode_ind` (`postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_messages
CREATE TABLE IF NOT EXISTS `ad_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` int(10) unsigned NOT NULL,
  `to` int(10) unsigned NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ind_messages_read_to` (`read`,`to`),
  KEY `ind_messages_from_read_to` (`from`,`to`,`read`),
  KEY `messages_to_foreign` (`to`),
  KEY `messages_created_at_index` (`created_at`),
  CONSTRAINT `ad_messages_from_foreign` FOREIGN KEY (`from`) REFERENCES `ad_users` (`id`),
  CONSTRAINT `ad_messages_to_foreign` FOREIGN KEY (`to`) REFERENCES `ad_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_migrations
CREATE TABLE IF NOT EXISTS `ad_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_news
CREATE TABLE IF NOT EXISTS `ad_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(260) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_shortly` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` int(10) unsigned NOT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `is_homepage` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_news_slug_unique` (`slug`),
  KEY `page_contents_created_at_index` (`created_at`),
  KEY `page_contents_is_homepage_published_index` (`is_homepage`,`published`),
  KEY `page_contents_creator_id_published_title_index` (`creator_id`,`published`,`title`),
  KEY `page_contents_source_type_published_index` (`source_type`,`published`),
  CONSTRAINT `ad_news_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `ad_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_password_resets
CREATE TABLE IF NOT EXISTS `ad_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `ad_password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_permissions
CREATE TABLE IF NOT EXISTS `ad_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_permission_role
CREATE TABLE IF NOT EXISTS `ad_permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `ad_permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `ad_permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `ad_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ad_permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `ad_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_personal_hostel_bookmarks
CREATE TABLE IF NOT EXISTS `ad_personal_hostel_bookmarks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `hostel_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_options_hostel_id_name_unique` (`user_id`,`hostel_id`),
  KEY `personal_hostel_bookmarks_hostel_id_foreign` (`hostel_id`),
  CONSTRAINT `ad_personal_hostel_bookmarks_hostel_id_foreign` FOREIGN KEY (`hostel_id`) REFERENCES `ad_hostels` (`id`),
  CONSTRAINT `ad_personal_hostel_bookmarks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `ad_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_personal_options
CREATE TABLE IF NOT EXISTS `ad_personal_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_options_user_id_name_unique` (`user_id`,`name`),
  CONSTRAINT `ad_personal_options_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `ad_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_regions
CREATE TABLE IF NOT EXISTS `ad_regions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state_id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_regions_name_state_id_unique` (`name`,`state_id`),
  UNIQUE KEY `ad_regions_slug_unique` (`slug`),
  KEY `fk_bp_regions_state_id_active_ixd` (`state_id`,`active`),
  CONSTRAINT `ad_regions_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `ad_states` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_settings
CREATE TABLE IF NOT EXISTS `ad_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_settings_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_states
CREATE TABLE IF NOT EXISTS `ad_states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'I',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_states_code_unique` (`code`),
  UNIQUE KEY `ad_states_name_unique` (`name`),
  UNIQUE KEY `ad_states_slug_unique` (`slug`),
  KEY `fk_bp_states_active_code_ixd` (`active`,`code`),
  KEY `fk_bp_states_active_slug_ixd` (`active`,`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_subregions
CREATE TABLE IF NOT EXISTS `ad_subregions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(10) unsigned NOT NULL,
  `state_id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_state_id_UNIQUE` (`name`,`state_id`),
  UNIQUE KEY `ad_subregions_slug_unique` (`slug`),
  KEY `subregions_region_id_foreign` (`region_id`),
  KEY `subregions_state_id_foreign` (`state_id`),
  CONSTRAINT `ad_subregions_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `ad_regions` (`id`),
  CONSTRAINT `ad_subregions_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `ad_states` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_suburbs
CREATE TABLE IF NOT EXISTS `ad_suburbs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `state_id` int(10) unsigned NOT NULL,
  `region_id` int(10) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`state_id`,`region_id`,`name`),
  KEY `suburbs_region_id_foreign` (`region_id`),
  KEY `slug_UNIQUE` (`slug`),
  CONSTRAINT `ad_suburbs_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `ad_regions` (`id`),
  CONSTRAINT `ad_suburbs_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `ad_states` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_users
CREATE TABLE IF NOT EXISTS `ad_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' N => New(Waiting activation), A=>Active, I=>Inactive',
  `account_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'I' COMMENT ' I=>Individual, B=>Business',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `verification_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `activated_at` datetime DEFAULT NULL,
  `avatar` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_users_username_unique` (`username`),
  UNIQUE KEY `ad_users_email_unique` (`email`),
  KEY `users_status_account_type_index` (`status`,`account_type`),
  KEY `users_creator_id_foreign` (`creator_id`),
  CONSTRAINT `ad_users_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `ad_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table Hostels2.ad_users_groups
CREATE TABLE IF NOT EXISTS `ad_users_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` smallint(5) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_groups_user_id_group_id_unique` (`user_id`,`group_id`),
  KEY `users_groups_group_id_foreign` (`group_id`),
  KEY `users_groups_created_at_index` (`created_at`),
  CONSTRAINT `ad_users_groups_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `ad_groups` (`id`),
  CONSTRAINT `ad_users_groups_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `ad_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
