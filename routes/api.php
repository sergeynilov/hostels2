<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group( [ 'prefix' => 'auth' ], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

//Request URL: http://127.0.0.1:8000/api/dashboard-settings


Route::post('app-settings', 'HomeController@settings');

//             axios.post( context.getters.apiUrl + '/users' , {filter_status: 'A'} )
Route::post('customers', 'HomeController@customers');
Route::post('customers_count', 'HomeController@customers_count');
//     Route::get('customers_count/{id}', 'Admin\CustomersController@get_count');
Route::post('/search/run_hostels_filter_search', 'SearchController@run_hostels_filter_search');
Route::post('customer_register_store', 'CustomerRegisterController@store');





Route::post('hostels', 'HostelController@hostels');
Route::post('hostels_miscellaneous', 'HostelController@hostels_miscellaneous');

Route::post('get-lists-data', 'HomeController@get_lists_data');

Route::get('get_hostels_by_region_id_state_id/{state_id}/{region_id}/{output_type}', 'HostelController@get_hostels_by_region_id_state_id');
//Route::get('get_hostels_sorted_by/{sort_by}', 'HostelController@get_hostels_sorted_by');

//                axios.get( '/api/get_hostel_by_slug/'+this.hostel_slug  )

Route::get('get_hostel_by_slug/{hostel_slug}', 'HostelController@get_hostel_by_slug');
//Route::get('get_hostel_rooms/{hostel_id}', 'HostelController@get_hostel_rooms');
Route::get('get_hostel_details/{hostel_id}', 'HostelController@get_hostel_details');
//Route::get('get_hostel_facilities/{hostel_slug}', 'HostelController@get_hostel_facilities');
//Route::post('store_new_hostel_inquery', 'HostelController@store_new_hostel_inquery');

//
Route::get('get_location_by_postcode/{postal_code}', 'HomeController@get_location_by_postcode');
Route::get('get_regions_listing_by_state_id/{state_id}', 'HomeController@get_regions_listing_by_state_id');
Route::get('get_subregions_listing_by_region_id/{region_id}', 'HomeController@get_subregions_listing_by_region_id');

//Route::get('get-hostel-by-slug/{slug}', 'HostelController@get_hostel_by_slug');


/* Route::get('all-news/{page_number?}', array(
    'as'      => 'all-news',
    'uses'    => 'PageController@all_news'
));
 */
Route::group([  'prefix' => 'news', 'as' => 'news.'], function ($router) {
    Route::get('get_list/{data_type}/{current_page?}', 'NewsController@get_list');
    Route::post('news', 'NewsController@news');
    Route::get('get_single_news_by_slug/{single_news_slug}', 'NewsController@get_single_news_by_slug');


/*    Route::post('get_single_forum_by_slug/{single_forum_slug}', 'ForumController@get_single_forum_by_slug');
    Route::post('get_forum_threads', 'ForumController@get_forum_threads');


    Route::get('get_single_forum_thread_by_slug/{single_forum_thread_slug}', 'ForumController@get_single_forum_thread_by_slug');

    Route::post('get_thread_posts', 'ForumController@get_thread_posts');
    Route::post('report_abuse_thread_post', 'ForumController@report_abuse_thread_post');
    Route::post('forum_post_store', 'ForumController@forum_post_store');

    Route::post('forum_post_delete', 'ForumController@forum_post_delete');
    Route::post('forum_post_store_user_subscribed', 'ForumController@forum_post_store_user_subscribed');

    Route::post('forum_post_update', 'ForumController@forum_post_update');
    Route::post('set_forum_as_best_decision', 'ForumController@set_forum_as_best_decision');


    Route::post('add_new_thread', 'ForumController@add_new_thread');*/

    //                    axios.get('/api/get_single_forum_by_slug/' + this.single_forum_slug)
});  // Route::group([  'prefix' => 'news', 'as' => 'news.'], function ($router) {



Route::group([  'prefix' => 'forum', 'as' => 'forum.'], function ($router) {
    Route::get('categorized_forums', 'ForumController@categorized_forums');


    Route::post('get_single_forum_by_slug/{single_forum_slug}', 'ForumController@get_single_forum_by_slug');
    Route::post('get_forum_threads', 'ForumController@get_forum_threads');


    Route::get('get_single_forum_thread_by_slug/{single_forum_thread_slug}', 'ForumController@get_single_forum_thread_by_slug');

    Route::post('get_thread_posts', 'ForumController@get_thread_posts');
    Route::post('report_abuse_thread_post', 'ForumController@report_abuse_thread_post');
    Route::post('forum_post_store', 'ForumController@forum_post_store');

    /* Request URL: http://local-hostels2.com/forum/forum_post_delete
Request Method: POST */
    Route::post('forum_post_delete', 'ForumController@forum_post_delete');
    Route::post('forum_post_store_user_subscribed', 'ForumController@forum_post_store_user_subscribed');

    Route::post('forum_post_update', 'ForumController@forum_post_update');
    Route::post('set_forum_as_best_decision', 'ForumController@set_forum_as_best_decision');


    Route::post('add_new_thread', 'ForumController@add_new_thread');

    //                    axios.get('/api/get_single_forum_by_slug/' + this.single_forum_slug)
});  // Route::group([  'prefix' => 'forum', 'as' => 'forum.'], function ($router) {


Route::group(['middleware' => 'jwt.auth',  'prefix' => 'personal', 'as' => 'personal.'], function ($router) {
    // LOGGED USER PERSONAL PROFILE BLOCK START

    Route::get('personal/{user_id}', 'PersonalController@get');

    Route::patch('personal/{id}', 'PersonalController@update');
    Route::delete('personal/{id}', 'PersonalController@destroy');
    // LOGGED USER PERSONAL PROFILE BLOCK END


    Route::get('managers', 'PersonalController@managers_list');
    Route::get('get_manager_messages/{customer_id}/{manager_id}', 'PersonalController@get_manager_messages');
    Route::post('send_message', 'PersonalController@send_message');




    Route::post('store_selected_payment_package', 'PersonalController@store_selected_payment_package');

    Route::get('hostel_bookmarks/{user_id}', 'PersonalController@get_hostel_bookmarks');
    Route::post('hostel_bookmarks', 'PersonalController@add_hostel_to_bookmarks');
    Route::delete('hostel_bookmarks/{user_id}/{hostel_id}', 'PersonalController@delete_from_hostel_bookmarks');


/*
    Route::get('hostel_bookmarks/{user_id}', 'PersonalController@get_hostel_bookmarks');
    Route::post('hostel_bookmarks', 'PersonalController@add_hostel_to_bookmarks');
    Route::delete('hostel_bookmarks/{user_id}/{hostel_id}', 'PersonalController@delete_from_hostel_bookmarks');



    Route::get('hostel_bookmarks/{user_id}', 'PersonalController@get_hostel_bookmarks');
    Route::post('hostel_bookmarks', 'PersonalController@add_hostel_to_bookmarks');
    Route::delete('hostel_bookmarks/{user_id}/{hostel_id}', 'PersonalController@delete_from_hostel_bookmarks');*/




    Route::post('hostel-enquery', 'PersonalController@add_hostel_enquery');
}); // Route::group(['middleware' => 'jwt.auth',  'prefix' => 'personal', 'as' => 'personal.'], function ($router) {


Route::group(['middleware' => 'jwt.auth',  'prefix' => 'admin', 'as' => 'admin.'], function ($router) {


    Route::get('users', 'JwtAuthenticateController@index');

    Route::get('get-system-info', 'DashboardController@system_info');
    Route::get('get_new_accepted_hostels_inqueries', 'DashboardController@get_new_accepted_hostels_inqueries');
    Route::get('get_forum_post_report_abuses', 'DashboardController@get_forum_post_report_abuses');


    //                axios.post(window.API_BACKEND_VERSION_LINK + '/admin/get-states', ['statesWithRegions', 'hostelsCount']).then((response) => {
    Route::post('get-states', 'Admin\StatesController@index');

    //                axios.post(window.API_BACKEND_VERSION_LINK + '/admin/state-active-status', {state_id : id}).then((response) => {
    Route::post('state-set-inactive', 'Admin\StatesController@state_set_inactive');
    Route::post('state-set-active', 'Admin\StatesController@state_set_active');

    Route::post('region-set-inactive', 'Admin\StatesController@region_set_inactive');
    Route::post('region-set-active', 'Admin\StatesController@region_set_active');




    Route::post('customers-filter', 'Admin\CustomersController@index');
    Route::get('customers/{id}', 'Admin\CustomersController@get');
    Route::post('customers', 'Admin\CustomersController@store');
    Route::patch('customers/{id}', 'Admin\CustomersController@update');
    Route::delete('customers/{id}', 'Admin\CustomersController@destroy');


    Route::post('facilities-filter', 'Admin\FacilitiesController@index');
    Route::get('facilities/{id}', 'Admin\FacilitiesController@get');
    Route::post('facilities', 'Admin\FacilitiesController@store');
    Route::patch('facilities/{id}', 'Admin\FacilitiesController@update');
    Route::delete('facilities/{id}', 'Admin\FacilitiesController@destroy');



    Route::post('hostels-filter', 'Admin\HostelsController@index');
    Route::get('hostels/{id}', 'Admin\HostelsController@get');
    Route::post('hostels', 'Admin\HostelsController@store');
    Route::patch('hostels/{id}', 'Admin\HostelsController@update');
    Route::delete('hostels/{id}', 'Admin\HostelsController@destroy');


    Route::get('hostels_images/{id}', 'Admin\HostelsController@get_hostels_images');
    Route::post('hostel_image_store', 'Admin\HostelsController@hostel_image_store');
    Route::delete('hostel_image_destroy/{id}', 'Admin\HostelsController@hostel_image_destroy');


    Route::get('hostels_inqueries/{id}', 'Admin\HostelsController@get_hostels_inqueries');

    Route::delete('hostel_inquery_destroy/{id}', 'Admin\HostelsController@hostel_inquery_destroy');
    Route::post('hostel_enquery_update_info', 'Admin\HostelsController@hostel_enquery_update_info');
    Route::post('hostel_enquery_set_status_action', 'Admin\HostelsController@hostel_enquery_set_status_action');


    Route::get('get_regions_list_by_state_id/state_id/{state_id}/format/{format}/{region_id}/current_region_id/{current_region_id}', 'HomeController@get_regions_list_by_state_id');


    Route::post('states', 'Admin\StatesController@index');
//    Route::get('states/{id}', 'Admin\StatesController@get');
    Route::post('states/set_active', 'Admin\StatesController@state_set_active');
    Route::post('states/set_inactive', 'Admin\StatesController@state_set_inactive');
//    Route::get('states/{id}', 'Admin\StatesController@get');

    //             axios.get('/api/categories_dictionaries')

    Route::get('categories_dictionaries', 'CategoriesController@dictionaries');
    Route::get('categories', 'CategoriesController@all');
    Route::get('categories/{id}', 'CategoriesController@get');
    Route::post('categories/new', 'CategoriesController@new');

//    axios.post(window.API_BACKEND_VERSION_LINK + '/admin/sql-monitor/run-sql-statement', { sql_statement : this.sql_statement }).then((response) => {
//                axios.post(window.API_BACKEND_VERSION_LINK + '/admin/sql-monitor/redraw-sql-statement', { sql_statement : this.sql_statement }).then((response) => {
    Route::post('sql-monitor/run-sql-statement', 'DashboardController@run_sql_statement');
    Route::post('sql-monitor/redraw-sql-statement', 'DashboardController@redraw_sql_statement');

}); // Route::group(['middleware' => 'jwt.auth',  'prefix' => 'admin', 'as' => 'admin.'], function ($router) {
