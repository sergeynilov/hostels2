

@setup
// Under directory /_wwwroot/lar/Hostels2 RUN
// export PATH="$PATH:~/.composer/vendor/bin"
// cd /_wwwroot/lar/Hostels2
//envoy run Hostels2Deploy  --YYgit_upload_description='New design applied '  --app_version=0.17 --run_migration=1   --run_migration_seed=1
    $server_login_user= 'lardeployer';

    $timezone= 'Europe/Kiev';

    $base_dir= '/var/www/html/Hostels2Deployed';
    $current_dir = $base_dir . '/current'; // THIS DIRECTORY DOES NOT EXISTS AT SERVER - it will be created as link


    $repo= 'git@bitbucket.org:sergeynilov/hostels2.git';
    $branch= 'master';


    $writableDirs= [  // Directory and files with chmod 755
        'storage/logs',
        'bootstrap/cache'
    ];
    $now_date = new DateTime('now', new DateTimeZone($timezone));

    $release_dir = $base_dir . '/release';
    $release_number_dir = $base_dir . '/release/' . $now_date->format('Ymd-His');

    $git_clone_command = 'git clone --depth 1 -b '.$branch.' "'.$repo.'" '.' "'.$release_number_dir.'"';

    $database_factories_path= $release_number_dir.'/database/factories';

@endsetup

@servers( [ 'dev' => [ "laravelserver" ] ] )


@task( 'git_upload_project', ['on'=>$on] )
    echo "git_upload_project Step # 00 git_upload_description ::{{ $git_upload_description }}";

    @if( $git_upload_description )
        git add .
        git commit -m $git_upload_description
        git push -u origin master
    @endif

{{--    mkdir -p {{$release_dir}}--}}
{{--    mkdir -p {{$release_number_dir}}--}}
{{--    {{ $git_clone_command }}--}}
{{--    mkdir -p {{$database_factories_path}}--}}

    echo "Step # 1 : Repository has been uploaded to git";
@endtask


@task( 'clone_project', ['on'=>$on] )
    echo 'clone_project $release_number_dir :: {{ $release_number_dir }}';
    echo '$database_factories_path :: {{ $database_factories_path }}';
    echo '$git_clone_command {{ $git_clone_command }}::';

    mkdir -p {{$release_dir}}
    mkdir -p {{$release_number_dir}}
    {{ $git_clone_command }}
    mkdir -p {{$database_factories_path}}

    echo "Step # 1 : Repository has been cloned";
@endtask


@task( 'composer_installing', ['on'=>$on] )
    cd {{ $release_number_dir }}

    echo "Step # 2 start : composer_installing dependencies installing";
    composer install --no-interaction --prefer-dist
    # composer install --no-interaction --no-dev --prefer-dist # PRODUCTION

    echo "Step # 2 : composer_installing dependencies has been installed";
@endtask


@task( 'artisan_running', ['on'=>$on] )
    echo "Step # 3 : Dev dependencies installing";
    cd {{ $release_number_dir }}

    ln -nfs {{ $base_dir }}/.env .env
    chgrp -h www-data .env

    echo "Step # 31";
    php artisan config:clear

    echo "Step # 32";
    # php artisan migrate
    # php artisan db:seed

    # echo "Step # 33";
    # php artisan db:seed  # Run only once


    echo "Step # 34";
    php artisan clear-compiled --env=dev
    # php artisan clear-compiled --env=production

    # php artisan optimize --env=dev

    echo "Step # 3 : Dev dependencies has been installed";
@endtask


@task( 'chmod_settings', ['on'=>$on] )
    echo "Step # 4 : chmod_settings = Permissions setting";
    echo 123 > 'envoy_test'.txt;
    chgrp -R www-data {{ $release_number_dir }}

    echo "Step # 40  $release_number_dir   :: {{ $release_number_dir }}";
    # chmod -R ug-rwx {{ $release_number_dir }}

    echo "Step # 41 : chmod_settings = Permissions setting";
@foreach($writableDirs as $file)
    echo "Step # 42 : {{ $release_number_dir }}/{{ $file }}";
    # chmod -R 775 {{ $release_number_dir }}/{{ $file }}

    @if ( !file_exists( $release_number_dir.'/'. $file )  )
        echo "Step # 43 :creaing directory";
        mkdir -p {{ $release_number_dir }}/{{ $file }};
    @endif

    chown -R {{ $server_login_user }}:www-data {{ $release_number_dir }}/{{ $file }}

    echo "Permissions has been for file {{ $file }}";
@endforeach
echo "Step # 4 : chmod_settings = Permissions has been set";
@endtask


@task( 'update_symlinks' )
    echo "Step # 50 : Symlink set $release_number_dir $release_number_dir::{{ $release_number_dir }}";
    echo "Step # 51 : Symlink set $current_dir $current_dir::{{ $current_dir }}";
    ln -nfs {{ $release_number_dir }} {{ $current_dir }};
    # ln -nfs {{ $release_number_dir }} {{ $current_dir }};

    echo "Step # 54 $release_number_dir : {{ $release_number_dir }}";

    # chgrp -h www-data {{ $current_dir }};
    echo "Step # 55 : Symlink has been set current_dir : {{  $current_dir}}";

    @foreach($writableDirs as $file)
        echo "Step # 61 : {{ $release_number_dir }}/{{ $file }}";
        chmod -R 777 {{ $release_number_dir }}/{{ $file }}

        chown -R {{ $server_login_user }}:www-data {{ $release_number_dir }}/{{ $file }}

        echo "Step # 62 Permissions has been for file {{ $file }}";

        echo "Step # 63 app_version ::{{ $app_version }}";
    @endforeach

    echo "Step # 620 before envoy:write-app-version ";
    cd {{ $release_number_dir }}

@endtask


@task('recreate_storage_link')
    echo "Step # 70 recreate_storage_link";
    cd /var/www/html/Hostels2Deployed/current/public
    rm -R /var/www/html/Hostels2Deployed/current/public/storage
    cd ../
    php artisan storage:link
    echo "Step # 74";
    chmod -R 777 /var/www/html/Hostels2Deployed/current/storage
@endtask

@task('write_app_version')
    echo "Step # 80";
    cd {{ $release_number_dir }}
    php artisan envoy:write-app-version {{$app_version}}
@endtask


@task('clean_old_releases')
    echo "Step # 92 clean old release_dir ::{{ $release_dir }}";
    # chmod -R 755 {{ $release_dir }}
    echo "Step # 93 :::";
    cd {{ $release_number_dir }}
    echo "Step # 94 :::";
    php artisan envoy:delete-old-versions  {{$release_dir}}
    echo "Step # 95 :::";

@endtask

@macro('Hostels2Deploy',['on'=>'dev'])
    git_upload_project
    clone_project
    composer_installing
    artisan_running
    chmod_settings
    update_symlinks
    recreate_storage_link
    write_app_version
@endmacro
# clean_old_releases




