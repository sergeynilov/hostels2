import {getLocalUser} from "./helpers/authFuncs";

import {bus} from './app';

import appMixin from './appMixin';

const user = getLocalUser();

export default {
    mixins: [appMixin],
    state: {  // data

        api_url: '/api',

        pageParams: {},

        currentLoggedUser: user,
        current_user_avatar_path: null,
        currentUserGroups: null,
        isLoggedIn: !!user,
        loading: false,
        auth_error: null,

        statesWithRegions: [],

        // activeCustomers: [],
        activeCustomersCount: 0,

        customerRegister : {
            username : '',
            email : '',
            first_name : '',
            last_name : '',
            password : '',
            account_type : '',
            phone : '',
            website : '',
            notes : '',
            access_to_privacy_details : false,
            show_online_status : false,
            avatarFile : {}
        },

        hostels: [],
        hostelsCount: 0,

        min_hostel_rooms_price: 0,
        max_hostel_rooms_price: 0,


        // personal data only for logged user
        personalOptions: [],
        hostelBookmarks: [],
        forumPostBookmarks: [],
        forumPostsUserSubscribed: [],


        // CUSTOMERS BLOCK BEGIN
        customers: [],
        customerAccountTypeValueArray: [],
        customerStatusValueArray: [],
        // CUSTOMERS BLOCK END




        // HOSTEL BLOCK START
        hostelFeatureValueArray: [],
        // HOSTEL BLOCK END


        // HOSTEL ROOMS BLOCK START
        hostelRoomIsDormValueArray: [],
        hostelRoomIsPrivateValueArray: [],
        hostelRoomTypeValueArray: [],
        // HOSTEL ROOMS BLOCK END

        usersSelectionList: [],
        shortUsersSelectionList: [],


        categories: [],
        categoryStatusValueArray: []
    },
    getters: {  // computed properties to get data
        apiUrl(state) {
            return state.api_url;
        },


        isLoading(state) {
            return state.loading;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },

        pageParams(state) {
            return state.pageParams;
        },

        currentLoggedUser(state) {
            return state.currentLoggedUser;
        },
        currentUserAvatarPath(state) {
            if ( state.current_user_avatar_path ) {
                return state.current_user_avatar_path;
            }
            let current_user_avatar_path = localStorage.getItem("current_user_avatar_path");
            if ( typeof current_user_avatar_path != "undefined" && current_user_avatar_path.length > 0 ) {
                return current_user_avatar_path;
            }
        },

        currentUserGroups(state) {
            if ( state.currentUserGroups ) {
                return state.currentUserGroups;
            }
            let currentUserGroups = localStorage.getItem("currentUserGroups");
            if ( typeof currentUserGroups != "undefined" && currentUserGroups.length > 0 ) {
                return currentUserGroups;
            }
        },

        authError(state) {
            return state.auth_error;
        },

        statesWithRegions(state) {
            return state.statesWithRegions;
        },

        personalOptions(state) {
            // return state.personalOptions;
            // that works ok after user logged into the system  and personalOptions is read on all page without refresh
            if ( state.personalOptions.length > 0 ) {
                return state.personalOptions;
            }

            // if personalOptions is empty check data in localStorage which were saved in refreshpersonalOptions mutation
            // console.log("localStorage.getItem(\"personalOptions\")::")
            // console.log( localStorage.getItem("personalOptions") )

            let hostelBookmarks= localStorage.getItem("hostelBookmarks")
            if ( hostelBookmarks != null && typeof hostelBookmarks != "undefined" ) {
                let localStoragePersonalOptions = JSON.parse(hostelBookmarks);
                if (localStoragePersonalOptions.length > 0) {
                    // console.log("localStoragePersonalOptions RETURNED::")
                    return localStoragePersonalOptions;
                }
            }
            return [];
        },

        hostelBookmarks(state) {
            // return state.hostelBookmarks;
            // that works ok after user logged into the system  and hostelBookmarks is read on all page without refresh
            // console.log("hostelBookmarks state::")
            // console.log( state )
            // debugger
            if ( state.hostelBookmarks.length > 0 ) {
                return state.hostelBookmarks;
            }

            // if hostelBookmarks is empty check data in localStorage which were saved in refreshhostelBookmarks mutation
            // console.log("localStorage.getItem(\"hostelBookmarks\")::")
            // console.log( localStorage.getItem("hostelBookmarks") )

            let hostelBookmarks= localStorage.getItem("hostelBookmarks")
            if ( hostelBookmarks!= null && typeof hostelBookmarks!= "undefined" ) {
                let localStorageHostelBookmarks = JSON.parse(hostelBookmarks);
                if ( localStorageHostelBookmarks.length > 0 ) {
                // console.log("localStorageHostelBookmarks RETURNED::")
                    return localStorageHostelBookmarks;
                }
            }
            return [];
        },

        forumPostBookmarks(state) {
            // return state.forumPostBookmarks;
            // that works ok after user logged into the system  and forumPostBookmarks is read on all page without refresh
            // console.log("forumPostBookmarks state::")
            // console.log( state )
            // debugger
            if ( state.forumPostBookmarks.length > 0 ) {
                return state.forumPostBookmarks;
            }

            // if forumPostBookmarks is empty check data in localStorage which were saved in refreshforumPostBookmarks mutation
            // console.log("localStorage.getItem(\"forumPostBookmarks\")::")
            // console.log( localStorage.getItem("forumPostBookmarks") )

            let forumPostBookmarks= localStorage.getItem("forumPostBookmarks")
            if ( forumPostBookmarks!= null && typeof forumPostBookmarks!= "undefined" ) {
                let localStoragePostBookmarks = JSON.parse(forumPostBookmarks);
                if ( localStoragePostBookmarks.length > 0 ) {
                // console.log("localStoragePostBookmarks RETURNED::")
                    return localStoragePostBookmarks;
                }
            }
            return [];
        },

        forumPostsUserSubscribed(state) {
            // return state.forumPostsUserSubscribed;
            // that works ok after user logged into the system  and forumPostsUserSubscribed is read on all page without refresh
            // console.log("forumPostsUserSubscribed state::")
            // console.log( state )
            // debugger
            if ( state.forumPostsUserSubscribed.length > 0 ) {
                return state.forumPostsUserSubscribed;
            }

            // if forumPostsUserSubscribed is empty check data in localStorage which were saved in refreshforumPostsUserSubscribed mutation
            // console.log("localStorage.getItem(\"forumPostsUserSubscribed\")::")
            // console.log( localStorage.getItem("forumPostsUserSubscribed") )

            let forumPostsUserSubscribed= localStorage.getItem("forumPostsUserSubscribed")
            if ( forumPostsUserSubscribed!= null && typeof forumPostsUserSubscribed!= "undefined" ) {
                let localStorageForumPostsUserSubscribed = JSON.parse(forumPostsUserSubscribed);
                if ( localStorageForumPostsUserSubscribed.length > 0 ) {
                console.log("localStorageForumPostsUserSubscribed RETURNED::")
                    return localStorageForumPostsUserSubscribed;
                }
            }
            return [];
        },

        hostelBookmarkByHostelId(state) {
            // console.log("BEFORE state::")
            // console.log("state::")
            // console.log( state )
            //
            // console.log("this::")
            // console.log( this )
            //
            // console.log("state.getters::")
            // console.log( state.getters )


            let hostelBookmarks= state.hostelBookmarks
            // console.log("hostelBookmarkByHostelId hostelBookmarks::")
            // console.log( hostelBookmarks )

            return hostel_id => {
                return hostelBookmarks.find(hostelBookmark =>hostelBookmark.hostel_id === hostel_id)
            }
        },

        customerRegister(state) {
            // console.log("INSIDE customerRegister state.customerRegister::")
            // console.log( state.customerRegister )
            return state.customerRegister;
        },


        activeCustomersCount(state) {
            return state.activeCustomersCount;
        },


        hostels(state) {
            // alert( "hostels::"+var_dump(-44) )
            return state.hostels;
        },


        min_hostel_rooms_price(state) {
            // alert( "min_hostel_rooms_price::"+var_dump(state.min_hostel_rooms_price) )
            return state.min_hostel_rooms_price;
        },

        max_hostel_rooms_price(state) {
            // alert( "max_hostel_rooms_price::"+var_dump(state.max_hostel_rooms_price) )
            return state.max_hostel_rooms_price;
        },


        featured_hostels(state) {
            return state.hostels.filter(hostel => {
                if (hostel.feature == 'F' ) {
                    return true;
                }
                return false;
            });
        },

        hostels_count(state) {
            return state.hostels.length;
        },

        states(state) {
            return state.states;
        },

        // CUSTOMERS BLOCK BEGIN
        customers(state) {
            return state.customers;
        },
        customerAccountTypeValueArray(state) {
            return state.customerAccountTypeValueArray;
        },
        customerStatusValueArray(state) {
            return state.customerStatusValueArray;
        },
        // CUSTOMERS BLOCK END







        // HOSTEL BLOCK START
        hostelFeatureValueArray(state) {
            // alert( "hostelFeatureValueArray ::"+var_dump(-71) )
            return state.hostelFeatureValueArray;
        },
        // HOSTEL BLOCK END



        // HOSTEL ROOMS BLOCK START
        hostelRoomIsDormValueArray(state) {
            // alert( "::"+var_dump(-71) )
            return state.hostelRoomIsDormValueArray;
        },
        hostelRoomIsPrivateValueArray(state) {
            return state.hostelRoomIsPrivateValueArray;
        },
        hostelRoomTypeValueArray(state) {
            return state.hostelRoomTypeValueArray;
        },
        // HOSTEL ROOMS BLOCK END


        usersSelectionList(state) {
            return state.usersSelectionList;
        },

        shortUsersSelectionList(state) {
            return state.shortUsersSelectionList;
        },


        categories(state) {
            return state.categories;
        },
        categoryStatusValueArray(state) {
            return state.categoryStatusValueArray;
        }
    }, // getters: {  // computed properties to get data

    mutations: {  // we need commit mutations for changing the state (data) - are synchronous
        login(state) {
            state.loading = true;
            state.auth_error = null;
        },

        setPageParams(state, payload) {
            // console.log("setPageParams payload::")
            // console.log( payload )
            state.pageParams = { modal_page_width : payload.modal_page_width, modal_page_height : payload.modal_page_height };
        },

        setLoginSuccess(state, payload) {
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            // console.log("payload::")
            // console.log( payload )

            state.currentLoggedUser = Object.assign({}, payload.user, {token: payload.access_token});
            state.current_user_avatar_path = payload.user_avatar_path;
            state.currentUserGroups = payload.usersGroups;
            // console.log("state.currentLoggedUser::")
            // console.log( state.currentLoggedUser )

            localStorage.setItem("loggedUser", JSON.stringify(state.currentLoggedUser));
            localStorage.setItem("current_user_avatar_path", state.current_user_avatar_path);
            localStorage.setItem("currentUserGroups", JSON.stringify(state.currentUserGroups));
            bus.$notify({
                group: 'hostels2_notification',
                title: "Login ",
                text: "Successful login !" ,
                type: 'success',
                duration: 1000, // TODO
                position : 'center top',
                speed: 1000
            });

        },

        setLoginFailed(state, payload) {
            // console.log("-0 INSIDE setLoginFailed payload::")
            state.loading = false;
            state.auth_error = payload.error;
            // console.log("-1 INSIDE setLoginFailed payload::")
            // console.log( typeof payload )
            // console.log( payload )

            bus.$notify({
                group: 'hostels2_notification',
                title: "Login ",
                text: "Login failed !" ,
                type: 'warn',
                duration: 1000, // TODO
                position : 'center top',
                speed: 1000
            });
            // console.log("-3 INSIDE setLoginFailed payload::")
        },

        setLogout(state) {
            localStorage.removeItem("loggedUser");
            state.isLoggedIn = false;
            state.currentLoggedUser = null;
            state.current_user_avatar_path = null;
            state.currentUserGroups = null;
        },



        refreshActiveCustomersCount(state, payload) {
            state.activeCustomersCount = payload;
        },


        // Customer Register BLOCK START
        refreshCustomerRegisterForm(state, payload) {
            // alert( "refreshCustomerRegisterForm::"+var_dump(payload) )
            // console.log("payload::")
            // console.log( payload )
            //
            // console.log("payload.username::")
            // console.log( payload.username )
            // console.log( typeof payload.username )

            if ( typeof payload.username != "undefined" ) {
                state.customerRegister.username = payload.username;
            }
            if ( typeof payload.email != "undefined" ) {
                state.customerRegister.email = payload.email;
            }
            if ( typeof payload.first_name != "undefined" ) {
                state.customerRegister.first_name = payload.first_name;
            }

            if ( typeof payload.last_name != "undefined" ) {
                state.customerRegister.last_name = payload.last_name;
            }

            if ( typeof payload.password != "undefined" ) {
                state.customerRegister.password = payload.password;
            }

            if ( typeof payload.account_type != "undefined" ) {
                state.customerRegister.account_type = payload.account_type;
            }

            if ( typeof payload.phone != "undefined" ) {
                state.customerRegister.phone = payload.phone;
            }
            if ( typeof payload.website != "undefined" ) {
                state.customerRegister.website = payload.website;
            }
            if ( typeof payload.notes != "undefined" ) {
                state.customerRegister.notes = payload.notes;
            }


            if ( typeof payload.access_to_privacy_details != "undefined" ) {
                state.customerRegister.access_to_privacy_details = payload.access_to_privacy_details;
            }

            if ( typeof payload.show_online_status != "undefined" ) {
                state.customerRegister.show_online_status = payload.show_online_status;
            }
            if ( typeof payload.avatarFile != "undefined" ) {
                state.customerRegister.avatarFile = payload.avatarFile;
            }



            // console.log("DEEPLY refreshCustomerRegisterForm  customerRegister state.customerRegister::")
            // console.log( state.customerRegister )
            // console.log( toObject(state.customerRegister) )

            /*         customerRegister : {
            username : '',
            email : '',
            first_name : '',
        },
 */
        },
        // Customer Register BLOCK END



        refreshHostels(state, payload) {
            // alert( "refreshHostelsWithRegions::"+var_dump(payload) )
            state.hostels = payload;
        },

        refreshMinHostelRoomsPrice(state, min_hostel_rooms_price) {
            state.min_hostel_rooms_price = min_hostel_rooms_price;
        },

        refreshMaxHostelRoomsPrice(state, max_hostel_rooms_price) {
            state.max_hostel_rooms_price = max_hostel_rooms_price;
        },

        refreshPersonalOptions(state, payload) {
            // alert( "refreshPersonalOptions::"+var_dump(payload) )
            state.personalOptions = payload;
            localStorage.setItem("personalOptions", JSON.stringify(payload) );
        },

        refreshHostelBookmarks(state, payload) {
            // alert( "refreshHostelBookmarks::"+var_dump(payload) )
            state.hostelBookmarks = payload;
            localStorage.setItem("hostelBookmarks", JSON.stringify(payload) );
        },

        refreshForumPostBookmarks(state, payload) {
            // alert( "refreshForumPostBookmarks::"+var_dump(payload) )
            state.forumPostBookmarks = payload;
            localStorage.setItem("forumPostBookmarks", JSON.stringify(payload) );
        },


        refreshForumPostsUserSubscribed(state, payload) {
            // alert( "refreshForumPostsUserSubscribed::"+var_dump(payload) )
            state.forumPostsUserSubscribed = payload;
            localStorage.setItem("forumPostsUserSubscribed", JSON.stringify(payload) );
        },


        refreshStatesWithRegions(state, payload) {
            // alert( "refreshStatesWithRegions::"+var_dump(payload) )
            state.statesWithRegions = payload;
        },

        refreshStates(state, payload) {
            state.states = payload;
        },
        // refreshCustomers(state, payload) {
        //     state.customers = payload;
        // },


        refreshCustomers(state, payload) {
            state.customers = payload;
        },

        // CUSTOMERS BLOCK BEGIN
        refreshCustomerAccountTypeValueArray(state, payload) {
            state.customerAccountTypeValueArray = payload;
        },

        refreshCustomerStatusValueArray(state, payload) {
            state.customerStatusValueArray = payload;
        },

        refreshCustomerSkinsArray(state, payload) {
            // alert( "refreshCustomerSkinsArray payload::"+var_dump(payload) )
            state.customerSkinsArray = payload;
        },
        // CUSTOMERS BLOCK END


        // HOSTEL BLOCK START
        refreshHostelFeatureValueArray(state, payload) {
            // alert( "refreshHostelFeatureValueArray::"+var_dump(-987) )
            state.hostelFeatureValueArray = payload;
        },
        // HOSTEL BLOCK END

        // HOSTEL ROOMS BLOCK START
        refreshHostelRoomIsDormValueArray(state, payload) {
            state.hostelRoomIsDormValueArray = payload;
        },
        refreshHostelRoomIsPrivateValueArray(state, payload) {
            state.hostelRoomIsPrivateValueArray = payload;
        },
        refreshHostelRoomTypeValueArray(state, payload) {
            state.hostelRoomTypeValueArray = payload;
        },
        // HOSTEL ROOMS BLOCK END


        refreshUsersSelectionList(state, payload) {
            state.usersSelectionList = payload;
        },

        refreshShortUsersSelectionList(state, payload) {
            state.shortUsersSelectionList = payload;
        },

        setCategories(state, payload) {
            state.categories = payload;
        },

        setCategoriesDictionaries(state, payload) {
            console.log("setCategoriesDictionaries payload::")
            console.log(payload)

            state.categoryStatusValueArray = payload;

        }
    },  // mutations: {  // we need commit mutations for changing the state (data) - are synchronous


    actions: {  // async methods
        ///////////// AUTH DATA BLOCK BEGIN ///////////////
        login(context) {
            context.commit("login");  // call mutation
        },



        retrievePersonalOptions(context, user_id) {
            // alert( "retrievePersonalOptions  user_id: "+var_dump(user_id) +"  context.api_url::" + context.getters.apiUrl )
            axios.get( context.getters.apiUrl + '/personal/personal/' + user_id )
                .then((response) => {
                    // console.log("++++1 response.data::")
                    // console.log( response.data )       // personalOptionsList
                    context.commit('refreshPersonalOptions', response.data.personalOptionsList);
                })
        }, // retrieveLoggedUserPersonalOptions(context, user_id) {
        ///////////// AUTH DATA BLOCK END ///////////////

        storeRetrieveDictionaries(context, paramsArray) {
            axios.post( context.getters.apiUrl + '/app-settings', paramsArray )
                .then((response) => {

                    // debugger
                    // CUSTOMERS BLOCK BEGIN
                    if (typeof response.data.customerAccountTypeValueArray == 'object') {
                        // alert( "customerAccountTypeValueArray ::"+(-5) + var_dump(response.data.customerAccountTypeValueArray) )
                        context.commit('refreshCustomerAccountTypeValueArray', response.data.customerAccountTypeValueArray);
                    }
                    if (typeof response.data.customerStatusValueArray == 'object') {
                        // alert( "::"+(-6) )
                        context.commit('refreshCustomerStatusValueArray', response.data.customerStatusValueArray);
                    }

                    if (typeof response.data.customerSkinsArray == 'object') {
                        // alert( "::"+(-656) + var_dump(response.data.customerSkinsArray) )
                        context.commit('refreshCustomerSkinsArray', response.data.customerSkinsArray);
                    }
                    // CUSTOMERS BLOCK END


                    // HOSTEL BLOCK START

                    if (typeof response.data.hostelFeatureValueArray == 'object') {
                        // alert("CALL refreshHostelFeatureValueArray hostelFeatureValueArray ::" + (-642))
                        context.commit('refreshHostelFeatureValueArray', response.data.hostelFeatureValueArray);
                    }
                    // HOSTEL ROOMS BLOCK END


                    // HOSTEL ROOMS BLOCK START
                    if (typeof response.data.hostelRoomIsDormValueArray == 'object') {
                        // alert("::" + (-61))
                        // debugger
                        context.commit('refreshHostelRoomIsDormValueArray', response.data.hostelRoomIsDormValueArray);
                    }
                    if (typeof response.data.hostelRoomIsPrivateValueArray == 'object') {
                        // alert("::" + (-62))
                        context.commit('refreshHostelRoomIsPrivateValueArray', response.data.hostelRoomIsPrivateValueArray);
                    }
                    if (typeof response.data.hostelRoomTypeValueArray == 'object') {
                        // alert("::" + (-64))
                        context.commit('refreshHostelRoomTypeValueArray', response.data.hostelRoomTypeValueArray);
                    }
                    // HOSTEL ROOMS BLOCK END


                    if (typeof response.data.usersSelectionList == 'object') {
                        // alert( "::"+(-6) )
                        context.commit('refreshUsersSelectionList', response.data.usersSelectionList);
                    }
                    if (typeof response.data.shortUsersSelectionList == 'object') {
                        // alert( "::"+(-6) )
                        context.commit('refreshShortUsersSelectionList', response.data.shortUsersSelectionList);
                    }
                })

        }, // storeRetrieveDictionaries(context, paramsArray) {


        ///////////// HOSTEL BOOKMARKS BLOCK BEGIN ///////////////
//         Route::get('hostel_bookmarks/{user_id}', 'PersonalController@get_hostel_bookmarks');
// Route::post('hostel_bookmarks', 'PersonalController@add_hostel_to_bookmarks');
// Route::delete('hostel_bookmarks/{user_id}/{hostel_id}', 'PersonalController@delete_from_hostel_bookmarks');

        retrieveHostelBookmarks(context, user_id) {
            axios.get(this.getters.apiUrl+'/personal/hostel_bookmarks/'+user_id )
                .then((response) => {
                    // alert( "retrieveHostelBookmarks response.data.hostelBookmarks::"+var_dump(response.data.hostelBookmarks) )
                    context.commit('refreshHostelBookmarks', response.data.hostelBookmarks);
                })
        }, // retrieveHostelBookmarks(context, filters) {

        retrieveForumPostBookmarks(context, user_id) {
            axios.get(this.getters.apiUrl+'/personal/hostel_bookmarks/'+user_id )
                .then((response) => {
                    alert( "retrieveForumPostBookmarks response.data.forumPostBookmarks::"+var_dump(response.data.forumPostBookmarks) )
                    context.commit('refreshForumPostBookmarks', response.data.forumPostBookmarks);
                })
        }, // retrieveHostelBookmarks(context, filters) {


        retrieveForumPostsUserSubscribed(context, user_id) {
            axios.get(this.getters.apiUrl+'/personal/hostel_bookmarks/'+user_id )
                .then((response) => {
                    // alert( "retrieveForumPostsUserSubscribed response.data.forumPostsUserSubscribed::"+var_dump(response.data.forumPostsUserSubscribed) )
                    context.commit('refreshForumPostsUserSubscribed', response.data.forumPostsUserSubscribed);
                })
        }, // retrieveForumPostsUserSubscribed(context, filters) {



        hostelBookmarkStore(context, relatedHostel ) {
            // debugger
            // alert( "hostelBookmarkStore relatedHostel::"+var_dump(relatedHostel) )
            bus.$emit( 'beforeHostelBookmarkStore', relatedHostel );
            axios({
                method: ( 'post' ),
                url: this.getters.apiUrl + '/personal/hostel_bookmarks',

                data: { user_id : this.getters.currentLoggedUser.id, hostel_id: relatedHostel.id,  },
            }).then((response) => {
                // debugger
                let hostelBookmarks = this.getters.hostelBookmarks
                hostelBookmarks.push({
                        "hostel_id": relatedHostel.id,
                        "name": relatedHostel.name,
                        "slug": relatedHostel.slug,
                        "phone": relatedHostel.phone,
                        "feature": relatedHostel.feature,
                        "rating": relatedHostel.rating,
                        "min_hostel_rooms_price": relatedHostel.min_hostel_rooms_price,
                        "region_name": relatedHostel.region_name,
                        "state_name": relatedHostel.state_name,
                        "state": relatedHostel.state,
                        "state_slug": relatedHostel.state_slug,
                        "region_slug": relatedHostel.region_slug,
                        "subregion_slug": relatedHostel.subregion_slug,
                })
                this.commit('refreshHostelBookmarks', hostelBookmarks);
                console.log("TRIGGER onHostelBookmarkStoreSuccess::")

                bus.$emit( 'onHostelBookmarkStoreSuccess', response );
            }).catch((error) => {
                // bus.$emit('onUserListStoreFailure', error);
                bus.$emit('onHostelBookmarkStoreFailure', error);
                // bus.$emit('onHostelBookmarkStoreFailure', error);
            });

        }, // hostelBookmarkStore(context, relatedHostel ) {


        hostelBookmarkDelete(context, relatedHostel ) {
            // alert( "hostelBookmarkDelete relatedHostel::"+var_dump(relatedHostel) )
            // debugger
            bus.$emit( 'beforeHostelBookmarkDelete', relatedHostel );
            axios({
                method: ( 'delete' ),
                url: this.getters.apiUrl + '/personal/hostel_bookmarks/' + this.getters.currentLoggedUser.id + "/" + relatedHostel.id,
            }).then((response) => {
                let L = this.getters.hostelBookmarks.length

                for (var I = 0; I < L; I++) {
                    if (relatedHostel.id == this.getters.hostelBookmarks[I].hostel_id) {
                        // this.getters.hostelBookmarks.splice(this.getters.hostelBookmarks.indexOf(this.getters.hostelBookmarks[I]), 1)
                        if ( this.getters.hostelBookmarks.length == 1 ) {
                            context.commit('refreshHostelBookmarks', []);
                            break;
                        } else {
                            this.getters.hostelBookmarks.splice(I, 1)
                        }
                        context.commit('refreshHostelBookmarks', this.getters.hostelBookmarks);
                        break;
                    }
                }

                bus.$emit( 'onHostelBookmarkDeleteSuccess', response );
            }).catch((error) => {
                bus.$emit('onHostelBookmarkDeleteFailure', error);
            });

        }, // hostelBookmarkDelete(context, relatedHostel ) {


        ///////////// HOSTEL BOOKMARKS BLOCK END ///////////////



        ///////////// ACTIVES USERS RETRIEVE BLOCK BEGIN ///////////////

        retrieveActiveCustomersCount(context) {
            axios.post( context.getters.apiUrl + '/customers_count' , {filter_status: 'A'} )
                .then((response) => {
                    context.commit('refreshActiveCustomersCount', response.data.customers);
                })
        }, // retrieveActiveCustomers(context) {

        ///////////// ACTIVES USERS RETRIEVE BLOCK BEGIN ///////////////





        ///////////// HOSTELS RETRIEVE BLOCK BEGIN ///////////////

        retrieveHostels(context) {
            // alert( "retrieveHostels  filters: "+var_dump(-9) )
            axios.post( context.getters.apiUrl + '/hostels' /*, {filter_status: 'A'}*/ )
                .then((response) => {
                    context.commit('refreshHostels', response.data.hostels);
                })
        }, // retrieveHostels(context) {


        retrieveStates(context, paramsArray) {
            axios.post(context.getters.apiUrl + '/get-lists-data', paramsArray )
                .then((response) => {
                    context.commit('refreshStatesWithRegions', response.data.statesWithRegionsList);
                })
        },

        getState(context, state_id) {
            axios.get(context.getters.apiUrl + '/admin/state/' + state_id)
                .then((response) => {
                    context.commit('refreshState', response.data.state);
                })
        },
        ///////////// STATES RETRIEVE BLOCK END ///////////////


    } // actions: {  // async methods

};

function var_dump(oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}




function  toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
    return rv;
}
