import { setAuthorizationToken } from "./commonFuncs";


export function login(credentials) {
    // alert( "login::"+(-111) )
    return new Promise((res, rej) => {
        axios.post('/api/auth/login', credentials)
            .then((response) => {
                // alert( "login::"+(-21) )
                console.log("+++ credentials::")
                console.log( credentials )

                console.log("+++ response::")
                console.log( response )
                // alert( "login::"+(-24) )
                setAuthorizationToken(response.data.access_token);
                res(response.data);
            })
            .catch((err) =>{
                // console.log("--- err::")
                // console.log( err )
                // console.error(err)
                rej("Wrong email or password");
            })
    })
}

export function getLocalUser() {
    const userStr = localStorage.getItem("loggedUser");

    if(!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}
