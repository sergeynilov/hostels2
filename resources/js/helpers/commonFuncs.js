import {bus} from '../app';
import appMixin from '../appMixin';

export default {
    name: 'new',
//        components: { Multiselect },
    data() {
        return {}
    },
    mixins: [appMixin],
}

import Vue from 'vue';

Vue.directive('onelementhover', function(el, binding) {

    el.onmouseover = function() {
        if ( binding.value == 'bold' ) {
            el.style.fontWeight = "bold";
        }
        if ( binding.value == 'underline' ) {
            el.style.textDecoration = "underline";
        }
    };
    el.onmouseleave = function() {
        if ( binding.value == 'bold' ) {
            el.style.fontWeight = "normal";
        }
        if ( binding.value == 'underline' ) {
            el.style.textDecoration = "none";
        }
    };
}) // Vue.directive('onelementhover', function(el, binding) {


Vue.directive('checkislogged', function(el, binding, vnode) {
    if ( (typeof binding.value.action_type) && binding.value.action_type == 'grayed' ) {
        // console.log("binding.value.loggedUser::")
        // console.log( binding.value.loggedUser )
        //
        if ( binding.value.loggedUser == null ) {

            el.style.color = vnode.context.disabled_text_color;
        } else {
            el.style.color = vnode.context.text_color;
        }
    }
}) //Vue.directive('checkislogged', function(el, binding, vnode) {


export function addToBookmarksKey(hostel_id) {
    // alert( "addToBookmarksKey hostel_id::"+var_dump(hostel_id) )
    return 'hostels_sorted__add_to_bookmarks_'+hostel_id;
}

export function deleteFromBookmarksKey(hostel_id) {
    return 'hostels_sorted__remove_from_bookmarks_'+hostel_id;
}


export function confirmAddToBookmarks(hostel_id, hostel, hostel_name, index)  {
    // console.log("confirmAddToBookmarks  --::")
    // console.log( this.add_to_bookmarks_key )
    // alert( "++1 confirmAddToBookmarks  this.add_to_bookmarks_key::"+var_dump(this.add_to_bookmarks_key) )
    if ( this.currentLoggedUser == null || typeof this.currentLoggedUser.id == "undefined" ) {
        this.showPopupMessage("Bookmarks", 'You need to login into the system to add bookmark !', 'warn');
        return;
    }
    this.confirmMsg( "Do you want to add '" + hostel_name + "' hostel to your bookmarks ?", { key : this.add_to_bookmarks_key, hostel_id : hostel_id, hostel : hostel, index : index },
        'Confirm', bus );
} // confirmAddToBookmarks(hostel_id, hostel, hostel_name, index)  {

export function confirmDeleteFromBookmarked(hostel_id, hostel, hostel_name, index)  {
    if ( this.currentLoggedUser == null || typeof this.currentLoggedUser.id == "undefined" ) {
        this.showPopupMessage("Bookmarks", 'You need to login into the system to remove bookmark !', 'warn');
        return;
    }
    this.confirmMsg( "Do you want to exclude '" + hostel_name + "' hostel from your bookmarks ?", { key : this.delete_from_bookmarks_key, hostel_id : hostel_id, hostel : hostel,  index : index
    }, 'Confirm', bus );
}


export function checkAuthorization(store, router) {
    router.beforeEach((to, from, next) => {
        console.log("checkAuthorization  to::")
        console.log( to )
        console.log("checkAuthorization  from::")
        console.log( from )

        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const currentLoggedUser = store.state.currentLoggedUser;

        console.log("requiresAuth::")
        console.log( requiresAuth )

        console.log("checkAuthorization  currentLoggedUser::")
        console.log( currentLoggedUser )

        if(requiresAuth && !currentLoggedUser) {
            // console.log("checkAuthorization redirect to login::")
            bus.$notify({
                group: 'hostels2_notification',
                title: "Access error",
                text: "You have no access to this page !" ,
                type: 'error',        // success  / warn / danger
                duration: 1000,  // TODO
                position : 'center top',
                speed: 1000
            });

            next('/login');
        } else if(to.path == '/login' && currentLoggedUser) {
            // console.log("0 checkAuthorization redirect to /::")
            next('/');
        } else {
            // console.log("-1 checkAuthorization redirect to next::")
            next();
        }

        if (!to.matched.length) {
            // alert( "::/not-found/" )
            next(  '/not-found/'+encodeURIComponent(to.path)  );
        } else {
            // console.log("-2 checkAuthorization redirect to next::")
            next();
        }

    });

    if (store.getters.currentLoggedUser) {
        setAuthorizationToken(store.getters.currentLoggedUser.token);
    }
}


export function setAuthorizationToken(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}

export function retrieveAppDictionaries(paramsArray) {
    var retArray= [];
    // alert( "retrieveAppDictionaries paramsArray::"+var_dump(paramsArray) )
    axios.post('/api/app-settings', paramsArray)
        .then((response) => {

            // FORUMS BLOCK BEGIN
            if ( typeof response.data.forumPostActionValueArray == 'object' ) {
                retArray['forumPostActionValueArray']= response.data.forumPostActionValueArray;
            }

            // FORUMS BLOCK END

            // APP CONFIGURATION BLOCK BEGIN
            // console.log("typeof response.data.forum_threads_per_page::")
            // console.log( typeof response.data.forum_threads_per_page )


            if ( typeof response.data.forum_posts_per_page == 'string' ) {
                retArray['forum_posts_per_page']= response.data.forum_posts_per_page;
            }
            if ( typeof response.data.forum_threads_per_page == 'string' ) {
                retArray['forum_threads_per_page']= response.data.forum_threads_per_page;
            }
            if ( typeof response.data.backend_per_page == 'string' ) {
                retArray['backend_per_page']= response.data.backend_per_page;
            }
            if ( typeof response.data.news_per_page == 'string' ) {
                retArray['news_per_page']= response.data.news_per_page;
            }
            if ( typeof response.data.hostels_per_page == 'string' ) {
                retArray['hostels_per_page']= response.data.hostels_per_page;
            }
            if ( typeof response.data.skinsArray == 'object' ) {
                retArray['skinsArray']= response.data.skinsArray;
            }
            // APP CONFIGURATION BLOCK END


            // PAYMENT_PACKAGE BLOCK BEGIN
            if ( typeof response.data.activePaymentPackagesArray == 'object' ) {
                retArray['activePaymentPackagesArray']= response.data.activePaymentPackagesArray;
            }
            // PAYMENT_PACKAGE BLOCK END

            // CUSTOMERS BLOCK BEGIN
            if ( typeof response.data.customers_active_count != 'undefined' ) {
                retArray['customers_active_count']= response.data.customers_active_count;
            }
            if ( typeof response.data.customers_inactive_count != 'undefined' ) {
                retArray['customers_inactive_count']= response.data.customers_inactive_count;
            }
            if ( typeof response.data.customers_all_count != 'undefined' ) {
                retArray['customers_all_count']= response.data.customers_all_count;
            }
            if ( typeof response.data.new_customer_inqueries == 'object' ) {
                retArray['new_customer_inqueries']= response.data.new_customer_inqueries;
            }

            if ( typeof response.data.customerAccountTypeValueArray == 'object' ) {
                retArray['customerAccountTypeValueArray']= response.data.customerAccountTypeValueArray;
            }
            if ( typeof response.data.customerStatusValueArray == 'object' ) {
                retArray['customerStatusValueArray']= response.data.customerStatusValueArray;
            }

            if ( typeof response.data.customerSkinsArray == 'object' ) {
                // alert( "retArray['customerSkinsArray']::"+var_dump(retArray['customerSkinsArray']) )
                retArray['customerSkinsArray']= response.data.customerSkinsArray;
            }
            // CUSTOMERS BLOCK END


            // HOSTELS BLOCK BEGIN
            if ( typeof response.data.hostelImageIsMainValueArray == 'object' ) {
                retArray['hostelImageIsMainValueArray']= response.data.hostelImageIsMainValueArray;
            }

            if ( typeof response.data.hostelImageIsVideoValueArray == 'object' ) {
                retArray['hostelImageIsVideoValueArray']= response.data.hostelImageIsVideoValueArray;
            }

            if ( typeof response.data.hostels_active_count != 'undefined' ) {
                retArray['hostels_active_count']= response.data.hostels_active_count;
            }
            if ( typeof response.data.hostels_inactive_count != 'undefined' ) {
                retArray['hostels_inactive_count']= response.data.hostels_inactive_count;
            }
            if ( typeof response.data.hostels_all_count != 'undefined' ) {
                retArray['hostels_all_count']= response.data.hostels_all_count;
            }
            if ( typeof response.data.new_hostel_inqueries_count != 'undefined' ) {
                retArray['new_hostel_inqueries_count']= response.data.new_hostel_inqueries_count;
            }
            if ( typeof response.data.accepted_hostel_inqueries_count != 'undefined' ) {
                retArray['accepted_hostel_inqueries_count']= response.data.accepted_hostel_inqueries_count;
            }
            if ( typeof response.data.newHostelInqueriesCountByStatesArray != 'undefined' ) {
                retArray['newHostelInqueriesCountByStatesArray']= response.data.newHostelInqueriesCountByStatesArray;
            }
            if ( typeof response.data.hostelFacilitiesCountByFacilityIdArray != 'undefined' ) {
                retArray['hostelFacilitiesCountByFacilityIdArray']= response.data.hostelFacilitiesCountByFacilityIdArray;
            }
            if ( typeof response.data.hostelStatusValueArray != 'undefined' ) {
                retArray['hostelStatusValueArray']= response.data.hostelStatusValueArray;
            }
            if ( typeof response.data.new_hostel_inqueries == 'object' ) {
                retArray['new_hostel_inqueries']= response.data.new_hostel_inqueries;
            }


            if ( typeof response.data.categoriesSelectionArray == 'object' ) {
                retArray['categoriesSelectionArray']= response.data.categoriesSelectionArray;
            }

            if ( typeof response.data.facilitiesSelectionArray == 'object' ) {
                retArray['facilitiesSelectionArray']= response.data.facilitiesSelectionArray;
            }

            if ( typeof response.data.hostelsSearchByFieldsArray == 'object' ) {
                retArray['hostelsSearchByFieldsArray']= response.data.hostelsSearchByFieldsArray;
            }

            if ( typeof response.data.searchByFieldsOrderingArray == 'object' ) {
                retArray['searchByFieldsOrderingArray']= response.data.searchByFieldsOrderingArray;
            }

            if ( typeof response.data.hostelInqueriesStatusValueArray == 'object' ) {
                retArray['hostelInqueriesStatusValueArray']= response.data.hostelInqueriesStatusValueArray;
            }
            if ( typeof response.data.hostelFeatureValueArray == 'object' ) {
                retArray['hostelFeatureValueArray']= response.data.hostelFeatureValueArray;
            }
            if ( typeof response.data.facilitiesIdValueArray == 'object' ) {
                retArray['facilitiesIdValueArray']= response.data.facilitiesIdValueArray;
            }
            // HOSTELS BLOCK END


            // HOSTEL ROOMS BLOCK START
            if ( typeof response.data.hostelRoomIsDormValueArray == 'object' ) {
                retArray['hostelRoomIsDormValueArray']= response.data.hostelRoomIsDormValueArray;
            }

            if ( typeof response.data.hostelRoomIsPrivateValueArray == 'object' ) {
                retArray['hostelRoomIsPrivateValueArray']= response.data.hostelRoomIsPrivateValueArray;
            }

            if ( typeof response.data.hostelRoomTypeValueArray == 'object' ) {
                retArray['hostelRoomTypeValueArray']= response.data.hostelRoomTypeValueArray;
            }

            // HOSTEL ROOMS BLOCK END



            if ( typeof response.data.usersSelectionList == 'object' ) {
                retArray['usersSelectionList']= response.data.usersSelectionList;
            }
            if ( typeof response.data.shortUsersSelectionList == 'object' ) {
                retArray['shortUsersSelectionList']= response.data.shortUsersSelectionList;
            }

            // LOCATIONS BLOCK BEGIN
            if ( typeof response.data.statesIdValueArray == 'object' ) {
                retArray['statesIdValueArray']= response.data.statesIdValueArray;
            }
            // LOCATIONS BLOCK END

            // console.log("-99 retrieveAppDictionaries retArray::")
            // console.log( retArray )

            // alert( "retrieveAppDictionaries retArray::"+var_dump(retArray) )
            bus.$emit('appDictionariesRetrieved', retArray);

        })
        .catch((error) => {
            console.error(error)
        });

    return retArray;
} // export function retrieveAppDictionaries(paramsArray, bus) {



function initTinyMCEEditor(by_selector_container, by_selector, width, height) {

    var deviceParams= effectiveDeviceWidth()
    var calculated_width= getCalculatedWidth(deviceParams.width)
    if ( typeof calculated_width != "undefined" ) {
        width = getCalculatedWidth(deviceParams.width)
    }
    // alert( "initTinyMCEEditor width::"+(width) + "  by_selector_container::"+by_selector_container)

    var editor_config = {
        path_absolute : "/",
        selector: '#' + by_selector_container,

        setup: function (editor) {
            editor.on('change', function () {
                var current_context= tinymce.get(by_selector_container).getContent()
                // alert( "current_context::"+var_dump(current_context) )
                $('#' + by_selector).html( current_context );
            });
        },


        theme: 'modern',
        width: width,
        height: height,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };

    tinymce.init(editor_config);
    // alert( "AFTER initTinyMCEEditor::"+var_dump(-99) )
}  // function initTinyMCEEditor(by_selector_container, by_selector, width, height) {


export function getModalCalculatedHeight(device_height) {
    if ( device_height <= 320  ) {
        // alert( -1 )
        return 300
    }
    if ( device_height > 320 && device_height <= 480  ) {
        // alert( -2 )
        return 300
    }

    if ( device_height > 480 && device_height <= 600  ) {
        // alert( -3 )
        return 440
    }

    if ( device_height > 600 && device_height <= 768  ) {
        // alert( -4 )
        return 560
    }
    if ( device_height > 768 && device_height <= 1024  ) {
        // alert( -5 )
        return 720
    }
    if ( device_height > 1024   ) {
        // alert( -6 )
        return 950
    }
}

export function getModalCalculatedWidth(device_width) {
    if ( device_width <= 320  ) {
        // alert( -11 )
        return 300
    }
    if ( device_width > 320 && device_width <= 480  ) {
        // alert( -22 )
        return 320
    }

    if ( device_width > 480 && device_width <= 600  ) {
        // alert( -33 )
        return 310
    }

    if ( device_width > 600 && device_width <= 768  ) {
        // alert( -44 )
        return 560
    }
    if ( device_width > 768 && device_width <= 1024  ) {
        // alert( -55 )
        return 720
    }
    if ( device_width > 1024   ) {
        // alert( -66 )
        return 950
    }
}

export function getCalculatedWidth(device_width) {
    if ( device_width <= 320  ) {
        // alert( -1 )
        return 300
    }
    if ( device_width > 320 && device_width <= 480  ) {
        // alert( -2 )
        return 320
    }

    if ( device_width > 480 && device_width <= 600  ) {
        // alert( -3 )
        return 310
    }

    if ( device_width > 600 && device_width <= 768  ) {
        // alert( -4 )
        return 470
    }
    if ( device_width > 768 && device_width <= 1024  ) {
        // alert( -5 )
        return 480
    }
    if ( device_width > 1024   ) {
        // alert( -6 )
        return 650
    }
}


function checkEmail(value) {
    regex=/^[a-zA-Z0-9._-]+@([a-zA-Z0-9.-]+\.)+[a-zA-Z0-9.-]{2,4}$/;
    return regex.test(value);
}

function checkInteger(value) {
    regex=/^[0-9]+$/;
    return regex.test(value);
}

function checkisNormalInteger(str, can_be_zero) {
    var n = Math.floor(Number(str));
    if ( can_be_zero ) {
        return String(n) === str && n >= 0;
    } else {
        return String(n) === str && n > 0;
    }
}


function var_dump(oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}


