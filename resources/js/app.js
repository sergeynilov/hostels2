require('./bootstrap');

require('jquery');

window.Vue = require('vue');


// import Argon from '@/plugins/argon-kit'
// Vue.use(Argon);


//Vue.component('yourComponentsName', require('./components/BS4/yourComponentsName.vue').default);


// import vSelect from 'vue-select'
// Vue.component('v-select', vSelect)

import VueRouter from 'vue-router';
import Vuex from 'vuex';
import {routes} from './routes';
import StoreData from './store';
import mainapp from './components/BS4/MainApp.vue';

import appheader from './components/BS4/AppHeader.vue';
import appfooter from './components/BS4/AppFooter.vue';
import navigation from './components/BS4/Navigation.vue';
import breadcrumbs from './components/BS4/Breadcrumbs.vue';
// import MainHeader from './components/BS4/MainHeader.vue';     // resources/js/components/BS4/MainHeader.vue
// import BackendHeader from './components/BS4/admin/BackendHeader.vue';

// import backendapp from './components/BS4/admin/BackendApp.vue';   // resources/js/components/admin/BackendApp.vue
import {checkAuthorization} from './helpers/commonFuncs';


let template_name = 'BS4';
Vue.component('editor-header', require('./components/'+template_name+'/lib/EditorHeader.vue').default);
// Vue.component('editor-header', require('./components/BS4/lib/EditorHeader.vue').default);
Vue.component('editor-buttons', require('./components/BS4/lib/EditorButtons.vue').default);
Vue.component('listing-header', require('./components/BS4/lib/ListingHeader.vue').default);
Vue.component('listing-pagination', require('./components/BS4/lib/ListingPagination.vue').default);

Vue.component('rating-stars-line', require('./components/BS4/lib/RatingStarsLine.vue').default);
Vue.component('hostel-list-item', require('./components/BS4/hostels/HostelListItem.vue').default);

Vue.component('regions-dropdown-selector', require('./components/BS4/lib/RegionsDropdownSelector.vue').default);

Vue.component('chat-app', require('./components/BS4/customer_chat/ChatApp.vue').default);


import Vue2Filters from 'vue2-filters' // https://github.com/freearhey/vue2-filters#placeholder
Vue.use(Vue2Filters)

import VeeValidate from 'vee-validate';   // https://baianat.github.io/vee-validate/guide/rules.html
import VeeValidateLaravel from 'vee-validate-laravel';


import VTooltip from 'v-tooltip'  // https://github.com/Akryum/v-tooltip
Vue.use(VTooltip)


//
// import Vue from 'vue';
import vSelect from 'vue-select'  // https://vue-select.org/
Vue.component('v-select', vSelect)



// import moment from 'moment'
// Vue.use(moment)

// Vue.use(require('vue-moment'));
import VueMoment from 'vue-moment';
Vue.use(VueMoment);

// alert( "Vue.moment();::"+var_dump( Vue.moment() ) )

import moment from 'moment-timezone' //https://medium.com/@muhamadalfisyahrezadaulay/default-timezone-in-vuejs-8b1df48d35bb
moment.tz.setDefault('Europe/Kiev')

const veeValidateConfigArray = {  // https://github.com/logaretm/vee-validate
    errorBagName: 'vueErrorsList', // change if property conflicts
    fieldsBagName: 'fields',
    delay: 0,
    locale: 'en',
    dictionary: null,
    strict: true,
    classes: false,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'dirty' // control has been interacted with
    },
    events: 'input|blur',
    inject: true,
    validity: false,
    aria: true,
//        i18n: null, // the vue-i18n plugin instance,
//        i18nRootKey: 'validations' // the nested key under which the validation messsages will be located
};
Vue.use(VeeValidate, veeValidateConfigArray);
Vue.use(VeeValidateLaravel);


import VModal from 'vue-js-modal' // https://github.com/euvl/vue-js-modal
Vue.use(VModal, { dialog: true })


import VueCarousel from 'vue-carousel'; // https://github.com/SSENSE/vue-carousel
Vue.use(VueCarousel);


// alert( moment('add', '6 days')  )


Vue.use(VueRouter);
Vue.use(Vuex);

export const bus = new Vue();


import Notifications from 'vue-notification' // https://github.com/euvl/vue-notification
Vue.use(Notifications)


// import { VTree, VSelectTree } from 'vue-tree-halower'
// Vue.use (VTree)                          // https://github.com/halower/vue-tree
// Vue.use (VSelectTree)
// Vue.use (VSelectTree)

// import VueLocalStorage from 'vue-localstorage'// https://github.com/pinguinjkeke/vue-local-storage
// Vue.use(VueLocalStorage)


const store = new Vuex.Store(StoreData);
store.dispatch('retrieveActiveCustomersCount' ); //ONLY FOR DEBUGGING

store.dispatch('retrieveStates', ['statesWithRegions']);
store.dispatch('retrieveHostels');
// store.dispatch('retrieveNews');

//                 return this.$store.getters.currentLoggedUser
// store.dispatch('retrieveHostelBookmarks', 5 ); //ONLY FOR DEBUGGING
// store.dispatch('retrievePersonalOptions', 5 ); //ONLY FOR DEBUGGING

const router = new VueRouter({
    routes,
    mode: 'history'
});

checkAuthorization(store, router);
axios.interceptors.response.use(null, (error) => {
    // alert(" axios.interceptors.response.use error.response.status"+error.response.status)
    console.log("axios.interceptors.response error::")
    console.log( error )

    if (error.response.status == 401) {
        console.log("error.response::")
        console.log( error.response )
        console.log( error.response.config )
        console.log( error.response.config.url )

        store.commit('setLogout');

        console.log("0012 route.path::")
        console.log( $route )
        console.log( $route.path )

        router.push('/login');
    }

    return Promise.reject(error);
});


const app = new Vue({
    el: '#app',
    router,
    store,
    bus,
    components: {
        mainapp, appheader, appfooter, navigation
    },
});


router.afterEach(( to, from ) => {
    // alert( "router.afterEach to::"+var_dump(to) +"  from::"+var_dump(from) )
    bus.$emit('page_changed', from, to);
});


// Vue.filter('checkUrlPrefix', function (url) {
//     if (!/^(f|ht)tps?:\/\//i.test(url)) {
//         url = "http://" + url;
//     }
//     return url;
// })

// Vue.filter('timeInAgoFormat', function (value) {
//     if ( typeof value == "undefined" || typeof value == "object") return '';
//     var formatted_val= Vue.moment(value).fromNow();
//     return formatted_val;
// })


Vue.filter('Capitalize', function (value) {
    var ret= value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
    return ret;
})


Vue.filter('json', function (value) {
    return JSON.stringify(value);
});

Vue.filter('pluck', function (objects, key) {
    return objects.map(function(object) {
        return object[key];
    });
});

Vue.filter('at', function (value, index) {
    return value[index];
});

Vue.filter('first', function (values) {
    if(Array.isArray(values)) {
        return values[0];
    }
    return values;
});

Vue.filter('last', function (values) {
    if(Array.isArray(values)) {
        return values[values.length - 1];
    }
    return values;
});

Vue.filter('without', function (values, exclude) { //Return a copy of the array without the given elements:
    return values.filter(function(element) {
        return !exclude.includes(element);
    });
});

Vue.filter('unique', function (values, unique) {
    return values.filter(function(element, index, self) {
        return index == self.indexOf(element);
    });
});


function var_dump(oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof(oElem) == 'string' || typeof(oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof(oElem) == 'innerHTML' || typeof(oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}
