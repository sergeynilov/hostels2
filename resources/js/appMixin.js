import moment from 'moment'
import Vue from 'vue'

export default {


    methods: {

        // if ( this.checkAccessRoleContent(this.comp_currentUserGroups,window.ACCESS_ROLE_CONTENT_EDITOR) ) {
        checkAccessRoleContent(userGroups, access_role_content_editor) {
            if ( typeof userGroups == "string" ) {
                userGroups= JSON.parse(userGroups);
            }
            if (typeof userGroups == "undefined") return false;
            if (typeof access_role_content_editor == "undefined") return false;

            let ret= false
            userGroups.map((nextUserGroup, index) => {
                // console.log("+++getDictionaryLabel nextUserGroup::")
                // console.log( nextUserGroup )
                // console.log("nextUserGroup.group_id::")
                // console.log( typeof nextUserGroup.group_id )
                // console.log( nextUserGroup.group_id )

                if (nextUserGroup.group_id == access_role_content_editor) {
                    // console.log("=== FOUND nextUserGroup.group_id")
                    // console.log( nextSelection )
                    ret= true
                    // alert( "INSIDE::"+(-87) +"  ret::"+ret )
                    return true;
                }
            });
            return ret

        },

        moneyLabel() {
            return 'AU$';
        },

        moneyFormat(price) {
            // alert( "moneyFormat price::"+(price) )
            // console.log("price::")
            // console.log( typeof price)
            // console.log( price)
            // console.log( '========')
            if (typeof price == 'undefined' || typeof price == 'object') {
                price = 0;
            }
            if (typeof price == 'string') {
                price = parseFloat(price)
            }
            // return price | currency('AU$');
            return 'AU$' + price.toFixed(2);
        },

        toFixed(price, limit) {
            return price.toFixed(limit);
        },

        toUSD(price) {
            return `$${price}`;
        },

        makeClone: function (obj) {     //https://stackoverflow.com/questions/728360/how-do-i-correctly-clone-a-javascript-object
            if (null == obj || "object" != typeof obj) return obj;
            var copy = obj.constructor();
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
            }
            return copy;
        },

        crlf: function (s) {
            if (typeof s != 'string') return '';
            return s.replace(/(\\r)*\\n/g, '<br>')
        },

        comments_count_label(nextNews) {
            return '42 comments'
        },

        featured_news() {
            return 'Featured news means lorem ipsum dolor sit amet'
        },

        last_modified_label(itemRow) {
            if (itemRow.updated_at != null && typeof itemRow.updated_at != "undefined") {
                return this.timeInAgoFormat(itemRow.updated_at);
            }
            return this.timeInAgoFormat(itemRow.created_at);
        },


        forumCategoryHasImage(itemRow) {
            // console.log("newsHasImage itemRow::")
            // console.log( itemRow )
            //
            if (typeof itemRow['filenameData'] == "undefined") return false;
            if (typeof itemRow['filenameData'].image_url == "undefined") return false;
            if (typeof itemRow['filenameData'].image_url == "string") return true;
            return true;
        },

        newsHasImage(itemRow) {
            // console.log("newsHasImage itemRow::")
            // console.log( itemRow )
            //
            if (typeof itemRow['filenameData'] == "undefined") return false;
            if (typeof itemRow['filenameData'].image_url == "undefined") return false;
            if (typeof itemRow['filenameData'].image_url == "string") return true;
            return true;
        },


        spaceStringValue(str, max_length) {
            if (str == null || typeof str == "undefined" ) return "";
            let l = str.length
            if (l <= max_length) return str;

            let i = 0
            i = i + max_length
            while (i < l) {
                str = str.substring(0, i - 1) + " " + str.substring(i)
                i = i + max_length
            }
            return str;
        },

        mailTo(email) {
            window.location = "mailto:" + email;
        },

        toObject(arr) {
            var rv = {};
            for (var i = 0; i < arr.length; ++i)
                rv[i] = arr[i];
            return rv;
        },


        getDictionaryLabel(value, selectionsList, default_value) {
            if (typeof default_value == "undefined") default_value = '';
            if (typeof selectionsList == "undefined") return default_value;
            // console.log("getDictionaryLabel selectionsList::")
            // console.log( typeof selectionsList )
            //
            // console.log("getDictionaryLabel value::")
            // console.log( value )

            var ret = default_value

            selectionsList.map((nextSelection, index) => {
                // console.log("+++getDictionaryLabel nextSelection::")
                // console.log( nextSelection )

                if (nextSelection.key == value) {
                    // console.log("=== FOUND getDictionaryLabel nextSelection::")
                    // console.log( nextSelection )
                    ret = nextSelection.label;
                }
            });
            // ret= ret.replace(" ",'&nbsp;');
            return ret
        }, //getDictionaryLabel( value, selectionsList, default_value ) {


        getDictionaryLabel_old(value, selectionsList, default_value) {
            if (typeof default_value == "undefined") default_value = '';
            if (typeof selectionsList == "undefined") return default_value;
            console.log("getDictionaryLabel selectionsList::")
            console.log(typeof selectionsList)
            // if ( typeof selectionsList == "array" ) {
            // selectionsList= toObject(selectionsList);
            // }
            // console.log( typeof selectionsList )

            // console.log("getDictionaryLabel value::")
            // console.log( value )

            var ret = default_value

            selectionsList.map((nextSelection, index) => {
                // debugger
                // selectionsList.prototype.map((nextSelection, index) => {
                console.log("+++getDictionaryLabel nextSelection::")
                nextSelection = Array.from(this.toObject(nextSelection))
                console.log(nextSelection)
                console.log(typeof nextSelection)

                if (this.toObject(nextSelection).key == value) {
                    // console.log("=== FOUND getDictionaryLabel nextSelection::")
                    // console.log( nextSelection )
                    ret = this.toObject(nextSelection).label;
                }
            });
            // ret= ret.replace(" ",'&nbsp;');
            return ret
        }, //getDictionaryLabel( value, selectionsList, default_value ) {

        clearErrorLabel(s, data_container) {
            // console.log("--clearErrorLabel s::")
            // console.log( s )
            //
            // console.log("clearErrorLabel data_container::")
            // console.log( data_container )

            if (typeof s == "undefined") return "";
            return s.replace(data_container, "").replace(/_/g, " ")
        },

        //
        // clearErrorLabel(s) {
        //     if (typeof s == "undefined") return "";
        //     return s.replace(/userRow./g, "").replace(/_/g, " ")
        // },

        checkUrlPrefix(url) {
            if (!/^(f|ht)tps?:\/\//i.test(url)) {
                url = "http://" + url;
            }
            return url;
        },

        timeInAgoFormat: function (value) {
            if (value == null || typeof value == "undefined") return;
            return this.Capitalize( moment(value).fromNow() );
        },


//                     this.checkFieldInErrorsList('name',this.errorsList)
        checkFieldInErrorsList(field_name, errorsList) {
            return false;
            console.log("errorsList::")
            console.log(errorsList)

            // debugger;
            if (errorsList == null) return false;
            errorsList = JSON.parse(JSON.stringify(errorsList))

            console.log("00errorsList::")
            console.log(errorsList)

            console.log(Object.values(errorsList));
            // errorsList= Object.values(errorsList)
            errorsList = Object.keys(errorsList)

            let ret = false
            errorsList.map((next_error_field_name, key_field_index) => {
                // if (key_field_index == 0) {
                //     debugger
                // }
                console.log("next_error_field_name::")
                console.log(next_error_field_name)
                console.log("key_field_index::")
                console.log(key_field_index)
                if (field_name == next_error_field_name) {
                    ret = true
                    return ret;
                }
                if (ret) {
                    return ret;
                }
            });
            return ret;

        },

        stringIntoDatetime(string_datetime) {
            // console.log(  moment.utc("2019-04-17 05:20:37").fromNow()  )
            console.log("stringIntoDatetime string_datetime::")
            console.log(string_datetime)

            return moment.utc(string_datetime);
            // return moment.utc("2019-04-17 05:25:37");
        },

        momentDatetime(datetime, datetime_format, default_val) {

            /*
                        console.log("=")
                        console.log("=")
                        console.log("datetime_format::")
                        console.log( datetime_format )
                        console.log("momentDatetime datetime::")
                        console.log( datetime )
                        console.log("typeof datetime::")
                        console.log( typeof datetime )
            */

            if (typeof datetime == "undefined" || datetime == null) {
                if (typeof default_val != "undefined" && default_val != null) {
                    return default_val;
                }
                return "";
            }
            if (typeof datetime == "object") {
                return Vue.moment(datetime).format(datetime_format);
            }
            if (typeof datetime == "string") {
                if (datetime_format == "") return '';
                let dt = Vue.moment(String(datetime));//.format('YYYY-MM-DD'); //2019-06-03 16:53:29
                return dt.format(datetime_format);
            } // if (typeof datetime == "string") {
            return "";
        }, // momentDatetime(datetime, datetime_format, default_val) {

        showPopupMessage: function (title, message, type) {  //https://github.com/euvl/vue-notification
            // alert("showPopupMessage  title::"+title+"  message::"+message + "  type::"+type)
            this.$notify({
                group: 'hostels2_notification',
                title: title,
                text: message,
                type: type,        // success  / warn / danger
                duration: 5000,
                position: 'top left',
                speed: 1000
            });

        }, // showPopupMessage: function (message, type) {

        isNoneEmptyString(val) {
            if (typeof (val) == "string" && val.length > 0) return true;
            return false;
        },

        // takes the form field value and returns true on valid number
        checkValidCreditCard(value) {
            // accept only digits, dashes or spaces
            alert("checkValidCreditCard value::" + (value))
            if (/[^0-9-\s]+/.test(value)) return false;

            // The Luhn Algorithm. It's so pretty.
            var nCheck = 0, nDigit = 0, bEven = false;
            value = value.replace(/\D/g, "");

            for (var n = value.length - 1; n >= 0; n--) {
                var cDigit = value.charAt(n),
                    nDigit = parseInt(cDigit, 10);

                if (bEven) {
                    if ((nDigit *= 2) > 9) nDigit -= 9;
                }

                nCheck += nDigit;
                bEven = !bEven;
            }

            return (nCheck % 10) == 0;
        },      // checkValidCreditCard(value) {

        getMoneyOptions() {
            return {
                // decimal: ',',
                // thousands: '.',
                decimal: '.',
                thousands: ' ',
                prefix: '$',
                suffix: '',
                precision: 2,
                masked: false
            };
        },

        // moment(...args) {
        //     return moment(...args);
        // },

        getValidationsError(errorResponse) {
            // console.log("getValidationsError errorResponse::")
            // console.log( errorResponse )
            var error_message = '';
            if (typeof errorResponse.data.errors == "undefined") return "";
            var dataErrors = errorResponse.data.errors
            // console.log("dataErrors::")
            // console.log( typeof dataErrors )
            //
            var typeof_data_errors = typeof dataErrors

            for (var property in dataErrors) {
                if (dataErrors.hasOwnProperty(property)) {
                    error_message = error_message + this.Capitalize(property) + " : " + dataErrors[property] + ';'
                }
            }
            return error_message;
        },


        markdownToHtml(text) {
            // var showdown = require('showdown')
            // var converter = new showdown.Converter()
            // return converter.makeHtml(text);
        },

        getLabelByKeyFromList(key, listing) {
            // console.log("getLabelByKeyFromList listing::")
            // console.log( listing )
            var ret = ''
            listing.map((nextListingItem, index) => {
                // console.log("nextListingItem::")
                // console.log( nextListingItem )
                // console.log("key::")
                // console.log( key )
                if (nextListingItem.key == key) {
                    // console.log("RETURNED nextListingItem.label::")
                    // console.log( nextListingItem.label )
                    ret = nextListingItem.label;
                    return ret;
                }
            });
            return ret;
        },


        getKeyByLabelFromList(label, listing) {
            // console.log("getKeyByLabelFromList listing::")
            // console.log( listing )
            var ret = ''
            listing.map((nextListingItem, index) => {
                // console.log("+++getKeyByLabelFromList label::")
                // console.log( label )
                if (nextListingItem.label == label) {
                    // alert( "RETURN nextListingItem.key::"+(nextListingItem.key) )
                    // console.log("RETURNED nextListingItem.key::")
                    // console.log( nextListingItem.key )
                    ret = nextListingItem.key;
                    return nextListingItem.key;
                }
            });
            return ret;
        },



        getSplitted(str, splitter, index) {
            if (typeof str == "undefined") return "";
            var valuesArray = str.split(splitter);
            if (typeof valuesArray[index] != "undefined") {
                return valuesArray[index];
            }
            return '';
        },


        formatTimeLabel(str) { //08:30:00-22:30:00
            if (typeof str == "undefined") return "";
            var valuesArray = str.split(':');
            if ( valuesArray.length == 3 ) return valuesArray[0]+':'+valuesArray[1]
            return str;
        },

        preg_quote(str) {	// Quote regular expression characters
            return str.replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },


        getFuturePastColor(future_past) { //
            if (typeof window.settings_futurePastColorsArray[future_past] != 'undefined') {
                return 'backgroundColor:' + window.settings_futurePastColorsArray[future_past]
            }
            return '#fff'
        },

        noWrapString(str) {
            return this.replaceAll(str, " ", '&nbsp;');
        },

        replaceAll(str, search, replacement) {
            // var target = this;
            return str.replace(new RegExp(search, 'g'), replacement);
        },

        goToNamedRef(ref_name) {
            alert("goToNamedRef ref_name::" + (ref_name))
            console.log("Vue::")
            console.log(Vue)

            console.log("Vue.$refs::")
            console.log(Vue.$refs)

            console.log(this.$refs)
            console.log(typeof this.$refs)

            var element = ''
            if (ref_name == 'porto') {

                console.log("+++this::")
                console.log(this)

                console.log("+++!!!this.$refs::") // I show printscreen below
                console.log(this.$refs)

                console.log("++++!!!ref_name::")
                console.log(ref_name)

            }
//                console.log("?????? element::")
//                console.log(element);

            if (typeof element != "undefined") {
                var top = element.offsetTop;
                console.log("INSIDE top::")
                console.log(top)

                window.scrollTo(0, top);
            }
        }, // goToNamedRef(ref_name) {


        addDaysToDate(add_days, current_date) {
            // console.log("typeof current_date::")
            // console.log( typeof current_date )
            // console.log( current_date )

            if (typeof current_date == "undefined") {
                current_date = new Date();
            }


            // console.log("-2 current_date::")
            // console.log( current_date )

            current_date.setDate(current_date.getDate() + add_days);
            return current_date;
        },

        getPaginationParams: function (page, order_by, order_direction, is_question) {
            if (typeof page != "number") page = 1
            var ret = '';
            // debugger;
            if (page > 0) {
                ret = ret + (is_question ? '?' : '&') + 'page=' + page
            }
            if (typeof order_by != "undefined" && this.trim(order_by) != "") {
                ret = ret + (ret == '' ? '' : '&') + 'order_by=' + order_by
            }

            if (typeof order_direction != "undefined" && this.trim(order_direction) != "") {
                ret = ret + (ret == '' ? '' : '&') + 'order_direction=' + order_direction
            }

            return ret;
        }, // getPaginationParams : function(page, order_by, order_direction, is_question) {


        setAppTitle: function (page_group, page_title, bus) {
            var site_name = window.settings_site_name;
            bus.$emit('appPageTitleSet', page_group, page_title);

            if (typeof page_title != "undefined" && page_title != '' && site_name != "undefined" && site_name != '') {
                if (document.getElementById("app_title")) {
                    document.getElementById("app_title").innerHTML = page_title + ' of ' + site_name
                }
            }
        }, //Application name : Page Title

        /*
                setAppTitle: function (page_title_text, show_only_page_title, return_string) {
                    if (typeof return_string == "undefined") return_string = false
                    var site_name = window.SITE_NAME;
                    // alert( "setAppTitle page_title_text::"+(page_title_text) +"  show_only_page_title::"+show_only_page_title +"  site_name::"+site_name)

                    // window.SITE_NAME= 'FIX ME : ';
                    if (typeof show_only_page_title == "undefined" || !show_only_page_title) {
                        if (typeof page_title_text != "undefined" && page_title_text != '' && site_name != "undefined" && site_name != '') {
                            // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                            if (return_string) return page_title_text + ' of ' + site_name;
                            if (document.getElementById("app_title")) {
                                // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                                document.getElementById("app_title").innerHTML = page_title_text + ' of ' + site_name
                            }
                            // if (document.getElementById("toolbar_app_title")) {
                            //     // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                            //     document.getElementById("toolbar_app_title").innerHTML = page_title_text + ' of ' + site_name
                            // }
                            return;
                        }
                        if (site_name != "undefined" && site_name != '') {
                            // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                            if (return_string) return site_name;
                            if (document.getElementById("app_title")) {
                                // alert( "+3+setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                                document.getElementById("app_title").innerHTML = site_name
                            }
                            // if (document.getElementById("toolbar_app_title")) {
                            //     // alert( "+3+setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                            //     document.getElementById("toolbar_app_title").innerHTML = site_name
                            // }
                            return;
                        }
                    }
                    if (typeof page_title_text != "undefined" && page_title_text != '') {
                        // alert( "--setAppTitle page_title_text::"+(page_title_text) )
                        if (return_string) return page_title_text;
                        if (document.getElementById("app_title")) {
                            // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                            document.getElementById("app_title").innerHTML = page_title_text
                        }
                        // if (document.getElementById("toolbar_app_title")) {
                        //     // alert( "++setAppTitle page_title_text::"+(page_title_text) +"  site_name::"+ site_name)
                        //     document.getElementById("toolbar_app_title").innerHTML = page_title_text
                        // }
                        return;
                    }
                }, //Application name : Page Title
        */


        inArray: function (needle, haystack) {
            var length = haystack.length;
            for (var i = 0; i < length; i++) {
                if (haystack[i] == needle) return true;
            }
            return false;
        },

        effectiveDeviceWidth: function (param) {
            var viewport = {
                width: $(window).width(),
                height: $(window).height()
            };
            //alert( "viewport::"+var_dump(viewport) )
            if (typeof param != "undefined") {
                if (param.toLowerCase() == 'width') {
                    return viewport.width;
                }
                if (param.toLowerCase() == 'height') {
                    return viewport.height;
                }
            }
            return viewport;
            //var deviceWidth = window.orientation == 0 ? window.screen.width : window.screen.height;
            //// iOS returns available pixels, Android returns pixels / pixel ratio
            //// http://www.quirksmode.org/blog/archives/2012/07/more_about_devi.html
            //if (navigator.userAgent.indexOf('Android') >= 0 && window.devicePixelRatio) {
            //    deviceWidth = deviceWidth / window.devicePixelRatio;
            //}
            //return deviceWidth;
        },


        getBootstrapPlugins: function () {
            var ret_str = '';
            ret_str += "alert:" + ((typeof ($.fn.alert) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "button:" + ((typeof ($.fn.button) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "carousel:" + ((typeof ($.fn.carousel) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "dropdown:" + ((typeof ($.fn.dropdown) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "modal:" + ((typeof ($.fn.modal) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "tooltip:" + ((typeof ($.fn.tooltip) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "popover:" + ((typeof ($.fn.popover) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "tab:" + ((typeof ($.fn.tab) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "affix:" + ((typeof ($.fn.affix) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "collapse:" + ((typeof ($.fn.collapse) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "scrollspy:" + ((typeof ($.fn.scrollspy) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            ret_str += "transition:" + ((typeof ($.fn.transition) != 'undefined') ? " <b>On</b>" : "Off") + ". ";
            //ret_str+= "alert:" + ( (typeof($.fn.alert) != 'undefined') ? " <b>On</b>" :"Off" ) + ". ";
            return ret_str;
        },


        getVueVersion: function () {
            return Vue.version;
        },


        formatNumberToHuman: function (number) {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        },

        convertMinsToHrsMins: function (minutes) {
            if (minutes < 60) return (minutes + ' minutes');
            var h = Math.floor(minutes / 60);
            var m = minutes % 60;
            h = h < 10 ? h : h;
            m = m < 10 ? m : m;
            return h + ':' + m + ' hour(s)';
        },

        pluralize: function (n, single_str, multi_str) {
            return n === 1 ? single_str : multi_str
        },

        showFullUserName: function (username, first_name, last_name) {
            var ret_str = first_name + " " + last_name + " ( " + username + " ) ";
            return ret_str;
        },

        getHeaderIcon(icon) {
            // alert( "menuIconsArray::"+var_dump(menuIconsArray) )
            if (typeof menuIconsArray != "undefined" && typeof menuIconsArray[icon] != "undefined") return menuIconsArray[icon];
            // if (typeof menuIconsArray != "undefined" && typeof menuIconsArray[icon] != "undefined") return '<i class="' + menuIconsArray[icon] + '"></i>';
        },

        getActiveUserGroupLabel(loggedUserInGroupsArray) {
            return '';
            // alert( "checkUserGroupAccess   typeof loggedUserInGroupsArray ::"+ ( typeof loggedUserInGroupsArray) + "  group_name::"+group_name+"   loggedUserInGroupsArray ::"+(loggedUserInGroupsArray) )
            var ret = '';
            loggedUserInGroupsArray.map((nextLoggedUserInGroup, index) => {
                // alert( "nextLoggedUserInGroup::" + var_dump(nextLoggedUserInGroup) )
                if (nextLoggedUserInGroup.group_name != "undefined" && nextLoggedUserInGroup.status == 'A') {
                    ret = ret + nextLoggedUserInGroup.group_name + ', ';
                }
            });
            return ret;
        },

        /*checkUserGroupAccess(loggedUserInGroupsArray, group_name) {
            return true;
            // alert( "checkUserGroupAccess   typeof loggedUserInGroupsArray ::"+ ( typeof loggedUserInGroupsArray) + "  group_name::"+group_name+"   loggedUserInGroupsArray ::"+(loggedUserInGroupsArray) )
            var ret = false;
            loggedUserInGroupsArray.map((nextLoggedUserInGroup, index) => {
                // alert( "nextLoggedUserInGroup::" + var_dump(nextLoggedUserInGroup) )
                if (nextLoggedUserInGroup.group_name == group_name && nextLoggedUserInGroup.status == 'A') {
                    ret = true;
                }
            });
            return ret;
        },
*/
        dateToMySqlFormat(dat) {
            if (typeof dat != 'object') return dat;
            // alert( "dat::"+(dat) +  "   typeof  dat::"+(typeof dat) + "  dat::"+var_dump(dat) )

            var mm = dat.getMonth() + 1; // getMonth() is zero-based
            var dd = dat.getDate();

            return [dat.getFullYear(),
                (mm > 9 ? '' : '0') + mm,
                (dd > 9 ? '' : '0') + dd
            ].join('-');
        },
        say(row) {
            alert('Open console and looking for response object!')
            console.log(row);
        },
        edit(value) {
            this.action.edit_value = value;
        },
        submit(res) {
            if (res.row[res.k] != this.action.edit_value) {
                let after_update = JSON.parse(JSON.stringify(res.row))
                this.$refs.my_table.set_row_undo(after_update, res.c)
                res.row[res.k] = this.action.edit_value
            }
            this.reset_edit()
        },
        reset_edit() {
            this.$refs.my_table.close()
        },
        set_checkbox(item) {
            this.action.checkbox = item
        },
        edit_row(row) {
            for (let k in row) {
                this.action.edit_row[k] = row[k]
            }
        },
        submit_row(row) {
            let after_update = JSON.parse(JSON.stringify(row))
            for (let key in this.action.edit_row) {
                row[key] = this.action.edit_row[key]
            }
            this.$refs.my_table.set_row_undo(after_update)
            this.$refs.my_table.close(true)
        },

        // timeInAgoFormat : function(value) {
        //     alert( "-2 timeInAgoFormat  value::"+(value) )
        //     return moment(value).fromNow();
        //     // return moment(value).calendar();
        // },
        //


        makeSystemLogout: function (api_version_link, show_message, bus) {
            // alert( "makeSystemLogout api_version_link::"+(api_version_link) )
            axios.post(api_version_link + 'logout').then((response) => {
                if (show_message) {
                    bus.$emit("showSnackbarMessage", {text: "You have no access to this page!", type: 'warn'});
                }
                Vue.localStorage.set('logged_user_id', '')
                Vue.localStorage.set('logged_user_username', '')
                Vue.localStorage.set('logged_user_full_name', '')
                Vue.localStorage.set('loggedUserProfile', {})
                this.$router.push({path: '/login'});
            }).catch((error) => {
                console.error(error)
            });
        }, //makeSystemLogout() {

        getSettingsValue: function (name, default_value, api_version_link, trigger_event_name, bus) {
            axios.get(api_version_link + '/get_settings_value?name=' + name)
                .then(function (response) {
                    // alert( "getSettingsValue response.data::"+(response.data) )
                    if (typeof trigger_event_name != "undefined" && typeof bus != "undefined") {
                        // alert( "INSIDE -1 trigger_event_name"+trigger_event_name )
                        bus.$emit(trigger_event_name, response.data.settings_value);
                    }
                    //                     bus.$emit('UserProfileIsSetEvent', response.data.loggedUser.first_name + ' ' + response.data.loggedUser.last_name, response.data.site_name);

                    return response.data.settings_value;
                })
                .catch(function (error) {
                    console.error(error)
                })
        },

        formatColor: function (rgb) {
            var isOk = /^#[0-9A-F]{6}$/i.test(rgb)
            // var isOk  = /^#[0-9A-F]{6}$/i.test('#aabbcc')
            // alert( "isOk::"+isOk )
            if (isOk) return rgb;

            // alert( typeof rgb )
            if (typeof rgb != "string" || this.trim(rgb) == "") return "";
            rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
            return (rgb && rgb.length === 4) ? "#" +
                ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
                ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
        },

        trim: function (str) {
            if (typeof str != "string") return "";
            return str.replace(/^\s+|\s+$/gm, '');
        },

        concatStrings: function (dataArray, splitter) {
            var ret = '';
            // alert( "dataArray::"+(typeof dataArray)+"   "+var_dump(dataArray) )
            var L = dataArray.length;
            let resArray = []
            for (let I = 0; I < L; I++) {
                var next_string = this.trim(dataArray[I]);
                if (typeof next_string == "string") {
                    if (next_string.length > 0) {
                        resArray[resArray.length] = next_string;
                    }
                }
            }
            var L = resArray.length;
            resArray.map((next_string, index) => {
                // next_string = jQuery.trim(next_string);
                var next_string = this.trim(next_string);
                if (typeof next_string == "string") {
                    if (next_string) {
                        if (L === index + 1) {
                            ret = ret + next_string;
                        } else {
                            ret = ret + next_string + splitter;
                        }
                    } // if ( next_string ) {
                }
            });
            return ret;
        },


        commonError: function (err_text) {
            return err_text;
        },

        concatStr: function (str, max_str_length_in_listing) {
            if (typeof settings_max_str_length_in_listing == "undefined" && typeof max_str_length_in_listing != "undefined") {
                var settings_max_str_length_in_listing = max_str_length_in_listing
            }
            if (typeof str == "undefined") str = '';
            if (str.length > settings_max_str_length_in_listing) {
                return str.slice(0, settings_max_str_length_in_listing) + '...';
            }
            return str;
        },

        getNowDateTime: function () {
            return Date.now();
        },

        getNowTimestamp: function () {
            return Date.now() / 1000 | 0;
        },


        getFileSizeAsString: function (file_size) {
            if (parseInt(file_size) < 1024) {
                return file_size + 'b';
            }
            if (parseInt(file_size) < 1024 * 1024) {
                return Math.floor(file_size / 1024) + 'kb';
            }
            return Math.floor(file_size / (1024 * 1024)) + 'mb';
        },


        Uppercase: function (string) {
            return string.toUpperCase();
        },

        lowerCase: function (string) {
            return string.toLowerCase(0);
        },

        Capitalize: function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },


        confirmMsg: function (question, paramsArray, title, bus) {
            // debugger
            this.$modal.show('dialog', {
                title: title,
                text: question,
                buttons: [
                    {
                        title: 'Yes',
                        default: true,    // Will be triggered by default if 'Enter' pressed.
                        handler: () => {
                            // alert( "dialog_confirmed paramsArray::"+var_dump(paramsArray) )
                            bus.$emit('dialog_confirmed', paramsArray);
                            this.$modal.hide('dialog')
                        }
                    },
                    /*
                                        {
                                            title: '',       // Button title
                                            handler: () => {
                                            } // Button click handler
                                        },
                    */
                    {
                        title: 'Cancel'
                    }
                ]
            })
        },

        func_setting_focus: function (focus_field) {
            $('#' + focus_field).focus();
        },

        alertMsg: function (content, title, confirm_button, icon, focus_field) {
            $.alert({
                title: title,
                content: content,
                icon: (typeof icon != 'undefined' ? icon : 'fa fa-info-circle'),
                confirmButton: (typeof confirm_button != 'undefined' ? confirm_button : 'OK'),
                keyboardEnabled: true,
                columnClass: 'col-md-8 col-md-offset-2  col-sm-8 col-sm-offset-2 ',
                confirm: function () {
                    setTimeout("func_setting_focus('" + focus_field + "')", 500);
                }
            });
        },

        br2nl: function (str) {
            return str.replace(/<br\s*\/?>/mg, "\n");
        },

        // nl2br: function (str) { // Inserts HTML line breaks before all newlines in a string
        //     return str.replace(/([^>])\n/g, '$1<br/>');
        // },


        nl2br: function (str, is_xhtml) {
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
        },


        cleartags: function (str) {
            if (typeof str != "string") return "";
            return str.replace(/<\/?[^>]+(>|$)/g, "");
        },


        prettyJSON: function (json) {
            if (json) {
                json = JSON.stringify(json, undefined, 4);
                json = json.replace(/&/g, '&').replace(/</g, '<').replace(/>/g, '>');
                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                    var cls = 'number';
                    if (/^"/.test(match)) {
                        if (/:$/.test(match)) {
                            cls = 'key';
                        } else {
                            cls = 'string';
                        }
                    } else if (/true|false/.test(match)) {
                        cls = 'boolean';
                    } else if (/null/.test(match)) {
                        cls = 'null';
                    }
                    return '<span class="' + cls + '">' + match + '</span>';
                });
            }
        },


    }, // methods: {

}

function var_dump(oElem, from_line, till_line) {
    if (typeof oElem == 'undefined') return 'undefined';
    var sStr = '';
    if (typeof (oElem) == 'string' || typeof (oElem) == 'number') {
        sStr = oElem;
    } else {
        var sValue = '';
        for (var oItem in oElem) {
            sValue = oElem[oItem];
            if (typeof (oElem) == 'innerHTML' || typeof (oElem) == 'outerHTML') {
                sValue = sValue.replace(/</g, '&lt;').replace(/>/g, '&gt;');
            }
            sStr += 'obj.' + oItem + ' = ' + sValue + '\n';
        }
    }
    //alert( "from_line::"+(typeof from_line) )
    if (typeof from_line == "number" && typeof till_line == "number") {
        return sStr.substr(from_line, till_line);
    }
    if (typeof from_line == "number") {
        return sStr.substr(from_line);
    }
    return sStr;
}
