
// FRONTEND PAGES BLOCK START
// import FlexHome from './components/BS4/flex/home.vue'; // resources/js/components/BS4/flex/home.vue
import Home from './components/BS4/Home.vue';
import SearchResults from './components/BS4/SearchResults.vue';

import Test from './components/BS4/Test.vue';

import NewsList from './components/BS4/news/NewsList.vue';
import SingleNews from './components/BS4/news/SingleNews.vue';

import PublicUserProfile from './components/BS4/public_user/PublicUserProfile.vue';  //resources/js/components/BS4/public_user/PublicUserProfile.vue

import ForumsList from './components/BS4/forum/ForumsList.vue';
import ForumView from './components/BS4/forum/ForumView.vue';
import ForumThreadView from './components/BS4/forum/ForumThreadView.vue';

import Login from './components/BS4/auth/Login.vue';
import CustomerRegister from './components/BS4/user/CustomerRegisterForm.vue'; // resources/js/components/BS4/user/CustomerRegister.vue

import Personal from './components/BS4/personal/Personal.vue';
import PersonalSelectSubscription from './components/BS4/personal/PersonalSelectSubscription.vue';
import ConfirmSelectedPersonalSubscription from './components/BS4/personal/ConfirmSelectedPersonalSubscription.vue';
import HostelsByRegion from './components/BS4/hostels/HostelsByRegion.vue';
import HostelsSorted from './components/BS4/hostels/HostelsSorted.vue';
import HostelsByLocation from './components/BS4/hostels/HostelsByLocation.vue';
import HostelView from './components/BS4/hostels/HostelView.vue'; //

// FRONTEND PAGES BLOCK END



// BACKEND CRUD PAGES BLOCK START
import DashboardContainer from './components/BS4/admin/dashboard/DashboardIndex.vue';
import AdminFeedback from './components/BS4/admin/dashboard/AdminFeedback.vue';

import SystemInfo from './components/BS4/admin/dashboard/SystemInfo.vue';

import SqlMonitorContainer from './components/BS4/admin/dashboard/SqlMonitor.vue'; //resources/js/components/BS4/admin/dashboard/SqlMonitor.vue


import customersContainer from './components/BS4/admin/customers/customers.vue';
import customersList from './components/BS4/admin/customers/list.vue';
import customersFilterList from './components/BS4/admin/customers/list.vue';
import editCustomer from './components/BS4/admin/customers/edit.vue';


import facilitiesContainer from './components/BS4/admin/facilities/facilities.vue';
import facilitiesList from './components/BS4/admin/facilities/list.vue';
import facilitiesFilterList from './components/BS4/admin/facilities/list.vue';
import editFacility from './components/BS4/admin/facilities/edit.vue';


import statesContainer from './components/BS4/admin/states/states.vue';
import statesList from './components/BS4/admin/states/list.vue';


import hostelsContainer from './components/BS4/admin/hostels/hostels.vue';
import hostelsList from './components/BS4/admin/hostels/list.vue';
import hostelsFilterList from './components/BS4/admin/hostels/list.vue';
import editHostel from './components/BS4/admin/hostels/edit.vue';


// BACKEND CRUD PAGES BLOCK END



// COMMON PAGES BLOCK START
import NotFound from './components/BS4/NotFound.vue'; // resources/assets/js/components/NotFound.vue
// COMMON PAGES BLOCK END



export const routes = [

    // FRONTEND PAGES BLOCK START

    {
        path: '/',
        component: Home,
        meta: {
            breadcrumb: 'Home Page',
            requiresAuth: false
        },
        name: 'HomePage'
    },

    {
        path: '/search-results',
        component: SearchResults,
        meta: {
            breadcrumb: 'Search',
            requiresAuth: false
        },
        name: 'SearchResultsPage'
    },

    {
        path: '/test',
        component: Test,
        meta: {
            breadcrumb: 'Test',
            requiresAuth: false
        },
        name: 'TestPage'
    },




    {
        path: '/news-list',
        component: NewsList,
        meta: {
            breadcrumb: 'News listing',
            requiresAuth: false
        },
        name: 'NewsList'
    },
    {
        path: '/news/:slug',
        component: SingleNews,
        meta: {
            breadcrumb: 'News',
            requiresAuth: false
        },
        name: 'SingleNews'
    },


    {
        path: '/user-profile/:id',
        component: PublicUserProfile,
        meta: {
            breadcrumb: 'PublicUserProfile',
            requiresAuth: false
        },
        name: 'PublicUserProfile'
    },



    {
        path: '/forums-list',
        component: ForumsList,
        meta: {
            breadcrumb: 'Forums',
            requiresAuth: false
        },
        name: 'ForumsList'
    },
    {
        path: '/forum/:slug',
        component: ForumView,
        meta: {
            breadcrumb: 'Forum',
            requiresAuth: false
        },
        name: 'ForumView'
    },

    {
        path: '/forum-thread/:slug',
        component: ForumThreadView,
        meta: {
            breadcrumb: 'Forum Thread',
            requiresAuth: false
        },
        name: 'ForumThreadView'
    },




    {
        path: '/login',
        component: Login,
        name: 'loginForm',
        meta: {
            breadcrumb: 'Login',
            requiresAuth: false,
        },
    },

    {
        path: '/customer-register',
        component: CustomerRegister,
        name: 'CustomerRegisterForm',
        meta: {
            breadcrumb: 'Customer Register',
            requiresAuth: false,
        },
    },

    {
        path: '/personal',
        component: Personal,
        meta: {
            breadcrumb: 'Personal',
            requiresAuth: true,
            authGroupsAccess: ['Admin', 'Manager', 'Customer'],
        },
        name: 'Personal'
    },

    {
        path: '/personal-select-subscription',
        component: PersonalSelectSubscription,
        meta: {
            breadcrumb: 'PersonalSelectSubscription',
            requiresAuth: true,
            authGroupsAccess: ['Customer'],
        },
        name: 'PersonalSelectSubscription'
    },

    {
        path: '/confirm-selected-personal-subscription/:id',
        component: ConfirmSelectedPersonalSubscription,
        meta: {
            breadcrumb: 'ConfirmSelectedPersonalSubscription',
            requiresAuth: true,
            authGroupsAccess: ['Customer'],
        },
        name: 'ConfirmSelectedPersonalSubscription'
    },

    {
        path: '/hostels-by-region/:state_slug/:region_slug',
        component: HostelsByRegion,
        name: 'hostelsByRegion',
        meta: {
            breadcrumb: 'Hostels By Region',
            requiresAuth: false,
        },
    },
    {
        path: '/hostels-sorted/:sort_by',
        component: HostelsSorted,
        name: 'hostelsSorted',
        meta: {
            breadcrumb: 'Hostels Sorted',
            requiresAuth: false,
        },
    },
    {
        path: '/hostels-by-location',
        component: HostelsByLocation,
        name: 'hostelsByLocationGroupListing',
        meta: {
            breadcrumb: 'Hostels By Locations',
            requiresAuth: false,
        },
    },
    {
        path: '/hostel/:slug/:state_slug/:region_slug/:subregion_slug',
        component: HostelView,
        name: 'hostelViewPage',
        meta: {
            breadcrumb: 'Login',
            requiresAuth: false,
        },
    },

    // FRONTEND PAGES BLOCK END


// COMMON PAGES BLOCK START
    {
        path: '/not-found/:invalid_url?',
        component: NotFound, name: 'notFound'
    },     // resources/assets/js/components/NotFound.vue
// COMMON PAGES BLOCK END

    // {
    //     path: '/categories',
    //     component: CategoriesMain,
    //     meta: {
    //         requiresAuth: true,
    //         authGroupsAccess: ['Admin'],
    //     },
    //     children: [
    //         {
    //             path: '/',
    //             component: CategoriesList,
    //             name: 'listCategories'
    //         },
    //         {
    //             path: 'new',
    //             component: CategoryEdit,
    //             name: 'newCategory'
    //         },
    //         {
    //             path: ':id',
    //             component: CategoryEdit,
    //             name: 'editCategory'
    //         }
    //     ]
    // },

// BACKEND CRUD PAGES BLOCK START
    {
        path: '/admin/dashboard',
        component: DashboardContainer,
        meta: {
            requiresAuth: true,
            authGroupsAccess: ['Admin', 'Manager'],
        },
        name: 'DashboardIndex'
    },

    {
        path: '/admin/admin-feedback',
        component: AdminFeedback,
        meta: {
            requiresAuth: true,
            authGroupsAccess: ['Admin', 'Manager'],
        },
        name: 'AdminFeedbackIndex'
    },

    {
        path: '/admin/system-info',
        component: SystemInfo,
        meta: {
            requiresAuth: false
        },
        name: 'SystemInfoPage'
    },

    {
        path: '/admin/sql-monitor',
        component: SqlMonitorContainer,
        meta: {
            requiresAuth: true,
            authGroupsAccess: ['Admin', 'Manager'],
        },
        name: 'SqlMonitor'
    },

    {
        path: '/admin/customers',
        component: customersContainer,
        meta: {
            requiresAuth: true,
            authGroupsAccess: ['Admin', 'Manager'],
        },
        children: [
            {
                path: '/admin/customers/filter/:filter',
                component: customersFilterList,
                name: 'filterListCustomers'
            },
            {
                path: '/admin/customers/',
                component: customersList,
                name: 'listCustomers'
            },

            {
                path: '/admin/customers/new',
                component: editCustomer,
                name: 'newCustomer'
            },
            {
                path: ':id',
                component: editCustomer,
                name: 'editCustomer'
            },

        ]
    },


    {
        path: '/admin/facilities',
        component: facilitiesContainer,
        meta: {
            requiresAuth: true,
            authGroupsAccess: ['Admin'],
        },
        children: [
            // {
            //     path: '/admin/facilities/filter/:filter',
            //     component: facilitiesFilterList,
            //     name: 'filterListFacilities'
            // },
            {
                path: '/admin/facilities/',
                component: facilitiesList,
                name: 'listFacilities'
            },

            {
                path: '/admin/facilities/new',
                component: editFacility,
                name: 'newFacility'
            },
            {
                path: ':id',
                component: editFacility,
                name: 'editFacility'
            },

        ]
    },   // path: '/admin/facilities',





    {
        path: '/admin/states',
        component: statesContainer,
        meta: {
            requiresAuth: true,
            authGroupsAccess: ['Admin'],
        },
        children: [
            // {
            //     path: '/admin/states/filter/:filter',
            //     component: statesFilterList,
            //     name: 'filterListStates'
            // },
            {
                path: '/admin/states/',
                component: statesList,
                name: 'listStates'
            },

            // {
            //     path: '/admin/facilities/new',
            //     component: editFacility,
            //     name: 'newFacility'
            // },
            // {
            //     path: ':id',
            //     component: editFacility,
            //     name: 'editFacility'
            // },
            //
        ]
    },   // path: '/admin/facilities',




    {
        path: '/admin/hostels',
        component: hostelsContainer,
        meta: {
            requiresAuth: true,
            authGroupsAccess: ['Admin', 'Manager'],
        },
        children: [
            {
                path: '/admin/hostels/filter/:filter',
                component: hostelsFilterList,
                name: 'filterListHostels'
            },
            {
                path: '/admin/hostels/',
                component: hostelsList,
                name: 'listHostels'
            },
            {
                path: '/admin/hostels/new',
                component: editHostel,
                name: 'newHostel'
            },
            {
                path: ':id/:additive_action?/:additive_action_id?',
                component: editHostel,
                name: 'editHostel'
            },
            /* {
 path: '/project/:name/:id?',
 name: 'projectDetail',
 component: ProjectDetail,
} */

        ]
    }   // path: '/admin/hostels',




// BACKEND CRUD PAGES BLOCK END

];
