<?php $current_dashboard_template = 'BS4' ?>

        <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title id="app_title">{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" href="favicon.ico"/>

    <!-- Fonts -->
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">--}}
    <link href="{{ asset(('css/'.$current_dashboard_template.'/app.css')) }}" rel="stylesheet" type="text/css">


    {{--iPhone portrait 320 x 480--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_xs_320.css') }}"
          media="only screen and (min-width: 320px) and (max-width: 479px) "/>

    {{--iPhone landscape 480 x 320--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_xs_480.css') }}"
          media="only screen and (min-width: 480px)  and (max-width: 599px) "/>

    {{--Kindle portrait 600 x 1024--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_xs_600.css') }}"
          media="only screen and (min-width: 600px)  and (max-width: 767px) "/>

    {{--iPad portrait 768 x 1024--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_sm.css') }}"
          media="only screen and (min-width: 768px)  and (max-width: 1023px) "/>

    {{--iPad landscape 1024 x 768--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_md.css') }}"
          media="only screen and (min-width: 1024px) and (max-width: 1279px) "/>

    {{--Macbook 1280 x 800--}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/'.$current_dashboard_template.'/style_lg.css') }}" media="only screen and (min-width: 1280px)"/>

</head>

<body>

<div id="app">

    <header>
        <appheader></appheader>
    </header>
    <main>

        <div class="wrapper">
            <div id="page_content">
                <navigation></navigation>
{{--                <breadcrumbs></breadcrumbs>--}}

                <mainapp></mainapp>
            </div>
        </div>

    </main>

    <footer>
        <appfooter></appfooter>
    </footer>

</div>

</body>

@include('footer')
<script src="{{ asset('js/app.js') }}{{  "?dt=".time()  }}"></script>

<script src="https://js.stripe.com/v3/"></script>

</html>
