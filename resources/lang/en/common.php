<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'site_name'             => 'Tasks/Chats/Events',
    'add'                   => 'add',
    'edit'                  => 'edit',
    'delete'                => 'delete',
    'enter_unique_name_of'  => 'enter unique name of {0}',
    'enter'                 => 'enter',

    'insert'                => 'insert',
    'update'                => 'update',
    'error_adding'          => 'error adding',
    'error_updating'        => 'error updating',
    'error_deleting'        => 'error deleting',

    'added_successfully'    => 'added successfully',
    'updated_successfully'  => 'updated successfully',
    'deleted_successfully'  => 'deleted successfully',


    'save_btn'              => 'save',
    'cancel_btn'            => 'cancel',
    'add_btn'               => 'add',
    'update_btn'            => 'update',
    'select'                => 'select',

    'markdown_format'       => 'markdown format',
    'info'                  => 'info',
    'all'                   => 'all',
    'new'                   => 'new',
    'optional'              => 'optional',
    'has'                   => 'has',

    'used_in'               => 'used in',
    'row'                   => 'row | rows',
    'of'                    => 'of',
    'page'                  => 'page | pages',
    'do_you_want_to_delete' => 'do you want to delete',
    'confirm'               => 'confirm',

    'dictionaries'          => 'dictionaries',
    'till_n_symbols'        => 'till {n} symbols',

    'you_have_no_access_to' => 'you have no access to {page_type}',

];
