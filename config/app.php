<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    'valid_color_format'=> '~^#(([0-9a-fA-F]{2}){3}|([0-9a-fA-F]){3})$~',
    'text_color' => '#00001D',
    'disabled_text_color' => '#9a9390',

    'colorsList'=> ["#005500", "#3e95cd", "#8e5ea2", "#ff063c", "#3cba9f", "#e8c3b9", "#ffff00", "#0000ff", "#fb6bff", "#55ffff", "#c6c7ff", "#aaff7f", "#a5aaff", "#fffda6", "#707070", "#c45850", "#dfdf00" ],


    'valid_nice_name_format'     => '~^[a-zA-Z]*$~',
    'ff_valid_phone_format'      =>  '(01)[0-9]{9}',
    'valid_percent_format'       =>  '^\d*(\.\d{1,2})?$',
    'valid_geographic_coordinate_format' =>  '^[\-]?\d*(\.\d{1,7})?$',
    'valid_money_format'         =>  '^\d+(\.\d{1,2})?$',
    'valid_shipping_decimal_format' =>  '^\d*(\.\d{1,2})?$',
    'valid_phone_format'         =>  '(01)[0-9]{9}',
    'js_moment_date_format'      => 'Do MMMM, YYYY',
    'js_moment_datetime_format'  => 'Do MMMM, YYYY h:mm A',
    'pickdate_format_submit'     => 'yyyy-mm-dd',
    'pickdate_format_view'       => 'd mmmm, yyyy',
    'datetime_carbon_format'     => 'Y-m-d H:i:s',
    'datetime_carbon_format_miliseconds'     => 'Y-m-d H:i:s.u',
    'date_carbon_format'         => 'Y-m-d',
    'date_carbon_format_as_text' => '%d %B, %Y',

    'main_image_recommended_height'=>"240px",
    'main_image_recommended_width'=>"320px",

    'datepicker_min_year'        => 1980,
    'datepicker_max_year'        => 2099,

    'currency'                   => 'AU$',
    'currency_short'             => 'AU$',
    'currency_left'              => true,

    'min_hostel_rooms_price'     => 0,
    'max_hostel_rooms_price'     => 100,

    'subscription_image'              => 'subscription.jpg',
    'images_extensions'               => ['png', /*'webp',*/ 'jpg', 'jpeg', 'gif'],
    'uploaded_file_max_mib'           => 2,
    'avatar_dimension_limits'         => ['max_width' => 64, 'max_height' => 64],

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services the application utilizes. Set this in your ".env" file.
    |
    */

    'env' => env('APP_ENV', 'production'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),
    'API_VERSION_LINK' => env('API_VERSION_LINK', '/api'),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => env('APP_URL', 'http://localhost'),

    'asset_url' => env('ASSET_URL', null),


    'skinsArray'=> [
        'BS4'=> ' Base responsive',
        'Fancy'=> ' Fancy responsive',
     ],



    'menuIconsArray'=> [ // https://fontawesome.com/icons?d=gallery
        'home'               => 'fa fa-home',
        'hostel'             => 'fa fa-bed',
        'room'               => 'fa fa-window-restore',
        'customer'           => 'fa fa-address-card',
        'task'               => 'fa fa-tasks',
        'dashboard'          => 'fa fa-th-large',
        'admin_feedback'     => 'fa fa-american-sign-language-interpreting',
        'event'              => 'fa fa-calendar',
        'user_task_type'     => 'fa fa-wrench',
        'user_chat'          => 'fa fa-comment',
        'enqueries'          => 'fa fa-at',
        'options'            => 'fa fa-arrows-alt',

        'profile'            => 'fa fa-user-md',
        'users'              => 'fa fa-users',
        'subscription'       => 'fa fa-envelope-open-o',
        'facility'           => 'fa fa-list-alt',
        'weather'            => 'fa fa-bolt',
        'flash'              => 'fa fa-exclamation',
        'twitter'            => 'fa fa-twitter',
        'file_source'        => 'fa fa-file-code',
        'todo'               => 'fa fa-check-circle',
        'not_found'          => 'fa fa-bug',
        'priority'           => 'fa fa-asterisk',
        'sign-out'           => 'fa fa-sign-out',
        'state'              => 'fa fa-globe',
        'address'              => 'fa fa-map-marker',
        'location'              => 'fa fa-map-pin',

        'remove'              => 'fa fa-trash',
        'remove2x'            => 'fa fa-2x fa-trash',
        'edit'                => 'fa fa-edit',

        'cancel'              => 'fa fa-window-close',
        'save'                => 'fa fa-save',
        'select'                => 'fa fa-mouse-pointer',

        'rating'             => 'fa fa-star-half-o',
        'reviews'            => 'fa fa-commenting-o',
        'inqueries'          => 'fa fa-flash',
        'phone'              => 'fa fa-phone',
        'email'              => 'fa fa-envelope',
        'website'            => 'fa fa-link',
        'modified'           => 'fa fa-pencil-square',
        'money'              => 'fa fa-money',
        'bookmark'           => 'fa fa-bookmark',
        'flag'               => 'fa fa-flag',
        'is_priority_listing'=> 'fa fa-magnet',
        'featured'           => 'fa fa-fire',



        'check'              => 'fa fa-check',
        'check-circle'       => 'fa fa-check-circle',
        'check-square'       => 'fa fa-check-square',


        'on'                 => 'fa fa-toggle-on',
        'off'                => 'fa fa-toggle-off',
        'system'             => 'fa fa-analytics',
        'tools'              => 'fa fa-toggle-tools',
        'monitor'            => 'fa fa-monitor-heart-rate',
        'activate'           => 'fa fa-address-book',
        'deactivate'         => 'fa fa-address-book',
        'news'               => 'fa fa-pagelines',
        'search'             => 'fa fa-search',
        'action'             => 'fa fa-exclamation-triangle',
        'graduation'         => 'fa fa-graduation-cap',

        'accept'             => 'fa fa-check-square',
        'complete'           => 'fa fa-th-list',
        'filled_list'        => 'fa fa-list-alt',
        'server'             => 'fa fa-server',
        'upload'             => 'fa fa-upload',
        'forum_category'     => 'fa fa-wpforms',
		'forum'              => 'fa fa-align-center',

        'details'   => "fa fa-info-circle",
        'info'      => "fa fa-info",
        'load'      => "fa fa-upload",
        'delete'    => 'fa fa-trash',
        'calendar'  => 'fa fa-calendar',
        'clock'     => 'fa fa-history',
        'plus'          => 'fa fa-plus',
        'plus-square'   => 'fa fa-plus-square',
        'request_callback'  => 'fa fa-american-sign-language-interpreting',
        'report_flag'   => 'fa fa-flag',
        'text'          => 'fa fa-text-width',
        'image'         => 'fa fa-image',

        'arrow_left'      => 'fa fa-arrow-left',
        'arrow_right'     => 'fa fa-arrow-right',
        'owner'           => 'fa fa-snapchat-ghost',
        'content_editor'  => 'fa fa-unlink',
        'filled_text'  => 'fa fa-comments',

    ],

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Europe/Kiev',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Faker Locale
    |--------------------------------------------------------------------------
    |
    | This locale will be used by the Faker PHP library when generating fake
    | data for your database seeds. For example, this will be used to get
    | localized telephone numbers, street address information and more.
    |
    */

    'faker_locale' => 'en_US',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        'Intervention\Image\ImageServiceProvider',

        /*
         * Package Service Providers...
         */

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
         App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
//        Tymon\JWTAuth\Providers\LaravelServiceProvider::class,
//        Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class,

        'Laracasts\Utilities\JavaScript\JavaScriptServiceProvider',

        Way\Generators\GeneratorsServiceProvider::class,
        Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class,
        Orangehill\Iseed\IseedServiceProvider::class,


//        Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class,
        Zizaco\Entrust\EntrustServiceProvider::class,


    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Image' => 'Intervention\Image\Facades\Image',

        'JWTAuth'   => Tymon\JWTAuth\Facades\JWTAuth::class,
        'JWTFactory' => Tymon\JWTAuth\Facades\JWTFactory::class,
        'Entrust' => Zizaco\Entrust\EntrustServiceProvider::class

    ],

];
