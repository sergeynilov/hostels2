WiznextEnvoy.blade.php
@setup
// Under directory /_wwwroot/lar/Ads RUN
// export PATH="$PATH:~/.composer/vendor/bin"
// cd /_wwwroot/lar/Ads
// envoy run Hostels2Deploy  --lardeployer_password=mypass1112233 --app_version=0.123
$server_login_user= 'lardeployer';
$user_password = isset($lardeployer_password) ? $lardeployer_password : "Not Defined";

// check vars in command line
// if ( !isset($repo) )
//     {
//        throw new Exception('--repo must be specified');
//     }

//     if ( !isset($base_dir) )
//     {
//         throw new Exception('--base_dir must be specified');
//     }

$timezone= 'Europe/Kiev';

$base_dir= '/var/www/html/Hostels2Deployed';

$current_dir = $base_dir . '/current'; // THIS DIRECTORY DOES NOT EXISTS AT SERVER - it will be created as link


$repo= 'git@bitbucket.org:sergeynilov/hostels2.git';

$branch= 'master';

// Directory and files with chmod 755
$writableDirs= [
'storage/logs',
'bootstrap/cache'
];
$now_date = new DateTime('now', new DateTimeZone($timezone));

$release_dir = $base_dir . '/release';
$release_number_dir = $base_dir . '/release/' . $now_date->format('Ymd-His');

$git_clone_command = 'git clone --depth 1 -b '.$branch.' "'.$repo.'" '.' "'.$release_number_dir.'"';

$database_factories_path= $release_number_dir.'/database/factories';

@endsetup

@servers(['dev' => $server_login_user.'@138.68.107.4'])
{{--@servers(['production' => $server_login_user.'@138.68.107.4'])--}}
{{--### @servers(['production' => $server_login_user.'@ec2-13-59-27-84.us-east-2.compute.amazonaws.com'])--}}


@task( 'clone_project', ['on'=>$on] )
echo "Step # 00 app_version ::{{ $app_version }}";


echo '$user_password password ::';
echo $user_password;
{{--echo ( isset(lardeployer_password) ? lardeployer_password : "Not Defined" );--}}
echo '$release_number_dir :: {{ $release_number_dir }}';

echo '$database_factories_path :: {{ $database_factories_path }}';

echo '$git_clone_command {{ $git_clone_command }}::';


# mkdir -p {{$current_dir}}

mkdir -p {{$release_dir}}

mkdir -p {{$release_number_dir}}


{{ $git_clone_command }}

mkdir -p {{$database_factories_path}}

echo "Step # 1 : Repository has been cloned";
@endtask


@task( 'composer_installing', ['on'=>$on] )
cd {{ $release_number_dir }}

echo "Step # 2 start : composer_installing dependencies installing";
composer install --no-interaction --prefer-dist
# composer install --no-interaction --no-dev --prefer-dist # PRODUCTION

echo "Step # 2 : composer_installing dependencies has been installed";
@endtask


@task( 'artisan_running', ['on'=>$on] )
echo "Step # 3 : Dev dependencies installing";
cd {{ $release_number_dir }}

ln -nfs {{ $base_dir }}/.env .env
chgrp -h www-data .env

echo "Step # 31";
php artisan config:clear

echo "Step # 32";
php artisan migrate

# echo "Step # 33";
# php artisan db:seed  # Run only once


echo "Step # 34";
php artisan clear-compiled --env=dev
# php artisan clear-compiled --env=production

# php artisan optimize --env=dev

echo "Step # 3 : Dev dependencies has been installed";
@endtask


@task( 'chmod_settings', ['on'=>$on] )
echo "Step # 4 : chmod_settings = Permissions setting";
echo 123 > 'envoy_test'.txt;
chgrp -R www-data {{ $release_number_dir }}

echo "Step # 40  $release_number_dir   :: {{ $release_number_dir }}";
# chmod -R ug-rwx {{ $release_number_dir }}

echo "Step # 41 : chmod_settings = Permissions setting";
@foreach($writableDirs as $file)
    echo "Step # 42 : {{ $release_number_dir }}/{{ $file }}";
    # chmod -R 775 {{ $release_number_dir }}/{{ $file }}

    @if ( !file_exists( $release_number_dir.'/'. $file )  )
        echo "Step # 43 :creaing directory";
        mkdir -p {{ $release_number_dir }}/{{ $file }};
    @endif

    chown -R {{ $server_login_user }}:www-data {{ $release_number_dir }}/{{ $file }}

    echo "Permissions has been for file {{ $file }}";
@endforeach
echo "Step # 4 : chmod_settings = Permissions has been set";
@endtask


@task( 'update_symlinks' )
echo "Step # 50 : Symlink set $release_number_dir $release_number_dir::{{ $release_number_dir }}";
echo "Step # 51 : Symlink set $current_dir $current_dir::{{ $current_dir }}";
ln -nfs {{ $release_number_dir }} {{ $current_dir }};
# ln -nfs {{ $release_number_dir }} {{ $current_dir }};

echo "Step # 54 $release_number_dir : {{ $release_number_dir }}";

# chgrp -h www-data {{ $current_dir }};
echo "Step # 55 : Symlink has been set current_dir : {{  $current_dir}}";

@foreach($writableDirs as $file)
    echo "Step # 56 : {{ $release_number_dir }}/{{ $file }}";
    chmod -R 777 {{ $release_number_dir }}/{{ $file }}

    chown -R {{ $server_login_user }}:www-data {{ $release_number_dir }}/{{ $file }}

    echo "Step # 54 Permissions has been for file {{ $file }}";

    echo "Step # 60 app_version ::{{ $app_version }}";
    echo "Step # 61 lardeployer_password ::{{ $lardeployer_password }}";
@endforeach

echo "Step # 620 before envoy:write-app-version ";
cd {{ $release_number_dir }}

# php artisan envoy:write-app-version
# php artisan envoy:write-app-version  "654"

@endtask


@task('clean_old_releases')
# This will list our releases by modification time and delete all but the 5 most recent.
purging=$(ls -dt {{ $release_dir }}/* | tail -n +3);

if [ "$purging" != "" ]; then
echo Purging old releases: $purging;
echo '<br>';
chmod -R 777 $purging
rm -rf $purging;
else
echo "No releases found for purging at this time";
fi
@endtask

@macro('Hostels2Deploy',['on'=>'dev'])
clone_project
composer_installing
artisan_running
chmod_settings
update_symlinks
clean_old_releases
@endmacro
