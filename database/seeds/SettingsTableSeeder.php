<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('settings')->delete();

        \DB::table('settings')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'site_name',
                'value' => 'Hostels 2 demo',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'copyright_text',
                'value' => '© 2018 - 2019 All rights reserved',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'elastic_automation',
                'value' => 'N',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'site_heading',
                'value' => 'Select hostel for your desire !',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'site_subheading',
                'value' => 'Vote\'em all !',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'contact_us_email',
                'value' => 'contact_us@hostels2demo.com',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'contact_us_phone',
            'value' => '(321)-987-654-0321',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'home_page_ref_items_per_pagination',
                'value' => '8',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'backend_per_page',
                'value' => '20',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'news_per_page',
                'value' => '20',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'noreply_email',
                'value' => 'noreply@make_votes.com',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'support_signature',
                'value' => 'Best Regards,<br>    Support of Hostels 2 demo Team',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            12 =>
            array (
                'id' => 13,
                'name' => 'userRegistrationFiles',
                'value' => 'slogan_1.jpg;our-services.doc;rules-of-our-site.pdf;our_prices.ods;terms.doc;',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            13 =>
            array (
                'id' => 14,
                'name' => 'showQuizQualityOptions',
                'value' => '1=Poor;2=Not good;3=So-so;4=Good;5=Excellent',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            14 =>
            array (
                'id' => 15,
                'name' => 'allow_facebook_authorization',
                'value' => 'Y',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            15 =>
            array (
                'id' => 16,
                'name' => 'allow_google_authorization',
                'value' => 'Y',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            16 =>
            array (
                'id' => 17,
                'name' => 'allow_linkedin_authorization',
                'value' => 'Y',
                'created_at' => '2019-04-29 11:03:51',
                'updated_at' => NULL,
            ),
            17 =>
            array (
                'id' => 18,
                'name' => 'allow_github_authorization',
                'value' => 'Y',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            18 =>
            array (
                'id' => 19,
                'name' => 'allow_twitter_authorization',
                'value' => 'Y',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            19 =>
            array (
                'id' => 20,
                'name' => 'allow_instagram_authorization',
                'value' => 'Y',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            20 =>
            array (
                'id' => 21,
                'name' => 'latest_news_on_homepage',
                'value' => '10',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            21 =>
            array (
                'id' => 22,
                'name' => 'infinite_scroll_rows_per_scroll_step',
                'value' => '12',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            22 =>
            array (
                'id' => 23,
                'name' => 'similar_news_on_limit',
                'value' => '15',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            23 =>
            array (
                'id' => 24,
                'name' => 'other_news_on_limit',
                'value' => '20',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            24 =>
            array (
                'id' => 25,
                'name' => 'feed_items_on_limit',
                'value' => '12',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            25 =>
            array (
                'id' => 26,
                'name' => 'feed_import_creator_id',
                'value' => '2',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            26 =>
            array (
                'id' => 27,
                'name' => 'most_rating_quiz_quality_on_homepage',
                'value' => '6',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
            27 =>
            array (
                'id' => 28,
                'name' => 'most_votes_taggable_on_homepage',
                'value' => '8',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),

            29 =>
            array (
                'id' => 30,
                'name' => 'text_color',
                'value' => '#00001D',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),
           30 =>
            array (
                'id' => 31,
                'name' => 'disabled_text_color',
                'value' => '#7e7876',
                'created_at' => '2019-04-29 11:03:52',
                'updated_at' => NULL,
            ),

            31 =>
                array (
                    'id' => 32,
                    'name' => 'hostels_per_page',
                    'value' => '10',
                    'created_at' => '2019-04-29 11:03:51',
                    'updated_at' => NULL,
                ),


            32 =>
                array (
                    'id' => 33,
                    'name' => 'forum_threads_per_page',
                    'value' => '2',
                    'created_at' => '2019-04-29 11:03:51',
                    'updated_at' => NULL,
                ),


            33 =>
                array (
                    'id' => 34,
                    'name' => 'forum_posts_per_page',
                    'value' => '2',
                    'created_at' => '2019-04-29 11:03:51',
                    'updated_at' => NULL,
                ),


            34 =>
                array (
                    'id' => 35,
                    'name' => 'search_results_per_page',
                    'value' => '12',
                    'created_at' => '2019-04-29 11:03:51',
                    'updated_at' => NULL,
                ),




        ));


    }
}
