<?php

use Illuminate\Database\Seeder;

class forumThreadsWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('forum_threads')->delete();

        \DB::table('forum_threads')->insert(array (
            0 =>
                array (
                    'id'                 => 1,
                    'title'              => 'Business & Marketing =>General Business => Thread #1',
                    'slug'               => 'business&-marketing-general-business-thread-1',
                    'creator_id'         => 1,
                    'forum_id'           => 1,
                    'is_salved'   => false,
                    'views'              => 12,
                ),

            1 =>
                array (
                    'id'                 => 2,
                    'title'              => 'Business & Marketing => General Business => Thread #2',
                    'slug'               => 'business-marketing-general-business-thread-2',
                    'creator_id'         => 4,
                    'forum_id'           => 1,
                    'is_salved'          => true,
                    'views'              => 9,
                ),

            2 =>
                array (
                    'id'                 => 3,
                    'title'              => 'Business & Marketing => General Business => Thread #3',
                    'slug'               => 'business-marketing-general-business-thread-3',
                    'creator_id'         => 5,
                    'forum_id'           => 1,
                    'is_salved'   => false,
                    'views'              => 32,
                ),

            3 =>
                array (
                    'id'                 => 4,
                    'title'              => 'Business & Marketing => General Marketing Thread #1',
                    'slug'               => 'business-marketing-general-marketing-thread-1',
                    'creator_id'         => 2,
                    'forum_id'           => 2,
                    'is_salved'   => false,
                    'views'              => 7,
                ),

            4 =>
                array (
                    'id'                 => 5,
                    'title'              => 'Business & Marketing => General Marketing Thread #2',
                    'slug'               => 'business-marketing-general-marketing-thread-2',
                    'creator_id'         => 2,
                    'forum_id'           => 2,
                    'is_salved'   => false,
                    'views'              => 15,
                ),

            5 =>
                array (
                    'id'                 => 6,
                    'title'              => 'Business & Marketing => General Marketing Thread #3',
                    'slug'               => 'business-marketing-general-marketing-thread-3',
                    'creator_id'         => 2,
                    'forum_id'           => 2,
                    'is_salved'   => false,
                    'views'              => 15,
                ),





            6 =>
                array (
                    'id'                 => 7,
                    'title'              => 'Search Engines => Google Thread #1',
                    'slug'               => 'search-engines-Google-thread-1',
                    'creator_id'         => 3,
                    'forum_id'           => 7,  // Google
                    'is_salved'   => false,
                    'views'              => 11,
                ),

            7 =>
                array (
                    'id'                 => 8,
                    'title'              => 'Search Engines => Google Thread #2',
                    'slug'               => 'search-engines-Google-thread-2',
                    'creator_id'         => 5,
                    'forum_id'           => 7,  // Google
                    'is_salved'   => false,
                    'views'              => 8,
                ),





            8 =>
                array (
                    'id'                 => 9,
                    'title'              => 'Search Engines => Yahoo Thread #1',
                    'slug'               => 'search-engines-yahoo-thread-1',
                    'creator_id'         => 3,
                    'forum_id'           => 7,  // Yahoo
                    'is_salved'   => false,
                    'views'              => 11,
                ),
            9 =>
                array (
                    'id'                 => 10,
                    'title'              => 'Search Engines => Yahoo Thread #2',
                    'slug'               => 'search-engines-yahoo-thread-2',
                    'creator_id'         => 1,
                    'forum_id'           => 7,  // Yahoo
                    'is_salved'   => false,
                    'views'              => 5,
                ),
            10 =>
                array (
                    'id'                 => 11,
                    'title'              => 'Search Engines => Yahoo Thread #3',
                    'slug'               => 'search-engines-yahoo-thread-3',
                    'creator_id'         => 4,
                    'forum_id'           => 7,  // Yahoo
                    'views'              => 8,
                    'is_salved'   => true,
                ),
        ));

    }
}
