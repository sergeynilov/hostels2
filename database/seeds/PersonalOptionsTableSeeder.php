<?php

use Illuminate\Database\Seeder;

class PersonalOptionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('personal_options')->delete();
        
        \DB::table('personal_options')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'name' => 'last_login',
                'value' => '2019-04-12 01:45:14',
                'created_at' => '2019-04-12 01:45:14',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 1,
                'name' => 'access_to_privacy_details',
                'value' => 'Y',
                'created_at' => '2019-04-12 02:51:04',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 1,
                'name' => 'chat_background_color',
                'value' => '#8cfffc',
                'created_at' => '2019-04-12 03:26:21',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 1,
                'name' => 'chat_color',
                'value' => '#1c3433',
                'created_at' => '2019-04-12 03:26:21',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 1,
                'name' => 'show_online_status',
                'value' => 'Y',
                'created_at' => '2019-04-12 03:27:06',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 1,
                'name' => 'skin_name',
                'value' => 'BS4',
                'created_at' => '2019-04-12 03:26:24',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'user_id' => 1,
                'name' => 'chat_last_visit',
                'value' => '2019-04-12 01:49:24',
                'created_at' => '2019-04-12 01:49:24',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 11,
                'user_id' => 5,
                'name' => 'last_login',
                'value' => '2019-04-13 11:50:26',
                'created_at' => '2019-04-13 11:50:26',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 12,
                'user_id' => 5,
                'name' => 'access_to_privacy_details',
                'value' => 'Y',
                'created_at' => '2019-04-12 02:53:34',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 13,
                'user_id' => 5,
                'name' => 'chat_background_color',
                'value' => '#1f1f17',
                'created_at' => '2019-04-12 09:45:41',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 14,
                'user_id' => 5,
                'name' => 'chat_color',
                'value' => '#e7e7b2',
                'created_at' => '2019-04-12 03:22:51',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 15,
                'user_id' => 5,
                'name' => 'show_online_status',
                'value' => 'Y',
                'created_at' => '2019-04-12 03:21:02',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 16,
                'user_id' => 5,
                'name' => 'skin_name',
                'value' => 'BS4',
                'created_at' => '2019-04-12 03:22:32',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 17,
                'user_id' => 5,
                'name' => 'chat_last_visit',
                'value' => '2019-04-13 11:52:31',
                'created_at' => '2019-04-13 11:52:31',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}