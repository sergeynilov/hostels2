<?php

use Illuminate\Database\Seeder;

class HostelRoomsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hostel_rooms')->delete();
        
        \DB::table('hostel_rooms')->insert(array (
            0 => 
            array (
                'id' => 1,
                'hostel_id' => 1,
                'type' => 1,
                'price' => '21.15',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-04-01 03:25:50',
            ),
            1 => 
            array (
                'id' => 2,
                'hostel_id' => 1,
                'type' => 4,
                'price' => '18.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-01 03:25:50',
            ),
            2 => 
            array (
                'id' => 3,
                'hostel_id' => 1,
                'type' => 6,
                'price' => '14.50',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-04-01 03:25:50',
            ),
            3 => 
            array (
                'id' => 4,
                'hostel_id' => 1,
                'type' => 8,
                'price' => '10.80',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-01 03:25:50',
            ),
            4 => 
            array (
                'id' => 5,
                'hostel_id' => 1,
                'type' => 10,
                'price' => '6.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-01 03:25:50',
            ),
            5 => 
            array (
                'id' => 6,
                'hostel_id' => 1,
                'type' => 12,
                'price' => '3.40',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-01 03:25:50',
            ),
            6 => 
            array (
                'id' => 13,
                'hostel_id' => 14,
                'type' => 1,
                'price' => '12.00',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-04-10 23:42:37',
            ),
            7 => 
            array (
                'id' => 14,
                'hostel_id' => 14,
                'type' => 4,
                'price' => '8.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-10 23:42:37',
            ),
            8 => 
            array (
                'id' => 15,
                'hostel_id' => 14,
                'type' => 6,
                'price' => '7.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-10 23:42:37',
            ),
            9 => 
            array (
                'id' => 16,
                'hostel_id' => 14,
                'type' => 8,
                'price' => '4.50',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-04-10 23:42:37',
            ),
            10 => 
            array (
                'id' => 17,
                'hostel_id' => 14,
                'type' => 10,
                'price' => '3.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-10 23:42:37',
            ),
            11 => 
            array (
                'id' => 18,
                'hostel_id' => 14,
                'type' => 12,
                'price' => '2.50',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-04-10 23:42:37',
            ),
            12 => 
            array (
                'id' => 19,
                'hostel_id' => 15,
                'type' => 1,
                'price' => '13.50',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-04-12 01:48:32',
            ),
            13 => 
            array (
                'id' => 20,
                'hostel_id' => 15,
                'type' => 4,
                'price' => '11.80',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-12 01:48:32',
            ),
            14 => 
            array (
                'id' => 21,
                'hostel_id' => 15,
                'type' => 6,
                'price' => '9.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-12 01:48:32',
            ),
            15 => 
            array (
                'id' => 22,
                'hostel_id' => 15,
                'type' => 8,
                'price' => '7.50',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-04-12 01:48:32',
            ),
            16 => 
            array (
                'id' => 23,
                'hostel_id' => 15,
                'type' => 10,
                'price' => '6.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-12 01:48:32',
            ),
            17 => 
            array (
                'id' => 24,
                'hostel_id' => 15,
                'type' => 12,
                'price' => '8.20',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-12 01:48:32',
            ),
            18 => 
            array (
                'id' => 25,
                'hostel_id' => 16,
                'type' => 1,
                'price' => '15.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-04-12 02:03:12',
            ),
            19 => 
            array (
                'id' => 26,
                'hostel_id' => 16,
                'type' => 4,
                'price' => '12.40',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-12 02:03:12',
            ),
            20 => 
            array (
                'id' => 27,
                'hostel_id' => 16,
                'type' => 6,
                'price' => '9.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-12 02:03:12',
            ),
            21 => 
            array (
                'id' => 28,
                'hostel_id' => 16,
                'type' => 8,
                'price' => '6.80',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-04-12 02:03:12',
            ),
            22 => 
            array (
                'id' => 29,
                'hostel_id' => 16,
                'type' => 10,
                'price' => '5.80',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-12 02:03:12',
            ),
            23 => 
            array (
                'id' => 30,
                'hostel_id' => 16,
                'type' => 12,
                'price' => '4.20',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-04-12 02:03:12',
            ),
            24 => 
            array (
                'id' => 37,
                'hostel_id' => 18,
                'type' => 1,
                'price' => '16.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-17 05:52:27',
            ),
            25 => 
            array (
                'id' => 38,
                'hostel_id' => 18,
                'type' => 4,
                'price' => '12.50',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-04-17 05:52:27',
            ),
            26 => 
            array (
                'id' => 39,
                'hostel_id' => 18,
                'type' => 6,
                'price' => '9.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-17 05:52:27',
            ),
            27 => 
            array (
                'id' => 40,
                'hostel_id' => 18,
                'type' => 8,
                'price' => '6.00',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-04-17 05:52:27',
            ),
            28 => 
            array (
                'id' => 41,
                'hostel_id' => 18,
                'type' => 10,
                'price' => '4.80',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-17 05:52:27',
            ),
            29 => 
            array (
                'id' => 42,
                'hostel_id' => 18,
                'type' => 12,
                'price' => '3.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-17 05:52:27',
            ),
            30 => 
            array (
                'id' => 43,
                'hostel_id' => 6,
                'type' => 1,
                'price' => '12.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-04-24 05:51:21',
            ),
            31 => 
            array (
                'id' => 44,
                'hostel_id' => 6,
                'type' => 4,
                'price' => '9.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-24 05:51:21',
            ),
            32 => 
            array (
                'id' => 45,
                'hostel_id' => 6,
                'type' => 6,
                'price' => '8.90',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-24 05:51:21',
            ),
            33 => 
            array (
                'id' => 46,
                'hostel_id' => 6,
                'type' => 8,
                'price' => '7.30',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-04-24 05:51:21',
            ),
            34 => 
            array (
                'id' => 47,
                'hostel_id' => 6,
                'type' => 10,
                'price' => '6.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-24 05:51:21',
            ),
            35 => 
            array (
                'id' => 48,
                'hostel_id' => 6,
                'type' => 12,
                'price' => '4.50',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-04-24 05:51:21',
            ),
            36 => 
            array (
                'id' => 49,
                'hostel_id' => 19,
                'type' => 1,
                'price' => '13.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-29 02:34:40',
            ),
            37 => 
            array (
                'id' => 50,
                'hostel_id' => 19,
                'type' => 4,
                'price' => '11.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-04-29 02:34:40',
            ),
            38 => 
            array (
                'id' => 51,
                'hostel_id' => 19,
                'type' => 6,
                'price' => '9.70',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-29 02:34:40',
            ),
            39 => 
            array (
                'id' => 52,
                'hostel_id' => 19,
                'type' => 8,
                'price' => '8.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-29 02:34:40',
            ),
            40 => 
            array (
                'id' => 53,
                'hostel_id' => 19,
                'type' => 10,
                'price' => '6.00',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-04-29 02:34:40',
            ),
            41 => 
            array (
                'id' => 54,
                'hostel_id' => 19,
                'type' => 12,
                'price' => '4.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-04-29 02:34:40',
            ),
            42 => 
            array (
                'id' => 55,
                'hostel_id' => 9,
                'type' => 1,
                'price' => '12.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-16 06:21:09',
            ),
            43 => 
            array (
                'id' => 56,
                'hostel_id' => 9,
                'type' => 4,
                'price' => '10.60',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-05-16 06:21:09',
            ),
            44 => 
            array (
                'id' => 57,
                'hostel_id' => 9,
                'type' => 6,
                'price' => '8.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-16 06:21:09',
            ),
            45 => 
            array (
                'id' => 58,
                'hostel_id' => 9,
                'type' => 8,
                'price' => '7.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-16 06:21:09',
            ),
            46 => 
            array (
                'id' => 59,
                'hostel_id' => 9,
                'type' => 10,
                'price' => '6.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-05-16 06:21:09',
            ),
            47 => 
            array (
                'id' => 60,
                'hostel_id' => 9,
                'type' => 12,
                'price' => '4.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-16 06:21:09',
            ),
            48 => 
            array (
                'id' => 61,
                'hostel_id' => 11,
                'type' => 1,
                'price' => '12.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-19 02:18:34',
            ),
            49 => 
            array (
                'id' => 62,
                'hostel_id' => 11,
                'type' => 4,
                'price' => '10.00',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-05-19 02:18:34',
            ),
            50 => 
            array (
                'id' => 63,
                'hostel_id' => 11,
                'type' => 6,
                'price' => '8.50',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-05-19 02:18:34',
            ),
            51 => 
            array (
                'id' => 64,
                'hostel_id' => 11,
                'type' => 8,
                'price' => '6.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-19 02:18:34',
            ),
            52 => 
            array (
                'id' => 65,
                'hostel_id' => 11,
                'type' => 10,
                'price' => '4.00',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-05-19 02:18:34',
            ),
            53 => 
            array (
                'id' => 66,
                'hostel_id' => 11,
                'type' => 12,
                'price' => '3.60',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-19 02:18:34',
            ),
            54 => 
            array (
                'id' => 67,
                'hostel_id' => 20,
                'type' => 1,
                'price' => '14.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-24 05:51:12',
            ),
            55 => 
            array (
                'id' => 68,
                'hostel_id' => 20,
                'type' => 4,
                'price' => '11.20',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-05-24 05:51:12',
            ),
            56 => 
            array (
                'id' => 69,
                'hostel_id' => 20,
                'type' => 6,
                'price' => '9.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-24 05:51:12',
            ),
            57 => 
            array (
                'id' => 70,
                'hostel_id' => 20,
                'type' => 8,
                'price' => '7.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-24 05:51:12',
            ),
            58 => 
            array (
                'id' => 71,
                'hostel_id' => 20,
                'type' => 10,
                'price' => '5.60',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-24 05:51:12',
            ),
            59 => 
            array (
                'id' => 72,
                'hostel_id' => 20,
                'type' => 12,
                'price' => '5.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-05-24 05:51:12',
            ),
            60 => 
            array (
                'id' => 73,
                'hostel_id' => 21,
                'type' => 1,
                'price' => '21.00',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-06-12 17:42:55',
            ),
            61 => 
            array (
                'id' => 74,
                'hostel_id' => 21,
                'type' => 4,
                'price' => '17.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-12 17:42:55',
            ),
            62 => 
            array (
                'id' => 75,
                'hostel_id' => 21,
                'type' => 6,
                'price' => '15.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-12 17:42:55',
            ),
            63 => 
            array (
                'id' => 76,
                'hostel_id' => 21,
                'type' => 8,
                'price' => '12.50',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-06-12 17:42:55',
            ),
            64 => 
            array (
                'id' => 77,
                'hostel_id' => 21,
                'type' => 10,
                'price' => '10.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-12 17:42:55',
            ),
            65 => 
            array (
                'id' => 78,
                'hostel_id' => 21,
                'type' => 12,
                'price' => '12.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-12 17:42:55',
            ),
            66 => 
            array (
                'id' => 79,
                'hostel_id' => 22,
                'type' => 1,
                'price' => '11.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 05:21:53',
            ),
            67 => 
            array (
                'id' => 80,
                'hostel_id' => 22,
                'type' => 4,
                'price' => '9.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 05:21:53',
            ),
            68 => 
            array (
                'id' => 81,
                'hostel_id' => 22,
                'type' => 6,
                'price' => '7.50',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-06-19 05:21:53',
            ),
            69 => 
            array (
                'id' => 82,
                'hostel_id' => 22,
                'type' => 8,
                'price' => '7.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 05:21:53',
            ),
            70 => 
            array (
                'id' => 83,
                'hostel_id' => 22,
                'type' => 10,
                'price' => '6.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 05:21:53',
            ),
            71 => 
            array (
                'id' => 84,
                'hostel_id' => 22,
                'type' => 12,
                'price' => '5.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 05:21:53',
            ),
            72 => 
            array (
                'id' => 85,
                'hostel_id' => 23,
                'type' => 1,
                'price' => '14.00',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-06-19 18:20:54',
            ),
            73 => 
            array (
                'id' => 86,
                'hostel_id' => 23,
                'type' => 4,
                'price' => '12.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 18:20:54',
            ),
            74 => 
            array (
                'id' => 87,
                'hostel_id' => 23,
                'type' => 6,
                'price' => '11.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 18:20:54',
            ),
            75 => 
            array (
                'id' => 88,
                'hostel_id' => 23,
                'type' => 8,
                'price' => '9.40',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-06-19 18:20:54',
            ),
            76 => 
            array (
                'id' => 89,
                'hostel_id' => 23,
                'type' => 10,
                'price' => '7.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 18:20:54',
            ),
            77 => 
            array (
                'id' => 90,
                'hostel_id' => 23,
                'type' => 12,
                'price' => '5.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-06-19 18:20:54',
            ),
            78 => 
            array (
                'id' => 91,
                'hostel_id' => 24,
                'type' => 1,
                'price' => '12.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-18 06:47:45',
            ),
            79 => 
            array (
                'id' => 92,
                'hostel_id' => 24,
                'type' => 4,
                'price' => '8.70',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-07-18 06:47:45',
            ),
            80 => 
            array (
                'id' => 93,
                'hostel_id' => 24,
                'type' => 6,
                'price' => '7.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-18 06:47:45',
            ),
            81 => 
            array (
                'id' => 94,
                'hostel_id' => 26,
                'type' => 1,
                'price' => '6.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-18 23:56:24',
            ),
            82 => 
            array (
                'id' => 95,
                'hostel_id' => 26,
                'type' => 4,
                'price' => '5.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-18 23:56:24',
            ),
            83 => 
            array (
                'id' => 96,
                'hostel_id' => 27,
                'type' => 1,
                'price' => '12.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-07-19 00:14:20',
            ),
            84 => 
            array (
                'id' => 97,
                'hostel_id' => 27,
                'type' => 4,
                'price' => '10.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-19 00:14:20',
            ),
            85 => 
            array (
                'id' => 98,
                'hostel_id' => 28,
                'type' => 1,
                'price' => '17.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-22 07:06:27',
            ),
            86 => 
            array (
                'id' => 99,
                'hostel_id' => 28,
                'type' => 4,
                'price' => '8.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-07-22 07:06:27',
            ),
            87 => 
            array (
                'id' => 100,
                'hostel_id' => 28,
                'type' => 6,
                'price' => '7.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-22 07:06:27',
            ),
            88 => 
            array (
                'id' => 101,
                'hostel_id' => 29,
                'type' => 1,
                'price' => '11.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-23 04:54:26',
            ),
            89 => 
            array (
                'id' => 102,
                'hostel_id' => 29,
                'type' => 4,
                'price' => '8.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-07-23 04:54:26',
            ),
            90 => 
            array (
                'id' => 103,
                'hostel_id' => 29,
                'type' => 6,
                'price' => '6.50',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-07-23 04:54:26',
            ),
            91 => 
            array (
                'id' => 104,
                'hostel_id' => 29,
                'type' => 8,
                'price' => '5.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-23 05:04:53',
            ),
            92 => 
            array (
                'id' => 105,
                'hostel_id' => 29,
                'type' => 10,
                'price' => '4.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-07-23 05:04:53',
            ),
            93 => 
            array (
                'id' => 106,
                'hostel_id' => 29,
                'type' => 12,
                'price' => '3.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-23 05:04:53',
            ),
            94 => 
            array (
                'id' => 107,
                'hostel_id' => 30,
                'type' => 1,
                'price' => '12.90',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-07-24 01:05:12',
            ),
            95 => 
            array (
                'id' => 108,
                'hostel_id' => 30,
                'type' => 4,
                'price' => '10.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-24 01:05:12',
            ),
            96 => 
            array (
                'id' => 109,
                'hostel_id' => 30,
                'type' => 6,
                'price' => '9.25',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-07-24 01:05:12',
            ),
            97 => 
            array (
                'id' => 110,
                'hostel_id' => 30,
                'type' => 8,
                'price' => '7.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-07-24 01:14:14',
            ),
            98 => 
            array (
                'id' => 111,
                'hostel_id' => 30,
                'type' => 10,
                'price' => '5.50',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-07-24 01:14:14',
            ),
            99 => 
            array (
                'id' => 112,
                'hostel_id' => 30,
                'type' => 12,
                'price' => '4.00',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-07-24 01:14:14',
            ),
            100 => 
            array (
                'id' => 113,
                'hostel_id' => 10,
                'type' => 1,
                'price' => '14.00',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-07-25 00:07:43',
            ),
            101 => 
            array (
                'id' => 114,
                'hostel_id' => 10,
                'type' => 4,
                'price' => '12.50',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-07-25 00:07:43',
            ),
            102 => 
            array (
                'id' => 115,
                'hostel_id' => 10,
                'type' => 6,
                'price' => '8.70',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-25 00:07:43',
            ),
            103 => 
            array (
                'id' => 116,
                'hostel_id' => 10,
                'type' => 8,
                'price' => '5.40',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-25 00:07:43',
            ),
            104 => 
            array (
                'id' => 117,
                'hostel_id' => 10,
                'type' => 10,
                'price' => '4.00',
                'is_dorm' => 'N',
                'is_private' => 'Y',
                'created_at' => '2013-07-25 00:07:43',
            ),
            105 => 
            array (
                'id' => 118,
                'hostel_id' => 10,
                'type' => 12,
                'price' => '3.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-25 00:07:43',
            ),
            106 => 
            array (
                'id' => 119,
                'hostel_id' => 31,
                'type' => 1,
                'price' => '8.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-07-26 01:25:20',
            ),
            107 => 
            array (
                'id' => 120,
                'hostel_id' => 31,
                'type' => 4,
                'price' => '7.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-07-26 01:25:20',
            ),
            108 => 
            array (
                'id' => 121,
                'hostel_id' => 32,
                'type' => 1,
                'price' => '14.80',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-08-27 21:53:30',
            ),
            109 => 
            array (
                'id' => 122,
                'hostel_id' => 32,
                'type' => 4,
                'price' => '8.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-08-27 21:53:30',
            ),
            110 => 
            array (
                'id' => 123,
                'hostel_id' => 33,
                'type' => 1,
                'price' => '5.00',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-08-27 22:01:36',
            ),
            111 => 
            array (
                'id' => 124,
                'hostel_id' => 33,
                'type' => 4,
                'price' => '3.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-08-27 22:01:36',
            ),
            112 => 
            array (
                'id' => 125,
                'hostel_id' => 33,
                'type' => 6,
                'price' => '2.50',
                'is_dorm' => 'Y',
                'is_private' => 'N',
                'created_at' => '2013-08-28 02:17:00',
            ),
            113 => 
            array (
                'id' => 126,
                'hostel_id' => 33,
                'type' => 8,
                'price' => '2.20',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-08-28 02:17:00',
            ),
            114 => 
            array (
                'id' => 127,
                'hostel_id' => 33,
                'type' => 10,
                'price' => '1.50',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-08-28 02:17:00',
            ),
            115 => 
            array (
                'id' => 128,
                'hostel_id' => 33,
                'type' => 12,
                'price' => '1.20',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-08-28 02:17:00',
            ),
            116 => 
            array (
                'id' => 147,
                'hostel_id' => 41,
                'type' => 1,
                'price' => '14.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-09-13 09:55:38',
            ),
            117 => 
            array (
                'id' => 148,
                'hostel_id' => 41,
                'type' => 4,
                'price' => '12.20',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-09-13 09:55:38',
            ),
            118 => 
            array (
                'id' => 151,
                'hostel_id' => 43,
                'type' => 1,
                'price' => '12.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-09-25 11:53:38',
            ),
            119 => 
            array (
                'id' => 152,
                'hostel_id' => 43,
                'type' => 4,
                'price' => '9.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-09-25 11:53:38',
            ),
            120 => 
            array (
                'id' => 153,
                'hostel_id' => 44,
                'type' => 1,
                'price' => '7.00',
                'is_dorm' => 'Y',
                'is_private' => 'Y',
                'created_at' => '2013-09-29 15:26:42',
            ),
            121 => 
            array (
                'id' => 154,
                'hostel_id' => 44,
                'type' => 4,
                'price' => '5.50',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-09-29 15:26:42',
            ),
            122 => 
            array (
                'id' => 155,
                'hostel_id' => 44,
                'type' => 6,
                'price' => '4.00',
                'is_dorm' => 'N',
                'is_private' => 'N',
                'created_at' => '2013-09-29 15:26:42',
            ),



            123 =>
                array (
                    'id' => 156,
                    'hostel_id' => 2,
                    'type' => 1,
                    'price' => '11.78',
                    'is_dorm' => 'Y',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            124 =>
                array (
                    'id' => 157,
                    'hostel_id' => 2,
                    'type' => 4,
                    'price' => '9.45',
                    'is_dorm' => 'Y',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),

            125 =>
                array (
                    'id' => 158,
                    'hostel_id' => 2,
                    'type' => 6,
                    'price' => '8.15',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            126 =>
                array (
                    'id' => 159,
                    'hostel_id' => 2,
                    'type' => 8,
                    'price' => '7.65',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            127 =>
                array (
                    'id' => 160,
                    'hostel_id' => 2,
                    'type' => 10,
                    'price' => '6.85',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            128 =>
                array (
                    'id' => 161,
                    'hostel_id' => 2,
                    'type' => 12,
                    'price' => '5.50',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),






            129 =>
                array (
                    'id' => 162,
                    'hostel_id' => 3,
                    'type' => 1,
                    'price' => '11.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            130 =>
                array (
                    'id' => 163,
                    'hostel_id' => 3,
                    'type' => 4,
                    'price' => '9.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            131 =>
                array (
                    'id' => 164,
                    'hostel_id' => 3,
                    'type' => 6,
                    'price' => '7.50',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            132 =>
                array (
                    'id' => 165,
                    'hostel_id' => 3,
                    'type' => 8,
                    'price' => '7.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            133 =>
                array (
                    'id' => 166,
                    'hostel_id' => 3,
                    'type' => 10,
                    'price' => '6.50',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            134 =>
                array (
                    'id' => 167,
                    'hostel_id' => 3,
                    'type' => 12,
                    'price' => '5.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),






            135 =>
                array (
                    'id' => 168,
                    'hostel_id' => 4,
                    'type' => 1,
                    'price' => '13.70',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            136 =>
                array (
                    'id' => 169,
                    'hostel_id' => 4,
                    'type' => 4,
                    'price' => '9.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            137 =>
                array (
                    'id' => 170,
                    'hostel_id' => 4,
                    'type' => 6,
                    'price' => '7.85',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            138 =>
                array (
                    'id' => 171,
                    'hostel_id' => 4,
                    'type' => 8,
                    'price' => '7.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            139 =>
                array (
                    'id' => 172,
                    'hostel_id' => 4,
                    'type' => 10,
                    'price' => '6.20',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            140 =>
                array (
                    'id' => 173,
                    'hostel_id' => 4,
                    'type' => 12,
                    'price' => '3.50',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),







            141 =>
                array (
                    'id' => 174,
                    'hostel_id' => 7,
                    'type' => 1,
                    'price' => '14.60',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            142 =>
                array (
                    'id' => 175,
                    'hostel_id' => 7,
                    'type' => 4,
                    'price' => '12.20',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            143 =>
                array (
                    'id' => 176,
                    'hostel_id' => 7,
                    'type' => 6,
                    'price' => '8.50',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            144 =>
                array (
                    'id' => 177,
                    'hostel_id' => 7,
                    'type' => 8,
                    'price' => '7.20',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            145 =>
                array (
                    'id' => 178,
                    'hostel_id' => 7,
                    'type' => 10,
                    'price' => '6.50',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            146 =>
                array (
                    'id' => 179,
                    'hostel_id' => 7,
                    'type' => 12,
                    'price' => '5.20',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),






            147 =>
                array (
                    'id' => 180,
                    'hostel_id' => 12,
                    'type' => 1,
                    'price' => '11.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            148 =>
                array (
                    'id' => 181,
                    'hostel_id' => 12,
                    'type' => 4,
                    'price' => '9.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            149 =>
                array (
                    'id' => 182,
                    'hostel_id' => 12,
                    'type' => 6,
                    'price' => '7.50',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            150 =>
                array (
                    'id' => 183,
                    'hostel_id' => 12,
                    'type' => 8,
                    'price' => '7.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            151 =>
                array (
                    'id' => 184,
                    'hostel_id' => 12,
                    'type' => 10,
                    'price' => '6.50',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),
            152 =>
                array (
                    'id' => 185,
                    'hostel_id' => 12,
                    'type' => 12,
                    'price' => '5.00',
                    'is_dorm' => 'N',
                    'is_private' => 'N',
                    'created_at' => '2013-06-19 05:21:53',
                ),




        ));
        
        
    }
}