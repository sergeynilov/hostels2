<?php

use Illuminate\Database\Seeder;

class forumPostBookmarkWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        \DB::table('forum_post_bookmark')->delete();

        \DB::table('forum_post_bookmark')->insert(array (
            0 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 7,
                    'info'             => 'Look at it later',
                ),

            1 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 12,
                    'info'             => 'Funny Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt...',
                ),

            2 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 2,
                    'info'             => null,
                ),

            3 =>
                array (
                    'user_id'          => 4,
                    'forum_post_id'    => 11,
                    'info'             => 'Funny...',
                ),

            4 =>
                array (
                    'user_id'          => 6,
                    'forum_post_id'    => 7,
                    'info'             => null,
                ),


        ));

    }
}
