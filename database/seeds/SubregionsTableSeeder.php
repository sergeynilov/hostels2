<?php

use Illuminate\Database\Seeder;

class SubregionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('subregions')->delete();
        
        \DB::table('subregions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Central Coast & Region',
                'slug' => 'central-coast-region',
            ),
            1 => 
            array (
                'id' => 2,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Lake Macquarie West',
                'slug' => 'lake-macquarie-west',
            ),
            2 => 
            array (
                'id' => 3,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Lake Macquarie East',
                'slug' => 'lake-macquarie-east',
            ),
            3 => 
            array (
                'id' => 4,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Newcastle & Region',
                'slug' => 'newcastle-region',
            ),
            4 => 
            array (
                'id' => 5,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Hunter Valley - Lower',
                'slug' => 'hunter-valley-lower',
            ),
            5 => 
            array (
                'id' => 6,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Mid North Coast',
                'slug' => 'mid-north-coast',
            ),
            6 => 
            array (
                'id' => 7,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Northern Rivers',
                'slug' => 'northern-rivers',
            ),
            7 => 
            array (
                'id' => 8,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Hunter Valley - Upper',
                'slug' => 'hunter-valley-upper',
            ),
            8 => 
            array (
                'id' => 9,
                'region_id' => 1,
                'state_id' => 1,
                'name' => 'Port Stephens',
                'slug' => 'port-stephens',
            ),
            9 => 
            array (
                'id' => 10,
                'region_id' => 2,
                'state_id' => 1,
                'name' => 'Wollongong & Illawarra',
                'slug' => 'wollongong-illawarra',
            ),
            10 => 
            array (
                'id' => 11,
                'region_id' => 2,
                'state_id' => 1,
                'name' => 'Southern Highlands',
                'slug' => 'southern-highlands',
            ),
            11 => 
            array (
                'id' => 12,
                'region_id' => 2,
                'state_id' => 1,
                'name' => 'Shoalhaven',
                'slug' => 'shoalhaven',
            ),
            12 => 
            array (
                'id' => 13,
                'region_id' => 2,
                'state_id' => 1,
                'name' => 'Eurobodalla Coast',
                'slug' => 'eurobodalla-coast',
            ),
            13 => 
            array (
                'id' => 14,
                'region_id' => 2,
                'state_id' => 1,
                'name' => 'Far South Coast',
                'slug' => 'far-south-coast',
            ),
            14 => 
            array (
                'id' => 15,
                'region_id' => 3,
                'state_id' => 1,
                'name' => 'Dubbo & Orana',
                'slug' => 'dubbo-orana',
            ),
            15 => 
            array (
                'id' => 16,
                'region_id' => 3,
                'state_id' => 1,
                'name' => 'Central West',
                'slug' => 'central-west',
            ),
            16 => 
            array (
                'id' => 17,
                'region_id' => 3,
                'state_id' => 1,
                'name' => 'Far West',
                'slug' => 'far-west',
            ),
            17 => 
            array (
                'id' => 18,
                'region_id' => 3,
                'state_id' => 1,
                'name' => 'New England - North West',
                'slug' => 'new-england-north-west',
            ),
            18 => 
            array (
                'id' => 19,
                'region_id' => 3,
                'state_id' => 1,
                'name' => 'Riverina',
                'slug' => 'riverina',
            ),
            19 => 
            array (
                'id' => 20,
                'region_id' => 3,
                'state_id' => 1,
                'name' => 'Murray Region',
                'slug' => 'murray-region',
            ),
            20 => 
            array (
                'id' => 21,
                'region_id' => 3,
                'state_id' => 1,
                'name' => 'Capital Monaro & Snowy',
                'slug' => 'capital-monaro-snowy',
            ),
            21 => 
            array (
                'id' => 22,
                'region_id' => 3,
                'state_id' => 1,
                'name' => 'Southern Tablelands',
                'slug' => 'southern-tablelands',
            ),
            22 => 
            array (
                'id' => 23,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'MacArthur/Camden',
                'slug' => 'macarthurcamden',
            ),
            23 => 
            array (
                'id' => 24,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Blue Mountains & Surrounds',
                'slug' => 'blue-mountains-surrounds',
            ),
            24 => 
            array (
                'id' => 25,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Hawkesbury',
                'slug' => 'hawkesbury',
            ),
            25 => 
            array (
                'id' => 26,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Western Sydney',
                'slug' => 'western-sydney',
            ),
            26 => 
            array (
                'id' => 27,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Hills',
                'slug' => 'hills',
            ),
            27 => 
            array (
                'id' => 28,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'St George',
                'slug' => 'st-george',
            ),
            28 => 
            array (
                'id' => 29,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Inner West',
                'slug' => 'inner-west',
            ),
            29 => 
            array (
                'id' => 30,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Canterbury/Bankstown',
                'slug' => 'canterburybankstown',
            ),
            30 => 
            array (
                'id' => 31,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Liverpool / Fairfield',
                'slug' => 'liverpool-fairfield',
            ),
            31 => 
            array (
                'id' => 32,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Sutherland',
                'slug' => 'sutherland',
            ),
            32 => 
            array (
                'id' => 33,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Northern Beaches',
                'slug' => 'northern-beaches',
            ),
            33 => 
            array (
                'id' => 34,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'North Shore - Lower',
                'slug' => 'north-shore-lower',
            ),
            34 => 
            array (
                'id' => 35,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Parramatta',
                'slug' => 'parramatta',
            ),
            35 => 
            array (
                'id' => 36,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Northern Suburbs',
                'slug' => 'northern-suburbs',
            ),
            36 => 
            array (
                'id' => 37,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'North Shore - Upper',
                'slug' => 'north-shore-upper',
            ),
            37 => 
            array (
                'id' => 38,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Eastern Suburbs',
                'slug' => 'eastern-suburbs',
            ),
            38 => 
            array (
                'id' => 39,
                'region_id' => 4,
                'state_id' => 1,
                'name' => 'Sydney City',
                'slug' => 'sydney-city',
            ),
            39 => 
            array (
                'id' => 40,
                'region_id' => 5,
                'state_id' => 2,
                'name' => 'Darwin Area',
                'slug' => 'darwin-area',
            ),
            40 => 
            array (
                'id' => 41,
                'region_id' => 5,
                'state_id' => 2,
                'name' => 'Alice Springs',
                'slug' => 'alice-springs',
            ),
            41 => 
            array (
                'id' => 42,
                'region_id' => 6,
                'state_id' => 3,
                'name' => 'Bayside',
                'slug' => 'bayside',
            ),
            42 => 
            array (
                'id' => 43,
                'region_id' => 6,
                'state_id' => 3,
                'name' => 'Logan',
                'slug' => 'logan',
            ),
            43 => 
            array (
                'id' => 44,
                'region_id' => 6,
                'state_id' => 3,
                'name' => 'Southside',
                'slug' => 'southside',
            ),
            44 => 
            array (
                'id' => 45,
                'region_id' => 6,
                'state_id' => 3,
                'name' => 'Inner South',
                'slug' => 'inner-south',
            ),
            45 => 
            array (
                'id' => 46,
                'region_id' => 6,
                'state_id' => 3,
                'name' => 'Redlands City',
                'slug' => 'redlands-city',
            ),
            46 => 
            array (
                'id' => 47,
                'region_id' => 6,
                'state_id' => 3,
                'name' => 'City & North',
                'slug' => 'city-north',
            ),
            47 => 
            array (
                'id' => 48,
                'region_id' => 6,
                'state_id' => 3,
                'name' => 'Redcliffe / Bribie / Caboolture',
                'slug' => 'redcliffe-bribie-caboolture',
            ),
            48 => 
            array (
                'id' => 49,
                'region_id' => 6,
                'state_id' => 3,
                'name' => 'West',
                'slug' => 'west',
            ),
            49 => 
            array (
                'id' => 50,
                'region_id' => 7,
                'state_id' => 3,
                'name' => 'Central',
                'slug' => 'central',
            ),
            50 => 
            array (
                'id' => 51,
                'region_id' => 7,
                'state_id' => 3,
                'name' => 'South',
                'slug' => 'south',
            ),
            51 => 
            array (
                'id' => 52,
                'region_id' => 7,
                'state_id' => 3,
                'name' => 'Mount Isa & North West',
                'slug' => 'mount-isa-north-west',
            ),
            52 => 
            array (
                'id' => 53,
                'region_id' => 8,
                'state_id' => 3,
                'name' => 'Far North Queensland',
                'slug' => 'far-north-queensland',
            ),
            53 => 
            array (
                'id' => 54,
                'region_id' => 8,
                'state_id' => 3,
                'name' => 'Townsville & District',
                'slug' => 'townsville-district',
            ),
            54 => 
            array (
                'id' => 55,
                'region_id' => 8,
                'state_id' => 3,
                'name' => 'Cairns & District',
                'slug' => 'cairns-district',
            ),
            55 => 
            array (
                'id' => 56,
                'region_id' => 8,
                'state_id' => 3,
                'name' => 'Mackay & Whitsundays',
                'slug' => 'mackay-whitsundays',
            ),
            56 => 
            array (
                'id' => 57,
                'region_id' => 8,
                'state_id' => 3,
                'name' => 'Gladstone Rockhampton & Capricornia',
                'slug' => 'gladstone-rockhampton-capricornia',
            ),
            57 => 
            array (
                'id' => 58,
                'region_id' => 8,
                'state_id' => 3,
                'name' => 'Bundaberg & Wide Bay',
                'slug' => 'bundaberg-wide-bay',
            ),
            58 => 
            array (
                'id' => 59,
                'region_id' => 9,
                'state_id' => 3,
                'name' => 'Gold Coast',
                'slug' => 'gold-coast',
            ),
            59 => 
            array (
                'id' => 60,
                'region_id' => 9,
                'state_id' => 3,
                'name' => 'Toowoomba & District',
                'slug' => 'toowoomba-district',
            ),
            60 => 
            array (
                'id' => 61,
                'region_id' => 9,
                'state_id' => 3,
                'name' => 'Ipswich & West Moreton',
                'slug' => 'ipswich-west-moreton',
            ),
            61 => 
            array (
                'id' => 62,
                'region_id' => 9,
                'state_id' => 3,
                'name' => 'Sunshine Coast',
                'slug' => 'sunshine-coast',
            ),
            62 => 
            array (
                'id' => 63,
                'region_id' => 10,
                'state_id' => 4,
                'name' => 'North & North East Suburbs',
                'slug' => 'north-north-east-suburbs',
            ),
            63 => 
            array (
                'id' => 64,
                'region_id' => 10,
                'state_id' => 4,
                'name' => 'Eastern Suburbs',
                'slug' => 'south-australia-eastern-suburbs',
            ),
            64 => 
            array (
                'id' => 65,
                'region_id' => 10,
                'state_id' => 4,
                'name' => 'Adelaide Hills',
                'slug' => 'adelaide-hills',
            ),
            65 => 
            array (
                'id' => 66,
                'region_id' => 10,
                'state_id' => 4,
                'name' => 'South & South East Suburbs',
                'slug' => 'south-south-east-suburbs',
            ),
            66 => 
            array (
                'id' => 67,
                'region_id' => 10,
                'state_id' => 4,
                'name' => 'Western & Beachside Suburbs',
                'slug' => 'western-beachside-suburbs',
            ),
            67 => 
            array (
                'id' => 68,
                'region_id' => 10,
                'state_id' => 4,
                'name' => 'Adelaide',
                'slug' => 'adelaide-1',
            ),
            68 => 
            array (
                'id' => 69,
                'region_id' => 11,
                'state_id' => 4,
                'name' => 'Far North',
                'slug' => 'far-north-1',
            ),
            69 => 
            array (
                'id' => 70,
                'region_id' => 12,
                'state_id' => 4,
                'name' => 'Mid North',
                'slug' => 'mid-north-1',
            ),
            70 => 
            array (
                'id' => 71,
                'region_id' => 12,
                'state_id' => 4,
                'name' => 'Clare Valley',
                'slug' => 'clare-valley',
            ),
            71 => 
            array (
                'id' => 72,
                'region_id' => 12,
                'state_id' => 4,
                'name' => 'Barossa Valley',
                'slug' => 'barossa-valley',
            ),
            72 => 
            array (
                'id' => 73,
                'region_id' => 12,
                'state_id' => 4,
                'name' => 'Riverland',
                'slug' => 'riverland',
            ),
            73 => 
            array (
                'id' => 74,
                'region_id' => 13,
                'state_id' => 4,
                'name' => 'Murraylands',
                'slug' => 'murraylands',
            ),
            74 => 
            array (
                'id' => 75,
                'region_id' => 13,
                'state_id' => 4,
                'name' => 'South East',
                'slug' => 'south-east-1',
            ),
            75 => 
            array (
                'id' => 76,
                'region_id' => 13,
                'state_id' => 4,
                'name' => 'Fleurieu Peninsula',
                'slug' => 'fleurieu-peninsula',
            ),
            76 => 
            array (
                'id' => 77,
                'region_id' => 13,
                'state_id' => 4,
                'name' => 'Kangaroo Island',
                'slug' => 'kangaroo-island',
            ),
            77 => 
            array (
                'id' => 78,
                'region_id' => 14,
                'state_id' => 4,
                'name' => 'Eyre Peninsula',
                'slug' => 'eyre-peninsula',
            ),
            78 => 
            array (
                'id' => 79,
                'region_id' => 14,
                'state_id' => 4,
                'name' => 'West Coast',
                'slug' => 'west-coast',
            ),
            79 => 
            array (
                'id' => 80,
                'region_id' => 14,
                'state_id' => 4,
                'name' => 'Yorke Peninsula',
                'slug' => 'yorke-peninsula',
            ),
            80 => 
            array (
                'id' => 81,
                'region_id' => 15,
                'state_id' => 5,
                'name' => 'Hobart & Southern',
                'slug' => 'hobart-southern',
            ),
            81 => 
            array (
                'id' => 82,
                'region_id' => 15,
                'state_id' => 5,
                'name' => 'East Coast',
                'slug' => 'east-coast',
            ),
            82 => 
            array (
                'id' => 83,
                'region_id' => 15,
                'state_id' => 5,
                'name' => 'Launceston & Northern',
                'slug' => 'launceston-northern',
            ),
            83 => 
            array (
                'id' => 84,
                'region_id' => 15,
                'state_id' => 5,
                'name' => 'West Coast',
                'slug' => 'tasmania-west-coast',
            ),
            84 => 
            array (
                'id' => 85,
                'region_id' => 15,
                'state_id' => 5,
                'name' => 'North East Tasmania',
                'slug' => 'north-east-tasmania',
            ),
            85 => 
            array (
                'id' => 86,
                'region_id' => 15,
                'state_id' => 5,
                'name' => 'Burnie Devonport & NW Tas',
                'slug' => 'burnie-devonport-nw-tas',
            ),
            86 => 
            array (
                'id' => 87,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'Yarra-dandenong Ranges',
                'slug' => 'yarra-dandenong-ranges',
            ),
            87 => 
            array (
                'id' => 88,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'East',
                'slug' => 'east',
            ),
            88 => 
            array (
                'id' => 89,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'Inner City',
                'slug' => 'inner-city',
            ),
            89 => 
            array (
                'id' => 90,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'Bayside',
                'slug' => 'victoria-bayside',
            ),
            90 => 
            array (
                'id' => 91,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'South East',
                'slug' => 'victoria-south-east-1',
            ),
            91 => 
            array (
                'id' => 92,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'North East',
                'slug' => 'north-east',
            ),
            92 => 
            array (
                'id' => 93,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'Geelong & District',
                'slug' => 'geelong-district',
            ),
            93 => 
            array (
                'id' => 94,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'West',
                'slug' => 'victoria-west',
            ),
            94 => 
            array (
                'id' => 95,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'North',
                'slug' => 'north',
            ),
            95 => 
            array (
                'id' => 96,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'North West',
                'slug' => 'north-west',
            ),
            96 => 
            array (
                'id' => 97,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'Phillip Island & District',
                'slug' => 'phillip-island-district',
            ),
            97 => 
            array (
                'id' => 98,
                'region_id' => 16,
                'state_id' => 6,
                'name' => 'Mornington Peninsula',
                'slug' => 'mornington-peninsula',
            ),
            98 => 
            array (
                'id' => 99,
                'region_id' => 17,
                'state_id' => 6,
                'name' => 'North Eastern',
                'slug' => 'north-eastern',
            ),
            99 => 
            array (
                'id' => 100,
                'region_id' => 17,
                'state_id' => 6,
                'name' => 'Greater Bendigo Region',
                'slug' => 'greater-bendigo-region',
            ),
            100 => 
            array (
                'id' => 101,
                'region_id' => 17,
                'state_id' => 6,
                'name' => 'North Western',
                'slug' => 'north-western',
            ),
            101 => 
            array (
                'id' => 102,
                'region_id' => 17,
                'state_id' => 6,
                'name' => 'Shepparton & Central North',
                'slug' => 'shepparton-central-north',
            ),
            102 => 
            array (
                'id' => 103,
                'region_id' => 17,
                'state_id' => 6,
                'name' => 'Macedon Ranges',
                'slug' => 'macedon-ranges',
            ),
            103 => 
            array (
                'id' => 104,
                'region_id' => 18,
                'state_id' => 6,
                'name' => 'Gippsland',
                'slug' => 'gippsland',
            ),
            104 => 
            array (
                'id' => 105,
                'region_id' => 18,
                'state_id' => 6,
                'name' => 'Lakes & Wilderness',
                'slug' => 'lakes-wilderness',
            ),
            105 => 
            array (
                'id' => 106,
                'region_id' => 19,
                'state_id' => 6,
                'name' => 'Great Ocean Rd / Otway Ranges',
                'slug' => 'great-ocean-rd-otway-ranges',
            ),
            106 => 
            array (
                'id' => 107,
                'region_id' => 19,
                'state_id' => 6,
                'name' => 'Grampians',
                'slug' => 'grampians',
            ),
            107 => 
            array (
                'id' => 108,
                'region_id' => 19,
                'state_id' => 6,
                'name' => 'Warrnambool & Port Fairy',
                'slug' => 'warrnambool-port-fairy',
            ),
            108 => 
            array (
                'id' => 109,
                'region_id' => 19,
                'state_id' => 6,
                'name' => 'Ballarat & Western District',
                'slug' => 'ballarat-western-district',
            ),
            109 => 
            array (
                'id' => 110,
                'region_id' => 20,
                'state_id' => 7,
                'name' => 'East',
                'slug' => 'western_australia-east',
            ),
            110 => 
            array (
                'id' => 111,
                'region_id' => 21,
                'state_id' => 7,
                'name' => 'North',
                'slug' => 'western_australia-north',
            ),
            111 => 
            array (
                'id' => 112,
                'region_id' => 21,
                'state_id' => 7,
                'name' => 'Northern Suburbs',
                'slug' => 'western_australia-northern-suburbs',
            ),
            112 => 
            array (
                'id' => 113,
                'region_id' => 22,
                'state_id' => 7,
                'name' => 'Southern Suburbs',
                'slug' => 'southern-suburbs',
            ),
            113 => 
            array (
                'id' => 114,
                'region_id' => 22,
                'state_id' => 7,
                'name' => 'Eastern Suburbs',
                'slug' => 'western_australia-eastern-suburbs',
            ),
            114 => 
            array (
                'id' => 115,
                'region_id' => 22,
                'state_id' => 7,
                'name' => 'Perth Hills',
                'slug' => 'perth-hills',
            ),
            115 => 
            array (
                'id' => 116,
                'region_id' => 22,
                'state_id' => 7,
                'name' => 'South',
                'slug' => 'south-1',
            ),
            116 => 
            array (
                'id' => 117,
                'region_id' => 22,
                'state_id' => 7,
                'name' => 'Western Suburbs',
                'slug' => 'western-suburbs',
            ),
            117 => 
            array (
                'id' => 118,
                'region_id' => 22,
                'state_id' => 7,
                'name' => 'Perth City',
                'slug' => 'perth-city',
            ),
            118 => 
            array (
                'id' => 119,
                'region_id' => 23,
                'state_id' => 7,
                'name' => 'Mandurah & surrounds',
                'slug' => 'mandurah-surrounds',
            ),
            119 => 
            array (
                'id' => 120,
                'region_id' => 23,
                'state_id' => 7,
                'name' => 'Bunbury & surrounds',
                'slug' => 'bunbury-surrounds',
            ),
            120 => 
            array (
                'id' => 121,
                'region_id' => 23,
                'state_id' => 7,
                'name' => 'Busselton-Dunsborough & Surrounds',
                'slug' => 'busselton-dunsborough-surrounds',
            ),
            121 => 
            array (
                'id' => 122,
                'region_id' => 23,
                'state_id' => 7,
                'name' => 'Margaret River & Surrounds',
                'slug' => 'margaret-river-surrounds',
            ),
            122 => 
            array (
                'id' => 123,
                'region_id' => 24,
                'state_id' => 8,
                'name' => 'Belconnen',
                'slug' => 'belconnen',
            ),
            123 => 
            array (
                'id' => 124,
                'region_id' => 24,
                'state_id' => 8,
                'name' => 'Tuggeranong',
                'slug' => 'tuggeranong',
            ),
            124 => 
            array (
                'id' => 125,
                'region_id' => 24,
                'state_id' => 8,
                'name' => 'Inner North',
                'slug' => 'inner-north',
            ),
            125 => 
            array (
                'id' => 126,
                'region_id' => 24,
                'state_id' => 8,
                'name' => 'Gungahlin',
                'slug' => 'gungahlin',
            ),
            126 => 
            array (
                'id' => 127,
                'region_id' => 24,
                'state_id' => 8,
                'name' => 'Inner South',
                'slug' => 'australian-capital-territory-inner-south',
            ),
            127 => 
            array (
                'id' => 128,
                'region_id' => 24,
                'state_id' => 8,
                'name' => 'Woden Valley',
                'slug' => 'woden-valley',
            ),
            128 => 
            array (
                'id' => 129,
                'region_id' => 24,
                'state_id' => 8,
                'name' => 'Weston Creek',
                'slug' => 'weston-creek',
            ),
        ));
        
        
    }
}