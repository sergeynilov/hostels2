<?php

use Illuminate\Database\Seeder;

class forumPostWatchedByUserWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('forum_posts_watched_by_user')->delete();

        \DB::table('forum_posts_watched_by_user')->insert(array (
            0 =>
                array (
                    'user_id'          => 1,
                    'forum_post_id'    => 7,
                ),

            1 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 7,
                ),

            2 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 7,
                ),

            3 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 8,
                ),

            4 =>
                array (
                    'user_id'          => 3,
                    'forum_post_id'    => 8,
                ),

        ));

    }
}
