<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class roleUsersWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $admin = new Role();
//        $admin->name         = 'admin';
//        $admin->display_name = 'User Administrator'; // optional
//        $admin->description  = 'User is allowed to manage and edit other users'; // optional
//        $admin->save();

        return;
        for( $i= 1; $i< 10; $i++ ) {
            $user= User::find($i);
            if($i== 1 or $i== 5) {
                if ($user) { // Shawn Hadray and admin have admin's role
                    $user->attachRole(ACCESS_ROLE_ADMIN);
                }
            }

            if($i== 2 or $i== 9) {
                if ($user) { // Pat Longred and Martha Lang have manager's role
                    $user->attachRole(ACCESS_ROLE_MANAGER);
//                    $user->
                }
            }

            if ($user) { // All users have customer's role
                $user->attachRole(ACCESS_ROLE_CUSTOMER);
            }

/*            'name' => ACCESS_ROLE_ADMIN,
                    'name' => ACCESS_ROLE_MANAGER,
                    'name' => ACCESS_ROLE_CUSTOMER,  */

        }
    }
}
