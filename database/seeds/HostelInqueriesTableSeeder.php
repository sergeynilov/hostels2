<?php

use Illuminate\Database\Seeder;

class HostelInqueriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('hostel_inqueries')->delete();

        \DB::table('hostel_inqueries')->insert(array (
            0 =>
            array (
                'id' => 1,
                'hostel_id' => 1,
                'creator_id' => 5,
                'email' => 'Tester@company.com',
                'full_name' => 'Sergey Tester',
                'phone' => '123456',
                'info' => '1111111
qqqqqq
wwwwww',
                'start_date' => '2013-04-18',
                'end_date' => '2013-04-22',
                'request_callback' => 'Y',
                'status' => 'C',
                'created_at' => '2013-03-18 14:23:26',
            ),
            1 =>
            array (
                'id' => 4,
                'hostel_id' => 1,
                'creator_id' => 2,
                'email' => 'tester@company.com',
                'full_name' => 'Sergey Tester',
                'phone' => '123456',
                'info' => 'Some extra info 222 Lorem  ipsum dolor sit amet, consectetur 
adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna 
aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco 
laboris nisi ut aliquip ex ea  commodo consequat. Duis aute 
irure dolor in rep
',
                'start_date' => '2013-06-18',
                'end_date' => '2013-06-26',
                'request_callback' => 'Y',
                'status' => 'N',
                'created_at' => '2013-05-19 11:25:26',
            ),
            2 =>
            array (
                'id' => 5,
                'hostel_id' => 2,
                'creator_id' => 5,
                'email' => 'tester@company.com',
                'full_name' => 'Sergey Tester',
                'phone' => '123456',
                'info' => 'Some extra info Lorem  ipsum dolor sit amet, consectetur 
adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna 
aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco 
laboris nisi ut aliquip ex ea  commodo consequat. Duis aute ...',
                'start_date' => '2013-06-18',
                'end_date' => '2013-06-26',
                'request_callback' => 'Y',
                'status' => 'A',
                'created_at' => '2013-05-19 11:25:26',
            ),
            3 =>
            array (
                'id' => 6,
                'hostel_id' => 14,
                'creator_id' => 8,
                'email' => 'tester@company.com',
                'full_name' => 'Sergey Tester',
                'phone' => '123456',
                'info' => 'Some extra info Lorem  ipsum dolor sit amet, consectetur 
adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna 
aliqua. Ut enim ad...',
                'start_date' => '2013-04-18',
                'end_date' => '2013-04-22',
                'request_callback' => 'Y',
                'status' => 'C',
                'created_at' => '2013-03-18 14:23:26',
            ),
            4 =>
            array (
                'id' => 7,
                'hostel_id' => 14,
                'creator_id' => 9,
                'email' => 'tester@company.com',
                'full_name' => 'Sergey Tester',
                'phone' => '123456',
                'info' => 'Some extra info Lorem  ipsum dolor sit amet, consectetur 
adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna ',
                'start_date' => '2013-06-18',
                'end_date' => '2013-06-26',
                'request_callback' => 'Y',
                'status' => 'N',
                'created_at' => '2013-05-19 11:25:26',
            ),
            5 =>
            array (
                'id' => 8,
                'hostel_id' => 19,
                'creator_id' => 8,
                'email' => 'sda@v.xomn',
                'full_name' => '1111',
                'phone' => '222',
                'info' => '3333
444444444
555555555',
                'start_date' => '2013-04-01',
                'end_date' => '2013-04-30',
                'request_callback' => 'Y',
                'status' => 'C',
                'created_at' => '2013-04-29 04:47:03',
            ),
            6 =>
            array (
                'id' => 9,
                'hostel_id' => 19,
                'creator_id' => 7,
                'email' => 'dsv@ss.com',
                'full_name' => '1111',
                'phone' => '222',
                'info' => '3333333333
44444444
5555555',
                'start_date' => '2013-04-02',
                'end_date' => '2013-04-24',
                'request_callback' => 'Y',
                'status' => 'O',
                'created_at' => '2013-04-29 05:03:07',
            ),
            7 =>
            array (
                'id' => 10,
                'hostel_id' => 1,
                'creator_id' => 8,
                'email' => 'jkhjkh@sas.com',
                'full_name' => 'jkhjkh',
                'phone' => 'jhbjk',
                'info' => 'jkhjk',
                'start_date' => '2013-05-09',
                'end_date' => '2013-05-17',
                'request_callback' => 'Y',
                'status' => 'N',
                'created_at' => '2013-05-12 23:36:42',
            ),
            8 =>
            array (
                'id' => 11,
                'hostel_id' => 20,
                'creator_id' => 7,
                'email' => 'mstdmstd@rambler.ru',
                'full_name' => 'mstdmstd',
                'phone' => '123',
                'info' => '55555
6666666
777777
8888888
9999999',
                'start_date' => '2013-05-25',
                'end_date' => '2013-05-30',
                'request_callback' => 'Y',
                'status' => 'A',
                'created_at' => '2013-05-24 06:13:11',
            ),
            9 =>
            array (
                'id' => 12,
                'hostel_id' => 20,
                'creator_id' => 5,
                'email' => 'mstdmstd@rambler.ru',
                'full_name' => 'mstdmstd',
                'phone' => 'sx',
                'info' => 'nbnb',
                'start_date' => '2013-05-10',
                'end_date' => '2013-05-25',
                'request_callback' => 'Y',
                'status' => 'N',
                'created_at' => '2013-05-24 06:28:40',
            ),
            10 =>
            array (
                'id' => 13,
                'hostel_id' => 20,
                'creator_id' => 9,
                'email' => 'mstdmstd@rambler.ru',
                'full_name' => 'mstdmstd',
                'phone' => 'bnbvnv',
                'info' => 'xxxxxx
ddddddf
fffffffff',
                'start_date' => '2013-05-01',
                'end_date' => '2013-05-31',
                'request_callback' => 'Y',
                'status' => 'O',
                'created_at' => '2013-05-24 06:29:22',
            ),
            11 =>
            array (
                'id' => 14,
                'hostel_id' => 19,
                'creator_id' => 2,
                'email' => 'tester@company.com',
                'full_name' => 'tester',
                'phone' => '122',
                'info' => '1111
22222',
                'start_date' => '2013-05-01',
                'end_date' => '2013-05-24',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-05-31 06:44:16',
            ),
            12 =>
            array (
                'id' => 15,
                'hostel_id' => 19,
                'creator_id' => 8,
                'email' => 'tester@company.com',
                'full_name' => 'jhgbvjh',
                'phone' => 'bvn',
                'info' => 'nb',
                'start_date' => '2013-05-17',
                'end_date' => '2013-05-30',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-05-31 06:46:27',
            ),
            13 =>
            array (
                'id' => 16,
                'hostel_id' => 20,
                'creator_id' => 7,
                'email' => 'tester@company.com',
                'full_name' => '11111',
                'phone' => '111',
                'info' => '222222222',
                'start_date' => '2013-05-03',
                'end_date' => '2013-05-04',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-05-31 06:49:13',
            ),
            14 =>
            array (
                'id' => 17,
                'hostel_id' => 20,
                'creator_id' => 9,
                'email' => 'tester@company.com',
                'full_name' => 'tester111',
                'phone' => '22222',
                'info' => '33334
',
                'start_date' => '2013-05-01',
                'end_date' => '2013-05-16',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-05-31 06:52:15',
            ),
            15 =>
            array (
                'id' => 18,
                'hostel_id' => 20,
                'creator_id' => 1,
                'email' => 'tester@company.com',
                'full_name' => 'tester',
                'phone' => '3333',
                'info' => '444444',
                'start_date' => '2013-05-05',
                'end_date' => '2013-05-24',
                'request_callback' => 'N',
                'status' => 'O',
                'created_at' => '2013-05-31 06:56:54',
            ),
            16 =>
            array (
                'id' => 19,
                'hostel_id' => 20,
                'creator_id' => 4,
                'email' => 'tester@company.com',
                'full_name' => 'tester',
                'phone' => '999999',
                'info' => '00000000',
                'start_date' => '2013-05-06',
                'end_date' => '2013-05-23',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-05-31 06:58:50',
            ),
            17 =>
            array (
                'id' => 20,
                'hostel_id' => 19,
                'creator_id' => 2,
                'email' => 'jik@site.com.au',
                'full_name' => 'jono',
                'phone' => '454654541',
                'info' => 'dasda sdasd asd',
                'start_date' => '2013-06-03',
                'end_date' => '2013-06-05',
                'request_callback' => 'N',
                'status' => 'A',
                'created_at' => '2013-05-31 23:41:14',
            ),
            18 =>
            array (
                'id' => 21,
                'hostel_id' => 19,
                'creator_id' => 2,
                'email' => 'jhkj@xccd.com',
                'full_name' => 'kjhbkj',
                'phone' => 'mjhn',
                'info' => 'jnjk',
                'start_date' => '2013-06-06',
                'end_date' => '2013-06-14',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-06-05 06:08:57',
            ),
            19 =>
            array (
                'id' => 22,
                'hostel_id' => 14,
                'creator_id' => 5,
                'email' => 'tester@company.com',
                'full_name' => 'tester Sergey',
                'phone' => 'wew',
                'info' => '111111
222222
333',
                'start_date' => '2013-08-01',
                'end_date' => '2013-08-5',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-06-22 01:09:21',
            ),
            20 =>
            array (
                'id' => 23,
                'hostel_id' => 22,
                'creator_id' => 8,
                'email' => 'tester@company.com',
                'full_name' => 'tester',
                'phone' => '90-',
                'info' => '333
444
555555',
                'start_date' => '2013-06-04',
                'end_date' => '2013-06-28',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-06-22 01:10:22',
            ),
            21 =>
            array (
                'id' => 24,
                'hostel_id' => 19,
                'creator_id' => 9,
                'email' => 'tester@company.com',
                'full_name' => 'sdaf',
                'phone' => 'tester@softreact',
                'info' => '11
wwwwww
sww',
                'start_date' => '2013-08-28',
                'end_date' => '2013-09-02',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-06-22 01:16:24',
            ),
            22 =>
            array (
                'id' => 25,
                'hostel_id' => 19,
                'creator_id' => 1,
                'email' => 'tester@company.com',
                'full_name' => 'dcs',
                'phone' => 'sd',
                'info' => 'sad',
                'start_date' => '2013-06-03',
                'end_date' => '2013-06-29',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-06-22 01:17:29',
            ),
            23 =>
            array (
                'id' => 26,
                'hostel_id' => 19,
                'creator_id' => 7,
                'email' => 'tester@company.com',
                'full_name' => 'vv',
                'phone' => 'sdxsa',
                'info' => 'xcc',
                'start_date' => '2013-06-06',
                'end_date' => '2013-06-29',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-06-22 01:38:38',
            ),
            24 =>
            array (
                'id' => 27,
                'hostel_id' => 19,
                'creator_id' => 5,
                'email' => 'tester@company.com',
                'full_name' => 'vv',
                'phone' => 'sdxsa',
                'info' => 'xcc',
                'start_date' => '2013-06-06',
                'end_date' => '2013-06-29',
                'request_callback' => 'N',
                'status' => 'C',
                'created_at' => '2013-06-22 01:38:38',
            ),
            25 =>
            array (
                'id' => 28,
                'hostel_id' => 19,
                'creator_id' => 3,
                'email' => 'tester@company.com',
                'full_name' => 'scasc',
                'phone' => 'xas',
                'info' => 'xcxc',
                'start_date' => '2013-06-13',
                'end_date' => '2013-06-21',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-06-22 01:39:41',
            ),
            26 =>
            array (
                'id' => 29,
                'hostel_id' => 19,
                'creator_id' => 2,
                'email' => 'tester@company.com',
                'full_name' => 'df',
                'phone' => '2222222',
                'info' => '3
44444
5555555',
                'start_date' => '2013-06-05',
                'end_date' => '2013-06-21',
                'request_callback' => 'N',
                'status' => 'A',
                'created_at' => '2013-06-22 01:55:54',
            ),
            27 =>
            array (
                'id' => 30,
                'hostel_id' => 19,
                'creator_id' => 5,
                'email' => 'tester@company.com',
                'full_name' => 'df',
                'phone' => '2222222',
                'info' => '3
44444
5555555',
                'start_date' => '2013-06-05',
                'end_date' => '2013-06-21',
                'request_callback' => 'N',
                'status' => 'C',
                'created_at' => '2013-06-22 01:55:57',
            ),
            28 =>
            array (
                'id' => 31,
                'hostel_id' => 23,
                'creator_id' => 9,
                'email' => 'tester@company.com',
                'full_name' => 'hg',
                'phone' => 'bgv',
                'info' => 'hvbbv',
                'start_date' => '2013-06-08',
                'end_date' => '2013-06-22',
                'request_callback' => 'N',
                'status' => 'C',
                'created_at' => '2013-06-27 05:44:47',
            ),
            29 =>
            array (
                'id' => 32,
                'hostel_id' => 18,
                'creator_id' => 7,
                'email' => 'tester@company.com',
                'full_name' => 'cvc',
                'phone' => 'vbc',
                'info' => 'vbc',
                'start_date' => '2013-06-12',
                'end_date' => '2013-06-13',
                'request_callback' => 'N',
                'status' => 'O',
                'created_at' => '2013-06-27 05:48:06',
            ),
            30 =>
            array (
                'id' => 33,
                'hostel_id' => 22,
                'creator_id' => 1,
                'email' => 'gf@safd.com',
                'full_name' => 'vc',
                'phone' => '222',
                'info' => '1111
22222
3333',
                'start_date' => '2013-07-16',
                'end_date' => '2013-07-24',
                'request_callback' => 'N',
                'status' => 'C',
                'created_at' => '2013-07-04 06:23:59',
            ),
            31 =>
            array (
                'id' => 34,
                'hostel_id' => 30,
                'creator_id' => 4,
                'email' => 'jik@site.com.au',
                'full_name' => 'clientTest',
                'phone' => '0312456783',
                'info' => 'sadiajs doiasd asiod aiwj aiodjioawj oiasjd lksjdoaisjd oasjdoias joiasjd oiasjdio asjdioasjd oiasjdoiajsdio asdnlknwi ajs a',
                'start_date' => '2013-07-01',
                'end_date' => '2013-07-08',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-07-11 18:14:41',
            ),
            32 =>
            array (
                'id' => 35,
                'hostel_id' => 30,
                'creator_id' => 3,
                'email' => 'jik@site.com.au',
                'full_name' => 'fdsfaf',
                'phone' => '213123123123',
                'info' => 'd;asoifj fj odsij',
                'start_date' => '2013-08-08',
                'end_date' => '2013-08-23',
                'request_callback' => 'N',
                'status' => 'C',
                'created_at' => '2013-07-31 23:40:38',
            ),
            33 =>
            array (
                'id' => 36,
                'hostel_id' => 30,
                'creator_id' => 2,
                'email' => 'jik@site.com.au',
                'full_name' => 'joijioj',
                'phone' => '1023712837',
                'info' => 'opska dposkadasdasds asdasd',
                'start_date' => '2013-08-12',
                'end_date' => '2013-08-13',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-01 09:14:23',
            ),
            34 =>
            array (
                'id' => 37,
                'hostel_id' => 19,
                'creator_id' => 6,
                'email' => 'jik@site.com.au',
                'full_name' => 'hostel test',
                'phone' => '8931274901',
                'info' => 'udhfv iouasdfhdsfhiauosdofhiasduhfiouasdf',
                'start_date' => '2013-08-20',
                'end_date' => '2013-08-17',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-01 20:10:44',
            ),
            35 =>
            array (
                'id' => 38,
                'hostel_id' => 19,
                'creator_id' => 9,
                'email' => 'jik@site.com.au',
                'full_name' => 'hostel test',
                'phone' => '8931274901',
                'info' => 'udhfv iouasdfhdsfhiauosdofhiasduhfiouasdf',
                'start_date' => '2013-08-15',
                'end_date' => '2013-08-17',
                'request_callback' => 'N',
                'status' => 'C',
                'created_at' => '2013-08-01 20:10:45',
            ),
            36 =>
            array (
                'id' => 39,
                'hostel_id' => 19,
                'creator_id' => 8,
                'email' => 'jik@site.com.au',
                'full_name' => 'jono hostel',
                'phone' => '123412',
                'info' => 'dsfafaw f s',
                'start_date' => '2013-08-12',
                'end_date' => '2013-08-16',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-01 20:22:55',
            ),
            37 =>
            array (
                'id' => 40,
                'hostel_id' => 19,
                'creator_id' => 9,
                'email' => 'jik@site.com.au',
                'full_name' => 'jono',
                'phone' => '24352345234',
                'info' => 'adasdasd',
                'start_date' => '2013-08-20',
                'end_date' => '2013-08-31',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-01 20:25:06',
            ),
            38 =>
            array (
                'id' => 41,
                'hostel_id' => 21,
                'creator_id' => 7,
                'email' => 'jik@site.com.au',
                'full_name' => 'dasdasdasd',
                'phone' => '131231234124',
                'info' => 'edfw asdf sdafasdf dsaf asdfasdf sasdfsdsdfdsf edfw asdf sdafasdf dsaf asdfasdf sasdfsdsdfdsf edfw asdf sdafasdf dsaf asdfasdf sasdfsdsdfdsf edfw asdf sdafasdf dsaf asdfasdf sasdfsdsdfdsf ',
                'start_date' => '2013-08-06',
                'end_date' => '2013-08-23',
                'request_callback' => 'N',
                'status' => 'C',
                'created_at' => '2013-08-01 20:36:59',
            ),
            39 =>
            array (
                'id' => 42,
                'hostel_id' => 21,
                'creator_id' => 9,
                'email' => 'jik@site.com.au',
                'full_name' => 'rasdf gdgsdfg sdfgsd',
                'phone' => '123128121',
                'info' => 'asdc sfasd fasdasdf asdf sdafasdasdc sfasd fasdasdf asdf sdafasdasdc sfasd fasdasdf asdf sdafasdasdc sfasd fasdasdf asdf sdafasd',
                'start_date' => '2013-08-01',
                'end_date' => '2013-08-16',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-01 20:38:24',
            ),
            40 =>
            array (
                'id' => 43,
                'hostel_id' => 21,
                'creator_id' => 4,
                'email' => 'jik@site.com.au',
                'full_name' => 'jonono',
                'phone' => '32423153245324',
                'info' => 'ew rqwerqweqwer weqr qw ew rqwerqweqwer weqr qw ew rqwerqweqwer weqr qw ew rqwerqweqwer weqr qw ',
                'start_date' => '2013-08-12',
                'end_date' => '2013-08-09',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-01 20:39:52',
            ),
            41 =>
            array (
                'id' => 44,
                'hostel_id' => 16,
                'creator_id' => 1,
                'email' => 'jik@site.com.au',
                'full_name' => 'jono',
                'phone' => '2314124123',
                'info' => 'fdsa sd fsda fasd sd fsdf asdfdsf asdd ',
                'start_date' => '2013-08-05',
                'end_date' => '2013-08-17',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-01 23:52:59',
            ),
            42 =>
            array (
                'id' => 45,
                'hostel_id' => 19,
                'creator_id' => 3,
                'email' => 'tester@company.com',
                'full_name' => 'tester sergey',
                'phone' => '123',
                'info' => '44
55
6666',
                'start_date' => '2013-08-01',
                'end_date' => '2013-08-23',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-02 00:34:17',
            ),
            43 =>
            array (
                'id' => 46,
                'hostel_id' => 32,
                'creator_id' => 2,
                'email' => 'j@j.com.au',
                'full_name' => 'aidojas',
                'phone' => '132',
                'info' => 'fds',
                'start_date' => '2013-08-05',
                'end_date' => '2013-08-07',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-28 18:36:40',
            ),
            44 =>
            array (
                'id' => 47,
                'hostel_id' => 33,
                'creator_id' => 1,
                'email' => 'j@j.com',
                'full_name' => 'asda',
                'phone' => '124',
                'info' => 'fasesfd',
                'start_date' => '2013-08-06',
                'end_date' => '2013-08-08',
                'request_callback' => 'N',
                'status' => 'N',
                'created_at' => '2013-08-28 18:38:34',
            ),
        ));


    }
}
