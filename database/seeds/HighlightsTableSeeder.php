<?php

use Illuminate\Database\Seeder;

class HighlightsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('highlights')->delete();
        
        \DB::table('highlights')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'The unique experience of seeing Melbourne by tram and enjoying fine food along the way',
                'created_at' => '2013-04-20 08:59:03',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'The tram\'s luxurious fittings and period details make the ride a journey into the past',
                'created_at' => '2013-02-23 08:32:03',
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'Choice of lunch and two dinner sittings',
                'created_at' => '2013-02-23 08:32:03',
            ),
            3 => 
            array (
                'id' => 6,
                'name' => ' A romantic dinner for two with a difference ',
                'created_at' => '2013-05-13 01:06:39',
            ),
        ));
        
        
    }
}