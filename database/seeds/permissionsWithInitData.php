<?php

use Illuminate\Database\Seeder;
use App\Permission;

class permissionsWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permission = [
            [
                'name' => 'facility-list',
                'display_name' => 'Display Facility Listing',
                'description' => 'See only Listing Of Facility'
            ],
            [
                'name' => 'facility-create',
                'display_name' => 'Create Facility',
                'description' => 'Create New Facility'
            ],
            [
                'name' => 'facility-edit',
                'display_name' => 'Edit Facility',
                'description' => 'Edit Facility'
            ],
            [
                'name' => 'facility-delete',
                'display_name' => 'Delete Facility',
                'description' => 'Delete Facility'
            ],
            [
                'name' => 'hostel-list',
                'display_name' => 'Display Hostel Listing',
                'description' => 'See only Listing Of Hostel'
            ],
            [
                'name' => 'hostel-create',
                'display_name' => 'Create Hostel',
                'description' => 'Create New Hostel'
            ],
            [
                'name' => 'hostel-edit',
                'display_name' => 'Edit Hostel',
                'description' => 'Edit Hostel'
            ],
            [
                'name' => 'hostel-delete',
                'display_name' => 'Delete Hostel',
                'description' => 'Delete Hostel'
            ]
        ];


        foreach ($permission as $key => $value) {
            Permission::create($value);
        }
    }
}
