<?php

use Illuminate\Database\Seeder;

class HostelImagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('hostel_images')->delete();

        \DB::table('hostel_images')->insert(array (
            0 =>
            array (
                'id' => 1,
                'hostel_id' => 30,
                'filename' => 'hostels-30-1.jpg',
                'is_main' => 1,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel image at Adelaide # 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:23:20',
            ),
            1 =>
            array (
                'id' => 2,
                'hostel_id' => 30,
                'filename' => 'hostels-30-2.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel image at Adelaide # 2 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:25:26',
            ),
            2 =>
            array (
                'id' => 3,
                'hostel_id' => 30,
                'filename' => 'hostels-30-3.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel image at Adelaide # 3 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:26:30',
            ),
            3 =>
            array (
                'id' => 4,
                'hostel_id' => 30,
                'filename' => 'hostels-30-4.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel image at Adelaide # 4 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:28:12',
            ),
            4 =>
            array (
                'id' => 5,
                'hostel_id' => 29,
                'filename' => 'Hostel-29-1.jpg',
                'is_main' => 1,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel image at Adelaide # 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:27:53',
            ),
            5 =>
            array (
                'id' => 6,
                'hostel_id' => 29,
                'filename' => 'Hostel-29-2.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel image at Adelaide # 2  lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:28:12',
            ),
            6 =>
            array (
                'id' => 7,
                'hostel_id' => 29,
                'filename' => 'Hostel-29-3.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel image at Adelaide # 3  lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:29:12',
            ),
            7 =>
            array (
                'id' => 8,
                'hostel_id' => 26,
                'filename' => 'Hostel-26-1.jpg',
                'is_main' => 1,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Small fashioned Hostel # 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:27:53',
            ),
            8 =>
            array (
                'id' => 9,
                'hostel_id' => 26,
                'filename' => 'Hostel-26-2.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Small fashioned Hostel # 2 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:27:53',
            ),
            9 =>
            array (
                'id' => 10,
                'hostel_id' => 26,
                'filename' => 'Hostel-26-3.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Small fashioned Hostel # 3 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:27:53',
            ),
            10 =>
            array (
                'id' => 11,
                'hostel_id' => 28,
                'filename' => 'Hostel-28-1.jpg',
                'is_main' => 1,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Small cheap hostel # 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:27:59',
            ),
            11 =>
            array (
                'id' => 12,
                'hostel_id' => 28,
                'filename' => 'Hostel-28-2.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Small cheap hostel # 2 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:28:55',
            ),
            12 =>
            array (
                'id' => 13,
                'hostel_id' => 28,
                'filename' => 'Hostel-28-3.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Small cheap hostel # 3 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:30:05',
            ),
            13 =>
            array (
                'id' => 14,
                'hostel_id' => 28,
                'filename' => 'Hostel-28-4.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Small cheap hostel # 4 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:32:15',
            ),
            14 =>
            array (
                'id' => 15,
                'hostel_id' => 31,
                'filename' => 'Hostel-31-1.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'New Hostel in Eurelia town # 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:22:27',
            ),
            15 =>
            array (
                'id' => 16,
                'hostel_id' => 31,
                'filename' => 'Hostel-31-2.jpg',
                'is_main' => 1,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'New Hostel in Eurelia town # 2 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:23:55',
            ),
            16 =>
            array (
                'id' => 17,
                'hostel_id' => 31,
                'filename' => 'Hostel-31-3.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'New Hostel in Eurelia town # 3 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:26:07',
            ),
            17 =>
            array (
                'id' => 18,
                'hostel_id' => 31,
                'filename' => 'Hostel-31-4.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'New Hostel in Eurelia town # 4 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:29:15',
            ),
            18 =>
            array (
                'id' => 19,
                'hostel_id' => 10,
                'filename' => 'Hostel-10-1.jpg',
                'is_main' => 1,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel near see beach # 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:23:50',
            ),
            19 =>
            array (
                'id' => 20,
                'hostel_id' => 10,
                'filename' => 'Hostel-10-2.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel near see beach # 2 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:24:58',
            ),
            20 =>
            array (
                'id' => 21,
                'hostel_id' => 10,
                'filename' => 'Hostel-10-3.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel near see beach # 3 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-02 11:26:23',
            ),
            21 =>
            array (
                'id' => 22,
                'hostel_id' => 11,
                'filename' => 'Hostel-11-1.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel at see # 1 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-01 15:21:03',
            ),
            22 =>
            array (
                'id' => 23,
                'hostel_id' => 11,
                'filename' => 'Hostel-11-2.jpg',
                'is_main' => 1,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel at see # 2 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-01 15:25:21',
            ),
            23 =>
            array (
                'id' => 24,
                'hostel_id' => 11,
                'filename' => 'Hostel-11-3.jpg',
                'is_main' => 0,
                'is_video' => 0,
                'video_width' => NULL,
                'video_height' => NULL,
                'info' => 'Hostel at see # 3 lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                'created_at' => '2019-04-01 15:21:03',
            ),


            24 =>
                array (
                    'id' => 25,
                    'hostel_id' => 22,
                    'filename' => 'Hostel-22-1.jpg',
                    'is_main' => 1,
                    'is_video' => 0,
                    'video_width' => NULL,
                    'video_height' => NULL,
                    'info' => 'Deep Winds Hostel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                    'created_at' => '2019-04-02 11:46:52',
                ),

            25 =>
                array (
                    'id' => 26,
                    'hostel_id' => 22,
                    'filename' => 'Hostel-22-2.jpg',
                    'is_main' => 0,
                    'is_video' => 0,
                    'video_width' => NULL,
                    'video_height' => NULL,
                    'info' => 'Deep Winds Hostel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                    'created_at' => '2019-04-02 11:47:51',
                ),


            26 =>
                array (
                    'id' => 27,
                    'hostel_id' => 14,
                    'filename' => 'Hostel-14-1.jpg',
                    'is_main' => 0,
                    'is_video' => 0,
                    'video_width' => NULL,
                    'video_height' => NULL,
                    'info' => 'Postos Hostel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                    'created_at' => '2019-04-02 11:58:21',
                ),

            27 =>
                array (
                    'id' => 28,
                    'hostel_id' => 14,
                    'filename' => 'Hostel-14-2.jpg',
                    'is_main' => 1,
                    'is_video' => 0,
                    'video_width' => NULL,
                    'video_height' => NULL,
                    'info' => 'Postos Hostel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                    'created_at' => '2019-04-02 11:59:32',
                ),

            28 =>
                array (
                    'id' => 29,
                    'hostel_id' => 14,
                    'filename' => 'Hostel-14-3.jpg',
                    'is_main' => 0,
                    'is_video' => 0,
                    'video_width' => NULL,
                    'video_height' => NULL,
                    'info' => 'Postos Hostel lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et',
                    'created_at' => '2019-04-02 12:03:46',
                ),

        ));


    }
}
