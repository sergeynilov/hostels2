<?php

use Illuminate\Database\Seeder;

class HostelReviewsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hostel_reviews')->delete();
        
        \DB::table('hostel_reviews')->insert(array (
            0 => 
            array (
                'id' => 5,
                'hostel_id' => 1,
                'email_inquiried' => 'from@mail.com',
                'full_name' => 'Sergey Volin',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'Review text # 2... Excellent',
                'stars_rating_type_id' => 5,
                'created_at' => '2013-03-23 03:11:34',
            ),
            1 => 
            array (
                'id' => 6,
                'hostel_id' => 1,
                'email_inquiried' => 'from@mail.com',
                'full_name' => 'Sergey Volin',
                'status' => 'I',
                'flag_status' => 'N',
                'review' => 'Review text # 3...',
                'stars_rating_type_id' => 2,
                'created_at' => '2013-03-21 07:09:42',
            ),
            2 => 
            array (
                'id' => 7,
                'hostel_id' => 2,
                'email_inquiried' => 'from@mail.com',
                'full_name' => 'Sergey Volin',
                'status' => 'N',
                'flag_status' => 'N',
                'review' => 'Review text...',
                'stars_rating_type_id' => 5,
                'created_at' => '2013-03-23 00:00:00',
            ),
            3 => 
            array (
                'id' => 8,
                'hostel_id' => 2,
                'email_inquiried' => 'from@mail.com',
                'full_name' => 'Sergey Volin',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'Review text # 2...',
                'stars_rating_type_id' => 5,
                'created_at' => '2013-03-23 03:11:34',
            ),
            4 => 
            array (
                'id' => 9,
                'hostel_id' => 2,
                'email_inquiried' => 'from@mail.com',
                'full_name' => 'Sergey Volin',
                'status' => 'I',
                'flag_status' => 'N',
                'review' => 'Review text # 3...',
                'stars_rating_type_id' => 1,
                'created_at' => '2013-03-21 07:09:42',
            ),
            5 => 
            array (
                'id' => 10,
                'hostel_id' => 14,
                'email_inquiried' => 'from@mail.com',
                'full_name' => 'Sergey Volin',
                'status' => 'N',
                'flag_status' => 'N',
                'review' => 'Review text...',
                'stars_rating_type_id' => 3,
                'created_at' => '2013-03-23 00:00:00',
            ),
            6 => 
            array (
                'id' => 11,
                'hostel_id' => 14,
                'email_inquiried' => 'from@mail.com',
                'full_name' => 'Sergey Volin',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'Review text # 2...',
                'stars_rating_type_id' => 1,
                'created_at' => '2013-03-23 03:11:34',
            ),
            7 => 
            array (
                'id' => 12,
                'hostel_id' => 14,
                'email_inquiried' => 'from@mail.com',
                'full_name' => 'Sergey Volin',
                'status' => 'I',
                'flag_status' => 'N',
                'review' => 'Review text # 3...',
                'stars_rating_type_id' => 5,
                'created_at' => '2013-03-21 07:09:42',
            ),
            8 => 
            array (
                'id' => 25,
                'hostel_id' => 1,
                'email_inquiried' => 'cf@ds.com',
                'full_name' => 'hnjgbhj',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'qqqq
wwwwww
eeee  Good Excel',
                'stars_rating_type_id' => 5,
                'created_at' => '2013-05-12 02:24:10',
            ),
            9 => 
            array (
                'id' => 40,
                'hostel_id' => 30,
                'email_inquiried' => 'j@j.com',
                'full_name' => 'Terry',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'Great staff and comfy beds. Perfect Melbourne location on spencer street.',
                'stars_rating_type_id' => 5,
                'created_at' => '2013-08-28 18:39:17',
            ),
            10 => 
            array (
                'id' => 42,
                'hostel_id' => 20,
                'email_inquiried' => 'fvs@xfs.com',
                'full_name' => 'xcvvzx',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => '5555555
66666666
7777777777
888888',
                'stars_rating_type_id' => 0,
                'created_at' => '2013-05-24 05:57:01',
            ),
            11 => 
            array (
                'id' => 43,
                'hostel_id' => 19,
                'email_inquiried' => 'sda@v.xomn',
                'full_name' => 'lkjl',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'l;k;l',
                'stars_rating_type_id' => 0,
                'created_at' => '2013-06-05 06:10:30',
            ),
            12 => 
            array (
                'id' => 44,
                'hostel_id' => 19,
                'email_inquiried' => 'sda@v.xomn',
                'full_name' => 'jkhbk',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => '1111
22
3333333333',
                'stars_rating_type_id' => 0,
                'created_at' => '2013-06-05 06:10:50',
            ),
            13 => 
            array (
                'id' => 45,
                'hostel_id' => 19,
                'email_inquiried' => 'jhkj@xccd.com',
                'full_name' => 'jkhb',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => '1111111
222222
333333333',
                'stars_rating_type_id' => 4,
                'created_at' => '2013-06-07 23:52:15',
            ),
            14 => 
            array (
                'id' => 46,
                'hostel_id' => 19,
                'email_inquiried' => 'jhkj@xccd.com',
                'full_name' => 'fdsafdsfds',
                'status' => 'A',
                'flag_status' => 'S',
                'review' => 'assssssss
qqqqqq
wwwwww
eeee
rrrrr
ttttt',
                'stars_rating_type_id' => 3,
                'created_at' => '2013-06-08 00:44:35',
            ),
            15 => 
            array (
                'id' => 47,
                'hostel_id' => 30,
                'email_inquiried' => 'j@j.com.au',
                'full_name' => 'Paul',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'Great hostel and great bar! Have rebooked for next month!',
                'stars_rating_type_id' => 4,
                'created_at' => '2013-08-28 18:37:06',
            ),
            16 => 
            array (
                'id' => 48,
                'hostel_id' => 32,
                'email_inquiried' => 'j@j.com.au',
                'full_name' => 'Greg',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'Awesome vibe and wicked staff! Cheap drinks at the bar too!',
                'stars_rating_type_id' => 4,
                'created_at' => '2013-08-28 18:38:05',
            ),
            17 => 
            array (
                'id' => 50,
                'hostel_id' => 30,
                'email_inquiried' => 'j@j.com',
                'full_name' => 'Jessica',
                'status' => 'A',
                'flag_status' => 'N',
                'review' => 'Bar was relatively cheap on saturday night. Hostel was in the city centre so was close to bars and night clubs',
                'stars_rating_type_id' => 3,
                'created_at' => '2013-08-28 18:40:00',
            ),
        ));
        
        
    }
}