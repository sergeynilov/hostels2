<?php

use Illuminate\Database\Seeder;

class UsersGroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users_groups')->delete();

        \DB::table('users_groups')->insert(array (
            0 =>
            array (
                'id' => 1,
                'user_id' => 1,
                'group_id' => 2,
                'created_at' => '2019-04-29 11:03:51',
            ),
            1 =>
            array (
                'id' => 2,
                'user_id' => 1,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),
            2 =>
            array (
                'id' => 3,
                'user_id' => 2,
                'group_id' => 2,
                'created_at' => '2019-04-29 11:03:51',
            ),
            3 =>
            array (
                'id' => 4,
                'user_id' => 2,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),
            4 =>
            array (
                'id' => 5,
                'user_id' => 3,
                'group_id' => 2,
                'created_at' => '2019-04-29 11:03:51',
            ),
            5 =>
            array (
                'id' => 6,
                'user_id' => 3,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),
            6 =>
            array (
                'id' => 7,
                'user_id' => 4,
                'group_id' => 1,
                'created_at' => '2019-04-29 11:03:51',
            ),
            7 =>
            array (
                'id' => 8,
                'user_id' => 4,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),
            8 =>
            array (
                'id' => 9,
                'user_id' => 5,
                'group_id' => 1,
                'created_at' => '2019-04-29 11:03:51',
            ),
            9 =>
            array (
                'id' => 10,
                'user_id' => 5,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),
            10 =>
            array (
                'id' => 11,
                'user_id' => 6,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),
            11 =>
            array (
                'id' => 12,
                'user_id' => 6,
                'group_id' => 1,
                'created_at' => '2019-04-29 11:03:51',
            ),
            12 =>
            array (
                'id' => 13,
                'user_id' => 7,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),
            13 =>
            array (
                'id' => 14,
                'user_id' => 8,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),
            14 =>
            array (
                'id' => 15,
                'user_id' => 9,
                'group_id' => 3,
                'created_at' => '2019-04-29 11:03:51',
            ),



            15 =>
                array (
                    'id' => 16,
                    'user_id' => 5,
                    'group_id' => 4,
                    'created_at' => '2019-04-29 11:03:51',
                ),

            16 =>
                array (
                    'id' => 17,
                    'user_id' => 2,
                    'group_id' => 4,
                    'created_at' => '2019-04-29 11:03:51',
                ),



        ));


    }
}
