<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);

//        $this->call(HostelExtraDetailsTableSeeder::class);
//        $this->call(hostelImagesWithInitData::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(FacilitiesTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(HighlightsTableSeeder::class);
        $this->call(HostelsTableSeeder::class);
//        $this->call(HostelExtraDetailsTableSeeder::class);
        $this->call(HostelFacilitiesTableSeeder::class);
        $this->call(HostelImagesTableSeeder::class);
        $this->call(HostelInqueriesTableSeeder::class);
        $this->call(HostelReviewsTableSeeder::class);
        $this->call(HostelRoomsTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PersonalHostelBookmarksTableSeeder::class);
        $this->call(PersonalOptionsTableSeeder::class);

        $this->call(StatesTableSeeder::class);

        $this->call(RegionsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(SubregionsTableSeeder::class);
        $this->call(SuburbsTableSeeder::class);
        $this->call(UsersGroupsTableSeeder::class);
        $this->call(LocationDataTableSeeder::class);
        $this->call(newsWithInitData::class);
        $this->call(HostelExtraDetailsTableSeeder::class);
        $this->call(forumCategoriesWithInitData::class);
        $this->call(forumForumsWithInitData::class);
        $this->call(forumThreadsWithInitData::class);
        $this->call(forumPostsWithInitData::class);
        $this->call(forumPostReportAbusesWithInitData::class);
        $this->call(forumPostWatchedByUserWithInitData::class);
        $this->call(forumPostBookmarkWithInitData::class);
        $this->call(forumPostUserSubscribedWithInitData::class);
        $this->call(paymentPackagesWithInitData::class);
    }
}
