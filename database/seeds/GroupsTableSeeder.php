<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('groups')->delete();

        \DB::table('groups')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Admin',
                'description' => 'Administrator',
                'created_at' => '2019-04-29 11:03:50',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Manager',
                'description' => 'Manager description...',
                'created_at' => '2019-04-29 11:03:50',
            ),

            2 =>
            array (
                'id' => 3,
                'name' => 'Customer',
                'description' => 'Customer description...',
                'created_at' => '2019-04-29 11:03:50',
            ),

            3 =>
            array (
                'id' => 4,
                'name' => 'Content Editor',
                'description' => 'Content Editor description...',
                'created_at' => '2019-04-29 11:03:50',
            ),

        ));


    }
}
