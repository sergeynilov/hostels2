<?php

//use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Seeder;
//use Illuminate\Support\Facades\Schema;

class forumForumsWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('forums')->delete();

        \DB::table('forums')->insert(array (
            0 =>                // forums=> 1 General Business
                array (         // forums=> 2 General Marketing
                    'id'           => 1,
                    'title'        => 'General Business',
                    'slug'         => 'general-business',
                    'description'  => '<p>General Business description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut</p>',
                    'creator_id'   => 1,
                    'forum_category_id'        => 2,
                    'published'    => true,
                    'views'      => 15,
                ),


            1 =>
                array (
                    'id'           => 2,
                    'title'        => 'General Marketing',
                    'slug'         => 'general-marketing',
                    'description'  => '<p>General Marketing description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.</p>',
                    'creator_id'   => 4,
                    'forum_category_id'        => 2,
                    'published'    => true,
                    'views'      => 20,
                ),


            2 =>
                array (
                    'id'           => 3,
                    'title'        => 'Social Networks',
                    'slug'         => 'social-networks',
                    'description'  => '<p>Social Networks description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. </p>',
                    'creator_id'   => 5,
                    'forum_category_id'        => 2,
                    'published'    => true,
                    'views'      => 17,
                ),


            3 =>
                array (
                    'id'           => 4,
                    'title'        => 'Legal Issues',
                    'slug'         => 'legal-issues',
                    'description'  => '<p>Legal Issues description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.</p>',
                    'creator_id'   => 2,
                    'forum_category_id'        => 2,
                    'published'    => true,
                    'views'      => 13,
                ),



            4 =>
                array (
                    'id'           => 5,
                    'title'        => 'Copywriting',
                    'slug'         => 'copywriting',
                    'description'  => '<p>Copywriting description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor </p>',
                    'creator_id'   => 2,
                    'forum_category_id'        => 2,
                    'published'    => false,
                    'views'      => 9,
                ),
            5 =>
                array (
                    'id'           => 6,
                    'title'        => 'Domain Names',
                    'slug'         => 'domain-names',
                    'description'  => '<p>Domain Names description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat..</p>',
                    'creator_id'   => 1,
                    'forum_category_id'        => 2,
                    'published'    => true,
                    'views'      => 22,
                ),



            6 =>
                array (
                    'id'           => 7,
                    'title'        => 'Google',
                    'slug'         => 'google',
                    'description'  => '<p>Google description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat..</p>',
                    'creator_id'   => 4,
                    'forum_category_id'        => 1,
                    'published'    => true,
                    'views'      => 14,
                ),

            7 =>
                array (
                    'id'           => 8,
                    'title'        => 'Yahoo',
                    'slug'         => 'yahoo',
                    'description'  => '<p>Yahoo description Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat..</p>',
                    'creator_id'   => 4,
                    'forum_category_id'        => 1,
                    'published'    => true,
                    'views'      => 14,
                ),

        ));

    }
}
