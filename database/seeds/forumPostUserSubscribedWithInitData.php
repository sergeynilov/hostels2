<?php

use Illuminate\Database\Seeder;

class forumPostUserSubscribedWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('forum_post_user_subscribed')->delete();

        \DB::table('forum_post_user_subscribed')->insert(array (
            0 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 1,
                ),

            1 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 3,
                ),

            2 =>
                array (
                    'user_id'          => 5,
                    'forum_post_id'    => 6,
                ),

            3 =>
                array (
                    'user_id'          => 4,
                    'forum_post_id'    => 9,
                ),

            4 =>
                array (
                    'user_id'          => 6,
                    'forum_post_id'    => 7,
                ),
            )

        );

    }
}
