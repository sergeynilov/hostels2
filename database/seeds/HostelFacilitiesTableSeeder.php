<?php

use Illuminate\Database\Seeder;

class HostelFacilitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hostel_facilities')->delete();
        
        \DB::table('hostel_facilities')->insert(array (
            0 => 
            array (
                'id' => 21,
                'facility_id' => 1,
                'hostel_id' => 8,
                'created_at' => '2013-03-25 10:29:42',
            ),
            1 => 
            array (
                'id' => 22,
                'facility_id' => 2,
                'hostel_id' => 8,
                'created_at' => '2013-03-25 10:29:42',
            ),
            2 => 
            array (
                'id' => 26,
                'facility_id' => 1,
                'hostel_id' => 2,
                'created_at' => '2013-03-25 10:33:58',
            ),
            3 => 
            array (
                'id' => 31,
                'facility_id' => 1,
                'hostel_id' => 12,
                'created_at' => '2013-03-25 20:26:16',
            ),
            4 => 
            array (
                'id' => 32,
                'facility_id' => 2,
                'hostel_id' => 12,
                'created_at' => '2013-03-25 20:26:16',
            ),
            5 => 
            array (
                'id' => 33,
                'facility_id' => 3,
                'hostel_id' => 12,
                'created_at' => '2013-03-25 20:26:16',
            ),
            6 => 
            array (
                'id' => 34,
                'facility_id' => 4,
                'hostel_id' => 12,
                'created_at' => '2013-03-25 20:26:16',
            ),
            7 => 
            array (
                'id' => 50,
                'facility_id' => 1,
                'hostel_id' => 15,
                'created_at' => '2013-04-12 01:49:24',
            ),
            8 => 
            array (
                'id' => 51,
                'facility_id' => 2,
                'hostel_id' => 15,
                'created_at' => '2013-04-12 01:49:24',
            ),
            9 => 
            array (
                'id' => 52,
                'facility_id' => 3,
                'hostel_id' => 15,
                'created_at' => '2013-04-12 01:49:24',
            ),
            10 => 
            array (
                'id' => 61,
                'facility_id' => 1,
                'hostel_id' => 9,
                'created_at' => '2013-05-19 02:17:00',
            ),
            11 => 
            array (
                'id' => 62,
                'facility_id' => 2,
                'hostel_id' => 9,
                'created_at' => '2013-05-19 02:17:00',
            ),
            12 => 
            array (
                'id' => 111,
                'facility_id' => 4,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            13 => 
            array (
                'id' => 112,
                'facility_id' => 5,
                'hostel_id' => 1,
                'created_at' => '2013-06-12 17:57:54',
            ),
            14 => 
            array (
                'id' => 113,
                'facility_id' => 6,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            15 => 
            array (
                'id' => 114,
                'facility_id' => 1,
                'hostel_id' => 1,
                'created_at' => '2013-06-12 17:57:54',
            ),
            16 => 
            array (
                'id' => 115,
                'facility_id' => 2,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            17 => 
            array (
                'id' => 116,
                'facility_id' => 7,
                'hostel_id' => 1,
                'created_at' => '2013-06-12 17:57:54',
            ),
            18 => 
            array (
                'id' => 117,
                'facility_id' => 8,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            19 => 
            array (
                'id' => 118,
                'facility_id' => 9,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            20 => 
            array (
                'id' => 119,
                'facility_id' => 3,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            21 => 
            array (
                'id' => 120,
                'facility_id' => 10,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            22 => 
            array (
                'id' => 121,
                'facility_id' => 11,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            23 => 
            array (
                'id' => 122,
                'facility_id' => 12,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            24 => 
            array (
                'id' => 123,
                'facility_id' => 13,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            25 => 
            array (
                'id' => 124,
                'facility_id' => 14,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            26 => 
            array (
                'id' => 125,
                'facility_id' => 20,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            27 => 
            array (
                'id' => 126,
                'facility_id' => 15,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            28 => 
            array (
                'id' => 127,
                'facility_id' => 16,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            29 => 
            array (
                'id' => 128,
                'facility_id' => 18,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            30 => 
            array (
                'id' => 129,
                'facility_id' => 22,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            31 => 
            array (
                'id' => 130,
                'facility_id' => 33,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            32 => 
            array (
                'id' => 131,
                'facility_id' => 34,
                'hostel_id' => 21,
                'created_at' => '2013-06-12 17:57:54',
            ),
            33 => 
            array (
                'id' => 136,
                'facility_id' => 4,
                'hostel_id' => 22,
                'created_at' => '2013-06-19 05:22:37',
            ),
            34 => 
            array (
                'id' => 137,
                'facility_id' => 5,
                'hostel_id' => 22,
                'created_at' => '2013-06-19 05:22:37',
            ),
            35 => 
            array (
                'id' => 138,
                'facility_id' => 6,
                'hostel_id' => 22,
                'created_at' => '2013-06-19 05:22:37',
            ),
            36 => 
            array (
                'id' => 139,
                'facility_id' => 1,
                'hostel_id' => 22,
                'created_at' => '2013-06-19 05:22:37',
            ),
            37 => 
            array (
                'id' => 199,
                'facility_id' => 4,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            38 => 
            array (
                'id' => 200,
                'facility_id' => 5,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            39 => 
            array (
                'id' => 201,
                'facility_id' => 2,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            40 => 
            array (
                'id' => 202,
                'facility_id' => 8,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            41 => 
            array (
                'id' => 203,
                'facility_id' => 9,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            42 => 
            array (
                'id' => 204,
                'facility_id' => 19,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            43 => 
            array (
                'id' => 205,
                'facility_id' => 16,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            44 => 
            array (
                'id' => 206,
                'facility_id' => 18,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            45 => 
            array (
                'id' => 207,
                'facility_id' => 36,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            46 => 
            array (
                'id' => 208,
                'facility_id' => 21,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            47 => 
            array (
                'id' => 209,
                'facility_id' => 31,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            48 => 
            array (
                'id' => 210,
                'facility_id' => 32,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            49 => 
            array (
                'id' => 211,
                'facility_id' => 33,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            50 => 
            array (
                'id' => 212,
                'facility_id' => 34,
                'hostel_id' => 23,
                'created_at' => '2013-07-01 01:29:06',
            ),
            51 => 
            array (
                'id' => 213,
                'facility_id' => 4,
                'hostel_id' => 24,
                'created_at' => '2013-07-18 06:47:45',
            ),
            52 => 
            array (
                'id' => 214,
                'facility_id' => 5,
                'hostel_id' => 24,
                'created_at' => '2013-07-18 06:47:45',
            ),
            53 => 
            array (
                'id' => 215,
                'facility_id' => 6,
                'hostel_id' => 24,
                'created_at' => '2013-07-18 06:47:45',
            ),
            54 => 
            array (
                'id' => 216,
                'facility_id' => 1,
                'hostel_id' => 24,
                'created_at' => '2013-07-18 06:47:45',
            ),
            55 => 
            array (
                'id' => 217,
                'facility_id' => 2,
                'hostel_id' => 24,
                'created_at' => '2013-07-18 06:47:45',
            ),
            56 => 
            array (
                'id' => 218,
                'facility_id' => 10,
                'hostel_id' => 24,
                'created_at' => '2013-07-18 06:47:45',
            ),
            57 => 
            array (
                'id' => 229,
                'facility_id' => 4,
                'hostel_id' => 26,
                'created_at' => '2013-07-18 23:56:24',
            ),
            58 => 
            array (
                'id' => 230,
                'facility_id' => 5,
                'hostel_id' => 26,
                'created_at' => '2013-07-18 23:56:24',
            ),
            59 => 
            array (
                'id' => 231,
                'facility_id' => 1,
                'hostel_id' => 26,
                'created_at' => '2013-07-18 23:56:24',
            ),
            60 => 
            array (
                'id' => 232,
                'facility_id' => 2,
                'hostel_id' => 26,
                'created_at' => '2013-07-18 23:56:24',
            ),
            61 => 
            array (
                'id' => 233,
                'facility_id' => 7,
                'hostel_id' => 26,
                'created_at' => '2013-07-18 23:56:24',
            ),
            62 => 
            array (
                'id' => 234,
                'facility_id' => 4,
                'hostel_id' => 27,
                'created_at' => '2013-07-19 00:14:20',
            ),
            63 => 
            array (
                'id' => 235,
                'facility_id' => 5,
                'hostel_id' => 27,
                'created_at' => '2013-07-19 00:14:20',
            ),
            64 => 
            array (
                'id' => 236,
                'facility_id' => 6,
                'hostel_id' => 27,
                'created_at' => '2013-07-19 00:14:20',
            ),
            65 => 
            array (
                'id' => 237,
                'facility_id' => 4,
                'hostel_id' => 28,
                'created_at' => '2013-07-22 07:06:27',
            ),
            66 => 
            array (
                'id' => 238,
                'facility_id' => 1,
                'hostel_id' => 28,
                'created_at' => '2013-07-22 07:06:27',
            ),
            67 => 
            array (
                'id' => 239,
                'facility_id' => 2,
                'hostel_id' => 28,
                'created_at' => '2013-07-22 07:06:27',
            ),
            68 => 
            array (
                'id' => 240,
                'facility_id' => 8,
                'hostel_id' => 28,
                'created_at' => '2013-07-22 07:06:27',
            ),
            69 => 
            array (
                'id' => 358,
                'facility_id' => 4,
                'hostel_id' => 29,
                'created_at' => '2013-07-23 06:40:06',
            ),
            70 => 
            array (
                'id' => 359,
                'facility_id' => 2,
                'hostel_id' => 29,
                'created_at' => '2013-07-23 06:40:06',
            ),
            71 => 
            array (
                'id' => 360,
                'facility_id' => 8,
                'hostel_id' => 29,
                'created_at' => '2013-07-23 06:40:06',
            ),
            72 => 
            array (
                'id' => 361,
                'facility_id' => 10,
                'hostel_id' => 29,
                'created_at' => '2013-07-23 06:40:06',
            ),
            73 => 
            array (
                'id' => 362,
                'facility_id' => 13,
                'hostel_id' => 29,
                'created_at' => '2013-07-23 06:40:06',
            ),
            74 => 
            array (
                'id' => 363,
                'facility_id' => 20,
                'hostel_id' => 29,
                'created_at' => '2013-07-23 06:40:06',
            ),
            75 => 
            array (
                'id' => 364,
                'facility_id' => 17,
                'hostel_id' => 30,
                'created_at' => '2013-07-23 06:40:06',
            ),
            76 => 
            array (
                'id' => 365,
                'facility_id' => 18,
                'hostel_id' => 29,
                'created_at' => '2013-07-23 06:40:06',
            ),
            77 => 
            array (
                'id' => 366,
                'facility_id' => 26,
                'hostel_id' => 30,
                'created_at' => '2013-07-23 06:40:06',
            ),
            78 => 
            array (
                'id' => 373,
                'facility_id' => 4,
                'hostel_id' => 30,
                'created_at' => '2013-07-24 01:14:13',
            ),
            79 => 
            array (
                'id' => 374,
                'facility_id' => 5,
                'hostel_id' => 30,
                'created_at' => '2013-07-24 01:14:14',
            ),
            80 => 
            array (
                'id' => 375,
                'facility_id' => 6,
                'hostel_id' => 30,
                'created_at' => '2013-07-24 01:14:14',
            ),
            81 => 
            array (
                'id' => 379,
                'facility_id' => 4,
                'hostel_id' => 31,
                'created_at' => '2013-07-26 01:25:33',
            ),
            82 => 
            array (
                'id' => 380,
                'facility_id' => 5,
                'hostel_id' => 31,
                'created_at' => '2013-07-26 01:25:33',
            ),
            83 => 
            array (
                'id' => 381,
                'facility_id' => 6,
                'hostel_id' => 31,
                'created_at' => '2013-07-26 01:25:33',
            ),
            84 => 
            array (
                'id' => 382,
                'facility_id' => 4,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            85 => 
            array (
                'id' => 383,
                'facility_id' => 5,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            86 => 
            array (
                'id' => 384,
                'facility_id' => 6,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            87 => 
            array (
                'id' => 385,
                'facility_id' => 1,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            88 => 
            array (
                'id' => 386,
                'facility_id' => 2,
                'hostel_id' => 30,
                'created_at' => '2013-08-27 21:53:30',
            ),
            89 => 
            array (
                'id' => 387,
                'facility_id' => 10,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            90 => 
            array (
                'id' => 388,
                'facility_id' => 11,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            91 => 
            array (
                'id' => 389,
                'facility_id' => 12,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            92 => 
            array (
                'id' => 390,
                'facility_id' => 13,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            93 => 
            array (
                'id' => 391,
                'facility_id' => 14,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            94 => 
            array (
                'id' => 392,
                'facility_id' => 20,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            95 => 
            array (
                'id' => 393,
                'facility_id' => 15,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            96 => 
            array (
                'id' => 394,
                'facility_id' => 16,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            97 => 
            array (
                'id' => 395,
                'facility_id' => 22,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            98 => 
            array (
                'id' => 396,
                'facility_id' => 23,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            99 => 
            array (
                'id' => 397,
                'facility_id' => 24,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            100 => 
            array (
                'id' => 398,
                'facility_id' => 25,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            101 => 
            array (
                'id' => 399,
                'facility_id' => 26,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            102 => 
            array (
                'id' => 400,
                'facility_id' => 27,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            103 => 
            array (
                'id' => 401,
                'facility_id' => 28,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            104 => 
            array (
                'id' => 402,
                'facility_id' => 31,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            105 => 
            array (
                'id' => 403,
                'facility_id' => 35,
                'hostel_id' => 32,
                'created_at' => '2013-08-27 21:53:30',
            ),
            106 => 
            array (
                'id' => 433,
                'facility_id' => 4,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            107 => 
            array (
                'id' => 434,
                'facility_id' => 5,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            108 => 
            array (
                'id' => 435,
                'facility_id' => 6,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            109 => 
            array (
                'id' => 436,
                'facility_id' => 2,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            110 => 
            array (
                'id' => 437,
                'facility_id' => 7,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            111 => 
            array (
                'id' => 438,
                'facility_id' => 9,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            112 => 
            array (
                'id' => 439,
                'facility_id' => 3,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            113 => 
            array (
                'id' => 440,
                'facility_id' => 10,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            114 => 
            array (
                'id' => 441,
                'facility_id' => 11,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            115 => 
            array (
                'id' => 442,
                'facility_id' => 12,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            116 => 
            array (
                'id' => 443,
                'facility_id' => 13,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            117 => 
            array (
                'id' => 444,
                'facility_id' => 19,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            118 => 
            array (
                'id' => 445,
                'facility_id' => 15,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            119 => 
            array (
                'id' => 446,
                'facility_id' => 16,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            120 => 
            array (
                'id' => 447,
                'facility_id' => 17,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            121 => 
            array (
                'id' => 448,
                'facility_id' => 18,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            122 => 
            array (
                'id' => 449,
                'facility_id' => 22,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            123 => 
            array (
                'id' => 450,
                'facility_id' => 23,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            124 => 
            array (
                'id' => 451,
                'facility_id' => 24,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            125 => 
            array (
                'id' => 452,
                'facility_id' => 25,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            126 => 
            array (
                'id' => 453,
                'facility_id' => 26,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            127 => 
            array (
                'id' => 454,
                'facility_id' => 27,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            128 => 
            array (
                'id' => 455,
                'facility_id' => 28,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            129 => 
            array (
                'id' => 456,
                'facility_id' => 30,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            130 => 
            array (
                'id' => 457,
                'facility_id' => 31,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            131 => 
            array (
                'id' => 458,
                'facility_id' => 32,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            132 => 
            array (
                'id' => 459,
                'facility_id' => 33,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            133 => 
            array (
                'id' => 460,
                'facility_id' => 34,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            134 => 
            array (
                'id' => 461,
                'facility_id' => 35,
                'hostel_id' => 33,
                'created_at' => '2013-08-28 02:17:00',
            ),
            135 => 
            array (
                'id' => 495,
                'facility_id' => 5,
                'hostel_id' => 41,
                'created_at' => '2013-09-12 18:56:13',
            ),
            136 => 
            array (
                'id' => 496,
                'facility_id' => 8,
                'hostel_id' => 41,
                'created_at' => '2013-09-12 18:56:13',
            ),
            137 => 
            array (
                'id' => 497,
                'facility_id' => 12,
                'hostel_id' => 41,
                'created_at' => '2013-09-12 18:56:13',
            ),
            138 => 
            array (
                'id' => 498,
                'facility_id' => 15,
                'hostel_id' => 41,
                'created_at' => '2013-09-12 18:56:13',
            ),
            139 => 
            array (
                'id' => 499,
                'facility_id' => 4,
                'hostel_id' => 20,
                'created_at' => '2013-09-14 02:35:19',
            ),
            140 => 
            array (
                'id' => 500,
                'facility_id' => 5,
                'hostel_id' => 20,
                'created_at' => '2013-09-14 02:35:19',
            ),
            141 => 
            array (
                'id' => 501,
                'facility_id' => 6,
                'hostel_id' => 20,
                'created_at' => '2013-09-14 02:35:19',
            ),
            142 => 
            array (
                'id' => 504,
                'facility_id' => 4,
                'hostel_id' => 43,
                'created_at' => '2013-09-25 11:53:38',
            ),
            143 => 
            array (
                'id' => 505,
                'facility_id' => 4,
                'hostel_id' => 44,
                'created_at' => '2013-09-29 15:26:42',
            ),
            144 => 
            array (
                'id' => 506,
                'facility_id' => 8,
                'hostel_id' => 44,
                'created_at' => '2013-09-29 15:26:42',
            ),
            145 => 
            array (
                'id' => 507,
                'facility_id' => 13,
                'hostel_id' => 44,
                'created_at' => '2013-09-29 15:26:42',
            ),
            146 => 
            array (
                'id' => 508,
                'facility_id' => 17,
                'hostel_id' => 44,
                'created_at' => '2013-09-29 15:26:42',
            ),
            147 => 
            array (
                'id' => 509,
                'facility_id' => 1,
                'hostel_id' => 6,
                'created_at' => '2014-08-04 13:54:15',
            ),
            148 => 
            array (
                'id' => 510,
                'facility_id' => 2,
                'hostel_id' => 6,
                'created_at' => '2014-08-04 13:54:15',
            ),
            149 => 
            array (
                'id' => 511,
                'facility_id' => 24,
                'hostel_id' => 6,
                'created_at' => '2014-08-04 13:54:15',
            ),
            150 => 
            array (
                'id' => 512,
                'facility_id' => 36,
                'hostel_id' => 6,
                'created_at' => '2014-08-04 13:54:15',
            ),
        ));
        
        
    }
}