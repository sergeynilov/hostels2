<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Transfers & Ground Transport',
                'created_at' => '2013-03-12 00:12:11',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Walking & Biking Tours',
                'created_at' => '2013-03-12 00:12:11',
            ),
            2 => 
            array (
                'id' => 6,
                'name' => 'Air, Helicopter & Balloon Tours',
                'created_at' => '2013-05-16 18:05:10',
            ),
            3 => 
            array (
                'id' => 7,
                'name' => 'Cruises, Sailing & Water Tours',
                'created_at' => '2013-05-16 18:05:33',
            ),
            4 => 
            array (
                'id' => 8,
                'name' => 'Cultural & Theme Tours',
                'created_at' => '2013-05-16 18:05:39',
            ),
            5 => 
            array (
                'id' => 9,
                'name' => 'Day Trips & Excursions',
                'created_at' => '2013-05-16 18:05:49',
            ),
            6 => 
            array (
                'id' => 10,
                'name' => 'Family Friendly',
                'created_at' => '2013-05-16 18:05:55',
            ),
            7 => 
            array (
                'id' => 11,
                'name' => 'Food, Wine & Nightlife',
                'created_at' => '2013-05-16 18:06:01',
            ),
            8 => 
            array (
                'id' => 12,
                'name' => 'Holiday & Seasonal Tours',
                'created_at' => '2013-05-16 18:06:06',
            ),
            9 => 
            array (
                'id' => 13,
                'name' => 'Luxury & Special Occasions',
                'created_at' => '2013-05-16 18:06:11',
            ),
            10 => 
            array (
                'id' => 14,
                'name' => 'Multi-day & Extended Tours',
                'created_at' => '2013-05-16 18:06:17',
            ),
            11 => 
            array (
                'id' => 15,
                'name' => 'Outdoor Activities',
                'created_at' => '2013-05-16 18:06:24',
            ),
            12 => 
            array (
                'id' => 16,
                'name' => 'Private & Custom Tours',
                'created_at' => '2013-05-16 18:06:29',
            ),
            13 => 
            array (
                'id' => 17,
                'name' => 'Shows, Concerts & Sports',
                'created_at' => '2013-05-16 18:06:34',
            ),
            14 => 
            array (
                'id' => 18,
                'name' => 'Sightseeing Tickets & Passes',
                'created_at' => '2013-05-16 18:06:39',
            ),
            15 => 
            array (
                'id' => 19,
                'name' => 'Tours & Sightseeing',
                'created_at' => '2013-05-16 18:06:44',
            ),
            16 => 
            array (
                'id' => 20,
                'name' => 'Water Sports',
                'created_at' => '2013-05-16 18:07:20',
            ),
            17 => 
            array (
                'id' => 21,
                'name' => 'Shopping & Fashion',
                'created_at' => '2013-05-16 18:07:26',
            ),
            18 => 
            array (
                'id' => 22,
                'name' => 'Theme Parks',
                'created_at' => '2013-06-18 08:44:03',
            ),
            19 => 
            array (
                'id' => 23,
                'name' => 'Shore Excursions',
                'created_at' => '2013-06-18 08:44:17',
            ),
            20 => 
            array (
                'id' => 24,
                'name' => 'Other',
                'created_at' => '2013-06-18 08:44:27',
            ),
        ));



        
    }
}