<?php

use Illuminate\Database\Seeder;

class PersonalHostelBookmarksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('personal_hostel_bookmarks')->delete();

        \DB::table('personal_hostel_bookmarks')->insert(array (

            0 =>
            array (
                'id' => 1,
                'user_id' => 5,
                'hostel_id' => 22,
                'created_at' => '2019-05-03 17:54:50',
            ),


            1 =>
            array (
                'id' => 2,
                'user_id' => 5,
                'hostel_id' => 29,
                'created_at' => '2019-05-03 17:54:50',
            ),


            2 =>
            array (
                'id' => 3,
                'user_id' => 4,
                'hostel_id' => 22,
                'created_at' => '2019-05-03 17:54:50',
            ),


            3 =>
            array (
                'id' => 4,
                'user_id' => 5,
                'hostel_id' => 10,
                'created_at' => '2019-05-03 17:54:50',
            ),

            4 =>
            array (
                'id' => 5,
                'user_id' => 4,
                'hostel_id' => 10,
                'created_at' => '2019-05-03 17:54:50',
            ),


        ));


    }
}
