<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'username' => 'Shawn Hadray',
                'email' => 'ShawnHadray@makevote.site.com',
                'password' => '$2y$10$cFehNMgODoHXdU8MDyg0M.R32X45a/gMdAB.BA7uM83S4.DyS6kdq',
                'remember_token' => NULL,
                'status' => 'N',
                'account_type' => 'I',
                'verified' => 1,
                'verification_token' => NULL,
                'first_name' => 'Shawn',
                'last_name' => 'Hadray',
                'phone' => '987-7543-916',
                'website' => 'shawn-hadray@make-vote.site.com',
                'notes' => '<strong>shawn_hadray</strong> Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?

Sed  ut perspiciatis unde omnis iste natus error sit <strong><i>voluptatem accusantium</i></strong>  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?
',
                'creator_id' => NULL,
                'activated_at' => NULL,
                'avatar' => 'shawn_hadray.jpg',
                'created_at' => '2019-04-29 11:03:50',
                'updated_at' => NULL,
            ),
            1 =>
            array (
                'id' => 2,
                'username' => 'Pat Longred',
                'email' => 'PatLongred@makevote.site.com',
                'password' => '$2y$10$2BjedNmS5GdfimdlpmuZeet.L6imw1UVNwdq3cGdPu22yfe9tQiIa',
                'remember_token' => NULL,
                'status' => 'A',
                'account_type' => 'I',
                'verified' => 1,
                'verification_token' => NULL,
                'first_name' => 'Pat',
                'last_name' => 'Longred',
                'phone' => '987-7543-916',
                'website' => 'shawn-hadray@make-vote.site.com',
                'notes' => '<strong>Pat Longred</strong> Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?

Sed  ut perspiciatis unde omnis iste natus error sit <strong><i>voluptatem accusantium</i></strong>  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?
',
                'creator_id' => NULL,
                'activated_at' => NULL,
                'avatar' => 'patlongred.jpg',
                'created_at' => '2019-04-29 11:03:50',
                'updated_at' => NULL,
            ),
            2 =>
            array (
                'id' => 3,
                'username' => 'Tony Black',
                'email' => 'TonyBlack@makevote.site.com',
                'password' => '$2y$10$/uI6jjQQxX.T6Hnd..QcF.uryPmIee0m9.BzgqI/qq80tFlfMmacK',
                'remember_token' => NULL,
                'status' => 'A',
                'account_type' => 'I',
                'verified' => 1,
                'verification_token' => NULL,
                'first_name' => 'Tony',
                'last_name' => 'Black',
                'phone' => '247-159-0976',
                'website' => 'tony-black@make-vote.site.com',
                'notes' => '<strong>Tony Black</strong> Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?

Sed  ut perspiciatis unde omnis iste natus error sit <strong><i>voluptatem accusantium</i></strong>  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?
',
                'creator_id' => NULL,
                'activated_at' => NULL,
                'avatar' => 'tony_black.jpg',
                'created_at' => '2019-04-29 11:03:50',
                'updated_at' => NULL,
            ),
            3 =>
            array (
                'id' => 4,
                'username' => 'Adam Lang',
                'email' => 'AdamLang@makevote.site.com',
                'password' => '$2y$10$AdBbGNdUPsRHpDT/80TnYOYZIvbe6POGh9N.OKBgd7WW.1rukmOqS',
                'remember_token' => NULL,
                'status' => 'I',
                'account_type' => 'I',
                'verified' => 1,
                'verification_token' => NULL,
                'first_name' => 'Adam',
                'last_name' => 'Lang',
                'phone' => '247-541-7172',
                'website' => 'adam-lang@make-vote.site.com',
                'notes' => '<strong>Adam Lang</strong> Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?

Sed  ut perspiciatis unde omnis iste natus error sit <strong><i>voluptatem accusantium</i></strong>  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?
',
                'creator_id' => NULL,
                'activated_at' => NULL,
                'avatar' => 'adam_lang.jpg',
                'created_at' => '2019-04-29 11:03:50',
                'updated_at' => NULL,
            ),
            4 =>
            array (
                'id' => 5,
                'username' => 'Admin',
                'email' => 'admin@mail.com',
                'password' => '$2y$10$vxBkNOEteK5GwP/j1VEDiutmR92XVW1ixSEEQ5ZMq6WPY4RkAO97S',
                'remember_token' => NULL,
                'status' => 'A',
                'account_type' => 'I',
                'verified' => 1,
                'verification_token' => NULL,
                'first_name' => 'Red',
                'last_name' => 'Song',
                'phone' => '247-541-7172',
                'website' => 'red-song@make-vote.site.com',
                'notes' => '<strong>Red Song</strong> Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?

Sed  ut perspiciatis unde omnis iste natus error sit <strong><i>voluptatem accusantium</i></strong>  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?
',
                'creator_id' => NULL,
                'activated_at' => NULL,
                'avatar' => '5.jpeg',
                'created_at' => '2018-03-25 12:39:36',
                'updated_at' => '2018-05-20 19:48:03',
            ),
            5 =>
            array (
                'id' => 6,
                'username' => 'hostels2_demo',
                'email' => 'hostels2_demo@hostels2.com',
                'password' => '$2y$10$Px3UpTb.kt.KQVOoFO7BreHPIbEeCBC/CNAzvnI3aBhFnLNrNcXym',
                'remember_token' => NULL,
                'status' => 'A',
                'account_type' => 'I',
                'verified' => 1,
                'verification_token' => NULL,
                'first_name' => 'Black',
                'last_name' => 'Book',
                'phone' => '284-921-7970',
                'website' => 'hostels2-demo@make-hostels2.site.com',
                'notes' => '<strong>Black Book</strong> Lorem  ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. Sed  ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?

Sed  ut perspiciatis unde omnis iste natus error sit <strong><i>voluptatem accusantium</i></strong>  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo  inventore veritatis et quasi architecto beatae vitae dicta sunt  explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut  odit aut fugit, sed quia consequuntur magni dolores eos qui ratione  voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum  quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam  eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam  corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse  quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo  voluptas nulla pariatur?
',
                'creator_id' => NULL,
                'activated_at' => NULL,
                'avatar' => '6.jpeg',
                'created_at' => '2019-04-29 11:03:50',
                'updated_at' => NULL,
            ),
            6 =>
            array (
                'id' => 7,
                'username' => 'Shaon Hahroy',
                'email' => 'customer1@hostels21-site.com',
                'password' => NULL,
                'remember_token' => NULL,
                'status' => 'A',
                'account_type' => 'I',
                'verified' => 0,
                'verification_token' => NULL,
                'first_name' => 'Shaon',
                'last_name' => 'Hahroy',
                'phone' => '7659879638765',
                'website' => 'http://shaon-hahroy@hostels21-site.com',
                'notes' => 'Some notes on <strong>Shaon Hahroy</strong>, who lorem <i>ipsum dolor sit</i> amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. 
lorem <i>ipsum dolor sit</i> amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
                'creator_id' => 2,
                'activated_at' => NULL,
                'avatar' => NULL,
                'created_at' => '2019-04-29 11:03:50',
                'updated_at' => NULL,
            ),
            7 =>
            array (
                'id' => 8,
                'username' => 'Black Adams',
                'email' => 'BlackAdams@hostels232.site.com',
                'password' => NULL,
                'remember_token' => NULL,
                'status' => 'A',
                'account_type' => 'I',
                'verified' => 0,
                'verification_token' => NULL,
                'first_name' => 'Black',
                'last_name' => 'Adams',
                'phone' => '7659289638745',
                'website' => 'http://customer2@hostels22-site.com',
                'notes' => 'Some notes on <strong>Black Adams</strong>, who lorem <i>ipsum dolor sit</i> amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. 
lorem <i>ipsum dolor sit</i> amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
                'creator_id' => 4,
                'activated_at' => NULL,
                'avatar' => NULL,
                'created_at' => '2019-04-29 11:03:50',
                'updated_at' => NULL,
            ),
            8 =>
            array (
                'id' => 9,
                'username' => 'Martha Lang',
                'email' => 'MarthaLang@hostels23-site.com',
                'password' => NULL,
                'remember_token' => NULL,
                'status' => 'N',
                'account_type' => 'I',
                'verified' => 0,
                'verification_token' => NULL,
                'first_name' => 'Martha',
                'last_name' => 'Lang',
                'phone' => '7659879638765',
                'website' => 'http://customer3@hostels23.site.com',
                'notes' => 'Some notes on <strong>Martha Lang</strong>, who lorem <i>ipsum dolor sit</i> amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum. 
lorem <i>ipsum dolor sit</i> amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim  veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint  occaecat cupidatat non proident, sunt in culpa qui officia deserunt  mollit anim id est laborum.',
                'creator_id' => 5,
                'activated_at' => NULL,
                'avatar' => NULL,
                'created_at' => '2019-04-29 11:03:50',
                'updated_at' => NULL,
            ),
        ));


    }
}
