<?php

use Illuminate\Database\Seeder;

class forumPostReportAbusesWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('forum_post_report_abuses')->delete();

        \DB::table('forum_post_report_abuses')->insert(array (
            0 =>
                array (
                    'id'                 => 1,
                    'user_id'            => 1,
                    'forum_post_id'      => 8,
                    'text'               => 'Report abuse # 1 content Lorem ipsum dolor ...',
                ),
        ));

        \DB::table('forum_post_report_abuses')->insert(array (
            1 =>
                array (
                    'id'                 => 2,
                    'user_id'            => 4,
                    'forum_post_id'      => 3,
                    'text'               => 'Report abuse # 2 content Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod  tempor incididunt ut labore et dolore magna aliqua...',
                ),
        ));

        \DB::table('forum_post_report_abuses')->insert(array (
            2 =>
                array (
                    'id'                 => 3,
                    'user_id'            => 2,
                    'forum_post_id'      => 5,
                    'text'               => 'Report abuse # 5.1 content Lorem ipsum dolor sit amet, consectetur adipiscing ...',
                ),
        ));

        \DB::table('forum_post_report_abuses')->insert(array (
            3 =>
                array (
                    'id'                 => 4,
                    'user_id'            => 5,
                    'forum_post_id'      => 5,
                    'text'               => 'Report abuse # 5.2 content Lorem ipsum dolor sit amet, consectetur adipiscing ...',
                ),
        ));

        \DB::table('forum_post_report_abuses')->insert(array (
            4 =>
                array (
                    'id'                 => 5,
                    'user_id'            => 1,
                    'forum_post_id'      => 5,
                    'text'               => 'Report abuse # 5.3 content Lorem ipsum dolor sit amet, consectetur adipiscing ...',
                ),
        ));

    }
}
