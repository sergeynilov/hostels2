<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('regions')->delete();

        \DB::table('regions')->insert(array (
            0 =>
            array (
                'id' => 1,
                'state_id' => 1,
                'name' => 'Hunter, Central & North Coasts',
                'slug' => 'hunter-central-north-coasts',
                'active' => 'A',
            ),
            1 =>
            array (
                'id' => 2,
                'state_id' => 1,
                'name' => 'Illawarra & South Coast',
                'slug' => 'illawarra-south-coast',
                'active' => 'I',
            ),
            2 =>
            array (
                'id' => 3,
                'state_id' => 1,
                'name' => 'Regional NSW',
                'slug' => 'regional-nsw',
                'active' => 'I',
            ),
            3 =>
            array (
                'id' => 4,
                'state_id' => 1,
                'name' => 'Sydney Region',
                'slug' => 'sydney-region',
                'active' => 'I',
            ),
            4 =>
            array (
                'id' => 5,
                'state_id' => 2,
                'name' => 'Northern Territory',
                'slug' => 'northern-territory',
                'active' => 'A',
            ),
            5 =>
            array (
                'id' => 6,
                'state_id' => 3,
                'name' => 'Brisbane Region',
                'slug' => 'brisbane-region',
                'active' => 'I',
            ),
            6 =>
            array (
                'id' => 7,
                'state_id' => 3,
                'name' => 'Central & West',
                'slug' => 'central-west',
                'active' => 'A',
            ),
            7 =>
            array (
                'id' => 8,
                'state_id' => 3,
                'name' => 'Coastal',
                'slug' => 'coastal',
                'active' => 'I',
            ),
            8 =>
            array (
                'id' => 9,
                'state_id' => 3,
                'name' => 'South East',
                'slug' => 'south-east',
                'active' => 'I',
            ),
            9 =>
            array (
                'id' => 10,
                'state_id' => 4,
                'name' => 'Adelaide',
                'slug' => 'adelaide',
                'active' => 'A',
            ),
            10 =>
            array (
                'id' => 11,
                'state_id' => 4,
                'name' => 'Far North',
                'slug' => 'far-north',
                'active' => 'A',
            ),
            11 =>
            array (
                'id' => 12,
                'state_id' => 4,
                'name' => 'Mid North',
                'slug' => 'mid-north',
                'active' => 'A',
            ),
            12 =>
            array (
                'id' => 13,
                'state_id' => 4,
                'name' => 'South',
                'slug' => 'south',
                'active' => 'I',
            ),
            13 =>
            array (
                'id' => 14,
                'state_id' => 4,
                'name' => 'Spencer Gulf & West Coast',
                'slug' => 'spencer-gulf-west-coast',
                'active' => 'A',
            ),
            14 =>
            array (
                'id' => 15,
                'state_id' => 5,
                'name' => 'Tasmania',
                'slug' => 'tasmania',
                'active' => 'A',
            ),
            15 =>
            array (
                'id' => 16,
                'state_id' => 6,
                'name' => 'Melbourne Region',
                'slug' => 'melbourne-region',
                'active' => 'A',
            ),
            16 =>
            array (
                'id' => 17,
                'state_id' => 6,
                'name' => 'Northern Victoria',
                'slug' => 'northern-victoria',
                'active' => 'A',
            ),
            17 =>
            array (
                'id' => 18,
                'state_id' => 6,
                'name' => 'South Eastern Victoria',
                'slug' => 'south-eastern-victoria',
                'active' => 'A',
            ),
            18 =>
            array (
                'id' => 19,
                'state_id' => 6,
                'name' => 'South Western Victoria',
                'slug' => 'south-western-victoria',
                'active' => 'A',
            ),
            19 =>
            array (
                'id' => 20,
                'state_id' => 7,
                'name' => 'East Region',
                'slug' => 'east-region',
                'active' => 'A',
            ),
            20 =>
            array (
                'id' => 21,
                'state_id' => 7,
                'name' => 'North Region',
                'slug' => 'north-region',
                'active' => 'A',
            ),
            21 =>
            array (
                'id' => 22,
                'state_id' => 7,
                'name' => 'Perth Region',
                'slug' => 'perth-region',
                'active' => 'A',
            ),
            22 =>
            array (
                'id' => 23,
                'state_id' => 7,
                'name' => 'Southern',
                'slug' => 'southern',
                'active' => 'A',
            ),
            23 =>
            array (
                'id' => 24,
                'state_id' => 8,
                'name' => 'Canberra',
                'slug' => 'canberra',
                'active' => 'I',
            ),
        ));


    }
}
