<?php

use Illuminate\Database\Seeder;

class FacilitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('facilities')->delete();

        \DB::table('facilities')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'ATM Machine',
                'descr' => 'ATM Machine',
                'created_at' => '2013-03-12 00:12:11',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Bar',
                'descr' => 'Bar Service Lorem ipsum dolor sit amet, consectetur adipiscing ',
                'created_at' => '2013-03-12 00:12:11',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Board Games',
                'descr' => 'Board Games',
                'created_at' => '2013-03-12 00:12:11',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => '24 Hour Reception',
                'descr' => '24 Hour Reception Lorem ipsum dolor sit amet, consectetur adipiscing ...',
                'created_at' => '2013-03-25 10:38:43',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => '24 Hour Security',
                'descr' => '24 Hour Security description...',
                'created_at' => '2013-05-16 23:22:34',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Air Conditioning',
                'descr' => 'Air Conditioning',
                'created_at' => '2013-05-16 23:22:41',
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'BBQ',
                'descr' => 'BBQ',
                'created_at' => '2013-05-16 23:22:47',
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'Bicycle hire',
                'descr' => 'Bicycle hire',
                'created_at' => '2013-05-16 23:22:53',
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'Bicycle parking',
                'descr' => 'Bicycle parking',
                'created_at' => '2013-05-16 23:22:59',
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'Book exchange',
                'descr' => 'Book exchange',
                'created_at' => '2013-05-16 23:23:04',
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'Breakfast',
                'descr' => 'Breakfast',
                'created_at' => '2013-05-16 23:23:15',
            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'Cable TV',
                'descr' => 'Cable TV',
                'created_at' => '2013-05-16 23:23:20',
            ),
            12 =>
            array (
                'id' => 13,
                'name' => 'Café',
                'descr' => 'Café',
                'created_at' => '2013-05-16 23:23:25',
            ),
            13 =>
            array (
                'id' => 14,
                'name' => 'Card phones',
                'descr' => 'Card phones',
                'created_at' => '2013-05-16 23:23:32',
            ),
            14 =>
            array (
                'id' => 15,
                'name' => 'Common Room',
                'descr' => 'Common Room',
                'created_at' => '2013-05-16 23:23:37',
            ),
            15 =>
            array (
                'id' => 16,
                'name' => 'DVDs',
                'descr' => 'DVDs',
                'created_at' => '2013-05-16 23:23:41',
            ),
            16 =>
            array (
                'id' => 17,
                'name' => 'Elevator',
                'descr' => 'Elevator',
                'created_at' => '2013-05-16 23:23:45',
            ),
            17 =>
            array (
                'id' => 18,
                'name' => 'Fax Service',
                'descr' => 'Fax Service',
                'created_at' => '2013-05-16 23:23:50',
            ),
            18 =>
            array (
                'id' => 19,
                'name' => 'City Maps',
                'descr' => 'City Maps Lorem ipsum dolor sit amet, consectetur adipiscing e',
                'created_at' => '2013-05-16 23:23:56',
            ),
            19 =>
            array (
                'id' => 20,
                'name' => 'CCTV Security',
                'descr' => 'CCTV Security',
                'created_at' => '2013-05-16 23:24:06',
            ),
            20 =>
            array (
                'id' => 21,
                'name' => 'Pool',
                'descr' => 'Pool',
                'created_at' => '2013-05-16 23:24:11',
            ),
            21 =>
            array (
                'id' => 22,
                'name' => 'Hair Dryers',
                'descr' => 'Hair Dryers',
                'created_at' => '2013-05-16 23:24:18',
            ),
            22 =>
            array (
                'id' => 23,
                'name' => 'Hot Showers',
                'descr' => 'Hot Showers',
                'created_at' => '2013-05-16 23:24:23',
            ),
            23 =>
            array (
                'id' => 24,
                'name' => 'House Keeping',
                'descr' => 'House Keeping',
                'created_at' => '2013-05-16 23:24:28',
            ),
            24 =>
            array (
                'id' => 25,
                'name' => 'Key Card Access',
                'descr' => 'Key Card Access Lorem ipsum dolor sit amet, consectetur adipiscing e',
                'created_at' => '2013-05-16 23:24:34',
            ),
            25 =>
            array (
                'id' => 26,
                'name' => 'Kitchen',
                'descr' => 'Kitchen',
                'created_at' => '2013-05-16 23:24:38',
            ),
            26 =>
            array (
                'id' => 27,
                'name' => 'Laundry',
                'descr' => 'Laundry',
                'created_at' => '2013-05-16 23:24:44',
            ),
            27 =>
            array (
                'id' => 28,
                'name' => 'Lockers',
                'descr' => 'Lockers',
                'created_at' => '2013-05-16 23:25:04',
            ),
            28 =>
            array (
                'id' => 29,
                'name' => 'Outdoor Terrace',
                'descr' => 'Outdoor Terrace',
                'created_at' => '2013-05-16 23:25:11',
            ),
            29 =>
            array (
                'id' => 30,
                'name' => 'Postal Service',
                'descr' => 'Postal Service',
                'created_at' => '2013-05-16 23:25:16',
            ),
            30 =>
            array (
                'id' => 31,
                'name' => 'Reading Light',
                'descr' => 'Reading Light',
                'created_at' => '2013-05-16 23:25:22',
            ),
            31 =>
            array (
                'id' => 32,
                'name' => 'Restaurant',
                'descr' => 'Restaurant Lorem ipsum dolor sit amet, consectetur adipiscing e',
                'created_at' => '2013-05-16 23:25:27',
            ),
            32 =>
            array (
                'id' => 33,
                'name' => 'Safety Deposit Box',
                'descr' => 'Safety Deposit Box',
                'created_at' => '2013-05-16 23:25:36',
            ),
            33 =>
            array (
                'id' => 34,
                'name' => 'Tours Travel Desk',
                'descr' => 'Tours Travel Desk Lorem ipsum dolor sit amet, consectetur adipiscing e',
                'created_at' => '2013-05-16 23:25:51',
            ),
            34 =>
            array (
                'id' => 35,
                'name' => 'Washing Machines',
                'descr' => 'Washing Machines',
                'created_at' => '2013-05-16 23:26:03',
            ),
            35 =>
            array (
                'id' => 36,
                'name' => 'Phone Charging',
                'descr' => 'Phone Charging',
                'created_at' => '2013-06-18 07:58:09',
            ),
        ));


    }
}
