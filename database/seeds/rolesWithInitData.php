<?php

use Illuminate\Database\Seeder;

class rolesWithInitData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->delete();


    \DB::table('roles')->insert(array (
            0 =>
                array (
                    'id' => ACCESS_ROLE_ADMIN,
                    'name' => ACCESS_ROLE_ADMIN_LABEL,
                    'display_name' => 'Admin',
                    'description' => 'Administrator. Can do all operations in system',
                    'created_at' => '2019-04-29 11:03:50',
                ),
            1 =>
                array (
                    'id' => ACCESS_ROLE_MANAGER,
                    'name' => ACCESS_ROLE_MANAGER_LABEL,
                    'display_name' => 'Manager',
                    'description' => 'Manager description. Can do all operations in frontend and CRUD for Hostels/CMS items in Backend...',
                    'created_at' => '2019-04-29 11:03:50',
                ),
            2 =>
                array (
                    'id' => ACCESS_ROLE_CUSTOMER,
                    'name' => ACCESS_ROLE_CUSTOMER_LABEL,
                    'display_name' => 'Customer',
                    'description' => 'Customer description. Can do all operations in frontend...',
                    'created_at' => '2019-04-29 11:03:50',
                ),
        ));

    }
}
