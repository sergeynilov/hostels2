<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('messages')->delete();
        
        \DB::table('messages')->insert(array (
            0 => 
            array (
                'id' => 1,
                'from' => 1,
                'to' => 2,
                'text' => '1:Shawn Hadray=>2:Pat Longred = Atque rerum ex maxime eos.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            1 => 
            array (
                'id' => 2,
                'from' => 1,
                'to' => 3,
                'text' => '1:Shawn Hadray=>3:Tony Black = Occaecati id laudantium et. Commodi exercitationem sit dignissimos dolor.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            2 => 
            array (
                'id' => 3,
                'from' => 1,
                'to' => 4,
                'text' => '1:Shawn Hadray=>4:Adam Lang = Consequatur odio et eum maxime quia minus.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            3 => 
            array (
                'id' => 4,
                'from' => 1,
                'to' => 5,
                'text' => '1:Shawn Hadray=>5:Admin = Corporis inventore nobis dicta sequi.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            4 => 
            array (
                'id' => 5,
                'from' => 1,
                'to' => 6,
                'text' => '1:Shawn Hadray=>6:hostels2_demo = Vero animi nostrum ut magni.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            5 => 
            array (
                'id' => 6,
                'from' => 1,
                'to' => 7,
                'text' => '1:Shawn Hadray=>7:Shaon Hahroy = Omnis laborum aut architecto amet.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            6 => 
            array (
                'id' => 7,
                'from' => 1,
                'to' => 8,
                'text' => '1:Shawn Hadray=>8:Black Adams = Velit deleniti accusantium ex quo et et in atque. Est dolor recusandae ut ut sint itaque.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            7 => 
            array (
                'id' => 8,
                'from' => 1,
                'to' => 9,
                'text' => '1:Shawn Hadray=>9:Martha Lang = Impedit libero.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:11',
            ),
            8 => 
            array (
                'id' => 9,
                'from' => 2,
                'to' => 1,
                'text' => '2:Pat Longred=>1:Shawn Hadray = Mollitia qui ad sed.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            9 => 
            array (
                'id' => 10,
                'from' => 2,
                'to' => 3,
                'text' => '2:Pat Longred=>3:Tony Black = Minima eos.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            10 => 
            array (
                'id' => 11,
                'from' => 2,
                'to' => 4,
                'text' => '2:Pat Longred=>4:Adam Lang = Sit suscipit cum voluptatibus quasi sunt minus placeat quia.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            11 => 
            array (
                'id' => 12,
                'from' => 2,
                'to' => 5,
                'text' => '2:Pat Longred=>5:Admin = Provident dolores non nulla velit.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            12 => 
            array (
                'id' => 13,
                'from' => 2,
                'to' => 6,
                'text' => '2:Pat Longred=>6:hostels2_demo = At accusamus.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            13 => 
            array (
                'id' => 14,
                'from' => 2,
                'to' => 7,
                'text' => '2:Pat Longred=>7:Shaon Hahroy = Ipsa molestias.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            14 => 
            array (
                'id' => 15,
                'from' => 2,
                'to' => 8,
                'text' => '2:Pat Longred=>8:Black Adams = Quis quia omnis minus. Et cumque et nobis dolorem.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:11',
            ),
            15 => 
            array (
                'id' => 16,
                'from' => 2,
                'to' => 9,
                'text' => '2:Pat Longred=>9:Martha Lang = Eos et repellat natus earum vero sequi et.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            16 => 
            array (
                'id' => 17,
                'from' => 3,
                'to' => 1,
                'text' => '3:Tony Black=>1:Shawn Hadray = Ipsum.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            17 => 
            array (
                'id' => 18,
                'from' => 3,
                'to' => 2,
                'text' => '3:Tony Black=>2:Pat Longred = Facilis est qui eum.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            18 => 
            array (
                'id' => 19,
                'from' => 3,
                'to' => 4,
                'text' => '3:Tony Black=>4:Adam Lang = Ipsam inventore modi occaecati doloribus possimus.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            19 => 
            array (
                'id' => 20,
                'from' => 3,
                'to' => 5,
                'text' => '3:Tony Black=>5:Admin = Unde veritatis quibusdam vel. Et ut cum ipsum ad sapiente et facilis.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            20 => 
            array (
                'id' => 21,
                'from' => 3,
                'to' => 6,
                'text' => '3:Tony Black=>6:hostels2_demo = Exercitationem expedita cupiditate libero commodi perspiciatis enim quas.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            21 => 
            array (
                'id' => 22,
                'from' => 3,
                'to' => 7,
                'text' => '3:Tony Black=>7:Shaon Hahroy = Et qui nostrum sunt.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            22 => 
            array (
                'id' => 23,
                'from' => 3,
                'to' => 8,
                'text' => '3:Tony Black=>8:Black Adams = Totam reiciendis et harum illum unde et. Aut in animi omnis.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            23 => 
            array (
                'id' => 24,
                'from' => 3,
                'to' => 9,
                'text' => '3:Tony Black=>9:Martha Lang = In.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            24 => 
            array (
                'id' => 25,
                'from' => 4,
                'to' => 1,
                'text' => '4:Adam Lang=>1:Shawn Hadray = Aut et corrupti vel tempore sapiente magni.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            25 => 
            array (
                'id' => 26,
                'from' => 4,
                'to' => 2,
                'text' => '4:Adam Lang=>2:Pat Longred = At sunt nihil omnis enim sit provident.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            26 => 
            array (
                'id' => 27,
                'from' => 4,
                'to' => 3,
                'text' => '4:Adam Lang=>3:Tony Black = Sequi sit deleniti nihil. Similique quisquam quo beatae unde id quam.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            27 => 
            array (
                'id' => 28,
                'from' => 4,
                'to' => 5,
                'text' => '4:Adam Lang=>5:Admin = Nulla odio aut dolor dignissimos. Quia distinctio ut non deserunt.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            28 => 
            array (
                'id' => 29,
                'from' => 4,
                'to' => 6,
                'text' => '4:Adam Lang=>6:hostels2_demo = Et nesciunt rerum expedita.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            29 => 
            array (
                'id' => 30,
                'from' => 4,
                'to' => 7,
                'text' => '4:Adam Lang=>7:Shaon Hahroy = Quaerat sunt voluptatum.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            30 => 
            array (
                'id' => 31,
                'from' => 4,
                'to' => 8,
                'text' => '4:Adam Lang=>8:Black Adams = Asperiores.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            31 => 
            array (
                'id' => 32,
                'from' => 4,
                'to' => 9,
                'text' => '4:Adam Lang=>9:Martha Lang = Est ducimus quam quis est ad. Autem sed natus natus ut.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            32 => 
            array (
                'id' => 33,
                'from' => 5,
                'to' => 1,
                'text' => '5:Admin=>1:Shawn Hadray = Quibusdam dolor vel ipsam eveniet et.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            33 => 
            array (
                'id' => 34,
                'from' => 5,
                'to' => 2,
                'text' => '5:Admin=>2:Pat Longred = Quas.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            34 => 
            array (
                'id' => 35,
                'from' => 5,
                'to' => 3,
                'text' => '5:Admin=>3:Tony Black = Illo enim sint et et.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            35 => 
            array (
                'id' => 36,
                'from' => 5,
                'to' => 4,
                'text' => '5:Admin=>4:Adam Lang = Voluptatum et.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            36 => 
            array (
                'id' => 37,
                'from' => 5,
                'to' => 6,
                'text' => '5:Admin=>6:hostels2_demo = Quam.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            37 => 
            array (
                'id' => 38,
                'from' => 5,
                'to' => 7,
                'text' => '5:Admin=>7:Shaon Hahroy = Et ut consectetur sint non aliquam.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            38 => 
            array (
                'id' => 39,
                'from' => 5,
                'to' => 8,
                'text' => '5:Admin=>8:Black Adams = Soluta saepe at impedit.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            39 => 
            array (
                'id' => 40,
                'from' => 5,
                'to' => 9,
                'text' => '5:Admin=>9:Martha Lang = Quam quasi magni iure nulla libero fuga nulla.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            40 => 
            array (
                'id' => 41,
                'from' => 6,
                'to' => 1,
                'text' => '6:hostels2_demo=>1:Shawn Hadray = Omnis excepturi voluptatem tenetur reprehenderit ad.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            41 => 
            array (
                'id' => 42,
                'from' => 6,
                'to' => 2,
                'text' => '6:hostels2_demo=>2:Pat Longred = Magni fuga sed ea voluptas quo quaerat aut.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            42 => 
            array (
                'id' => 43,
                'from' => 6,
                'to' => 3,
                'text' => '6:hostels2_demo=>3:Tony Black = Repellendus dolore voluptatem dolor.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            43 => 
            array (
                'id' => 44,
                'from' => 6,
                'to' => 4,
                'text' => '6:hostels2_demo=>4:Adam Lang = Autem.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            44 => 
            array (
                'id' => 45,
                'from' => 6,
                'to' => 5,
                'text' => '6:hostels2_demo=>5:Admin = Vero adipisci cumque qui vero.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            45 => 
            array (
                'id' => 46,
                'from' => 6,
                'to' => 7,
                'text' => '6:hostels2_demo=>7:Shaon Hahroy = Fuga reprehenderit.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            46 => 
            array (
                'id' => 47,
                'from' => 6,
                'to' => 8,
                'text' => '6:hostels2_demo=>8:Black Adams = Sunt mollitia rerum eius ut illum et.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            47 => 
            array (
                'id' => 48,
                'from' => 6,
                'to' => 9,
                'text' => '6:hostels2_demo=>9:Martha Lang = Ad omnis dolorem.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            48 => 
            array (
                'id' => 49,
                'from' => 7,
                'to' => 1,
                'text' => '7:Shaon Hahroy=>1:Shawn Hadray = Temporibus quis quibusdam eius eos id pariatur nulla.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            49 => 
            array (
                'id' => 50,
                'from' => 7,
                'to' => 2,
                'text' => '7:Shaon Hahroy=>2:Pat Longred = Eos.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            50 => 
            array (
                'id' => 51,
                'from' => 7,
                'to' => 3,
                'text' => '7:Shaon Hahroy=>3:Tony Black = Ipsam saepe tempore.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            51 => 
            array (
                'id' => 52,
                'from' => 7,
                'to' => 4,
                'text' => '7:Shaon Hahroy=>4:Adam Lang = Est.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            52 => 
            array (
                'id' => 53,
                'from' => 7,
                'to' => 5,
                'text' => '7:Shaon Hahroy=>5:Admin = Ab aut perferendis assumenda omnis eos earum.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            53 => 
            array (
                'id' => 54,
                'from' => 7,
                'to' => 6,
                'text' => '7:Shaon Hahroy=>6:hostels2_demo = Iste non eaque.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            54 => 
            array (
                'id' => 55,
                'from' => 7,
                'to' => 8,
                'text' => '7:Shaon Hahroy=>8:Black Adams = Quae deleniti rerum in dolorem cupiditate possimus.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            55 => 
            array (
                'id' => 56,
                'from' => 7,
                'to' => 9,
                'text' => '7:Shaon Hahroy=>9:Martha Lang = Repellendus a saepe laudantium est repudiandae recusandae. Consequatur qui aut maiores.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            56 => 
            array (
                'id' => 57,
                'from' => 8,
                'to' => 1,
                'text' => '8:Black Adams=>1:Shawn Hadray = Qui adipisci recusandae aut amet odio.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            57 => 
            array (
                'id' => 58,
                'from' => 8,
                'to' => 2,
                'text' => '8:Black Adams=>2:Pat Longred = Et recusandae aperiam qui.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            58 => 
            array (
                'id' => 59,
                'from' => 8,
                'to' => 3,
                'text' => '8:Black Adams=>3:Tony Black = Eos aut voluptas sint illo.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            59 => 
            array (
                'id' => 60,
                'from' => 8,
                'to' => 4,
                'text' => '8:Black Adams=>4:Adam Lang = Ut dolores aliquid harum. Vitae est nulla quo.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            60 => 
            array (
                'id' => 61,
                'from' => 8,
                'to' => 5,
                'text' => '8:Black Adams=>5:Admin = Itaque officiis.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            61 => 
            array (
                'id' => 62,
                'from' => 8,
                'to' => 6,
                'text' => '8:Black Adams=>6:hostels2_demo = Vel ullam et rerum et sint.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            62 => 
            array (
                'id' => 63,
                'from' => 8,
                'to' => 7,
                'text' => '8:Black Adams=>7:Shaon Hahroy = Illum numquam iusto et reiciendis incidunt minus.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            63 => 
            array (
                'id' => 64,
                'from' => 8,
                'to' => 9,
                'text' => '8:Black Adams=>9:Martha Lang = Ut.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            64 => 
            array (
                'id' => 65,
                'from' => 9,
                'to' => 1,
                'text' => '9:Martha Lang=>1:Shawn Hadray = Ad repudiandae et quos.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            65 => 
            array (
                'id' => 66,
                'from' => 9,
                'to' => 2,
                'text' => '9:Martha Lang=>2:Pat Longred = Aut id nam et ut. Enim cupiditate sed rerum neque.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            66 => 
            array (
                'id' => 67,
                'from' => 9,
                'to' => 3,
                'text' => '9:Martha Lang=>3:Tony Black = Et eos delectus.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            67 => 
            array (
                'id' => 68,
                'from' => 9,
                'to' => 4,
                'text' => '9:Martha Lang=>4:Adam Lang = Omnis.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            68 => 
            array (
                'id' => 69,
                'from' => 9,
                'to' => 5,
                'text' => '9:Martha Lang=>5:Admin = Quae ad alias consequatur accusamus quia quia.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
            69 => 
            array (
                'id' => 70,
                'from' => 9,
                'to' => 6,
                'text' => '9:Martha Lang=>6:hostels2_demo = Consectetur molestiae error facere quis.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            70 => 
            array (
                'id' => 71,
                'from' => 9,
                'to' => 7,
                'text' => '9:Martha Lang=>7:Shaon Hahroy = Consequatur rem cum quo aut deleniti tempora.',
                'read' => 0,
                'created_at' => '2019-04-29 11:04:12',
            ),
            71 => 
            array (
                'id' => 72,
                'from' => 9,
                'to' => 8,
                'text' => '9:Martha Lang=>8:Black Adams = Ullam facere hic ex architecto molestiae corporis. Architecto quas iure in.',
                'read' => 1,
                'created_at' => '2019-04-29 11:04:12',
            ),
        ));
        
        
    }
}