<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('states')->delete();

        \DB::table('states')->insert(array (
            0 =>
            array (
                'id' => 1,
                'code' => 'NSW',
                'name' => 'New South Wales',
                'slug' => 'new-south-wales',
                'active' => 'I',
            ),
            1 =>
            array (
                'id' => 2,
                'code' => 'NT',
                'name' => 'Northern Territory',
                'slug' => 'northern-territory',
                'active' => 'I',
            ),
            2 =>
            array (
                'id' => 3,
                'code' => 'Qld',
                'name' => 'Queensland',
                'slug' => 'queensland',
                'active' => 'I',
            ),
            3 =>
            array (
                'id' => 4,
                'code' => 'SA',
                'name' => 'South Australia',
                'slug' => 'south-australia',
                'active' => 'A',
            ),
            4 =>
            array (
                'id' => 5,
                'code' => 'TAS',
                'name' => 'Tasmania',
                'slug' => 'tasmania',
                'active' => 'I',
            ),
            5 =>
            array (
                'id' => 6,
                'code' => 'VIC',
                'name' => 'Victoria',
                'slug' => 'victoria',
                'active' => 'A',
            ),
            6 =>
            array (
                'id' => 7,
                'code' => 'WA',
                'name' => 'Western Australia',
                'slug' => 'western-australia',
                'active' => 'A',
            ),
            7 =>
            array (
                'id' => 8,
                'code' => 'ACT',
                'name' => 'Australian Capital Territory',
                'slug' => 'australian-capital-territory',
                'active' => 'A',
            ),
        ));


    }
}
