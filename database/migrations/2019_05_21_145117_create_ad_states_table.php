<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('states', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 3)->unique();
			$table->string('name', 50)->unique();
			$table->string('slug', 55)->unique();
            $table->enum('active', ['A', 'I'])->default('I')->comment(' A=>Active, I=>Inactive');
			$table->index(['active','code'], 'fk_bp_states_active_code_ixd');
			$table->index(['active','slug'], 'fk_bp_states_active_slug_ixd');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('states');
	}

}
