<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdHostelExtraDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hostel_extra_details', function(Blueprint $table)
		{

			$table->increments('id');
            $table->integer('hostel_id')->unsigned();
            $table->foreign('hostel_id')->references('id')->on('hostels')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->tinyInteger('num_beds')->unsigned()->nullable();
			$table->tinyInteger('min_nights')->unsigned()->nullable();
			$table->tinyInteger('max_nights')->unsigned()->nullable();
            $table->enum('bedsheets', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
//            $table->enum('bedsheets', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('towels', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('parking', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('airport_train', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('luggage', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('coed_dorm', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('bathroom', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('credit_cards', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('nonsmoking_rooms', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('smoke_free', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('pets_allowed', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('service_animals', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('wheelchair', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('internet_computers', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('wireless_internet', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('checkout', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->time('reception_hours_start')->default('12:00');
            $table->time('reception_hours_end')->default('12:00');

			$table->timestamp('created_at')->useCurrent();
		});

		//HostelExtraDetailsTableSeeder
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hostel_extra_details');
	}

}
