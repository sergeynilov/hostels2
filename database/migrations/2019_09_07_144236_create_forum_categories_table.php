<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_categories', function (Blueprint $table) {
            $table->smallIncrements('id');      //smallInteger
            $table->string('title', 100)->unique();
            $table->string('slug', 260)->unique();

            $table->mediumText('description');

            $table->integer('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->boolean('published')->default(false);
            $table->string('image', 100)->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->index(['created_at'], 'forum_categories_created_at_index');

            $table->index(['published', 'title', 'creator_id'], 'forum_categories_id_published_creator_title_index');
        });
//        Artisan::call('db:seed', array('--class' => 'forumCategoriesWithInitData'));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_categories');
    }
}
