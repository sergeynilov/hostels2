<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('from')->unsigned();
            $table->foreign('from')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');

//			$table->integer('from')->unsigned();
//			$table->integer('to')->unsigned()->index('messages_to_foreign');

            $table->integer('to')->unsigned();
            $table->foreign('to')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->text('text', 65535);
			$table->boolean('read')->default(0);
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->index('messages_created_at_index');
			$table->index(['read','to'], 'ind_messages_read_to');
			$table->index(['from','to','read'], 'ind_messages_from_read_to');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
