<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdHostelInqueriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hostel_inqueries', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('hostel_id')->unsigned();
            $table->foreign('hostel_id')->references('id')->on('hostels')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->integer('creator_id')->unsigned()->nullable()->index('hostel_inqueries_creator_id_foreign');
			$table->string('email', 50);
			$table->string('full_name', 100);
			$table->string('phone', 50);
			$table->text('info', 65535);
			$table->date('start_date');
			$table->date('end_date')->nullable();
			$table->enum('request_callback', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
			$table->enum('status', ['N','A','O','C'])->default('N')->comment(' N=>New, A=>Accepted, O=> Completed, C => Cancelled ');

			$table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->index(['status','email'], 'fk_bp_hostel_inquery_status_email_ixd');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hostel_inqueries');
	}

}
