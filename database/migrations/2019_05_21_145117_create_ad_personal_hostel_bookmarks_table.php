<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdPersonalHostelBookmarksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */

	// create_ad_personal_hostel_bookmarks_table
	public function up()
	{
		Schema::create('personal_hostel_bookmarks', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->integer('hostel_id')->unsigned();
            $table->foreign('hostel_id')->references('id')->on('hostels')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->unique(['user_id','hostel_id'], 'personal_options_hostel_id_name_unique');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personal_hostel_bookmarks');
	}

}
