<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumPostWatchedByUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_posts_watched_by_user', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->bigInteger('forum_post_id')->unsigned();
            $table->foreign('forum_post_id')->references('id')->on('forum_posts')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->timestamp('created_at')->useCurrent();

            $table->index(['user_id', 'forum_post_id'], 'forum_posts_watched_by_user_user_id_forum_post_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_posts_watched_by_user');
    }
}
