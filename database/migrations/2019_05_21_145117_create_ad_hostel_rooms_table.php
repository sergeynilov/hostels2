<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdHostelRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hostel_rooms', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('hostel_id')->unsigned();
            $table->foreign('hostel_id')->references('id')->on('hostels')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->integer('type');
			$table->decimal('price', 7);
            $table->enum('is_dorm', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
            $table->enum('is_private', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');

            $table->timestamp('created_at')->useCurrent();
			$table->unique(['hostel_id','type'], 'hostel_room_all');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hostel_rooms');
	}

}
