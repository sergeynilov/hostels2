<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumPostReportAbusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_post_report_abuses', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->bigInteger('forum_post_id')->unsigned();
            $table->foreign('forum_post_id')->references('id')->on('forum_posts')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->string('text', 255);

            $table->timestamp('created_at')->useCurrent();
            $table->index(['created_at'], 'forum_post_report_abuses_created_at_index');

            $table->index(['user_id', 'text'], 'forum_post_report_abuses_user_id_text_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_post_report_abuses');
    }
}
