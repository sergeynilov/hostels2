<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdSuburbsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suburbs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('state_id')->unsigned();
			$table->integer('region_id')->unsigned()->index('suburbs_region_id_foreign');
			$table->string('name', 50);
			$table->string('slug', 100)->index('suburbs_slug_UNIQUE');
			$table->unique(['state_id','region_id','name'], 'suburbs_name_UNIQUE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suburbs');
	}

}
