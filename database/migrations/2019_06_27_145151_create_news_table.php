<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('slug', 260)->unique();

            $table->mediumText('content');
            $table->string('content_shortly', 255)->nullable();

            $table->integer('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');


            $table->boolean('is_featured')->default(false);
            $table->boolean('is_homepage')->default(false);
            $table->boolean('is_top')->default(false);
            $table->boolean('published')->default(false);
            $table->string('image', 100)->nullable();

            $table->string('source_type', 20)->nullable();
            $table->string('source_url', 255)->nullable();

            $table->timestamp('created_at');//->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->index(['created_at'], 'news_created_at_index');

            $table->index(['is_homepage', 'published'], 'news_is_homepage_published_index');
            $table->index(['is_top', 'published'], 'news_is_top_published_index');

            $table->index(['creator_id', 'published', 'title'], 'news_creator_id_published_title_index');
            $table->index(['source_type', 'published'], 'news_source_type_published_index');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
