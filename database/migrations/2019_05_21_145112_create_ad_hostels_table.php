<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdHostelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hostels', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid', 128)->nullable();
			$table->integer('user_id')->index('fk_bp_hostel_1_idx');
			$table->integer('region_id')->nullable()->index('fk_bp_hostel_1_idx2');
			$table->integer('subregion_id')->nullable()->index('fk_bp_hostel_subregion');
			$table->integer('state_id')->index('fk_bp_hostel_2_idx');
			$table->string('town', 50)->nullable();
			$table->string('street_addr', 150)->nullable();
			$table->string('postal_code', 4);
			$table->string('phone');
			$table->string('email', 50);
			$table->string('name', 100)->unique('hostels_name_UNIQUE');
			$table->enum('feature', ['S', 'F'])->default('S')->comment('S=>Simple, F=>Featured');
			$table->string('slug', 150)->unique('hostels_slug_UNIQUE');
			$table->string('short_descr');
			$table->text('descr', 65535);
			$table->integer('distance')->nullable();
			$table->string('website', 150)->nullable();

//            $table->enum('request_callback', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No');
//            $table->enum('status', ['N','A','O','C'])->default('N')->comment(' N=>New, A=>Accepted, O=> Completed, C => Cancelled ');

            $table->enum('is_priority_listing', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No')->index('fk_bp_hostel_is_priority_listing');
			$table->enum('is_homepage_spotlight', ['Y', 'N'])->default('N')->comment(' Y=>Yes, N=>No')->index('fk_bp_hostel_is_homepage_spotlight');

			$table->string('book_now_link')->nullable();
			$table->decimal('rating', 4)->nullable();
			$table->string('status')->default('N')->index('fk_bp_status_idx');
			$table->string('payment_log_id', 45)->nullable();
			$table->dateTime('payment_expired_at')->nullable();
			$table->date('next_payment_at')->nullable();
			$table->string('subscription_id', 128)->nullable()->index('fk_bp_hostel_subscription_id');
			$table->string('payment_gateway')->nullable()->default('E');
			$table->integer('view_count')->default(0);
			$table->text('about_us', 65535)->nullable();
			$table->text('cancellation_policy', 65535)->nullable();
			$table->text('must_read_information', 65535)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
			$table->integer('modified_by')->index('fk_bp_hostel_1_idx1');
			$table->index(['status','postal_code'], 'fk_bp_status_postal_code_idx');
			$table->index(['state_id','region_id','subregion_id','town'], 'fk_locations_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hostels');
	}

}
