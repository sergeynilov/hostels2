<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
//        DB::connection()->getpdo()->exec("drop database if exists Hostels2");
//        DB::connection()->getpdo()->exec("create database Hostels2");
//        DB::connection()->getpdo()->exec("use Hostels2");

		Schema::create('categories', function(Blueprint $table)
		{

            $table->increments('id');
			$table->string('name', 50)->unique();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->index('categories_created_at_index');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
