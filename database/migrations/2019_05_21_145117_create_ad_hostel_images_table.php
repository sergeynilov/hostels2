<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdHostelImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hostel_images', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('hostel_id')->unsigned();
            $table->foreign('hostel_id')->references('id')->on('hostels')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->string('filename');
			$table->boolean('is_main')->default(0);
			$table->boolean('is_video')->default(0);
			$table->smallInteger('video_width')->nullable();
			$table->smallInteger('video_height')->nullable();
			$table->string('info')->nullable();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->index('hostel_created_at_index');
			$table->unique(['hostel_id','filename'], 'hostel_images_id_filename_unique');
			$table->index(['hostel_id','is_main'], 'hostel_images_id_is_main');
			$table->index(['hostel_id','is_video','filename'], 'hostel_images_id_is_video_filename');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hostel_images');
	}

}
