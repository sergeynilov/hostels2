<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumForumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forums', function (Blueprint $table) {

//            $table->integer('forum_id')->unsigned();
//            $table->foreign('forum_id')->references('id')->on('forums')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->increments('id')->unsigned();
            $table->string('title', 255)->unique();
            $table->string('slug', 260)->unique();

            $table->mediumText('description');

            $table->integer('creator_id')->unsigned();
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->smallInteger('forum_category_id')->unsigned();
            $table->foreign('forum_category_id')->references('id')->on('forum_categories')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->boolean('published')->default(false);

            $table->integer('views')->unsigned()->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->index(['created_at'], 'forums_created_at_index');

            $table->index(['creator_id', 'title'], 'forums_creator_id_title_index');
            $table->index(['published', 'title', 'creator_id'], 'forums_id_published_creator_title_index');
        });
//        Artisan::call('db:seed', array('--class' => 'forumForumsWithInitData'));




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forums');
    }
}
