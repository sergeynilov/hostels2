<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdHostelFacilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hostel_facilities', function(Blueprint $table)
		{
			$table->increments('id');

            $table->integer('facility_id')->unsigned();
            $table->foreign('facility_id')->references('id')->on('facilities')->onUpdate('RESTRICT')->onDelete('CASCADE');

            $table->integer('hostel_id')->unsigned();
            $table->foreign('hostel_id')->references('id')->on('hostels')->onUpdate('RESTRICT')->onDelete('CASCADE');


            $table->index(['facility_id','hostel_id'], 'fk_facility_id_hostel_id_idx');

			$table->timestamp('created_at')->useCurrent();

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hostel_facilities');
	}

}
