<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->string('username', 100)->unique();
			$table->string('email', 100)->unique();
			$table->string('password')->nullable();
			$table->string('remember_token', 100)->nullable();

            $table->enum('status', ['N', 'A', 'I'])->default('N')->comment(' N => New(Waiting activation), A=>Active, I=>Inactive');

            $table->enum('account_type', ['I', 'B'])->default('I')->comment(' I=>Individual, B=>Business');

			$table->boolean('verified')->default(0);
			$table->string('verification_token')->nullable();
			$table->string('first_name', 50)->nullable();
			$table->string('last_name', 50)->nullable();
			$table->string('phone')->nullable();
			$table->string('website', 100)->nullable();
			$table->text('notes', 16777215)->nullable();
			$table->integer('creator_id')->unsigned()->nullable()->index('users_creator_id_foreign');
			$table->dateTime('activated_at')->nullable();
			$table->string('avatar', 100)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
			$table->index(['status','account_type'], 'users_status_account_type_index');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
