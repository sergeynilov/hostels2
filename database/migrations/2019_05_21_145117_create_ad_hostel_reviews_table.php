<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdHostelReviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hostel_reviews', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('hostel_id')->unsigned();
            $table->foreign('hostel_id')->references('id')->on('hostels')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->string('email_inquiried', 50)->index('fk_bp_hostel_review_email_inquiried_idx');
			$table->string('full_name', 225);
            $table->enum('status', ['N','A','I'])->default('N')->comment(' N=>New, A=>Active');
            $table->enum('flag_status', ['N','S','O','C'])->default('N')->comment(' N=>New, A=>Active');

            $table->text('review', 65535);
			$table->tinyInteger('stars_rating_type_id');
            $table->timestamp('created_at')->useCurrent();
			$table->index(['status','created_at'], 'fk_bp_hostel_review_status_created_at_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hostel_reviews');
	}

}
