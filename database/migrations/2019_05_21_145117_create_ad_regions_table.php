<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdRegionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('regions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('state_id')->unsigned();
			$table->string('name', 50);
			$table->string('slug', 55)->unique();
            $table->enum('active', ['A', 'I'])->default('I')->comment(' A=>Active, I=>Inactive');
            $table->unique(['name','state_id']);
			$table->index(['state_id','active'], 'fk_bp_regions_state_id_active_ixd');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('regions');
	}

}
