<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdSubregionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subregions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('region_id')->unsigned()->index('subregions_region_id_foreign');
			$table->integer('state_id')->unsigned()->index('subregions_state_id_foreign');
			$table->string('name', 50);
			$table->string('slug', 100)->unique();
			$table->unique(['name','state_id'], 'name_state_id_UNIQUE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subregions');
	}

}
