<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdLocationDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location_data', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('state', 10)->index('fk_location_data_state_ind');
			$table->string('region', 50)->index('fk_location_data_region_ind');
			$table->string('subregion', 50)->index('fk_location_data_subregion_ind');
			$table->string('suburb', 50)->index('fk_location_data_suburb_ind');
			$table->string('postcode', 4)->index('fk_location_data_postcode_ind');
			$table->timestamp('created_at')->useCurrent();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location_data');
	}

}
