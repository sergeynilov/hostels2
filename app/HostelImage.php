<?php

namespace App;
use DB;
use App\MyAppModel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\FuncsTrait;
use Illuminate\Validation\Rule;


class HostelImage extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'hostel_images';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected static $img_preview_width= 160;
    protected static $img_preview_height= 120;


    protected $hostelImageImagePropsArray = [];
    protected $img_filename_max_length = 255;

    protected static $uploads_hostel_images_dir = 'hostels/-hostel-';
    protected $fillable = [ 'filename', 'is_main', 'hostel_id' , 'is_video', 'video_width', 'video_height', 'info' ];


    private static $hostelImageIsMainLabelValueArray = Array('1' => 'Is Main', '0' => 'Is Not Main');
    private static $hostelImageIsVideoLabelValueArray = Array('1' => 'Is Video', '0' => 'Is Not Video');

    protected $casts = [
    ];



    public function getImgFilenameMaxLength(): int
    {
        return $this->img_filename_max_length;
    }


    public function hostel()
    {
        return $this->belongsTo('App\Hostel');
    }


    protected static function boot() {
        parent::boot();
        static::deleting(function($hostelImage) {
            $hostel_image_image_path= HostelImage::getHostelImageImagePath($hostelImage->hostel_id, $hostelImage->filename);
            HostelImage::deleteFileByPath($hostel_image_image_path, true);
        });
    }


    public function scopeGetByHostelId($query, $hostel_id= null)
    {
        if (!empty($hostel_id)) {
            if ( is_array($hostel_id) ) {
                $query->whereIn(with(new HostelImage)->getTable().'.hostel_id', $hostel_id);
            } else {
                $query->where(with(new HostelImage)->getTable().'.hostel_id', $hostel_id);
            }
        }
        return $query;
    }


    public static function getHostelImageIsMainValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$hostelImageIsMainLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getHostelImageIsMainLabel(string $is_main):string
    {
        if (!empty(self::$hostelImageIsMainLabelValueArray[$is_main])) {
            return self::$hostelImageIsMainLabelValueArray[$is_main];
        }
        return self::$hostelImageIsMainLabelValueArray[0];
    }


    public static function getHostelImageIsVideoValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$hostelImageIsVideoLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getHostelImageIsVideoLabel(string $is_video):string
    {
        if (!empty(self::$hostelImageIsVideoLabelValueArray[$is_video])) {
            return self::$hostelImageIsVideoLabelValueArray[$is_video];
        }
        return self::$hostelImageIsVideoLabelValueArray[0];
    }


    public static function getHostelImageDir(int $hostel_id): string
    {
        return self::$uploads_hostel_images_dir . $hostel_id . '/';
    }

    public static function getHostelImageImagePath(int $hostel_id, $image): string
    {
        if (empty($image)) {
            return '';
        }
        return self::$uploads_hostel_images_dir . $hostel_id . '/' . $image;
    }


    //
    public static function setHostelImageImageProps(int $hostel_id, string $image = null, bool $skip_non_existing_file = null): array
    {
        if (empty($image) and $skip_non_existing_file) {
            return [];
        }
        $file_was_found= true;
        $dir_path = self::$uploads_hostel_images_dir . '' . $hostel_id . '';
//        echo '<pre>$dir_path::'.print_r($dir_path,true).'</pre>';
        $file_full_path = $dir_path . '/' . $image;
//        echo '<pre>$file_full_path::'.print_r($file_full_path,true).'</pre>';
        $file_exists    = ( !empty($image) and Storage::disk('local')->exists('public/' . $file_full_path) );
//        echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
        if ( ! $file_exists) {
            if ($skip_non_existing_file) {
                return [];
            }
            $file_was_found= false;
            $file_full_path = config('app.empty_img_url');
        }

        $image_path = $file_full_path;
        if ($file_exists) {
            $image_url       = 'storage/app/' . $dir_path . '/' . $image;
            $imagePropsArray = ['image' => $image, 'image_path' => $image_path, 'image_url' => '/storage/' . $dir_path . '/' . $image];
            $image_full_path = base_path() . '/storage/app/public/' . $image_path;
        } else {
            $imagePropsArray = ['image' => $image, 'image_path' => $image_path, 'image_url' => $file_full_path];
            $image_full_path = base_path() . '/public/' . $image_path;
        }

        if ( ! empty($previewSizeArray['width'])) {
            $imagePropsArray['preview_width']  = $previewSizeArray['width'];
            $imagePropsArray['preview_height'] = $previewSizeArray['height'];
        }
        $hostelImageImgProps = with(new HostelImage)->getCFImageProps($image_full_path, $imagePropsArray);

        if ( !$file_was_found ) {
            $hostelImageImgProps['file_info'] = 'File "' . $image . '" not found ';
        }
        return $hostelImageImgProps;

    }

    /* get additional properties of hostel image : path, url, size etc... */
    public function getHostelImageImagePropsAttribute(): array
    {
        return $this->hostelImageImagePropsArray;
    }

    /* set additional properties of hostel image : path, url, size etc... */
    public function setHostelImageImagePropsAttribute(array $hostelImageImagePropsArray)
    {
        $this->hostelImageImagePropsArray = $hostelImageImagePropsArray;
    }


    public static function getSimilarHostelImageByFilename( string $filename, int $hostel_id, int $id= null, $return_count= false )
    {
        $quoteModel = HostelImage::where( 'filename', $filename );
        $quoteModel = $quoteModel->where( 'hostel_id', '=' , $hostel_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }

        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    //         return HostelImage::getValidationRulesArray( $request->get('hostel_id'), $request->get('id') );
    public static function getValidationRulesArray($hostel_id, $hostel_image_id = null): array
    {
        $additional_item_value_validation_rule= 'check_hostel_image_unique_by_name:'.$hostel_id.','.( !empty($hostel_image_id)?$hostel_image_id:'');


        $validationRulesArray            = [
            'hostel_id'      => 'required|exists:'.( with(new Hostel)->getTable() ).',id',
//            'hostel_id'             => 'required|max:255|' . $additional_item_value_validation_rule,


            'name'             => 'required|max:255|' . $additional_item_value_validation_rule,
            'is_video'       => 'required|in:' . with(new HostelImage)->getValueLabelKeys(HostelImage::getHostelImageIsCorrectValueArray(false)),
            'ordering'         => 'required|integer',
        ];

        return $validationRulesArray;
    }


    public function getImgPreviewWidth() : int
    {
        return self::$img_preview_width;
    }

    public function getImgPreviewHeight() : int
    {
        return self::$img_preview_height;
    }

}
