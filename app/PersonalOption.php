<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class PersonalOption extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'personal_options';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'user_id', 'name', 'value', 'updated_at'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }
    public function scopeGetByUserId($query, $user_id= null)
    {
        if (!empty($user_id)) {
            if ( is_array($user_id) ) {
                $query->whereIn(with(new PersonalOption)->getTable().'.user_id', $user_id);
            } else {
                $query->where(with(new PersonalOption)->getTable().'.user_id', $user_id);
            }
        }
        return $query;
    }

    public static function getValidationRulesArray(): array
    {

        $validationRulesArray = [
            'user_id'      => 'required|exists:'.( with(new User)->getTable() ).',id',
            'name'          => [
                'required',
                'string',
                'max:255',
            ],
            'value'                => 'nullable|max:255'
        ];
        return $validationRulesArray;
    }

}
