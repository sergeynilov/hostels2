<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\MyAppModel;
use App\Http\Traits\funcsTrait;
use Illuminate\Validation\Rule;


class PaymentPackage extends MyAppModel
{
    use funcsTrait;

    protected $table      = 'payment_packages';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'name', 'active', 'is_premium', 'is_free', 'description','color', 'background_color', 'price', 'subscription_weight', 'updated_at' ];

    private static $paymentPackageActiveLabelValueArray = Array('1' => 'Is active', '0' => 'Is not active');
    private static $paymentPackageIsPremiumLabelValueArray = Array('1' => 'Is premium', '0' => 'Is not premium');
    private static $paymentPackageIsFreeLabelValueArray = Array('1' => 'Is free', '0' => 'Is not free');



    protected static function boot() {
        parent::boot();
    }


    public static function getPaymentPackageActiveValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$paymentPackageActiveLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getPaymentPackageActiveLabel(string $active):string
    {
        if (!empty(self::$paymentPackageActiveLabelValueArray[$active])) {
            return self::$paymentPackageActiveLabelValueArray[$active];
        }
        return '';
    }


    public static function getPaymentPackageIsPremiumValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$paymentPackageIsPremiumLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getPaymentPackageIsPremiumLabel(string $is_premium):string
    {
        if (!empty(self::$paymentPackageIsPremiumLabelValueArray[$is_premium])) {
            return self::$paymentPackageIsPremiumLabelValueArray[$is_premium];
        }
        return '';
    }




    public static function getPaymentPackageIsFreeValueArray($key_return= true) : array
    {
        $resArray = [];
        foreach (self::$paymentPackageIsFreeLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = [ 'key' => $key, 'label' => $value ];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getPaymentPackageIsFreeLabel(string $is_free):string
    {
        if (!empty(self::$paymentPackageIsFreeLabelValueArray[$is_free])) {
            return self::$paymentPackageIsFreeLabelValueArray[$is_free];
        }
        return '';
    }


    public function scopeGetByName($query, $name= null, $partial= false)
    {
        if (empty($name)) return $query;
        return $query->where('.name', (!$partial?'=':'like'), ($partial?'%':''). $name .($partial?'%':'') );
    }


    public function scopeGetByActive($query, $active= null)
    {
        if ( !isset($active)  or strlen($active) == 0 ) return $query;
        return $query->where( with(new PaymentPackage)->getTable().'.active', $active );
    }

    /* check if provided name is unique for payment_packages.name field */
    public static function getSimilarPaymentPackageByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = PaymentPackage::where( 'name', $name );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getValidationRulesArray($payment_package_id= null) : array
    {

        /*         Schema::create('payment_packages', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 255)->unique();
            $table->string('stripe_plan_id', 50)->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('is_premium')->default(false);
            $table->boolean('is_free')->default(false);
            $table->mediumText('description')->nullable();
            $table->string('color', 7);
            $table->string('background_color', 7);
            $table->tinyInteger('subscription_weight');
            $table->decimal('price', 9, 2)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->index(['created_at'], 'payment_packages_created_at_index');
            $table->index(['active'], 'payment_packages_active_index');
        });
        */
        $validationRulesArray = [
            'name' => [
                'required',
                'string',
                'max:255',
                Rule::unique(with(new PaymentPackage)->getTable())->ignore($payment_package_id),
            ],
            'active'     =>   'required|in:'.with( new PaymentPackage)->getValueLabelKeys( PaymentPackage::getPaymentPackageActiveValueArray(false) ),
        ];
        return $validationRulesArray;
    }

    public static function getPaymentPackagesSelectionArray(int $filter_active=null) :array {
        $paymentPackages = PaymentPackage::orderBy('name','desc')->getByActive($filter_active)->get();
        $paymentPackagesSelectionArray= [];
        foreach( $paymentPackages as $nextPaymentPackage ) {
            $paymentPackagesSelectionArray[$nextPaymentPackage->id]= $nextPaymentPackage->name;
        }
        return $paymentPackagesSelectionArray;
    }

}
