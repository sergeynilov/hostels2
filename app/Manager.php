<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;
//use Illuminate\Support\Facades\Redirect;
//use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;


class Manager extends MyAppModel
{
    use FuncsTrait;

    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'username',
        'account_type',
        'email',
        'status',
        'phone',
        'website',
        'notes',
        'created_at',
        'updated_at'
    ];

    protected static $uploads_user_avatar_temps_dir = 'tmp/-user-avatar-temp-';
    protected static $uploads_user_avatars_dir = 'user-avatars/-user-avatar-';

    private static $managerStatusLabelValueArray = Array('A' => 'Active', 'I' => 'Inactive', 'N' => 'New');

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('RoleManager', function (Builder $builder) {
            $builder->leftJoin( 'users_groups', 'users_groups.user_id', '=', 'users.id')
                    ->select( with(new Manager)->getTable().'.*')
                    ->where( 'users_groups.group_id', ACCESS_ROLE_MANAGER );
        });
    }

    public static function getManagerStatusValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$managerStatusLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getManagerStatusLabel(string $status): string
    {
        if ( ! empty(self::$managerStatusLabelValueArray[$status])) {
            return self::$managerStatusLabelValueArray[$status];
        }
        return self::$managerStatusLabelValueArray[0];
    }

//

    public function scopeGetById($query, $id= null)
    {
        if (empty($id)) {
            return $query;
        }
        return $query->where(with(new Manager)->getTable().'.id', $id);
    }


    public function scopeGetByUsername($query, $username = null, $partial = false)
    {
        if (empty($username)) {
            return $query;
        }
        return $query->where(with(new Manager)->getTable() . '.username', (! $partial ? '=' : 'like'), ($partial ? '%' : '') . $username . ($partial ? '%' : ''));
    }

    public function scopeGetByStatus($query, $status = null)
    {
        if (empty($status)) {
            return $query;
        }
        return $query->where(with(new Manager)->getTable().'.status', $status);
    }

    public static function getUserAvatarTempDir(string $session_id): string
    {
        return self::$uploads_user_avatar_temps_dir . $session_id . '/';
    }

    public static function getUserAvatarTempPath(string $session_id, $image): string
    {
        if (empty($image)) {
            return '';
        }
        return self::$uploads_user_avatar_temps_dir . $session_id . '/' . $image;
    }

    public static function setUserAvatarProps(int $user_id, string $avatar = null, bool $skip_non_existing_file = false): array
    {
        if (empty($avatar) and $skip_non_existing_file) {
            return [];
        }

        $dir_path = self::$uploads_user_avatars_dir . '' . $user_id . '';

        $file_full_path = $dir_path . '/' . $avatar;
        $file_exists    = ( !empty($avatar) and Storage::disk('local')->exists('public/' . $file_full_path) );

        if ( ! $file_exists) {
            if ($skip_non_existing_file) {
                return [];
            }
            $file_full_path = /*'public' . */
                config('app.empty_img_url');
            $avatar          = with(new MyAppModel())->getFilenameBasename($file_full_path);
        }

        $avatar_path = $file_full_path;
        //        $avatar_path= public_path('storage/'.$dir_path . '/' . $avatar);
        //        $avatar_path= public_path('storage/app/'.$dir_path . '/' . $avatar);
        //        $avatar_path= 'storage/app/'.$dir_path . '/' . $avatar;
        //        $avatar_path= $file_full_path;
        if ($file_exists) {
            $avatar_url       = 'storage/app/' . $dir_path . '/' . $avatar;
            $avatarPropsArray = ['avatar' => $avatar, 'avatar_path' => $avatar_path, 'avatar_url' => '/storage/' . $dir_path . '/' . $avatar];
            $avatar_full_path = base_path() . '/storage/app/public/' . $avatar_path;
        } else {
            $avatarPropsArray = ['avatar' => $avatar, 'avatar_path' => $avatar_path, 'avatar_url' => $file_full_path];
            $avatar_full_path = base_path() . '/public/' . $avatar_path;
        }

        if ( ! empty($previewSizeArray['width'])) {
            $avatarPropsArray['preview_width']  = $previewSizeArray['width'];
            $avatarPropsArray['preview_height'] = $previewSizeArray['height'];
        }
        $userImgProps = with(new MyAppModel())->getCFImageProps($avatar_full_path, $avatarPropsArray);

        return $userImgProps;

    }


    public static function getValidationRulesArray($manager_id = null): array
    {

        $validationRulesArray = [
            'username'          => [
                'required',
                'string',
                'max:100',
            ],
            'account_type'         => 'required|in:' . with(new Manager)->getValueLabelKeys(Manager::getManagerAccountTypeValueArray(false)),
            'status'               => 'required|in:' . with(new Manager)->getValueLabelKeys(Manager::getManagerStatusValueArray(false)),
            'email'                => 'required|max:255|email',
            'phone'                => 'required|max:255',
            'website'              => 'required|max:100',
            'notes'                => 'nullable',
        ];
        return $validationRulesArray;
    }

    public static function getManagersSelectionArray() :array {
        $managers = Manager::orderBy('username','asc')->get();
        $managersSelectionArray= [];
        foreach( $managers as $nextManager ) {
            $managersSelectionArray[$nextManager->id]= $nextManager->username;
        }
        return $managersSelectionArray;
    }
}
