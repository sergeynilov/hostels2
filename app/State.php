<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class State extends MyAppModel
{
    use FuncsTrait;

    protected $table = 'states';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [ 'name', 'slug', 'active' ];

    private static $StateActiveLabelValueArray = Array('A' => 'Active', 'I' => 'Inactive');
    public static function getStateActiveValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$StateActiveLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }
    public static function getStateActiveLabel(string $active): string
    {
        if ( ! empty(self::$StateActiveLabelValueArray[$active])) {
            return self::$StateActiveLabelValueArray[$active];
        }

        return self::$StateActiveLabelValueArray[0];
    }


    protected $casts = [
    ];

    public function scopeGetByName($query, $name= null, $partial= false)
    {
        if (empty($name)) return $query;
        return $query->where(with(new State)->getTable().'.name', (!$partial?'=':'like'), ($partial?'%':''). $name .($partial?'%':'') );
    }

    public function scopeGetBySlug($query, $slug= null)
    {
        if (empty($slug)) return $query;
        return $query->where(with(new State)->getTable().'.slug', $slug );
    }

    public function scopeGetByCode($query, $code= null)
    {
        if (empty($code)) return $query;
        return $query->where(with(new State)->getTable().'.code', $code );
    }

    public function scopeGetByActive($query, $active= null)
    {
        if (!empty($active)) {
            $query->where(with(new State)->getTable().'.active', $active);
        }
        return $query;
    }

    public static function getStatesSelectionArray($active= '', $order_by= 'name') :array {
        $states = State
            ::getByActive($active)
            ->orderBy($order_by,'asc')
            ->get();
        $statesSelectionArray= [];
        foreach( $states as $nextState ) {
            $statesSelectionArray[]= (object)[ 'id'=> $nextState->id, 'code'=> $nextState->code, 'name'=>$nextState->name, 'slug'=>$nextState->slug, 'active'=>$nextState->active ];
        }
        return $statesSelectionArray;
    }

}
