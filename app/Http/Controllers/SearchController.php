<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Settings;
use App\Http\Traits\FuncsTrait;
use App\Customer;
use App\State;
use App\Region;
use App\Hostel;
use App\HostelFacility;
use App\HostelInquery;
use App\HostelRoom;
use App\HostelImage;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Requests\HostelRequest;
use App\Http\Requests\HostelInqueryRequest;

use App\User;
use App\library\CheckValueType;

class SearchController extends BaseController
{
    use FuncsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }


    public function run_hostels_filter_search()
    {

        $prefix      = DB::getTablePrefix();
        $request     = request();
        $requestData = $request->all();
//        $requestData = [
//            'filter_search_text' => 'te',
//            'filter_price_values' => [2,98],
//            'filter_rating_values' => [.6,3.8],
//            'filter_only_feature' => 1,
//            'filter_facilitiesSelectionArray' => ['3', '9', '8', '7', '2', '1', '6', '5', '4'],
//            'filter_categoriesSelection' => ['3', '9', '8', '7', '2', '1', '6', '5', '4']
//        ];

        $this->debToFile(print_r($requestData, true), '  run_hostels_filter_search - $requestData::');
        $retArray    = ['error_code' => 0, 'message' => ''];


        $filter_search_text= (!empty($requestData['filter_search_text']) ? $requestData['filter_search_text'] : '' );
        $filter_price_values= (!empty($requestData['filter_price_values']) ? $requestData['filter_price_values'] : '' );
        $filter_rating_values= (!empty($requestData['filter_rating_values']) ? $requestData['filter_rating_values'] : '' );

        $filter_categoriesSelection= [];
        $tempCategoriesSelection= (!empty($requestData['filter_categoriesSelection']) ? $requestData['filter_categoriesSelection'] : [] );
        foreach( $tempCategoriesSelection as $next_key=>$nextTempCategoriesSelection ) {
            if ( !empty($nextTempCategoriesSelection['is_checked']) ) {
                $filter_categoriesSelection[]= $nextTempCategoriesSelection['key'];
            }
        }

        $filter_facilitiesSelectionArray= [];
        $tempFacilitiesSelection= (!empty($requestData['filter_facilitiesSelectionArray']) ? $requestData['filter_facilitiesSelectionArray'] : [] );
        foreach( $tempFacilitiesSelection as $next_key=>$nextTempFacilitiesSelection ) {
            if ( !empty($nextTempFacilitiesSelection['is_checked']) ) {
                $filter_facilitiesSelectionArray[]= $nextTempFacilitiesSelection['key'];
            }

        }
        //                     filter_hostels_search_by_fields: filter_hostels_search_by_fields,
        //                    filter_search_by_fields_ordering: filter_search_by_fields_ordering,
        $filter_hostels_search_by_fields= (!empty($requestData['filter_hostels_search_by_fields']) ? $requestData['filter_hostels_search_by_fields'] : 'min_hostel_rooms_price' );
        if ( strtolower($filter_hostels_search_by_fields) == 'price' ) {
            $filter_hostels_search_by_fields = 'min_hostel_rooms_price';
        }
        $filter_search_by_fields_ordering= (!empty($requestData['filter_search_by_fields_ordering']) ? $requestData['filter_search_by_fields_ordering'] : 'asc' );

        $filter_only_feature= (!empty($requestData['filter_only_feature']) ? $requestData['filter_only_feature'] : '' );
        $hostelsList= [];
        //    public function scopeGetByFacilityId($query, $facility_id= null)
        $tempHostelsList = Hostel
            ::getByStatus('A')
            ->getByName( $filter_search_text, true )
            ->getByFeature( ( $filter_only_feature ? "F" : "") )
            ->getByFacilityId( $filter_facilitiesSelectionArray, 'hostel_facilities' )
            ->getByPriceFrom( ( !empty($filter_price_values[0]) ? $filter_price_values[0] : null ), $prefix.'hostel_rooms' )
            ->getByPriceTill( ( !empty($filter_price_values[1]) ? $filter_price_values[1] : null ), $prefix.'hostel_rooms' )
            ->getByRatingFrom( ( !empty($filter_rating_values[0]) ? $filter_rating_values[0] : null ), $prefix.'hostels' )
            ->getByRatingTill( ( !empty($filter_rating_values[1]) ? $filter_rating_values[1] : null ), $prefix.'hostels' )

//            ->getByRatingTill( ( !empty($filter_rating_values[1]) ? $filter_rating_values[1] : null ), $prefix.'hostels' )
//            ->getByRatingTill( ( !empty($filter_rating_values[1]) ? $filter_rating_values[1] : null ), $prefix.'hostels' )

                //     public function scopeGetByHostelId($query, $hostel_id= null)

//            ->getByPriceTill( !empty($filter_price_values[1]) ? $filter_price_values[1] : null ) //scopeGetByPriceFrom
//            ->getByStateId(4)
//            ->whereIn('hostels.id', [29, 30])
            ->leftJoin('states', 'states.id', '=', 'hostels.state_id')
            ->leftJoin('regions', 'regions.id', '=', 'hostels.region_id')
            ->leftJoin('subregions', 'subregions.id', '=', 'hostels.subregion_id')
            ->leftJoin('hostel_rooms', 'hostel_rooms.hostel_id', '=', 'hostels.id')
            ->leftJoin('hostel_facilities', 'hostel_facilities.hostel_id', '=', 'hostels.id')
//            ->orderBy('subregion_name', 'asc')
//            ->orderBy('hostels.is_priority_listing', 'asc')
            ->orderBy($filter_hostels_search_by_fields, $filter_search_by_fields_ordering)
//            ->orderBy('min_hostel_rooms_price', 'asc')
//            ->orderBy('subregion_name', 'asc')
//            ->orderBy('min_hostel_rooms_price', 'asc')
//            ->offset( 0 )
//            ->take( 4 )
            ->select(
                'hostels.id', 'hostels.name', 'hostels.phone', 'hostels.email', 'hostels.website',  'hostels.subregion_id',  'hostels.region_id',  'hostels.state_id', 'hostels.town', 'hostels.slug', 'hostels.short_descr', 'hostels.is_priority_listing',  'hostels.status', 'hostels.is_homepage_spotlight',
                'hostels.descr', 'hostels.feature', 'hostels.rating', 'hostels.updated_at', 'hostels.created_at',
                'states.name as state_name', 'states.slug as state_slug',
                'regions.name as region_name', 'regions.slug as region_slug',
                'subregions.name as subregion_name', 'subregions.slug as subregion_slug',
                \DB::raw(' ( select count('.$prefix.'hostel_reviews.id) from '.$prefix.'hostel_reviews where '.$prefix.'hostel_reviews.hostel_id = '.$prefix.'hostels.id ) as hostel_reviews_count'),
                \DB::raw(' ( select count('.$prefix.'hostel_inqueries.id) from '.$prefix.'hostel_inqueries where '.$prefix.'hostel_inqueries.hostel_id = '.$prefix.'hostels.id ) as hostel_inqueries_count'),
                \DB::raw(' ( select min('.$prefix.'hostel_rooms.price) from '.$prefix.'hostel_rooms where '.$prefix.'hostel_rooms.hostel_id = '.$prefix.'hostels.id ) as min_hostel_rooms_price'),
                \DB::raw(' ( select '.$prefix.'hostel_images.filename from '.$prefix.'hostel_images where '.$prefix.'hostel_images.hostel_id = '.$prefix.'hostels.id and '.$prefix.'hostel_images.is_main = 1 limit 1) as filename')
            )
            ->distinct()
            ->get();
//        $this->debToFile(print_r($tempHostelsList, true), '-10 $tempHostelsList::');
//        echo '<pre>count($tempHostelsList)::'.print_r(count($tempHostelsList),true).'</pre>';
        foreach( $tempHostelsList as $nextTempHostel ) {

            $filenameData                     = HostelImage::setHostelImageImageProps($nextTempHostel->id, $nextTempHostel->filename, true);
            $filenameData['info']             = $nextTempHostel->info;
//            $this->debToFile(print_r($filenameData, true), '  - $filenameData::');
            $nextTempHostel['filenameData']   = $filenameData;
            $nextTempHostel['details_loaded'] = '';
            $hostelsList[]= $nextTempHostel;
        }

//        echo '<pre>count($hostelsList)::'.print_r(count($hostelsList),true).'</pre>';
//        die("-1 XXZ");
        $retArray['hostels']= $hostelsList;
        $retArray['hostel_rows_count']= count($hostelsList);

//        $this->debToFile(print_r($hostelsList, true), '-11 $hostels::');
        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function run_hostels_filter_search()





}
