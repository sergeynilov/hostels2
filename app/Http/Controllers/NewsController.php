<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Settings;
use App\Http\Traits\FuncsTrait;
use App\News;
use Illuminate\Routing\Controller as BaseController;

use App\User;
use App\library\CheckValueType;

class NewsController extends BaseController
{
    use FuncsTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    //    Route::get('get_list/{data_type}', 'NewsController@get_list');
    public function get_list($data_type = null, $page = null)
    {
        $retArray = ['error_code' => 0, 'message' => ''];

        $this->debToFile(print_r($data_type, true), '  - get_list $data_type::');
        $this->debToFile(print_r($page, true), '  - get_list $page::');
        $newsList      = [];
        $news_per_page = Settings::getValue('news_per_page', CheckValueType::cvtInteger, 20);

        if (empty($data_type) or strtolower($data_type) == 'all') {
            $limit_start = ($page - 1) * $news_per_page;

            $this->debToFile(print_r($limit_start, true), '  get_forum_threads  -3 $limit_start::');

            $news_total_count = News
                ::getByPublished(true)
                ->count();
            $tempNewsList     = News
                ::getByPublished(true)
                ->leftJoin('users', 'users.id', '=', 'news.creator_id')
                ->orderBy('news.created_at', 'desc')
                ->select(
                    'news.id', 'news.title', 'news.content_shortly', 'news.slug', 'news.content', 'news.creator_id', 'news.is_featured',
                    'news.published', 'news.is_homepage', 'news.image', 'news.source_type', 'news.source_url', 'news.created_at', 'news.updated_at',
                    'users.first_name', 'users.last_name'
                )
                ->offset($limit_start)
                ->take($news_per_page)
//                ->whereIn( 'news.id', [8,9] ) // DEBUGGING
                ->get();
        } // if (empty($data_type) or strtolower($data_type) == 'all') {

        if ( ! empty($data_type) and strtolower($data_type) == 'is_top') {
            $news_total_count = News
                ::getByPublished(true)
                ->getByIsTop(true)
                ->count();
            $tempNewsList     = News
                ::getByPublished(true)
                ->getByIsTop(true)
                ->leftJoin('users', 'users.id', '=', 'news.creator_id')
                ->orderBy('news.created_at', 'desc')
                ->select(
                    'news.id', 'news.title', 'news.content_shortly', 'news.slug', 'news.content', 'news.creator_id', 'news.is_featured',
                    'news.published', 'news.is_homepage', 'news.image', 'news.source_type', 'news.source_url', 'news.created_at', 'news.updated_at',
                    'users.first_name', 'users.last_name'
                )
                ->get();
        } // if (!empty($data_type) and strtolower($data_type) == 'is_top') {

        foreach ($tempNewsList as $nextTempNews) {

            $filenameData = News::setNewsImageProps($nextTempNews->id, $nextTempNews->image, true);
            if ( ! empty($filenameData)) {
                $filenameData['info'] = $nextTempNews->info;
                $this->debToFile(print_r($filenameData, true), '  +++ $filenameData::');
                $nextTempNews['filenameData'] = $filenameData;
            }
            $nextTempNews['details_loaded'] = '';
            $nextTempNews['creator_name']   = $nextTempNews->creator->full_name;
            $newsList[]                     = $nextTempNews;
        }

        $retArray['newsList']         = $newsList;
        $retArray['news_total_count'] = $news_total_count;

        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function get_list($data_type)


    public function get_single_news_by_slug($single_news_slug)
    {
        $prefix = DB::getTablePrefix();

        $singleNews = News
            ::getBySlug($single_news_slug)
            ->getByPublished(true)
            ->leftJoin('users', 'users.id', '=', 'news.creator_id')
            ->select(
                'news.id',
                'news.title',
                'news.created_at',
                'news.content_shortly',
                'news.slug',
                'news.content',
                'news.creator_id',
                'news.is_featured',
                'news.published',
                'news.is_homepage',
                'news.image',
                'news.source_type',
                'news.source_url',
                'news.updated_at',
                \DB::raw('concat( ' . $prefix . 'users.first_name, \' \',  ' . $prefix . 'users.last_name ) as creator_name')
            )
            ->first();


        $filenameData = News::setNewsImageProps($singleNews->id, $singleNews->image, true);
        if ( ! empty($filenameData)) {
            $filenameData['info'] = $singleNews->info;
            $this->debToFile(print_r($filenameData, true), '  +++ $filenameData::');
            $singleNews['filenameData'] = $filenameData;
        }
        $singleNews['details_loaded'] = '';
        $singleNews['creator_name']   = $singleNews->creator->full_name;

        /*foreach ($tempNewsList as $nextTempNews) {

            $filenameData         = News::setNewsImageProps($nextTempNews->id, $nextTempNews->image, true);
            if ( !empty($filenameData) ) {
                $filenameData['info'] = $nextTempNews->info;
                $this->debToFile(print_r($filenameData, true), '  +++ $filenameData::');
                $nextTempNews['filenameData']   = $filenameData;
            }
            $nextTempNews['details_loaded'] = '';
            $nextTempNews['creator_name']   = $nextTempNews->creator->full_name;
            $newsList[]                     = $nextTempNews;
        }
*/

        return response()->json(['error_code' => 0, 'message' => '', 'singleNews' => $singleNews], HTTP_RESPONSE_OK);
    } // public function get_hostel_by_slug($single_news_slug)


}
