<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use App\Http\Traits\FuncsTrait;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
//    use FuncsTrait;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        \Log::info('__construct LoginController' );
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {   // link https://github.com/laravel/framework/blob/6.x/src/Illuminate/Foundation/Auth/AuthenticatesUsers.php#L120
//        \Log::info(print_r($user->id, true), 'authenticated -1 $user->id::' );
//
//
////        $this->debToFile(print_r($user->id, true), 'authenticated -1 $user->id::');
//        $user_avatar_path= User::getUserAvatarPath($user->id, $user->avatar);
//
//
//        $user->user_avatar_path= $user_avatar_path;
//        $hostel_image_image_path= HostelImage::getHostelImageImagePath($hostelImage->hostel_id, $hostelImage->filename);


    }
}
