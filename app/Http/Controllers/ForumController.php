<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Settings;
use App\Http\Traits\FuncsTrait;
use App\Forum;
use App\ForumCategory;
use App\ForumThread;
use App\ForumPost;
use App\ForumPostReportAbuse;
use App\ForumPostUserSubscribed;
use Illuminate\Routing\Controller as BaseController;

use App\User;
use App\library\CheckValueType;

//use App\UserProfileDocument;
//use App\DocumentCategory;
use App\Http\Requests\ForumThreadRequest;


class ForumController extends BaseController
{
    use FuncsTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function categorized_forums()
    {
        $request = request();
        $prefix  = DB::getTablePrefix();
//        $requestData = $request->all();
        $retArray = ['error_code' => 0, 'message' => ''];

        $forums_total_count = Forum
            ::getByPublished(true)
//            ->where('forums.id', '<=', 10)    // DEBUGGING
            ->count();
        $forumCategoriesList         = [];
        //    public function scopeGetByPublished($query, $published = null)
        // image
        $tempForumCategoriesList = ForumCategory
            ::getByPublished(true)
            ->leftJoin('users', 'users.id', '=', 'forum_categories.creator_id')
            ->orderBy('forum_categories.created_at', 'asc')
            ->select(
                'forum_categories.id', 'forum_categories.title', 'forum_categories.description', /*'forum_categories.slug',*/ 'forum_categories.creator_id', 'forum_categories.published', 'forum_categories.created_at', 'forum_categories.updated_at', 'users.first_name', 'users.last_name'
            )
            ->get();

        foreach ($tempForumCategoriesList as $next_key => $nextTempForumCategory) { // listing of all published Forum Categories


            $tempForumsList = Forum
                ::getByPublished(true)
                ->getByForumCategoryId($nextTempForumCategory->id)
                ->leftJoin('users', 'users.id', '=', 'forums.creator_id')
                ->orderBy('forums.created_at', 'asc')
                ->select(
                    'forums.id', 'forums.title', 'forums.slug', 'forums.description', 'forums.creator_id',
                    'forums.forum_category_id', 'forums.created_at', 'forums.updated_at', 'users.first_name', 'users.last_name'
                )
                ->get();

            $forumsList= [];
            foreach ($tempForumsList as $nextTempForum) {  // listing of all published Forum of $nextTempForumCategory
                $nextTempForum->forum_threads_count= ForumThread
                    ::getByForumId($nextTempForum->id)
                    ->count();
                $nextTempForum->latestPost= ForumPost
                    ::getByForumId($nextTempForum->id)
                    ->join('forum_threads', 'forum_threads.id', '=', 'forum_posts.forum_thread_id')
                    ->leftJoin('users', 'users.id', '=', 'forum_posts.user_id')
                    ->select(
                        'forum_posts.*',
                        'users.first_name',
                        'users.last_name'
                    )
                    ->orderBy('forum_posts.id', 'desc')
                    ->first()
                ;
                if ( !empty($nextTempForum->latestPost) ) {
                    $nextTempForum->latestPost->creator_name = $nextTempForum->latestPost->user->full_name;
                    $nextTempForum->latestPost->creator_id   = $nextTempForum->latestPost->user->id;
                }



                $nextTempForum->forum_posts_count= ForumPost
                    ::getByForumId($nextTempForum->id)
                    ->join('forum_threads', 'forum_threads.id', '=', 'forum_posts.forum_thread_id')
                    ->count();

                $nextTempForum['creator_name'] = $nextTempForum->creator->full_name;
                $nextTempForum['creator_id']   = $nextTempForum->creator->id;
                $forumsList[]= $nextTempForum;
            } // foreach( $tempForumsList as $nextTempForum ) {  // listing of all published Forum of $nextTempForumCategory
            $nextTempForumCategory['creator_name'] = $nextTempForumCategory->creator->full_name;
            $nextTempForumCategory['creator_id']   = $nextTempForumCategory->creator->id;
            $nextTempForumCategory['forumsList']   = $forumsList;
            $forumCategoriesList[]                 = $nextTempForumCategory;

        } // foreach( $tempForumCategoriesList as $nextTempForumCategory ) { // listing of all published Forum Categories

        $retArray['forumCategoriesList']  = $forumCategoriesList;
        $retArray['forums_total_count']   = $forums_total_count;

//        $this->debToFile(print_r($forumList, true), '-11 $forum::');
        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function categorized_forums()


    //// FORUM THREADS BLOCK END
    public function get_single_forum_by_slug($forum_slug)
    {
        $prefix = DB::getTablePrefix();

        $singleForum = Forum
            ::getBySlug($forum_slug)
            ->getByPublished(true)
            ->leftJoin('users', 'users.id', '=', 'forums.creator_id')
            ->leftJoin('forum_categories', 'forum_categories.id', '=', 'forums.forum_category_id')
            ->select(
                'forums.id',
                'forums.title',
                'forums.slug',
                'forums.created_at',
                'forums.slug',
                'forums.description',
                'forums.creator_id',
                'users.first_name',
                'users.last_name',
                \DB::raw($prefix . 'forum_categories.title  as forum_categories_title')
            )
            ->first();


        $latestPost= ForumPost
            ::getByForumId($singleForum->id)
            ->join('forum_threads', 'forum_threads.id', '=', 'forum_posts.forum_thread_id')
            ->leftJoin('users', 'users.id', '=', 'forum_posts.user_id')
            ->select(
                'forum_posts.*',
                'users.first_name',
                'users.last_name'
            )
            ->orderBy('forum_posts.id', 'desc')
            ->first();
        if ( !empty($latestPost) ) {
            $latestPost['creator_name']   = $latestPost->user->full_name;
            $latestPost['creator_id']     = $latestPost->user->id;
        }

        $forum_posts_count= ForumPost
            ::getByForumId($singleForum->id)
            ->join('forum_threads', 'forum_threads.id', '=', 'forum_posts.forum_thread_id')
            ->count();

//        $this->debToFile(print_r($singleForum, true), '-1 get_single_forum_by_slug $singleForum::');
        return response()->json(['error_code' => 0, 'message' => '', 'singleForum' => $singleForum, 'latestPost'=> $latestPost, 'forum_posts_count'=> $forum_posts_count],
            HTTP_RESPONSE_OK);
    } // public function get_single_forum_by_slug($forum_slug)






    public function get_forum_threads()
    {
        $prefix = DB::getTablePrefix();

        $request     = request();
        $requestData = $request->all();
        $this->debToFile(print_r($requestData, true), '  get_forum_threads  -0 $requestData::');

        $forum_id        =  $requestData['forum_id'];
        $page            = ! empty($requestData['page']) ? (int)$requestData['page'] : 1;
        $order_by        = ! empty($requestData['order_by']) ? $requestData['order_by'] : 'id';
        $order_direction = ! empty($requestData['order_direction']) ? $requestData['order_direction'] : 'desc';


        $forum_threads_per_page = Settings::getValue('forum_threads_per_page', CheckValueType::cvtInteger, 20);

        $limit_start         = ($page - 1) * $forum_threads_per_page;

//        $this->debToFile(print_r($limit_start, true), '  get_forum_threads  -3 $limit_start::');

        $forum_threads_count= ForumThread
            ::getByForumId($forum_id)
            ->count();

        $forumThreads= ForumThread::with('latestForumPost')->withCount('forumPosts')
                                  ->getByForumId($forum_id)
                                  ->orderBy($order_by, $order_direction)
                                  ->offset($limit_start)
                                  ->take($forum_threads_per_page)
                                  ->get();


        return response()->json(['error_code' => 0, 'message' => '', 'forumThreads'=> $forumThreads, 'forum_threads_count'=> $forum_threads_count],
            HTTP_RESPONSE_OK);
    } // public function get_forum_threads($single_forum_slug)



    public function add_new_thread(ForumThreadRequest $request)
    {

        $loggedUser = Auth::guard('api')->user();
        if ( empty($loggedUser->id) ) {
            return response()->json(['error_code'=> 1, 'message'=> "You must be logged!", 'forumThreadRow'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $requestData= $request->all();
            $this->debToFile(print_r( $requestData,true),'ForumThreadRequest $requestData:: ');

            $newForumThread = new ForumThread();
            $newForumThread->creator_id= $loggedUser->id;
            $newForumThread->forum_id= $requestData['forum_id'];
            $newForumThread->title= $requestData['title'];
            $newForumThread->save();

            $newForumPost = new ForumPost();
            $newForumPost->user_id= $loggedUser->id;
            $newForumPost->body= $requestData['body'];
            $newForumPost->forum_thread_id= $newForumThread->id;
            $newForumPost->save();

            DB::commit();
            $this->sendSMSMessageByTwilio( print_r( $requestData,true),'ForumThreadRequest $requestData:: ' );
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'forumThreadRow'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $newForumThread->forum_posts_count= 1;
        $newForumThread->views= 1;
        $newForumThread->created_at= Carbon::now(config('app.timezone'));
        return response()->json(['error_code'=> 0, 'message'=> '', 'forumThreadRow'=>$newForumThread],HTTP_RESPONSE_OK);
    } // public function add_new_thread(ForumThreadRequest $request)

    //// FORUM THREADS BLOCK END



    //// THREAD POSTS BLOCK START
    public function get_single_forum_thread_by_slug($forum_slug)
    {
        $prefix = DB::getTablePrefix();

        $singleForumThread = ForumThread
            ::getBySlug($forum_slug)
            ->leftJoin('users', 'users.id', '=', 'forum_threads.creator_id')
            ->leftJoin('forums', 'forums.id', '=', 'forum_threads.forum_id')
            ->select(
                'forum_threads.id',
                'forum_threads.title',
                'forum_threads.slug',
                'forum_threads.is_salved',
                'forum_threads.created_at',
                'forum_threads.slug',
                'forum_threads.creator_id',
                'forum_threads.updated_at',
                'users.first_name',
                'users.last_name',
                \DB::raw($prefix . 'forums.title  as forums_title')
            )
            ->first();

        if ( empty($singleForumThread) ) {
            return response()->json(['error_code'=> 1, 'message'=> "Forum thread '".$forum_slug."' not found !", 'forumThreadRow'=>null],
            HTTP_RESPONSE_INTERNAL_SERVER_ERROR);

        }
            $parentForum= $singleForumThread->forum;
            $singleForumThread->creator_name = $singleForumThread->creator->full_name;
            $singleForumThread->creator_id   = $singleForumThread->creator->id;


        $thread_posts_count= ForumPost
            ::getByForumThreadId($singleForumThread->id)
            ->join('forum_threads', 'forum_threads.id', '=', 'forum_posts.forum_thread_id')
            ->count();

        $loggedUser = Auth::guard('api')->user();

        $is_forum_thread_owner=  $singleForumThread->creator_id == ( !empty($loggedUser->id) ? $loggedUser->id : null );
        $this->debToFile(print_r($parentForum, true), '-222 $parentForum::');

        $this->debToFile(print_r($parentForum, true), '-222 $parentForum::');
        return response()->json(['error_code' => 0, 'message' => '', 'is_forum_thread_owner' => $is_forum_thread_owner, 'singleForumThread' => $singleForumThread, 'parentForum' => $parentForum, 'thread_posts_count'=> $thread_posts_count],
            HTTP_RESPONSE_OK);
    } // public function get_single_forum_thread_by_slug($forum_slug)

    public function get_thread_posts()
    {
        $loggedUser = Auth::guard('api')->user();
        $prefix = DB::getTablePrefix();

        $request     = request();
        $requestData = $request->all();
        $this->debToFile(print_r($requestData, true), '  get_thread_posts  -0 $requestData::');

        $forum_thread_id        =  !empty($requestData['forum_thread_id']) ? $requestData['forum_thread_id'] : 3; // DEBUGGING
        $page            = ! empty($requestData['page']) ? (int)$requestData['page'] : 1;
        $order_by        = ! empty($requestData['order_by']) ? $requestData['order_by'] : 'id';
        $order_direction = ! empty($requestData['order_direction']) ? $requestData['order_direction'] : 'asc';


        $forum_posts_per_page = Settings::getValue('forum_posts_per_page', CheckValueType::cvtInteger, 20);

        $limit_start         = ($page - 1) * $forum_posts_per_page;

        $this->debToFile(print_r($limit_start, true), '  get_thread_posts  -3 $limit_start::');

        $thread_posts_count= ForumPost
            ::getByForumThreadId($forum_thread_id)
            ->count();

        $threadPosts= ForumPost
            ::getByForumThreadId($forum_thread_id)
            ->leftJoin('users', 'users.id', '=', 'forum_posts.user_id')

            ->orderBy($order_by, $order_direction)
            ->offset($limit_start)
            ->take($forum_posts_per_page)

            ->select(
                'forum_posts.*',
                'users.first_name',
                'users.last_name',
                'users.avatar',
                \DB::raw( $prefix . 'users.created_at as creator_created_at' )
            )
            ->get()
            ->map(function ($item) use ($loggedUser )  {
                $author_thread_posts_count= ForumPost
                    ::getByUserId($item->user_id)
                    ->count();
                $filenameData                     = User::setUserAvatarProps($item->user_id, $item->avatar, true);
//                $this->debToFile(print_r($filenameData, true), '  +++ $filenameData::');
                $item['creator_name'] = $item->user->full_name;
                $item['creator_id']   = $item->user->id;
                $item['filenameData'] = $filenameData;
                $item['author_thread_posts_count'] = $author_thread_posts_count;
                $item['is_post_owner'] = $item->user_id == ( !empty($loggedUser->id) ? $loggedUser->id : null );
                return $item;
            })
            ->all();


        return response()->json(['error_code' => 0, 'message' => '', 'threadPosts'=> $threadPosts, 'thread_posts_count'=> $thread_posts_count],
            HTTP_RESPONSE_OK);
    } // public function get_thread_posts($single_forum_slug)

    public function report_abuse_thread_post()
    {
        $request     = request();
        $requestData = $request->all();

        $forum_post_id         =  !empty($requestData['forum_post_id']) ? $requestData['forum_post_id'] : '';
        $user_id               =  !empty($requestData['user_id']) ? $requestData['user_id'] : '';
        $text                  =  !empty($requestData['text']) ? $requestData['text'] : '';

        $forumPost                 = new ForumPostReportAbuse();
        $forumPost->forum_post_id  = $forum_post_id;
        $forumPost->user_id        = $user_id;
        $forumPost->text           = $text;
        $forumPost->save();

        return response()->json(['error_code' => 0, 'message' => '', 'id'=> $forumPost->id],HTTP_RESPONSE_OK);
    } // public function report_abuse_thread_post()

    public function forum_post_store()
    {
        $loggedUser = Auth::guard('api')->user();
        if ( empty($loggedUser->id) ) {
            return response()->json(['error_code'=> 1, 'message'=> "You must be logged!", 'forumThreadRow'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $request     = request();
        $requestData = $request->all();

        $forum_thread_id       =  !empty($requestData['forum_thread_id']) ? $requestData['forum_thread_id'] : '';
        $user_id               =  !empty($requestData['user_id']) ? $requestData['user_id'] : '';
        $forum_post_body       =  !empty($requestData['forum_post_body']) ? $requestData['forum_post_body'] : '';

        $forumPost                 = new ForumPost();
        $forumPost->forum_thread_id= $forum_thread_id;
        $forumPost->user_id        = $user_id;
        $forumPost->body           = $forum_post_body;
        $forumPost->save();

        return response()->json(['error_code' => 0, 'message' => '', 'forumPost'=> $forumPost],HTTP_RESPONSE_OK);
    } // public function forum_post_store()

    public function forum_post_update()
    {
        $loggedUser = Auth::guard('api')->user();
        if ( empty($loggedUser->id) ) {
            return response()->json(['error_code'=> 1, 'message'=> "You must be logged!", 'forumThreadRow'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $request     = request();
        $requestData = $request->all();

        $forum_post_id         =  !empty($requestData['forum_post_id']) ? $requestData['forum_post_id'] : '';
        $forum_thread_id       =  !empty($requestData['forum_thread_id']) ? $requestData['forum_thread_id'] : '';
        $user_id               =  !empty($requestData['user_id']) ? $requestData['user_id'] : '';
        $forum_post_body       =  !empty($requestData['forum_post_body']) ? $requestData['forum_post_body'] : '';
        $forumPost             =  ForumPost::find($forum_post_id);
        if ($forumPost == null) {
            return response()->json(['error_code' => 11, 'message' => 'Forum Post "' . $forum_post_id . '" not found !', 'forumPost' => null],            HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $forumPost->body           = $forum_post_body;
        $forumPost->updated_at     = Carbon::now(config('app.timezone'));

        try {
            DB::beginTransaction();
            $forumPost->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'forum_thread_id'=> $forum_thread_id],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'forumPost'=> $forumPost],HTTP_RESPONSE_OK);
    } // public function forum_post_update()

    public function forum_post_delete()
    {
        $loggedUser = Auth::guard('api')->user();
        if ( empty($loggedUser->id) ) {
            return response()->json(['error_code'=> 1, 'message'=> "You must be logged!", 'forumThreadRow'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $request     = request();
        $requestData = $request->all();
        $forum_post_id       =  !empty($requestData['forum_post_id']) ? $requestData['forum_post_id'] : '';
        $this->debToFile(print_r($forum_post_id, true), '-11 $forum_post_id::');

        $forumPost           = ForumPost::find($forum_post_id);
        $forum_thread_id     = $forumPost->forum_thread_id;
        $forum_thread_deleted = false;

        try {
            DB::beginTransaction();
            $forumPost->delete();

            $forum_posts_count           = ForumPost::getByForumThreadId($forum_thread_id)->count();
            $this->debToFile(print_r($forum_posts_count, true), '-13 $forum_posts_count::');

            if ( $forum_posts_count == 0 ) {
                $this->debToFile(print_r(-14, true), '-14 CKECK DELETING::');
                $forumThread = ForumThread::find($forum_thread_id);
                if ( !empty($forumThread)) {
                    $forumThread->delete();
                    $this->debToFile(print_r(-15, true), '-15 make DELETING::');
                    $forum_thread_deleted= true;
                }
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'forum_post_id'=>$forum_post_id, 'forum_thread_deleted'=>$forum_thread_deleted],
            HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }



        return response()->json(['error_code' => 0, 'message' => '', 'forum_thread_deleted'=> $forum_thread_deleted],HTTP_RESPONSE_OK);
    } // public function forum_post_delete()

    public function forum_post_store_user_subscribed()
    {
        $loggedUser = Auth::guard('api')->user();
        if ( empty($loggedUser->id) ) {
            return response()->json(['error_code'=> 1, 'message'=> "You must be logged!", 'forumThreadRow'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $request     = request();
        $requestData = $request->all();

        $forum_post_id       =  !empty($requestData['forum_post_id']) ? $requestData['forum_post_id'] : '';
//        $this->debToFile(print_r($forum_post_id, true), '-11 $forum_post_id::');


        $forumPostUserSubscribed = ForumPostUserSubscribed::find($forum_post_id);
        if (!empty($forumPostUserSubscribed)) {
            return response()->json(['error_code'=> 1, 'message'=> "You are already subscribed to this post !", 'forumThreadRow'=>null],
                HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $newForumPostUserSubscribed= new ForumPostUserSubscribed();
        $newForumPostUserSubscribed->user_id           = $loggedUser->id;
        $newForumPostUserSubscribed->forum_post_id     = $forum_post_id;

        try {
            DB::beginTransaction();
            $newForumPostUserSubscribed->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'forum_post_id'=>$forum_post_id ],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }


        return response()->json(['error_code' => 0, 'message' => ''],HTTP_RESPONSE_OK);
    } // public function forum_post_store_user_subscribed()

    public function set_forum_as_best_decision()
    {
        $request     = request();
        $requestData = $request->all();

        $loggedUser = Auth::guard('api')->user();

        if ( empty($loggedUser->id) ) {
            return response()->json(['error_code'=> 1, 'message'=> "You must be logged!", 'forumThreadRow'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $forum_post_id       =  !empty($requestData['forum_post_id']) ? $requestData['forum_post_id'] : '';
        $forumPost           = ForumPost::find($forum_post_id);
        if ( empty($forumPost) ) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'Logged user id "' . $forum_post_id . '" not found !',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $relatedForumThread= $forumPost->forumThread()->first();
                $this->debToFile(print_r($relatedForumThread->creator_id, true), '  - set_forum_as_best_decision $relatedForumThread->creator_id::');

        if ( $relatedForumThread->creator_id != $loggedUser->id ) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'This thread is not made by you. You can not set best decision !',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $forumPost->is_best_decision     = true;
        $forumPost->updated_at           = Carbon::now(config('app.timezone'));
        $forumPost->save();

        return response()->json(['error_code' => 0, 'message' => '', 'id'=> $forum_post_id],HTTP_RESPONSE_OK);
    } // public function set_forum_as_best_decision()

    //// THREAD POSTS BLOCK END

}
