<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRegisterRequest;
use App\Http\Traits\FuncsTrait;
use App\UsersGroups;
use Illuminate\Http\Request;

use ImageOptimizer;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Settings;
use App\Customer;
use App\MyAppModel;
use App\Message;
use App\State;
use App\Facility;
use App\Region;
use App\Hostel;
use App\HostelRoom;
use App\PersonalHostelBookmark;
use App\PersonalOption;
use Illuminate\Routing\Controller as BaseController;

use App\User;
use App\library\CheckValueType;
use function Psy\debug;

class CustomerRegisterController extends BaseController
{
    use FuncsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    // CUSTOMER'S REGISTER BLOCK BEGIN
    public function store(CustomerRegisterRequest $request)
    {
        /*     'images_extensions'               => ['png', 'jpg', 'jpeg', 'gif'],
    'uploaded_file_max_mib'           => 14,
    'avatar_dimension_limits'         => ['max_width' => 64, 'max_height' => 64],
 */
        $customerAvatarUploadedFile = $request->file('avatar');

//        $uploaded_file_max_mib = (int)\Config::get('app.uploaded_file_max_mib', 1);
//        $max_size              = 1024 * $uploaded_file_max_mib;
//        $rules                 = array(
//            'image' => 'max:' . $max_size,
//        );
//        $validator             = Validator::make($request->all(), $rules);
//
//        if ($validator->fails()) {
//            return redirect()->back()
//                             ->withErrors($validator);
//        }
//


        try {
            DB::beginTransaction();
            $requestData= $request->all();
//            $this->debToFile(print_r( $requestData,true),'  app/Http/Controllers/Admin/CustomersController.php store $requestData::');
            $newCustomer = new Customer();
            $newCustomer->username= $requestData['username'];
            $newCustomer->first_name= $requestData['first_name'];
            $newCustomer->last_name= $requestData['last_name'];
            $newCustomer->password= bcrypt($requestData['password']);
            $newCustomer->account_type= $requestData['account_type'];
            $newCustomer->status= 'A';
            $newCustomer->verified= true;
            $newCustomer->email= $requestData['email'];
            $newCustomer->phone= $requestData['phone'];
            $newCustomer->website= $requestData['website'];
            $newCustomer->notes= $requestData['notes'];

            if ( ! empty($customerAvatarUploadedFile)) {
                $customer_avatar_image     = MyAppModel::checkValidImgName
                ( $requestData['avatar_filename'], with(new Customer)->getAvatarFilenameMaxLength(), true );

//                $this->debToFile(print_r( $customer_avatar_image,true),' INSISRE app/Http/Controllers/Admin/CustomersController.php store $customer_avatar_image::');

                $customer_avatar_file_path = $customerAvatarUploadedFile->getPathName();
//                $this->debToFile(print_r( $customer_avatar_file_path,true),' INSISRE 2 app/Http/Controllers/Admin/CustomersController.php store $customer_avatar_file_path::');
                $newCustomer->avatar    = $customer_avatar_image;
            }

            $newCustomer->save();

            if ( ! empty($customer_avatar_image)) {
                $dest_avatar = 'public/' . User::getUserAvatarPath($newCustomer->id, $customer_avatar_image);
                Storage::disk('local')->put($dest_avatar, File::get($customer_avatar_file_path));
                ImageOptimizer::optimize( storage_path().'/app/'.$dest_avatar, null );
            } // if ( !empty($customer_avatar_image) ) {

//            $this->debToFile(print_r( $newCustomer->id,true),'  CustomersController:store  -000 $newCustomer->id::');
//            $this->debToFile(print_r( $newCustomer,true),'  CustomersController:store  -000 $newCustomer::');
//
            $newUsersGroups= new UsersGroups();
            $newUsersGroups->user_id= $newCustomer->id;
            $newUsersGroups->group_id= ACCESS_ROLE_CUSTOMER;
            $newUsersGroups->save();

            DB::commit();
        } catch (Exception $e) {
//
//            $this->debToFile(print_r( $e->getMessage(),true),'   -0 $e->getMessage()::');
//            $this->debToFile(print_r( $e,true),'   -1 $e::');
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'newCustomer'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'customerRow'=>$newCustomer],HTTP_RESPONSE_OK);
    } // public function store(CreateCustomerRequest $request)

    // CUSTOMER'S REGISTER BLOCK END

}
