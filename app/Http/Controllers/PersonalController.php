<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

use App\Settings;
use App\Customer;
use App\Manager;
use App\Message;
use App\Http\Traits\FuncsTrait;
use App\State;
use App\Facility;
use App\Region;
use App\Hostel;
use App\HostelRoom;
use App\PersonalHostelBookmark;
use App\PersonalOption;
use App\HostelInquery;
use App\Http\Requests\PersonalRequest;
use Illuminate\Routing\Controller as BaseController;

use App\User;
use App\library\CheckValueType;
use function Psy\debug;

class PersonalController extends BaseController
{
    use FuncsTrait;

    public function get_personal($logged_user_id)
    {
//        $request     = request();
//        $requestData = $request->all();
//        $this->debToFile(print_r($requestData, true), '  send_message  -1 $requestData::');
//        $logged_user_id = ! empty($requestData['user_id']) ? $requestData['user_id'] : '';
//        $this->debToFile(print_r($logged_user_id, true), '  hostel_bookmarks $logged_user_id::');
        $loggedUser = User::find($logged_user_id);
        if (empty($loggedUser)) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'Logged user id "' . $logged_user_id . '" not found !',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $personalOptionsList     = [];
        $tempPersonalOptionsList = PersonalOption
            ::getByUserId($logged_user_id)
            ->get();
        foreach ($tempPersonalOptionsList as $nextTempPersonalOption) {
            $personalOptionsList[$nextTempPersonalOption->name] = $nextTempPersonalOption->value;
        }

//        $this->debToFile(print_r($personalOptionsList, true), '  hostel_bookmarks $personalOptionsList::');
        return response()->json(['error_code' => 0, 'message' => '', "personalOptionsList" => $personalOptionsList], HTTP_RESPONSE_OK);
    } //public function get_personal()

//Route::get('personal/{user_id}', 'PersonalController@get');
//Route::patch('personal/{id}', 'PersonalController@update');
//Route::delete('personal/{id}', 'PersonalController@destroy');


//Route::get('personal/{user_id}', 'PersonalController@get_personal');
//Route::patch('personal/{id}', 'PersonalController@update_personal');
//Route::delete('personal/{id}', 'PersonalController@destroy_personal');


    public function update(PersonalRequest $request)
    {
        $requestData = $request->all();

        $user_id = ! empty($requestData['id']) ? $requestData['id'] : '';
        $this->debToFile(print_r($_POST, true), '  app/Http/Controllers/Admin/PersonalController.php update $_POST::');
        $this->debToFile(print_r($_REQUEST, true), '  app/Http/Controllers/Admin/PersonalController.php update $_REQUEST::');
        $this->debToFile(print_r($user_id, true), '  app/Http/Controllers/Admin/PersonalController.php update $user_id::');
        $this->debToFile(print_r($requestData, true), '  app/Http/Controllers/Admin/PersonalController.php update $requestData::');

        $user = User::find($user_id);
        if ($user == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'User\'s task type # "' . $user_id . '" not found !',
                'Customer'   => (object)[
                    'name'        => 'Customer # "' . $user_id . '" not
            # found !',
                    'description' => ''
                ]
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $user->account_type = $requestData['account_type'];
            $user->first_name   = $requestData['first_name'];
            $user->last_name    = $requestData['last_name'];
            $user->phone        = $requestData['phone'];
            $user->website      = $requestData['website'];
            $user->notes        = $requestData['notes'];
            $user->updated_at   = Carbon::now(config('app.timezone'));
            $user->save($requestData);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'user' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'user' => $user], HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function destroy($id)  // ok
    {
        $this->debToFile(print_r($id, true), '  PersonalController  - destroy $id::');

        try {
            $user = Customer::find($id);
            if ($user == null) {
                return response()->json(['error_code' => 11, 'message' => 'Customer # "' . $id . '" not found !', 'user' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();

            $user->delete();

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'user' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }

    // LOGGED USER PERSONAL PROFILE BLOCK END


    // CUSTOMER'S BOOKMARKS BLOCK BEGIN
    public function get_hostel_bookmarks($logged_user_id)
    {
//        $request     = request();
//        $requestData = $request->all();
//        $this->debToFile(print_r($requestData, true), '  send_message  -1 $requestData::');
//        $logged_user_id = ! empty($requestData['user_id']) ? $requestData['user_id'] : '';
//        $this->debToFile(print_r($logged_user_id, true), '  get_hostel_bookmarks $logged_user_id::');
        $prefix     = DB::getTablePrefix();
        $loggedUser = User::find($logged_user_id);
        if (empty($loggedUser)) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'Logged user id "' . $logged_user_id . '" not found !',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $hostelBookmarks = PersonalHostelBookmark
            ::getByUserId($logged_user_id)
            ->join('hostels', 'hostels.id', '=', 'personal_hostel_bookmarks.hostel_id')
            ->leftJoin('regions', 'regions.id', '=', 'hostels.region_id')
            ->leftJoin('subregions', 'subregions.id', '=', 'hostels.subregion_id')
            ->leftJoin('states', 'states.id', '=', 'hostels.state_id')
            ->select(        //
                'personal_hostel_bookmarks.hostel_id',
                'hostels.name',
                'hostels.phone',
                'hostels.slug as slug',
                'hostels.short_descr',
                'hostels.feature',
                'hostels.rating',
                \DB::raw(' ( select min(' . $prefix . 'hostel_rooms.price) from ' . $prefix . 'hostel_rooms where ' . $prefix . 'hostel_rooms.hostel_id = ' . $prefix . 'hostels.id ) as min_hostel_rooms_price'),
                'regions.name as region_name',
                'states.name as state_name',
                'states.slug as state_slug',
                'regions.slug as region_slug',
                'subregions.slug as subregion_slug'
            )
            ->orderBy('personal_hostel_bookmarks.created_at', 'asc')
            ->get();

        return response()->json(['error_code' => 0, 'message' => '', "hostelBookmarks" => $hostelBookmarks], HTTP_RESPONSE_OK);
    } //public function get_hostel_bookmarks()

    public function add_hostel_to_bookmarks()
    {
        $request     = request();
        $requestData = $request->all();
        $user_id     = ! empty($requestData['user_id']) ? $requestData['user_id'] : '';
        $hostel_id   = ! empty($requestData['hostel_id']) ? $requestData['hostel_id'] : '';

        $newPersonalHostelBookmark            = new PersonalHostelBookmark();
        $newPersonalHostelBookmark->user_id   = $user_id;
        $newPersonalHostelBookmark->hostel_id = $hostel_id;

        $newPersonalHostelBookmark->save();

        return response()->json(['error_code' => 0, 'message' => '', "new_personal_hostel_bookmark_id" => $newPersonalHostelBookmark->id], HTTP_RESPONSE_OK);
    } //public function add_hostel_to_bookmarks()

    //    Route::delete('hostel_bookmarks/{user_id}/{hostel_id}', 'PersonalController@delete_from_hostel_bookmarks');
    public function delete_from_hostel_bookmarks($user_id, $hostel_id)
    {
        $this->debToFile(print_r($user_id, true), '  --1 $user_id::');
        $this->debToFile(print_r($hostel_id, true), '  --2 $hostel_id::');
        $deletePersonalHostelBookmark = PersonalHostelBookmark
            ::getByHostelId($hostel_id)
            ->getByUserId($user_id)
            ->first();
        if ($deletePersonalHostelBookmark == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'Personal hostel bookmark not found !',
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $deletePersonalHostelBookmark->delete();

        return response()->json(['error_code' => 0, 'message' => '', "deletePersonalHostelBookmark_id" => $deletePersonalHostelBookmark->id], HTTP_RESPONSE_OK);
    } //public function delete_from_hostel_bookmarks()

    // CUSTOMER'S BOOKMARKS BLOCK BEGIN


    // HOSTEL ENQUERY BLOCK SRART
    public function add_hostel_enquery()
    {
        $request     = request();
        $requestData = $request->all();
        $this->debToFile(print_r($requestData, true), '  add_hostel_enquery update $requestData::');

        $newHostelInquery                   = new HostelInquery();
        $newHostelInquery->creator_id       = ! empty($requestData['creator_id']) ? $requestData['creator_id'] : '';
        $newHostelInquery->hostel_id        = ! empty($requestData['hostel_id']) ? $requestData['hostel_id'] : '';
        $newHostelInquery->full_name        = ! empty($requestData['full_name']) ? $requestData['full_name'] : '';
        $newHostelInquery->email            = ! empty($requestData['email']) ? $requestData['email'] : '';
        $newHostelInquery->phone            = ! empty($requestData['phone']) ? $requestData['phone'] : '';
        $newHostelInquery->info             = ! empty($requestData['info']) ? $requestData['info'] : '';
        $newHostelInquery->start_date       = ! empty($requestData['start_date']) ? $requestData['start_date'] : '';
        $newHostelInquery->end_date         = ! empty($requestData['end_date']) ? $requestData['end_date'] : '';
        $newHostelInquery->request_callback = !empty($requestData['request_callback']) ? "Y" : 'N';
        try {
            DB::beginTransaction();
            $newHostelInquery->save();
            DB::commit();
            $this->sendSMSMessageByTwilio( $newHostelInquery->full_name .' sent new hostel_enquery request !' );
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'new_personal_hostel_enquery_id' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', "new_personal_hostel_enquery_id" => $newHostelInquery->id], HTTP_RESPONSE_OK);
    } //public function add_hostel_enquery()
    // HOSTEL ENQUERY BLOCK BEGIN


    // CUSTOMER'S CHAT BLOCK BEGIN
    public function managers_list()
    {

        $managersList     = [];
        $tempManagersList = Manager
            ::getByStatus('A')
            ->orderBy('username')
            ->get();
        foreach ($tempManagersList as $nextTempManager) {
            //     public static function setUserAvatarProps(int $user_id, string $avatar = null, bool $skip_non_existing_file = false): array
            $avatarData     = Manager::setUserAvatarProps($nextTempManager->id, $nextTempManager->avatar, true);
            $managersList[] = [
                'id'             => $nextTempManager->id,
                'user_id'        => $nextTempManager->id,
                'username'       => $nextTempManager->username,
                'user_full_name' => $nextTempManager->full_name,
                'is_top'         => $nextTempManager->is_top,
                'text'           => $nextTempManager->text,
                'created_at'     => $nextTempManager->created_at,
//                'chat_message_documents_count'     => $nextTempManager->chat_message_documents_count,
                'avatarData'     => $avatarData,
//                'user_id'=> $nextTempManager->user_id,
            ];
        }

        /*        foreach ($tempChatMessages as $nextTempChatMessage) {
            $avatarData     = User::setUserAvatarProps($nextTempChatMessage->user_id, $nextTempChatMessage->user->avatar, true);
            $chatMessages[] = [
                'id'             => $nextTempChatMessage->id,
                'user_id'        => $nextTempChatMessage->user_id,
                'username'       => $nextTempChatMessage->user->username,
                'user_full_name' => $nextTempChatMessage->user->full_name,
                'is_top'         => $nextTempChatMessage->is_top,
                'text'           => $nextTempChatMessage->text,
                'created_at'     => $nextTempChatMessage->created_at,
                'chat_message_documents_count'     => $nextTempChatMessage->chat_message_documents_count,
                'avatarData'     => $avatarData,
//                'user_id'=> $nextTempChatMessage->user_id,
            ];
        }

        $viewParams['chatMessages'] = $chatMessages;
 */


        $this->debToFile(print_r($managersList, true), '  managers_list $managersList::');

        return response()->json(['error_code' => 0, 'message' => '', "managersList" => $managersList], HTTP_RESPONSE_OK);
    } // public function managers_list()

    /*
        Route::get('get_manager_messages/{customer_id}/{manager_id}', 'PersonalController@get_manager_messages');
 */
    public function get_manager_messages($customer_id, $manager_id)
    {
        $this->debToFile(print_r($manager_id, true), '  get_manager_messages $manager_id::');
        // mark all messages with the selected contact as read
        Message::where('from', $manager_id)->where('to', $customer_id)->update(['read' => true]);

        // get all messages between the authenticated user and the selected user
        $messages = Message::where(function ($q) use ($customer_id, $manager_id) {
            $q->where('from', $customer_id);
            $q->where('to', $manager_id);
        })->orWhere(function ($q) use ($customer_id, $manager_id) {
            $q->where('from', $manager_id);
            $q->where('to', $customer_id);
        })
                           ->get();
        $this->debToFile(print_r($messages, true), '  get_manager_messages $messages::');

        return response()->json($messages);
    }

//        Route::post('send_message', 'PersonalController@send_message');

    public function send_message()
    {
        $request     = request();
        $requestData = $request->all();
        $this->debToFile(print_r($requestData, true), '  send_message  -1 $requestData::');
        //                     data: { text: text, from : this.customerRow.id, to: this.contact.id },
        $message = Message::create([
            'from' => $requestData['from'],
            'to'   => $requestData['to'],
            'text' => $requestData['text']
        ]);

//        broadcast(new NewMessage($message));
        return response()->json(['error_code' => 0, 'message' => '', "message" => $message], HTTP_RESPONSE_OK);
    }

    //

    public function index()
    {
        $this->debToFile(print_r(-11, true), '  PersonalController  -11 index::');
    }

    public function get($id)
    {
//        $this->debToFile(  print_r($id, true), '  PersonalController  -10 id)::') ;

        $user                = User::find($id);
        $personalOptions     = [];
        $tempPersonalOptions = PersonalOption
            ::getByUserId($id)
            ->get();
        foreach ($tempPersonalOptions as $nextTempPersonalOption) {
            $personalOptions[$nextTempPersonalOption->name] = $nextTempPersonalOption->value;
        }

        $check_roles_as_text = $user->checkRolesAsText();
//        echo '<pre>$check_roles_as_text::'.print_r($check_roles_as_text,true).'</pre>';
//        die("-1 XXZ");
//        $this->debToFile(print_r($user, true), '  PersonalController  -12 $user::');
//        $this->debToFile(print_r($personalOptions, true), '  PersonalController  -12 personalOptions::');

//        echo '<pre>$::'.print_r($user,true).'</pre>';
//        die("-1 XXZ");
        return response()->json([
            'error_code'          => 0,
            'message'             => '',
            "user"                => [
                'id'           => $user->id,
                'username'     => $user->username,
                'email'        => $user->email,
                'status'       => $user->status,
                'account_type' => $user->account_type,
                'first_name'   => $user->first_name,
                'last_name'    => $user->last_name,
                'phone'        => $user->phone,
                'website'      => $user->website,
                'notes'        => $user->notes,
                'creator_id'   => $user->creator_id,
                'activated_at' => $user->activated_at,
                'avatar'       => $user->avatar,
                'created_at'   => $user->created_at,
                'updated_at'   => $user->updated_at,
            ],
            'personalOptions'     => (object)$personalOptions,
            'check_roles_as_text' =>
                $check_roles_as_text
        ],
            HTTP_RESPONSE_OK);
    }
    // CUSTOMER'S CHAT BLOCK END


    // CUSTOMER'S CHAT BLOCK END
    // store_selected_payment_package

}
