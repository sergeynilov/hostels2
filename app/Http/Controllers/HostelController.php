<?php

namespace App\Http\Controllers;

use App\HostelExtraDetail;
use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Settings;
use App\Http\Traits\FuncsTrait;
use App\Customer;
use App\State;
use App\Config;
use App\Region;
use App\Hostel;
use App\HostelFacility;
use App\HostelInquery;
use App\HostelRoom;
use App\HostelImage;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Requests\HostelRequest;
use App\Http\Requests\HostelInqueryRequest;

use App\User;
use App\library\CheckValueType;

class HostelController extends BaseController
{
    use FuncsTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }


    public function hostels_miscellaneous()
    {
        $min_hostel_rooms_price = config('app.min_hostel_rooms_price', 0);
        $max_hostel_rooms_price = config('app.max_hostel_rooms_price', 100);

        $retArray = [
            'error_code'             => 0,
            'message'                => '',
            'min_hostel_rooms_price' => $min_hostel_rooms_price,
            'max_hostel_rooms_price' => $max_hostel_rooms_price
        ];

        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function hostels_miscellaneous()

    public function hostels()
    {

        $prefix  = DB::getTablePrefix();
        $request = request();
//        $requestData = $request->all();
        $retArray = ['error_code' => 0, 'message' => ''];

        $hostelsList     = [];
        $tempHostelsList = Hostel
            ::getByStatus('A')
//            ->getByRegionId(11)
//            ->getByStateId(4)
//            ->whereIn('hostels.id', [29, 30])
            ->leftJoin('states', 'states.id', '=', 'hostels.state_id')
            ->leftJoin('regions', 'regions.id', '=', 'hostels.region_id')
            ->leftJoin('subregions', 'subregions.id', '=', 'hostels.subregion_id')
//            ->orderBy('subregion_name', 'asc')
//            ->orderBy('hostels.is_priority_listing', 'asc')
            ->orderBy('subregion_name', 'asc')
            ->orderBy('min_hostel_rooms_price', 'asc')
//            ->offset( 0 )
//            ->take( 4 )
            ->select(
                'hostels.id', 'hostels.name', 'hostels.phone', 'hostels.email', 'hostels.website', 'hostels.subregion_id', 'hostels.region_id',
                'hostels.state_id', 'hostels.town', 'hostels.slug', 'hostels.short_descr', 'hostels.is_priority_listing', 'hostels.status',
                'hostels.is_homepage_spotlight', 'hostels.status',
                'hostels.descr', 'hostels.feature', 'hostels.rating', 'hostels.updated_at', 'hostels.created_at',
                'states.name as state_name', 'states.slug as state_slug',
                'regions.name as region_name', 'regions.slug as region_slug',
                'subregions.name as subregion_name', 'subregions.slug as subregion_slug',
                \DB::raw(' ( select count(' . $prefix . 'hostel_reviews.id) from ' . $prefix . 'hostel_reviews where ' . $prefix . 'hostel_reviews.hostel_id = ' . $prefix . 'hostels.id ) as hostel_reviews_count'),
                \DB::raw(' ( select count(' . $prefix . 'hostel_inqueries.id) from ' . $prefix . 'hostel_inqueries where ' . $prefix . 'hostel_inqueries.hostel_id = ' . $prefix . 'hostels.id ) as hostel_inqueries_count'),
                \DB::raw(' ( select min(' . $prefix . 'hostel_rooms.price) from ' . $prefix . 'hostel_rooms where ' . $prefix . 'hostel_rooms.hostel_id = ' . $prefix . 'hostels.id ) as min_hostel_rooms_price'),
                \DB::raw(' ( select ' . $prefix . 'hostel_images.filename from ' . $prefix . 'hostel_images where ' . $prefix . 'hostel_images.hostel_id = ' . $prefix . 'hostels.id and ' . $prefix . 'hostel_images.is_main = 1 limit 1) as filename')
            )
            ->distinct()
            ->get();
//        $this->debToFile(print_r($tempHostelsList, true), '-10 $tempHostelsList::');
        foreach ($tempHostelsList as $nextTempHostel) {

            $filenameData         = HostelImage::setHostelImageImageProps($nextTempHostel->id, $nextTempHostel->filename, true);
            $filenameData['info'] = $nextTempHostel->info;
//                    $this->debToFile(print_r($filenameData, true), '  - $filenameData::');
            $nextTempHostel['filenameData']   = $filenameData;
            $nextTempHostel['details_loaded'] = '';
            $hostelsList[]                    = $nextTempHostel;
        }

        $retArray['hostels'] = $hostelsList;

//        $this->debToFile(print_r($hostelsList, true), '-11 $hostels::');
        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function hostels()


    public function get_hostel_details($hostel_id)
    {

        $hostelImages     = [];
        $tempHostelImages = HostelImage
            ::getByHostelId($hostel_id)
            ->orderBy('is_main', 'desc')
            ->get();

        foreach ($tempHostelImages as $nextTempHostelImage) {
            $filenameData         = HostelImage::setHostelImageImageProps($nextTempHostelImage->hostel_id, $nextTempHostelImage->filename, true);
            $filenameData['info'] = $nextTempHostelImage->info;
            $hostelImages[]       = [
                'id'           => $nextTempHostelImage->id,
                'filename'     => $nextTempHostelImage->filename,
                'is_main'      => $nextTempHostelImage->is_main,
                'is_video'     => $nextTempHostelImage->is_video . ' ' . $nextTempHostelImage->is_video,
                'info'         => $nextTempHostelImage->info,
                'created_at'   => $nextTempHostelImage->created_at,
                'filenameData' => $filenameData,
            ];
        }  // foreach ($tempHostelImages as $nextTempHostelImage) {

        $hostelRooms = HostelRoom
            ::getByHostelId($hostel_id)
            ->orderBy('price', 'desc')
            ->get();

        $hostelFacilities = HostelFacility
            ::getByHostelId($hostel_id)
            ->leftJoin('facilities', 'facilities.id', '=', 'hostel_facilities.facility_id')
            ->orderBy('name', 'desc')
            ->select('hostel_facilities.*',
                'facilities.name'
            )
            ->get();

        return response()->json(['error_code'        => 0,
                                 'message'           => '',
                                 'hostelImages'      => $hostelImages,
                                 'hostelRooms'       => $hostelRooms,
                                 'hostelFacilities'  => $hostelFacilities,
                                 'img_preview_width' => with(new HostelImage)->getImgPreviewWidth()
        ], HTTP_RESPONSE_OK);
    } // public function get_hostel_details($hostel_id)

    public function get_hostel_by_slug($hostel_slug)
    {

        $hostel = Hostel
            ::getBySlug($hostel_slug)
            ->leftJoin('regions', 'regions.id', '=', 'hostels.region_id')
            ->leftJoin('subregions', 'subregions.id', '=', 'hostels.subregion_id')
            ->leftJoin('states', 'states.id', '=', 'hostels.state_id')
            ->select(        //
                'hostels.id',
                'hostels.name',
                'hostels.phone',
                'hostels.email',
                'hostels.website',
                'hostels.descr',
                'hostels.town',
                'hostels.status',
                'hostels.street_addr',
                'hostels.feature',
                'hostels.rating',
                'hostels.about_us',
                'hostels.cancellation_policy',
                'hostels.must_read_information',
                'hostels.updated_at',
                'hostels.created_at',
                'regions.name as region_name',
                'subregions.name as subregion_name',
                'states.name as state_name',
                \DB::raw(' ( select count(' . DB::getTablePrefix() . 'hostel_reviews.id) from ' . DB::getTablePrefix() . 'hostel_reviews where ' . DB::getTablePrefix() . 'hostel_reviews.hostel_id = ' . DB::getTablePrefix() . 'hostels.id ) as hostel_reviews_count'),
                \DB::raw(' ( select count(' . DB::getTablePrefix() . 'hostel_inqueries.id) from ' . DB::getTablePrefix() . 'hostel_inqueries where ' . DB::getTablePrefix() . 'hostel_inqueries.hostel_id = ' . DB::getTablePrefix() . 'hostels.id ) as hostel_inqueries_count')
            )
            ->first();


        $hostelExtraDetails = HostelExtraDetail
            ::getByHostelId($hostel->id)
            ->first();

        if (empty($hostelExtraDetails)) {
            $hostelExtraDetails = [
                "id"                    => null,
                "hostel_id"             => $hostel->id,
                "num_beds"              => 0,
                "min_nights"            => 0,
                "max_nights"            => 0,
                "bedsheets"             => "N",
                "towels"                => "N",
                "parking"               => "N",
                "airport_train"         => "N",
                "luggage"               => "N",
                "coed_dorm"             => "N",
                "bathroom"              => "N",
                "credit_cards"          => "N",
                "nonsmoking_rooms"      => "N",
                "smoke_free"            => "N",
                "pets_allowed"          => "N",
                "service_animals"       => "N",
                "wheelchair"            => "N",
                "internet_computers"    => "N",
                "wireless_internet"     => "N",
                "checkout"              => "N",
                "reception_hours_start" => "00:00:00",
                "reception_hours_end"   => "23:50:00"
            ];
        }
        $this->debToFile(print_r($hostelExtraDetails, true), '  GET  -0 $hostelExtraDetails::');

        return response()->json(['error_code' => 0, 'message' => '', 'hostel' => $hostel, 'hostelExtraDetails'=> $hostelExtraDetails], HTTP_RESPONSE_OK);
    } // public function get_hostel_by_slug($hostel_slug)


    /* 	public function get_subregions_list_by_region_id() {
		$this->load->model('msubregion', '', true);
		$UriArray = $this->uri->uri_to_assoc(3);
		$region_id = urldecode( AppUtils::getParameter($this, $UriArray, [], 'region-id') );
		$current_subregion_id = urldecode( AppUtils::getParameter($this, $UriArray, [], 'current-subregion-id') );
		// current_subregion_id
		$format = urldecode( AppUtils::getParameter($this, $UriArray, [], 'format', 'html') );
		$SubregionsList= $this->msubregion->getSubregionsList( false, '', $region_id );
		$html= '<option value="">Select an Area</option>';
		foreach( $SubregionsList as $Subregion ) {
			if ( $format== 'html' )
				$html.= '<option value="'.$Subregion['id'].'"  '.($current_subregion_id==$Subregion['id']?" selected ":"").' >'.$Subregion['name'].'</option>';
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'html'=> $html ) ) );
	}

	public function get_regions_list_by_state_id() {
		$this->load->model('mregion', '', true);
		$UriArray = $this->uri->uri_to_assoc(3);
		$state_id = urldecode( AppUtils::getParameter($this, $UriArray, [], 'state-id') );
		$current_region_id = urldecode( AppUtils::getParameter($this, $UriArray, [], 'current-region-id') );
		$format = urldecode( AppUtils::getParameter($this, $UriArray, [], 'format', 'html') );
		$RegionsList= $this->mregion->getRegionsList( false, '', $state_id );
		$html= '<option value="">Select a Region</option>';
		foreach( $RegionsList as $Region ) {
			if ( $format== 'html' )
				$html.= '<option value="'.$Region['id'].'"  '.($current_region_id==$Region['id']?" selected ":"").'  >'.$Region['name'].'</option>';
		}
		$this->output->set_content_type('application/json')->set_output(json_encode(array('ErrorMessage' => '', 'ErrorCode' => 0, 'html'=> $html ) ) );
	}

 */

    /*    public function get_hostels_sorted_by($sort_by)
        {
            $prefix   = DB::getTablePrefix();
            $sort_by_key = 'hostels.name';
            $sort_by_desc = 'asc';
            if ( empty($sort_by) or $sort_by == 'price' ) {
                $sort_by_key = 'min_hostel_rooms_price';
                $sort_by_desc= 'asc';
            }
            if ( $sort_by == 'name' ) {
                $sort_by_key = 'hostels.name';
                $sort_by_desc= 'asc';
            }
            if ( $sort_by == 'rating' ) {
                $sort_by_key = 'hostels.rating';
                $sort_by_desc= 'desc';
            }
            $limit_start= 0;
            $backend_per_page= 8;

            $this->debToFile(print_r($sort_by, true), '  get_hostels_sorted_by $sort_by::');
            $this->debToFile(print_r($sort_by_key, true), '  get_hostels_sorted_by $sort_by_key::');
            $hostelsList = Hostel
                ::getByStatus('A')
                ->leftJoin('subregions', 'subregions.id', '=', 'hostels.subregion_id')
    //            ->orderBy('subregion_name', 'asc')
    //            ->orderBy('hostels.is_priority_listing', 'asc')
                ->orderBy( $sort_by_key, $sort_by_desc )
                ->offset( $limit_start )
                ->take( $backend_per_page )

                ->select(
                    'hostels.id', 'hostels.name', 'hostels.phone', 'hostels.email', 'hostels.website', 'hostels.descr', 'hostels.feature', 'hostels.rating', 'hostels.updated_at',
                    'hostels.created_at', 'subregions.name as subregion_name', 'subregions.slug as subregion_slug',
                    \DB::raw(' ( select count('.$prefix.'hostel_reviews.id) from '.$prefix.'hostel_reviews where '.$prefix.'hostel_reviews.hostel_id = '.$prefix.'hostels.id ) as hostel_reviews_count'),
                    \DB::raw(' ( select count('.$prefix.'hostel_inqueries.id) from '.$prefix.'hostel_inqueries where '.$prefix.'hostel_inqueries.hostel_id = '.$prefix.'hostels.id ) as hostel_inqueries_count'),
                    \DB::raw(' ( select min( ifNull('.$prefix.'hostel_rooms.price , 0 ) ) from '.$prefix.'hostel_rooms where '.$prefix.'hostel_rooms.hostel_id = '.$prefix.'hostels.id ) as min_hostel_rooms_price')
                )
                ->get();
                return response()->json(['error_code' => 0, 'message' => '', 'hostelsList'=> $hostelsList], HTTP_RESPONSE_OK);
        } // public function get_hostels_sorted_by($sorted_by)*/

    public function get_hostels_by_region_id_state_id($state_id, $region_id, $output_type = '')
    {
        $is_debug = '';
        $prefix   = DB::getTablePrefix();
        $this->debToFile(print_r($output_type, true), '  TEXT  -12 $output_type::');
        $hostelList = Hostel
            ::getByRegionId($region_id)
            ->getByStateId($state_id)
            ->leftJoin('subregions', 'subregions.id', '=', 'hostels.subregion_id')
            ->orderBy('subregion_name', 'asc')
//            ->orderBy('hostels.is_priority_listing', 'asc')
            ->orderBy('min_hostel_rooms_price', 'asc')
            ->select(
                'hostels.id', 'hostels.name', 'hostels.phone', 'hostels.email', 'hostels.website', 'hostels.subregion_id', 'hostels.region_id',
                'hostels.state_id', 'hostels.town', 'hostels.slug', 'hostels.short_descr', 'hostels.is_priority_listing',
                'hostels.descr', 'hostels.feature', 'hostels.rating', 'hostels.updated_at', 'hostels.created_at', 'subregions.name as subregion_name',
                'subregions.slug as subregion_slug',
                \DB::raw(' ( select count(' . $prefix . 'hostel_reviews.id) from ' . $prefix . 'hostel_reviews where ' . $prefix . 'hostel_reviews.hostel_id = ' . $prefix . 'hostels.id ) as hostel_reviews_count'),
                \DB::raw(' ( select count(' . $prefix . 'hostel_inqueries.id) from ' . $prefix . 'hostel_inqueries where ' . $prefix . 'hostel_inqueries.hostel_id = ' . $prefix . 'hostels.id ) as hostel_inqueries_count'),
                \DB::raw(' ( select min(' . $prefix . 'hostel_rooms.price) from ' . $prefix . 'hostel_rooms where ' . $prefix . 'hostel_rooms.hostel_id = ' . $prefix . 'hostels.id ) as min_hostel_rooms_price')
            )
            ->distinct()
            ->get();
        if ($is_debug) {
            echo '<pre>0 $output_type ::' . print_r($output_type, true) . '</pre>';
        }
        if ($output_type == 'subregion_name_groupped') {
            if ($is_debug) {
                echo '<pre>-1 $output_type ::' . print_r($output_type, true) . '</pre>';
            }
            $is_first_row           = true;
            $current_subregion_id   = '';
            $current_subregion_name = '';
            $current_subregion_slug = '';

            $hostelsSubGroupArray = [];
            $subregionGroups      = [];
            foreach ($hostelList as $next_key => $nextHostel) { // all hostels by turn
                if ($is_debug) {
                    echo '<pre>0 $nextHostel->id::' . print_r($nextHostel->id, true) . '</pre>';
                    echo '<pre>0 $is_first_row::' . print_r($is_first_row, true) . '</pre>';
                }
                if ($is_first_row or $current_subregion_id != $nextHostel->subregion_id) {

                    if ( ! $is_first_row) {  // That is NOT first Row
                        if ($is_debug) {
                            echo '<pre>-3 $nextHostel->subregion_id::' . print_r($nextHostel->subregion_id, true) . '</pre>';
                        }
                        $subregionGroups[]      = [
                            'subregion_id'         => $current_subregion_id,
                            'subregion_name'       => $current_subregion_name,
                            'subregion_slug'       => $current_subregion_slug,
                            'hostelsSubGroupArray' => $hostelsSubGroupArray,
                        ];
                        $hostelsSubGroupArray   = [];
                        $current_subregion_name = $nextHostel->subregion_name;
                        $current_subregion_slug = $nextHostel->subregion_slug;
                        $current_subregion_id   = $nextHostel->subregion_id;
                    } // if ( !$is_first_row ) {  // That is NOT first Row
                    else {   // That is first Row
                        $current_subregion_name = $nextHostel->subregion_name;
                        $current_subregion_slug = $nextHostel->subregion_slug;
                        $current_subregion_id   = $nextHostel->subregion_id;
                        $hostelsSubGroupArray[] = [
                            'id'                     => $nextHostel->id,
                            'name'                   => $nextHostel->name,
                            'town'                   => $nextHostel->town,
                            'feature'                => $nextHostel->feature,
                            'slug'                   => $nextHostel->slug,
                            'short_descr'            => $nextHostel->short_descr,
                            'rating'                 => $nextHostel->rating,
                            'website'                => $nextHostel->website,
                            'phone'                  => $nextHostel->phone,
                            'email'                  => $nextHostel->email,
                            'is_priority_listing'    => $nextHostel->is_priority_listing,
                            'hostel_reviews_count'   => $nextHostel->hostel_reviews_count,
                            'hostel_inqueries_count' => $nextHostel->hostel_inqueries_count,
                            'min_hostel_rooms_price' => $nextHostel->min_hostel_rooms_price,
                        ];
                    } // else {   // That is first Row

                } else {
                    $hostelsSubGroupArray[] = [
                        'id'                     => $nextHostel->id,
                        'name'                   => $nextHostel->name,
                        'town'                   => $nextHostel->town,
                        'feature'                => $nextHostel->feature,
                        'slug'                   => $nextHostel->slug,
                        'short_descr'            => $nextHostel->short_descr,
                        'rating'                 => $nextHostel->rating,
                        'website'                => $nextHostel->website,
                        'phone'                  => $nextHostel->phone,
                        'email'                  => $nextHostel->email,
                        'is_priority_listing'    => $nextHostel->is_priority_listing,
                        'hostel_reviews_count'   => $nextHostel->hostel_reviews_count,
                        'hostel_inqueries_count' => $nextHostel->hostel_inqueries_count,
                        'min_hostel_rooms_price' => $nextHostel->min_hostel_rooms_price,
                    ];
                }

                $is_first_row = false;
                if ($is_debug) {
                    echo '<pre>$current_subregion_id::' . print_r($current_subregion_id, true) . '</pre>';
                    echo '<pre>$current_subregion_name::' . print_r($current_subregion_name, true) . '<hr><hr><hr><hr></pre>';
                }
            } // foreach ($hostelList as $next_key => $nextHostel) { // all hostels by turn
            if ( ! empty($current_subregion_id)) {
                $subregionGroups[] = [
                    'subregion_id'         => $current_subregion_id,
                    'subregion_name'       => $current_subregion_name,
                    'subregion_slug'       => $current_subregion_slug,
                    'hostelsSubGroupArray' => $hostelsSubGroupArray,
                ];
            }


            if ($is_debug) {
                echo '<pre>$subregionGroups::' . print_r($subregionGroups, true) . '</pre>';
            }
            $this->debToFile(print_r($subregionGroups, true), '  TEXT  -14 $subregionGroups::');

//            die("-1 XXZ");

            return response()->json(['error_code' => 0, 'message' => '', 'hostelsByRegionIdStateIdList' => $subregionGroups], HTTP_RESPONSE_OK);
        } // if ( $output_type == 'subregion_name_groupped' ) {

//        die("-1 XXZ");
        $this->debToFile(print_r($hostelList, true), '  TEXT  -11 $hostelList::');

        return response()->json(['error_code' => 0, 'message' => '', 'hostelsByRegionIdStateIdList' => $hostelList], HTTP_RESPONSE_OK);
    } // public function get_hostels_by_region_id_state_id($state_id, $region_id, $output_type= '')


    public function store_new_hostel_inquery__DEL(HostelInqueryRequest $request)
    {

        $loggedUser = Auth::guard('api')->user();
        if (empty($loggedUser->id)) {
            return response()->json(['error_code' => 1, 'message' => "You must be logged!", 'newHostel' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $requestData = $request->all();
            $this->debToFile(print_r($requestData, true), 'HostelInqueryRequest $requestData:: ');

            $newHostelInquery              = new HostelInquery();
            $newHostelInquery->hostel_id   = $requestData['hostel_id'];
            $newHostelInquery->customer_id = $requestData['customer_id'];
            $newHostelInquery->descr       = $requestData['descr'];
            $newHostelInquery->logo        = $requestData['logo'];
            $newHostelInquery->save();
            /* CREATE TABLE `ad_hostel_inqueries` (
                `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                `hostel_id` INT(11) NOT NULL,
                `creator_id` INT(10) UNSIGNED NULL DEFAULT NULL,
                `email` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
                `full_name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_unicode_ci',
                `phone` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
                `info` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
                `start_date` DATE NOT NULL,
                `end_date` DATE NULL DEFAULT NULL,
                `request_callback` VARCHAR(255) NOT NULL DEFAULT 'N' COLLATE 'utf8mb4_unicode_ci',
                `status` VARCHAR(255) NOT NULL DEFAULT 'N' COMMENT ' N=>New, A=>Accepted, O=> Completed, C => Cancelled ' COLLATE 'utf8mb4_unicode_ci',
                `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, */
            DB::commit();
            $this->sendSMSMessageByTwilio(print_r($requestData, true), 'HostelInqueryRequest $requestData:: ');
        } catch (Exception $e) {

            $this->debToFile(print_r($e->getMessage(), true), '   -0 $e->getMessage()::');
            $this->debToFile(print_r($e, true), '   -1 $e::');
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'newHostel' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'hostelRow' => $newHostelInquery], HTTP_RESPONSE_OK);
    } // public function store(CreateHostelRequest $request)


}
