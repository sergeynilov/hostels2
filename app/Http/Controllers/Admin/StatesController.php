<?php

namespace App\Http\Controllers\Admin;

use DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Hostel;
use Config;
use App\Customer;
use App\State;
use App\Region;

use App\Settings;

use App\Http\Requests\HostelRequest;
use App\Http\Traits\FuncsTrait;
use App\library\CheckValueType;

/*     public function index()
    {
        $this->addVariablesToJS();
        return view('admin.user_profiles.index');
    }
 */

class StatesController extends BaseController
{
    use FuncsTrait;

    public function index()
    {
        $request     = request();
        $requestData = $request->all();
        $this->debToFile(print_r($requestData, true), '  StatesController  -0 $requestData::');

//        $page                    = !empty($requestData['page']) ? (int)$requestData['page'] : 1;
        $statesList = [];
        //     public static function getStatesSelectionArray($active= '', $order_by= 'name') :array {
        $tempStatesList = State::getStatesSelectionArray();
        $regionsList    = Region::getRegionsSelectionArray();
        foreach ($tempStatesList as $nextTempState) {
            $regionsOfStateArray = [];
            foreach ($regionsList as $nextRegion) {
//                        $this->debToFile(print_r( $nextRegion,true),'  TEXT  -2 $nextRegion::');
                if ($nextRegion->state_id == $nextTempState->id) {
                    $related_common_hostels_count   = Hostel
                        ::getByRegionId($nextRegion->id)
                        ->getByStateId($nextTempState->id)
                        ->getByStatus('A')
                        ->count();
                    $related_featured_hostels_count = Hostel
                        ::getByRegionId($nextRegion->id)
                        ->getByStateId($nextTempState->id)
                        ->getByStatus('A')
                        ->getByFeature('F')
                        ->count();

                    $regionsOfStateArray[] = (object)[
                        'id'        => $nextRegion->id,
                        'text'      => $nextRegion->name . '(' . ' ( <u>' . Region::getRegionActiveLabel($nextRegion->active) . ', </u> '
                                       . $related_common_hostels_count . ' hostels, ' . $related_featured_hostels_count . ' featured )',
                        'label'     => $nextRegion->name . Region::getRegionActiveLabel($nextRegion->active),
                        'value'     => $nextRegion->id,
                        'item_type' => 'region',
                        'active'    => $nextRegion->active,
                        'icon'      => '',
                        'opened'    => false,
                        'selected'  => false,
                        'disabled'  => false,
                        'loading'   => false,
                        'children'  => [],
                    ];

                }
            }
            $statesList[] = (object)[
                'id'        => $nextTempState->id,
                'text'      => $nextTempState->code . ' : ' . $nextTempState->name . ' ( <u>' . State::getStateActiveLabel($nextTempState->active) . ' </u> )',
                'label'     => $nextTempState->code . ' : ' . $nextTempState->name,
                'value'     => $nextTempState->code . ' : ' . $nextTempState->name,
                'item_type' => 'state',
                'active'    => $nextTempState->active,
                'icon'      => '',
                'opened'    => true,
                'selected'  => false,
                'disabled'  => false,
                'loading'   => false,
                'children'  => $regionsOfStateArray,

            ];
        } // foreach( $tempStatesList as $next_key=>$nextTempState ) {
        $retArray['statesWithRegionsList'] = $statesList;

        $this->debToFile(print_r($statesList, true), '  StatesController  -4 $statesList::');

//        sleep(1);
        return response()->json(['error_code' => 0, 'message' => '', 'statesList' => $statesList], HTTP_RESPONSE_OK);
    }

    public function get($id)  //            axios.get('/api/hostel/'+hostel_id)
    {
        $hostel = Hostel::whereId($id)->first();

        return response()->json(["hostel" => $hostel], HTTP_RESPONSE_OK);
    }

    public function state_set_inactive()
    {
        $request     = request();
        $requestData = $request->all();
//        $this->debToFile(print_r($requestData, true), '  StatesController  -0 state_set_inactive::');
        $state_id = $requestData['state_id'] ? $requestData['state_id'] : '';

        $state = State::find($state_id);
        if (empty($state)) {
            return response()->json(['error_code' => 1, 'message' => 'state # ' . $state_id . ' not found !'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        try {
            DB::beginTransaction();
            $state->active = 'I';
            $state->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'state' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'state' => $state], HTTP_RESPONSE_OK);
    } //public function state_set_inactive()


    public function state_set_active()
    {
        $request     = request();
        $requestData = $request->all();
//        $this->debToFile(print_r($requestData, true), '  StatesController  -0 state_set_active::');
        $state_id = $requestData['state_id'] ? $requestData['state_id'] : '';

        $state = State::find($state_id);
        if (empty($state)) {
            return response()->json(['error_code' => 1, 'message' => 'State # ' . $state_id . ' not found !'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        try {
            DB::beginTransaction();
            $state->active = 'A';
            $state->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'state' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'state' => $state], HTTP_RESPONSE_OK);
    } //public function state_set_active()



    public function region_set_inactive()
    {
        $request     = request();
        $requestData = $request->all();
//        $this->debToFile(print_r($requestData, true), '  RegionsController  -0 region_set_inactive::');
        $region_id = $requestData['region_id'] ? $requestData['region_id'] : '';

        $region = Region::find($region_id);
        if (empty($region)) {
            return response()->json(['error_code' => 1, 'message' => 'region # ' . $region_id . ' not found !'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        try {
            DB::beginTransaction();
            $region->active = 'I';
            $region->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'region' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'region' => $region], HTTP_RESPONSE_OK);
    } //public function region_set_inactive()


    public function region_set_active()
    {
        $request     = request();
        $requestData = $request->all();
//        $this->debToFile(print_r($requestData, true), '  RegionsController  -0 region_set_active::');
        $region_id = $requestData['region_id'] ? $requestData['region_id'] : '';

        $region = Region::find($region_id);
        if (empty($region)) {
            return response()->json(['error_code' => 1, 'message' => 'Region # ' . $region_id . ' not found !'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        try {
            DB::beginTransaction();
            $region->active = 'A';
            $region->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'region' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'region' => $region], HTTP_RESPONSE_OK);
    } //public function region_set_active()


}
