<?php

namespace App\Http\Controllers\Admin;

use DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Facility;
use App\User;
use App\Settings;

use App\Http\Requests\FacilityRequest;
use App\Http\Traits\FuncsTrait;
use App\library\CheckValueType;

class FacilitiesController extends BaseController
{
    use FuncsTrait;


    public function index()
    {
//        factory(Facility::class, 200)->create();
        $request = request();
        $requestData= $request->all();
        $this->debToFile(print_r( $requestData,true),'  FacilitiesController  -0 $requestData::');

        $page                    = !empty($requestData['page']) ? (int)$requestData['page'] : 1;
        $order_by                = !empty($requestData['order_by']) ? $requestData['order_by'] : '';
        $order_direction         = !empty($requestData['order_direction']) ? $requestData['order_direction'] : '';
        $filter_name             = !empty($requestData['filter_name']) ? $requestData['filter_name'] : '';
        $backend_per_page = Settings::getValue('backend_per_page', CheckValueType::cvtInteger, 20);

        $this->debToFile(print_r( $filter_name,true),'  FacilitiesController  -3 $filter_name::');
        $limit_start= ($page - 1) * $backend_per_page ;
        $table_rows_count = Facility::count();
        $filtered_rows_count = Facility
            ::getByName($filter_name, true)
            ->orderBy( $order_by, $order_direction )
            ->count();
        $facilitiesList = Facility
            ::getByName($filter_name, true)
            ->orderBy( $order_by, $order_direction )
            ->offset( $limit_start )
            ->take( $backend_per_page )
            ->get();
        $this->debToFile(print_r( $limit_start,true),'  FacilitiesController  -4 $limit_start::');
//        sleep(2);
        return response()->json( [ 'error_code'=> 0, 'message'=> '','per_page'=> $backend_per_page, 'table_rows_count'=> $table_rows_count, 'filtered_rows_count'=> $filtered_rows_count, "facilitiesList" => $facilitiesList
        ], HTTP_RESPONSE_OK);
    }

    public function get($id)  //            axios.get('/api/facility/'+facility_id)
    {
        $facility = Facility::whereId($id)->first();
//        sleep(1);

        return response()->json( [ "facility" => $facility ], HTTP_RESPONSE_OK);
    }

    public function store(FacilityRequest $request)
    {

        $this->debToFile(print_r( $_POST,true),'  app/Http/Controllers/Admin/FacilitiesController.php store $_POST::');
        $loggedUser = Auth::guard('api')->user();
        if ( empty($loggedUser->id) ) {
            return response()->json(['error_code'=> 1, 'message'=> "You must be logged!", 'newFacility'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $requestData= $request->all();
            $newFacility = new Facility();
            $newFacility->name= $requestData['name'];
            $newFacility->descr= $requestData['descr'];
            $newFacility->save();
            DB::commit();
        } catch (Exception $e) {

            $this->debToFile(print_r( $e->getMessage(),true),'   -0 $e->getMessage()::');
            $this->debToFile(print_r( $e,true),'   -1 $e::');
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'newFacility'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'facilityRow'=>$newFacility],HTTP_RESPONSE_OK);
    } // public function store(CreateFacilityRequest $request)


    public function update(FacilityRequest $request)
    {
        $requestData= $request->all();

        $facility_id= !empty($requestData['id']) ? $requestData['id'] : '';
        $this->debToFile(print_r( $_POST,true),'  app/Http/Controllers/Admin/FacilitiesController.php update $_POST::');
        $this->debToFile(print_r( $_REQUEST,true),'  app/Http/Controllers/Admin/FacilitiesController.php update $_REQUEST::');
        $this->debToFile(print_r( $facility_id,true),'  app/Http/Controllers/Admin/FacilitiesController.php update $facility_id::');
        $this->debToFile(print_r( $requestData,true),'  app/Http/Controllers/Admin/FacilitiesController.php update $requestData::');
        $facility = Facility::find($facility_id);
        if ( $facility == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'User\'s task type # "'.$facility_id.'" not found !', 'Facility'=>(object)['name'=> 'Facility # "'.$facility_id.'" not
            # found !', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $facility->update($requestData);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'facility'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'facility'=>$facility],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function destroy($id)  // ok
    {
        $this->debToFile(print_r( $id,true),'  FacilitiesController  - destroy $id::');

        try {
            $facility = Facility::find($id);
            if ( $facility == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'Facility # "'.$id.'" not found !', 'facility'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();

            $facility->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'facility'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

}
