<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Settings;
use Config;
use App\Customer;
use App\State;
use App\Facility;
use App\Region;
use App\Hostel;
use App\HostelInquery;
use App\ForumPostReportAbuse;
use App\Http\Traits\FuncsTrait;
use App\HostelRoom;
use Illuminate\Routing\Controller as BaseController;

use App\User;
use App\library\CheckValueType;

class DashboardController extends BaseController
{
    use FuncsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function get_forum_post_report_abuses()
    {
        $prefix = DB::getTablePrefix();

        $usedForumPosts                = [];
        //    protected $fillable = [ 'user_id', 'forum_post_id',	'text' ];
        $feedbackForumPostReportAbuses = ForumPostReportAbuse
            ::leftJoin('users', 'users.id', '=', 'forum_post_report_abuses.user_id')
            ->leftJoin('forum_posts', 'forum_posts.id', '=', 'forum_post_report_abuses.forum_post_id')
//            ->groupBy('start_date')
            ->orderBy('forum_post_id', 'desc')
            ->select(
                'forum_post_report_abuses.*',
                DB::raw( 'concat( ' . $prefix . 'users.first_name, \' \',  ' . $prefix . 'users.last_name ) as creator_name' ),
                DB::raw( $prefix . 'forum_posts.body as forum_post_body' )
            )
            ->get();

        foreach ($feedbackForumPostReportAbuses as $nextForumPostReportAbuse) {
            if ( ! in_array($nextForumPostReportAbuse->id, $usedForumPosts)) {
                $usedForumPosts[] = [
                    'forum_post_id'    => $nextForumPostReportAbuse->forum_post_id,
                    'forum_post_body'  => $nextForumPostReportAbuse->forum_post_body
                ];
            }
        }

        return response()->json(['feedbackForumPostReportAbuses' => $feedbackForumPostReportAbuses, 'usedForumPosts' => $usedForumPosts], HTTP_RESPONSE_OK);

    } // public function get_forum_post_report_abuses()


    public function get_new_accepted_hostels_inqueries()
    {

        $prefix = DB::getTablePrefix();

        $usedDays                = [];
        $hostelInqueriesEvents   = [];
        $feedbackHostelInqueries = HostelInquery
            ::getByStatus(["N", "A"])// 'N' => 'New', 'A' => 'Accepted'

//                $query->whereIn(with(new ForumPostReportAbuse)->getTable().'.forum_post_id', $forum_post_id);
//            ->whereIn('hostel_inqueries.id', [48,49]) // DEGUG

            ->leftJoin('users', 'users.id', '=', 'hostel_inqueries.creator_id')
            ->orderBy('start_date', 'desc')
            ->select(
                'hostel_inqueries.*', DB::raw('concat( ' . $prefix . 'users.first_name, \' \',  ' . $prefix . 'users.last_name ) as creator_name')
            )
            ->get();
        foreach ($feedbackHostelInqueries as $nextNewHostelInquery) {
            $hostelInqueriesEvents[]  = [
                'hostel_id' => $nextNewHostelInquery->hostel_id,
                'status'    => $nextNewHostelInquery->status,
                'id'        => $nextNewHostelInquery->id,
                'url'       => '/admin/hostels/'.$nextNewHostelInquery->hostel_id,
                'startDate' => $nextNewHostelInquery->start_date,
                'endDate'   => $nextNewHostelInquery->end_date,
                'title'     => $nextNewHostelInquery->id.' : ' .
                               HostelInquery::getHostelInqueryStatusLabel($nextNewHostelInquery->status).' : '
                                .$nextNewHostelInquery->creator_name . ' : '.
                                $nextNewHostelInquery->info,
            ];
            if ( ! in_array($nextNewHostelInquery->start_date, $usedDays)) {
                $usedDays[] = $nextNewHostelInquery->start_date;
            }
        }

/*        $tempHostelInqueriesEvents = HostelInquery
            ::getByStatus(["N", "A"])// 'N' => 'New', 'A' => 'Accepted'
            ->leftJoin('users', 'users.id', '=', 'hostel_inqueries.creator_id')
            ->orderBy('start_date', 'desc')
            ->select(
                'hostel_inqueries.*', DB::raw('concat( ' . $prefix . 'users.first_name, \' \',  ' . $prefix . 'users.last_name ) as creator_name')
            )
            ->get();
        foreach ($feedbackHostelInqueries as $nextNewHostelInquery) {
            if ( ! in_array($nextNewHostelInquery->start_date, $usedDays)) {
                $usedDays[] = $nextNewHostelInquery->start_date;
            }
        }*/
        /* id - A unique identifier for the event. This is required and must be unique.
startDate - The date the event starts on the calendar. This must be either passed as a JavaScript date object, or as a string following an ISO-like form of "yyyy-mm-dd HH:MM:SS" (time is optional, and within time, minutes and seconds are both optional).
endDate - The date the event ends on the calendar. Defaults to the same date as startDate. This must be either passed as a JavaScript date object, or as a string following an ISO-like form of "yyyy-mm-dd HH:MM:SS" (time is optional, and within time, minutes and seconds are both optional).
title - The name of the event shown on the calendar. Defaults to "Untitled".
url - The URL associated with the event. The component has no built-in action associated with this, but it does add a "hasUrl" class to the event. To "follow" the URL, you'll need to listen for the click-event event and take the appropriate action.
classes - A String with any additional CSS classes you wish to assign to the event.
style - A String with any additional CSS styles you wish to apply to the event. */

        return response()->json(['feedbackHostelInqueries' => $feedbackHostelInqueries, 'usedDays' => $usedDays, 'hostelInqueriesEvents' => $hostelInqueriesEvents], HTTP_RESPONSE_OK);

    } // public function get_new_accepted_hostels_inqueries()


    public function system_info()
    {
//        $this->debToFile(print_r(-11, true), '  HomeController  -11 index::');
        $systemInfo = $this->getSystemInfo();
        return response()->json( [ 'error_code'=> 0, 'message'=> '', 'systemInfo'=> $systemInfo ], HTTP_RESPONSE_OK );
    }



    public function run_sql_statement()
    {
        $request     = request();
        $requestData = $request->all();
        $sql_statement= !empty($requestData['sql_statement']) ? $requestData['sql_statement'] : '';
        $output_result_type= !empty($requestData['output_result_type']) ? $requestData['output_result_type'] : '';
        $save_sql= !empty($requestData['save_sql']) ? $requestData['save_sql'] : 'N';
        $sql_info= !empty($requestData['sql_info']) ? $requestData['sql_info'] : '';
        $top= !empty($requestData['top']) ? $requestData['top'] : '';

        $this->debToFile(print_r($output_result_type, true), 'run_sql_statement -1 output_result_type::');
//        $this->debToFile(print_r($sql_statement, true), 'run_sql_statement -1 $sql_statement::');

//        $output_result_type = $this->getParameter( 'output_result_type', '' ); // 'B'=>'Both Result set and Explain', 'R'=>'Only Result set', 'E'=>'Only Explain'
        $result_explain_text= '';
        $resultRows= [];
//        $save_sql= $this->getParameter( 'save_sql', 'N' );
//        $sql_info= $this->getParameter( 'sql_info', '' );

//        $top = $this->getParameter( 'top', 'N' );
        $loggedUser = Auth::guard('api')->user();
        $user_id= $loggedUser->id;
        $key_id =  urlencode($this->generateKey(6).'_'.time().'_'.$user_id);
        try {
            if ( $output_result_type == 'B' or $output_result_type== 'R' ) { // 'B'=>'Both Result set and Explain', 'R'=>'Only Result set'
                $resultRows = DB::select($sql_statement);
                foreach ($resultRows as $next_key => $nextRow) {
                    $resultRows[$next_key] = (array)$nextRow;
                }
                $this->debToFile(print_r($resultRows, true), 'run_sql_statement -2 $resultRows::');
            } //
            if ( $save_sql == 'Y' ) { // save sql-statement
                $retArray = $this->addSmLine(
                    $key_id,              // string $key_id
                    $sql_statement,       // string $sql
                    $sql_info,            // string $info= ''
                    $top,                 // bool $top= false
                    'Y',            // bool $valid= false
                    $output_result_type
                );
            } // if ( $save ) { // save sql-statement

            if ( $output_result_type == 'B' or $output_result_type== 'E' ) { // 'B'=>'Both Result set and Explain', 'E'=>'Only Explain'
                $resultExplainRows = DB::select('EXPLAIN ' . $sql_statement);
                $this->debToFile(print_r($resultExplainRows, true), 'run_sql_statement -2 $resultExplainRows::');

                if ( ! empty($resultExplainRows) and is_array($resultExplainRows)) {
                    foreach ($resultExplainRows as $next_key => $nextResultExplainRow) {
                        $arr = (array)$nextResultExplainRow;
                        if ( ! empty($arr["QUERY PLAN"])) {
                            $result_explain_text .= ($next_key + 1) . '->' . str_repeat("&nbsp;", ($next_key + 1)) . $arr["QUERY PLAN"] . '<br>';
                        }
                    }
                }
            }

                return [ 'error_message' => '', 'error_code' => 0, 'output_result_type'=> $output_result_type, 'result_explain_text'=> $result_explain_text, 'resultRows'=> $resultRows, 'result_rows_count'=> count($resultRows), 'result_rows_as_text'=> print_r($resultRows,true), 'sql_statement' => $sql_statement ];
        } catch (\Exception $e) {
            if ( $save_sql == 'Y' ) { // save sql-statement
                $retArray = $this->addSmLine(
                    $key_id,          // string $key_id
                    $sql_statement,   // string $sql
                    $sql_info,            // string $info= ''
                    $top,             // bool $top= false
                    'N',            // bool $valid= false
                    $output_result_type,
                    $e->getMessage()
                );
            } // if ( $save ) { // save sql-statement
            return [ 'error_message' => $this->workTextString($e->getMessage()), 'error_code' => 1, 'resultRows'=> null, 'sql_statement' => $sql_statement ];
        }

        $retArray    = ['error_code' => 0, 'message' => ''];

        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function run_sql_statement()

    public function redraw_sql_statement()
    {
        $request     = request();
        $requestData = $request->all();

        $sql_statement= !empty($requestData['sql_statement']) ? $requestData['sql_statement'] : '';
        $this->debToFile(print_r($sql_statement, true), 'settings -1 $sql_statement::');

        //    private function formatSql($sql, $is_break_line = true, $is_include_html = true)
        $retArray    = [ 'error_code' => 0, 'message' => '', 'sql_statement'=> $this->formatSql( $sql_statement, false, false ) ];

        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function redraw_sql_statement()

    private function formatSql($sql, $is_break_line = true, $is_include_html = true)
    {
        $break_line = '';
        $space_char = '  ';
        if ($is_break_line) {
            if ( $is_include_html ) {
                $break_line = '<br>';
            }  else {
                $break_line = PHP_EOL;
            }
        }
        $bold_start= '';
        $bold_end= '';
        if ( $is_include_html ) {
            $bold_start= '<B>';
            $bold_end= '</B>';
        }
        $sql = ' ' . $sql . ' ';
        $left_cond= '~\b(?<![%\'])';
        $right_cond= '(?![%\'])\b~i';
        $sql = preg_replace( $left_cond . "insert[\s]+into" . $right_cond, $space_char . $space_char .$bold_start . "INSERT INTO" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "insert" . $right_cond, $space_char . $bold_start . "INSERT" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "delete" . $right_cond, $space_char . $bold_start . "DELETE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "values" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "VALUES" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "update" . $right_cond, $space_char . $bold_start . "UPDATE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "inner[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "INNER JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "straight[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "STRAIGHT_JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "left[\s]+join" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "LEFT JOIN" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "select" . $right_cond, $space_char . $bold_start . "SELECT" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "from" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "FROM" . $bold_end, $sql );
        $sql = preg_replace( $left_cond . "where" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "WHERE" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "group by" . $right_cond, $break_line . $space_char . $space_char . "GROUP BY" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "having" . $right_cond, $break_line . $space_char . $bold_start . "HAVING" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "order[\s]+by" . $right_cond, $break_line . $space_char . $space_char . $bold_start . "ORDER BY" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "and" . $right_cond,  $space_char . $space_char . $bold_start . "AND" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "or" . $right_cond,  $space_char . $space_char . $bold_start . "OR" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "as" . $right_cond,  $space_char . $space_char . $bold_start . "AS" . $bold_end, $sql);
        $sql = preg_replace( $left_cond . "exists" . $right_cond,  $break_line . $space_char . $space_char . $bold_start . "EXISTS" . $bold_end, $sql);
        return $sql;
    } // function formatSql($sql, $is_break_line = true, $is_include_html = true)

//Route::post('sql-monitor/run-sql-statement', 'DashboardController@run_sql_statement');
//Route::post('sql-monitor/redraw-sql-statement', 'DashboardController@redraw_sql_statement');

    private function generateKey($length= 8)	{
        $alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
        $Res = '';
        for ($I = 0; $I < $length; $I++) {
            $Index = random_int(0, strlen($alphabet) - 1);
            $Res .= substr($alphabet, $Index, 1);
        }
        return $Res;
    }

}
