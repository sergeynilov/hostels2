<?php

namespace App\Http\Controllers\Admin;

use DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

use App\Customer;
use App\UsersGroups;
use App\User;
use App\Settings;

use App\Http\Requests\CustomerRequest;
use App\Http\Traits\FuncsTrait;
use App\library\CheckValueType;

class CustomersController extends BaseController
{
    use FuncsTrait;

    public function index()
    {
        $request = request();
        $requestData= $request->all();
        $this->debToFile(print_r( $requestData,true),'  CustomersController  -0 $requestData::');

        /*                 axios.post(window.API_BACKEND_VERSION_LINK + '/admin/customers', { page : page, order_by : order_by, order_direction : order_direction, filter_username :
                filters.filter_username,  filter_status : filters.filter_status }).then((response) => {
 */
        $page                    = !empty($requestData['page']) ? (int)$requestData['page'] : 1;
        $order_by                = !empty($requestData['order_by']) ? $requestData['order_by'] : '';
        $order_direction         = !empty($requestData['order_direction']) ? $requestData['order_direction'] : '';
        $filter_username         = !empty($requestData['filter_username']) ? $requestData['filter_username'] : '';
        $filter_status           = !empty($requestData['filter_status']) ? $requestData['filter_status'] : '';
        if ( $filter_status == 'all' ) {
            $filter_status= '';
        }
        if ( strtolower($filter_status) == 'active' ) {
            $filter_status= 'A';
        }
        if ( strtolower($filter_status) == 'inactive' ) {
            $filter_status= 'I';
        }
        if ( strtolower($filter_status) == 'new' ) {
            $filter_status= 'N';
        }
        $backend_per_page = Settings::getValue('backend_per_page', CheckValueType::cvtInteger, 20);

        $this->debToFile(print_r( $filter_status,true),'  CustomersController  -3 $filter_status::');
        $limit_start= ($page - 1) * $backend_per_page ;
        $table_rows_count = Customer::count();
        $filtered_rows_count = Customer          // Select with users_groups.user_d = ACCESS_ROLE_CUSTOMER
            ::getByUsername($filter_username, true)
            ->getByStatus($filter_status)
            ->count();

        $customersList = Customer               // Select with users_groups.user_d = ACCESS_ROLE_CUSTOMER
            ::getByUsername($filter_username, true)
            ->getByStatus($filter_status)
            ->orderBy( $order_by, $order_direction )
            ->offset( $limit_start )
            ->take( $backend_per_page )
            ->get();
        $this->debToFile(print_r( $limit_start,true),'  CustomersController  -4 $limit_start::');
//        sleep(2);
        return response()->json( [ 'error_code'=> 0, 'message'=> '','per_page'=> $backend_per_page, 'table_rows_count'=> $table_rows_count, 'filtered_rows_count'=> $filtered_rows_count, "customersList" => $customersList
        ], HTTP_RESPONSE_OK);
    }

    public function get($id)
    {
//        $customer = Customer::whereId($id)->first();
        $customer = Customer::find($id);
//        sleep(1);
        return response()->json( [ "customer" => $customer ], HTTP_RESPONSE_OK);
    }

    public function store(CustomerRequest $request)
    {

        $this->debToFile(print_r( $_POST,true),'  app/Http/Controllers/Admin/CustomersController.php store $_POST::');
        $loggedUser = Auth::guard('api')->user();
        if ( empty($loggedUser->id) ) {
            return response()->json(['error_code'=> 1, 'message'=> "You must be logged!", 'newCustomer'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $requestData= $request->all();
            $newCustomer = new Customer();
            $newCustomer->username= $requestData['username'];
            $newCustomer->first_name= $requestData['first_name'];
            $newCustomer->last_name= $requestData['last_name'];
            $newCustomer->account_type= $requestData['account_type'];
            $newCustomer->status= $requestData['status'];
            $newCustomer->email= $requestData['email'];
            $newCustomer->phone= $requestData['phone'];
            $newCustomer->website= $requestData['website'];
            $newCustomer->notes= $requestData['notes'];
            $newCustomer->creator_id= $loggedUser->id;
            $newCustomer->save();
            /* CREATE TABLE `users` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    	`username` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	    `email` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`password` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`remember_token` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	    `status` ENUM('N','A','I') NOT NULL DEFAULT 'N' COMMENT ' N => New(Waiting activation), A=>Active, I=>Inactive' COLLATE 'utf8mb4_unicode_ci',
	    `account_type` ENUM('I','B') NOT NULL DEFAULT 'I' COMMENT ' I=>Individual, B=>Business' COLLATE 'utf8mb4_unicode_ci',
	`verified` TINYINT(1) NOT NULL DEFAULT '0',
	`verification_token` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`first_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`last_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`phone` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`website` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`notes` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`creator_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`activated_at` DATETIME NULL DEFAULT NULL,
	`avatar` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (`id`), */

            $this->debToFile(print_r( $newCustomer->id,true),'  CustomersController:store  -000 $newCustomer->id::');
            $this->debToFile(print_r( $newCustomer,true),'  CustomersController:store  -000 $newCustomer::');

            $newUsersGroups= new UsersGroups();
            $newUsersGroups->user_id= $newCustomer->id;
            $newUsersGroups->group_id= ACCESS_ROLE_CUSTOMER;
            $newUsersGroups->save();


            DB::commit();
        } catch (Exception $e) {

            $this->debToFile(print_r( $e->getMessage(),true),'   -0 $e->getMessage()::');
            $this->debToFile(print_r( $e,true),'   -1 $e::');
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'newCustomer'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'customerRow'=>$newCustomer],HTTP_RESPONSE_OK);
    } // public function store(CreateCustomerRequest $request)


    public function update(CustomerRequest $request)
    {
        $requestData= $request->all();

        $customer_id= !empty($requestData['id']) ? $requestData['id'] : '';
        $this->debToFile(print_r( $_POST,true),'  app/Http/Controllers/Admin/CustomersController.php update $_POST::');
        $this->debToFile(print_r( $_REQUEST,true),'  app/Http/Controllers/Admin/CustomersController.php update $_REQUEST::');
        $this->debToFile(print_r( $customer_id,true),'  app/Http/Controllers/Admin/CustomersController.php update $customer_id::');
        $this->debToFile(print_r( $requestData,true),'  app/Http/Controllers/Admin/CustomersController.php update $requestData::');
        $customer = Customer::find($customer_id);
        if ( $customer == null ) {
            return response()->json(['error_code'=> 11, 'message'=> 'User\'s task type # "'.$customer_id.'" not found !', 'Customer'=>(object)['name'=> 'Customer # "'.$customer_id.'" not
            # found !', 'description'=>'']], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $requestData['updated_at']= Carbon::now(config('app.timezone'));
            $customer->update($requestData);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'customer'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'customer'=>$customer],HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function destroy($id)  // ok
    {
        $this->debToFile(print_r( $id,true),'  CustomersController  - destroy $id::');

        try {
            $customer = Customer::find($id);
            if ( $customer == null ) {
                return response()->json(['error_code'=> 11, 'message'=> 'Customer # "'.$id.'" not found !', 'customer'=>null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();

            $customer->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'customer'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        return response()->json(['error_code'=> 0, 'message'=> ''],HTTP_RESPONSE_OK);
    }

}
