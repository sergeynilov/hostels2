<?php

namespace App\Http\Controllers\Admin;


use DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Routing\Controller as BaseController;
use App\Hostel;
use App\Settings;
use App\Facility;
use App\HostelFacility;
use App\HostelExtraDetail;
use App\HostelInquery;
use App\HostelImage;
use Illuminate\Validation\Rule;
use ImageOptimizer;

use App\Http\Requests\HostelRequest;
use App\Http\Traits\FuncsTrait;
use App\library\CheckValueType;

/*     public function index()
    {
        $this->addVariablesToJS();
        return view('admin.user_profiles.index');
    }
 */

class HostelsController extends BaseController

{
    use FuncsTrait;

    public function index()
    {
        $prefix      = DB::getTablePrefix();
        $request     = request();
        $requestData = $request->all();
        $this->debToFile(print_r($requestData, true), '  HostelsController  -0 $requestData::');

        $page            = ! empty($requestData['page']) ? (int)$requestData['page'] : 1;
        $order_by        = ! empty($requestData['order_by']) ? $requestData['order_by'] : '';
        $order_direction = ! empty($requestData['order_direction']) ? $requestData['order_direction'] : '';
        $filter_name     = ! empty($requestData['filter_name']) ? $requestData['filter_name'] : '';
        $filter_status   = ! empty($requestData['filter_status']) ? $requestData['filter_status'] : '';
        $filter_feature  = ! empty($requestData['filter_feature']) ? $requestData['filter_feature'] : '';
        $filter_hostel_inqueries  = ! empty($requestData['filter_hostel_inqueries']) ? $requestData['filter_hostel_inqueries'] : '';

        if ($filter_status == 'all') {
            $filter_status = '';
        }
        if (strtolower($filter_status) == 'active') {
            $filter_status = 'A';
        }
        if (strtolower($filter_status) == 'inactive') {
            $filter_status = 'I';
        }
        if (strtolower($filter_status) == 'new') {
            $filter_status = 'N';
        }

        if ($filter_feature == 'all') {
            $filter_feature = '';
        }
        if (strtolower($filter_feature) == 'feature') {
            $filter_feature = 'F';
        }
        if (strtolower($filter_feature) == 'not_feature') {
            $filter_feature = 'S';
        }



        $backend_per_page = Settings::getValue('backend_per_page', CheckValueType::cvtInteger, 20);

        $this->debToFile(print_r($filter_name, true), '  HostelsController  -3 $filter_name::');
        $limit_start         = ($page - 1) * $backend_per_page;
        $table_rows_count    = Hostel::count();
        $filtered_rows_count = Hostel
            ::getByName($filter_name, true)
            ->getByStatus($filter_status)
            ->getByFeature($filter_feature)
            ->orderBy($order_by, $order_direction)
            ->count();
        $hostelsList         = Hostel
            ::getByName($filter_name, true)
            ->getByStatus($filter_status)
            ->getByFeature($filter_feature)
            ->newHostelInqueriesCount($filter_hostel_inqueries == 'new')
            ->acceptedHostelInqueriesCount($filter_hostel_inqueries == 'accepted')
            ->leftJoin('states', 'states.id', '=', 'hostels.state_id')
            ->leftJoin('regions', 'regions.id', '=', 'hostels.region_id')
            ->orderBy($order_by, $order_direction)
            ->offset($limit_start)
            ->take($backend_per_page)
//            ->havingRaw('new_hostel_inqueries_count > 0')
//            ->havingRaw('accepted_hostel_inqueries_count > 0')
            ->select(
                'hostels.id',
                'hostels.name',
                'hostels.status',
                'hostels.state_id',
                'hostels.feature',
                'hostels.region_id',
                'hostels.created_at',
                'regions.name as region_name',
                'states.name as state_name',
                \DB::raw(' ( select count('.$prefix.'hostel_inqueries.id) from '.$prefix.'hostel_inqueries where '.$prefix.'hostel_inqueries.hostel_id = '
                         .$prefix.'hostels.id and '.$prefix.'hostel_inqueries.status = \'N\' ) as new_hostel_inqueries_count'),
                \DB::raw(' ( select count('.$prefix.'hostel_inqueries.id) from '.$prefix.'hostel_inqueries where '.$prefix.'hostel_inqueries.hostel_id = '
                         .$prefix.'hostels.id and '.$prefix.'hostel_inqueries.status = \'A\' ) as accepted_hostel_inqueries_count')

            //     private static $hostelInqueryRequestCallbackLabelValueArray = Array('N' => 'New', 'A' => 'Accepted', 'O' => 'Completed', 'C' => 'Cancelled');
//                \DB::raw('count('.DB::getTablePrefix().'hostel_inqueries.id) as new_hostel_inqueries_count')
            )
            ->get();
/*         $tempHostelsList = Hostel
            ::getByStatus('A')
            ->getByName( $filter_search_text, true )
            ->getByFeature( ( $filter_only_feature ? "F" : "") )
            ->getByFacilityId( $filter_facilitiesSelectionArray, 'hostel_facilities' )
            ->getByPriceFrom( ( !empty($filter_price_values[0]) ? $filter_price_values[0] : null ), $prefix.'hostel_rooms' )
            ->getByPriceTill( ( !empty($filter_price_values[1]) ? $filter_price_values[1] : null ), $prefix.'hostel_rooms' )
            ->getByRatingFrom( ( !empty($filter_rating_values[0]) ? $filter_rating_values[0] : null ), $prefix.'hostels' )
            ->getByRatingTill( ( !empty($filter_rating_values[1]) ? $filter_rating_values[1] : null ), $prefix.'hostels' )

//            ->getByRatingTill( ( !empty($filter_rating_values[1]) ? $filter_rating_values[1] : null ), $prefix.'hostels' )
//            ->getByRatingTill( ( !empty($filter_rating_values[1]) ? $filter_rating_values[1] : null ), $prefix.'hostels' )

                //     public function scopeGetByHostelId($query, $hostel_id= null)

//            ->getByPriceTill( !empty($filter_price_values[1]) ? $filter_price_values[1] : null ) //scopeGetByPriceFrom
//            ->getByStateId(4)
//            ->whereIn('hostels.id', [29, 30])
            ->leftJoin('states', 'states.id', '=', 'hostels.state_id')
            ->leftJoin('regions', 'regions.id', '=', 'hostels.region_id')
            ->leftJoin('subregions', 'subregions.id', '=', 'hostels.subregion_id')
            ->leftJoin('hostel_rooms', 'hostel_rooms.hostel_id', '=', 'hostels.id')
            ->leftJoin('hostel_facilities', 'hostel_facilities.hostel_id', '=', 'hostels.id')
//            ->orderBy('subregion_name', 'asc')
//            ->orderBy('hostels.is_priority_listing', 'asc')
            ->orderBy($filter_hostels_search_by_fields, $filter_search_by_fields_ordering)
//            ->orderBy('min_hostel_rooms_price', 'asc')
//            ->orderBy('subregion_name', 'asc')
//            ->orderBy('min_hostel_rooms_price', 'asc')
//            ->offset( 0 )
//            ->take( 4 )
            ->select(
                'hostels.id', 'hostels.name', 'hostels.phone', 'hostels.email', 'hostels.website',  'hostels.subregion_id',  'hostels.region_id',  'hostels.state_id', 'hostels.town', 'hostels.slug', 'hostels.short_descr', 'hostels.is_priority_listing',  'hostels.status', 'hostels.is_homepage_spotlight',
                'hostels.descr', 'hostels.feature', 'hostels.rating', 'hostels.updated_at', 'hostels.created_at',
                'states.name as state_name', 'states.slug as state_slug',
                'regions.name as region_name', 'regions.slug as region_slug',
                'subregions.name as subregion_name', 'subregions.slug as subregion_slug',
                \DB::raw(' ( select count('.$prefix.'hostel_reviews.id) from '.$prefix.'hostel_reviews where '.$prefix.'hostel_reviews.hostel_id = '.$prefix.'hostels.id ) as hostel_reviews_count'),
                \DB::raw(' ( select count('.$prefix.'hostel_inqueries.id) from '.$prefix.'hostel_inqueries where '.$prefix.'hostel_inqueries.hostel_id = '.$prefix.'hostels.id ) as hostel_inqueries_count'),
                \DB::raw(' ( select min('.$prefix.'hostel_rooms.price) from '.$prefix.'hostel_rooms where '.$prefix.'hostel_rooms.hostel_id = '.$prefix.'hostels.id ) as min_hostel_rooms_price'),
                \DB::raw(' ( select '.$prefix.'hostel_images.filename from '.$prefix.'hostel_images where '.$prefix.'hostel_images.hostel_id = '.$prefix.'hostels.id and '.$prefix.'hostel_images.is_main = 1 limit 1) as filename')
            ) */

/*             if ($next_param_name == 'newHostelInqueriesCountByStatesArray') {
                $retArray['newHostelInqueriesCountByStatesArray'] =
                    HostelInquery
                        ::getByStatus('N')
                        ->select( "states.name as state_name", \DB::raw('count('.DB::getTablePrefix().'hostel_inqueries.id) as new_hostel_inqueries_count') )
                        ->join( "hostels", "hostels.id", '=', 'hostel_inqueries.hostel_id' )
                        ->join( "states", "states.id", '=', 'hostels.state_id' )
                        ->groupBy("states.name")
                        ->get();
            }
 */

/*        $hostelsList         = Hostel
            ::getByName($filter_name, true)
            ->getByStatus($filter_status)
            ->getByFeature($filter_feature)
            ->leftJoin('states', 'states.id', '=', 'hostels.state_id')
            ->leftJoin('regions', 'regions.id', '=', 'hostels.region_id')
            ->orderBy($order_by, $order_direction)
            ->offset($limit_start)
            ->take($backend_per_page)
            ->select('hostels.id', 'hostels.name', 'hostels.status', 'hostels.state_id', 'hostels.feature', 'hostels.region_id', 'hostels.created_at',
                'regions.name as region_name', 'states.name as state_name')
            ->get();
        */

        $this->debToFile(print_r($limit_start, true), '  HostelsController  -4 $limit_start::');

//        sleep(1);
        return response()->json([
            'error_code'          => 0,
            'message'             => '',
            'per_page'            => $backend_per_page,
            'table_rows_count'    => $table_rows_count,
            'filtered_rows_count' => $filtered_rows_count,
            "hostelsList"         => $hostelsList
        ], HTTP_RESPONSE_OK);
        /*         $filtersArray= ['fill_labels'=>1, 'short_description'=>'', 'name'=>'', 'show_task_assigned_to_users_count'=> 1];
                $page= (int)$this->getParameter( 'page', 1 );
                $order_by= $this->getParameter( 'order_by', 'name' );
                $order_direction= $this->getParameter( 'order_direction', 'asc' );
                try {
                } catch (Exception $e) {
                    return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'rows_count'=> null, 'userTaskTypesList'=>null ],
                        HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
                }
                $per_page= with(new UserTaskType)->getItemsPerPage();
                return response()->json( [ 'error_code'=> 0, 'message'=> '', 'rows_count'=> $rows_count, 'userTaskTypesList'=>$userTaskTypesList, 'per_page'=> $per_page ],
                    HTTP_RESPONSE_OK);
         */
    }

    public function get($id)
    {
//        $hostel                     = Hostel::whereId($id)->first();

        $hostel = Hostel
            ::whereIn('hostels.id', [$id])
            ->leftJoin('states', 'states.id', '=', 'hostels.state_id')
            ->leftJoin('regions', 'regions.id', '=', 'hostels.region_id')
            ->leftJoin('subregions', 'subregions.id', '=', 'hostels.subregion_id')
            ->select(
                'hostels.*',
                'states.name as state_name', 'states.slug as state_slug',
                'regions.name as region_name', 'regions.slug as region_slug',
                'subregions.name as subregion_name', 'subregions.slug as subregion_slug'
            )
            ->first();

        $facilitiesIdValueArray     = [];
        $tempFacilitiesIdValueArray = Facility::getFacilitiesSelectionArray();
        $this->debToFile(print_r($tempFacilitiesIdValueArray, true), '  -10 $tempFacilitiesIdValueArray::');
        foreach ($tempFacilitiesIdValueArray as $next_id => $next_name) {


            $relatedHostelFacility = HostelFacility
                ::getByHostelId($id)
                ->getByFacilityId($next_id)
                ->get();

            $facilitiesIdValueArray[] = [
                'key'        => $next_id,
                'label'      => $next_name,
                'is_checked' => count($relatedHostelFacility) > 0,
            ];
        }

        $this->debToFile(print_r($facilitiesIdValueArray, true), '  -11 facilitiesIdValueArray::');

        $hostelExtraDetails = HostelExtraDetail
            ::getByHostelId($id)
            ->first();

        if (empty($hostelExtraDetails)) {
            $hostelExtraDetails = [
                "id"                    => null,
                "hostel_id"             => $id,
                "num_beds"              => 0,
                "min_nights"            => 0,
                "max_nights"            => 0,
                "bedsheets"             => "N",
                "towels"                => "N",
                "parking"               => "N",
                "airport_train"         => "N",
                "luggage"               => "N",
                "coed_dorm"             => "N",
                "bathroom"              => "N",
                "credit_cards"          => "N",
                "nonsmoking_rooms"      => "N",
                "smoke_free"            => "N",
                "pets_allowed"          => "N",
                "service_animals"       => "N",
                "wheelchair"            => "N",
                "internet_computers"    => "N",
                "wireless_internet"     => "N",
                "checkout"              => "N",
                "reception_hours_start" => "00:00:00",
                "reception_hours_end"   => "23:50:00"
            ];
        }
//        $this->debToFile(print_r($hostelExtraDetails, true), '  GET  -0 $hostelExtraDetails::');


        return response()->json(["hostel" => $hostel, 'facilitiesIdValueArray' => $facilitiesIdValueArray, 'hostelExtraDetails' => $hostelExtraDetails],
            HTTP_RESPONSE_OK);
    }


    public function store(HostelRequest $request)
    {

//        $this->debToFile(print_r($_POST, true), '  app/Http/Controllers/Admin/HostelsController.php store $_POST::');
        $loggedUser = Auth::guard('api')->user();
        if (empty($loggedUser->id)) {
            return response()->json(['error_code' => 1, 'message' => "You must be logged!", 'newHostel' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();
            $requestData                      = $request->all();
            $newHostel                        = new Hostel();
            $newHostel->name                  = $requestData['name'];
            $newHostel->user_id               = $loggedUser->id;
            $newHostel->modified_by           = $loggedUser->id;
            $newHostel->region_id             = $requestData['region_id'];
            $newHostel->state_id              = $requestData['state_id'];
            $newHostel->subregion_id          = $requestData['subregion_id'];
            $newHostel->email                 = $requestData['email'];
            $newHostel->phone                 = $requestData['phone'];
            $newHostel->town                  = $requestData['town'];
            $newHostel->street_addr           = $requestData['street_addr'];
            $newHostel->postal_code           = $requestData['postal_code'];
            $newHostel->feature               = $requestData['feature'];
            $newHostel->short_descr           = $requestData['short_descr'];
            $newHostel->descr                 = $requestData['descr'];
            $newHostel->website               = $requestData['website'];
            $newHostel->status                = $requestData['status'];
            $newHostel->about_us              = !empty($requestData['about_us']) ? $requestData['about_us'] : '';
            $newHostel->cancellation_policy   = !empty($requestData['cancellation_policy']) ? $requestData['cancellation_policy'] : '';
            $newHostel->must_read_information = !empty($requestData['must_read_information']) ? $requestData['must_read_information'] : '';
            $newHostel->save();
            $facilitiesIdValueArray = ! empty($requestData['facilitiesIdValueArray']) ? $requestData['facilitiesIdValueArray'] : [];
            foreach ($facilitiesIdValueArray as $nextHostelFacility) {
                if ($nextHostelFacility['is_checked']) {
                    $newHostelFacility              = new HostelFacility();
                    $newHostelFacility->facility_id = $nextHostelFacility['key'];
                    $newHostelFacility->hostel_id   = $newHostel;
                    $newHostelFacility->save();
                }
            }

            DB::commit();
        } catch (Exception $e) {

            $this->debToFile(print_r($e->getMessage(), true), '   -0 $e->getMessage()::');
            $this->debToFile(print_r($e, true), '   -1 $e::');
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'newHostel' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'hostelRow' => $newHostel], HTTP_RESPONSE_OK);
    } // public function store(CreateHostelRequest $request)


    public function update(HostelRequest $request)
    {
        $requestData = $request->all();


        //                 this.$data.hostelRow.facilitiesIdValueArray= this.facilitiesIdValueArray;
        $hostel_id              = ! empty($requestData['id']) ? $requestData['id'] : '';
        $facilitiesIdValueArray = ! empty($requestData['facilitiesIdValueArray']) ? $requestData['facilitiesIdValueArray'] : [];
        $hostelExtraDetails     = ! empty($requestData['hostelExtraDetails']) ? $requestData['hostelExtraDetails'] : [];
        $this->debToFile(print_r($requestData, true), '  GET  -000 $requestData::');
        $this->debToFile(print_r($hostelExtraDetails, true), '  GET  -001 $hostelExtraDetails::');
//
//        $newHostel->website               = $requestData['website'];
//        $newHostel->status                = $requestData['status'];
//        $newHostel->about_us              = !empty($requestData['about_us']) ? $requestData['about_us'] : '';
//        $newHostel->cancellation_policy   = !empty($requestData['cancellation_policy']) ? $requestData['cancellation_policy'] : '';
//        $newHostel->must_read_information = !empty($requestData['must_read_information']) ? $requestData['must_read_information'] : '';
//        $newHostel->save();

        //        $this->debToFile(print_r( $_POST,true),'  app/Http/Controllers/Admin/HostelsController.php update $_POST::');
//        $this->debToFile(print_r( $_REQUEST,true),'  app/Http/Controllers/Admin/HostelsController.php update $_REQUEST::');
//        $this->debToFile(print_r( $hostel_id,true),'  app/Http/Controllers/Admin/HostelsController.php update $hostel_id::');
//        $this->debToFile(print_r( $requestData,true),'  app/Http/Controllers/Admin/HostelsController.php update $requestData::');

        $hostel = Hostel::find($hostel_id);
        if ($hostel == null) {
            return response()->json([
                'error_code' => 11,
                'message'    => 'User\'s task type # "' . $hostel_id . '" not found !',
                'Hostel'     => (object)[
                    'name'        => 'Hostel # "' . $hostel_id . '" not # found !',
                    'description' => ''
                ]
            ], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        try {
            DB::beginTransaction();

            $requestData['updated_at']= Carbon::now(config('app.timezone'));
            $this->debToFile(print_r($requestData, true), '  GET  -111 $requestData::');
            $hostel->update($requestData);

            $relatedHostelFacilities = HostelFacility
                ::getByHostelId($hostel_id)
                ->get();

            foreach ($relatedHostelFacilities as $nextRelatedHostelFacility) {
                $nextRelatedHostelFacility->delete();
            }
            foreach ($facilitiesIdValueArray as $nextHostelFacility) {
                if ($nextHostelFacility['is_checked']) {
                    $newHostelFacility              = new HostelFacility();
                    $newHostelFacility->facility_id = $nextHostelFacility['key'];
                    $newHostelFacility->hostel_id   = $hostel_id;
                    $newHostelFacility->save();
                }
            }
            // hostelExtraDetails
            $hostelExtraDetailsRow = HostelExtraDetail
                ::getByHostelId($hostel_id)
                ->first();

            $this->debToFile(print_r($hostelExtraDetailsRow, true), '  GET  -0 $hostelExtraDetailsRow::');

            if (empty($hostelExtraDetailsRow)) {
                $hostelExtraDetailsRow            = new HostelExtraDetail();
                $hostelExtraDetailsRow->hostel_id = $hostel_id;
            }
            $hostelExtraDetailsRow->num_beds   = ( !empty($hostelExtraDetails['num_beds']) ? $hostelExtraDetails['num_beds'] : 0);
            $hostelExtraDetailsRow->min_nights   = ( !empty($hostelExtraDetails['min_nights']) ? $hostelExtraDetails['min_nights'] : 0);
            $hostelExtraDetailsRow->max_nights   = ( !empty($hostelExtraDetails['max_nights']) ? $hostelExtraDetails['max_nights'] : 0);
            $hostelExtraDetailsRow->bedsheets  = ( !empty($hostelExtraDetails['bedsheets']) ? $hostelExtraDetails['bedsheets'] : 'N');
            $hostelExtraDetailsRow->towels     = ( !empty($hostelExtraDetails['towels']) ? $hostelExtraDetails['towels'] : 'N');
            $hostelExtraDetailsRow->parking  = ( !empty($hostelExtraDetails['parking']) ? $hostelExtraDetails['parking'] : 'N');

            $hostelExtraDetailsRow->airport_train  = ( !empty($hostelExtraDetails['airport_train']) ? $hostelExtraDetails['airport_train'] : 'N');
            $hostelExtraDetailsRow->luggage  = ( !empty($hostelExtraDetails['luggage']) ? $hostelExtraDetails['luggage'] : 'N');
            $hostelExtraDetailsRow->coed_dorm  = ( !empty($hostelExtraDetails['coed_dorm']) ? $hostelExtraDetails['coed_dorm'] : 'N');
            $hostelExtraDetailsRow->bathroom  = ( !empty($hostelExtraDetails['bathroom']) ? $hostelExtraDetails['bathroom'] : 'N');
            $hostelExtraDetailsRow->credit_cards  = ( !empty($hostelExtraDetails['credit_cards']) ? $hostelExtraDetails['credit_cards'] : 'N');
            $hostelExtraDetailsRow->nonsmoking_rooms  = ( !empty($hostelExtraDetails['nonsmoking_rooms']) ? $hostelExtraDetails['nonsmoking_rooms'] : 'N');
            $hostelExtraDetailsRow->smoke_free  = ( !empty($hostelExtraDetails['smoke_free']) ? $hostelExtraDetails['smoke_free'] : 'N');
            $hostelExtraDetailsRow->pets_allowed  = ( !empty($hostelExtraDetails['pets_allowed']) ? $hostelExtraDetails['pets_allowed'] : 'N');
            $hostelExtraDetailsRow->service_animals  = ( !empty($hostelExtraDetails['service_animals']) ? $hostelExtraDetails['service_animals'] : 'N');
            $hostelExtraDetailsRow->wheelchair  = ( !empty($hostelExtraDetails['wheelchair']) ? $hostelExtraDetails['wheelchair'] : 'N');
            $hostelExtraDetailsRow->internet_computers  = ( !empty($hostelExtraDetails['internet_computers']) ? $hostelExtraDetails['internet_computers'] : 'N');
            $hostelExtraDetailsRow->wireless_internet  = ( !empty($hostelExtraDetails['wireless_internet']) ? $hostelExtraDetails['wireless_internet'] : 'N');
            $hostelExtraDetailsRow->checkout  = ( !empty($hostelExtraDetails['checkout']) ? $hostelExtraDetails['checkout'] : 'N');
            $hostelExtraDetailsRow->reception_hours_start  = ( !empty($hostelExtraDetails['reception_hours_start']) ? $hostelExtraDetails['reception_hours_start'] : 'N');
            $hostelExtraDetailsRow->reception_hours_end  = ( !empty($hostelExtraDetails['reception_hours_end']) ? $hostelExtraDetails['reception_hours_end'] : 'N');
            $hostelExtraDetailsRow->save();
            /*
                                "checkout"=> "N",
                                "reception_hours_start"=> "00:00:00",
                                "reception_hours_end"=> "23:50:00"
                            ];
                        }*/


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'hostel' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => '', 'hostel' => $hostel], HTTP_RESPONSE_OK_RESOURCE_UPDATED);
    }


    public function destroy($id)  // ok
    {
        $this->debToFile(print_r($id, true), '  HostelsController  - destroy $id::');

        try {
            $hostel = Hostel::find($id);
            if ($hostel == null) {
                return response()->json(['error_code' => 11, 'message' => 'Hostel # "' . $id . '" not found !', 'hostel' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();

            $hostel->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'hostel' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }


    // HOSTEL INQUERIES BLOCK START
    public function get_hostels_inqueries($id)
    {
        $relatedHostelInqueries = HostelInquery
            ::getByHostelId($id)
            ->leftJoin('users', 'users.id', '=', 'hostel_inqueries.creator_id')
            ->orderBy('hostel_inqueries.status', 'desc')
            ->orderBy('hostel_inqueries.start_date', 'desc')
            ->select(
                'hostel_inqueries.*', 'users.first_name',  'users.last_name'
            )
            ->get()
            ->map(function ($item) {
                $item['creator_name'] = $item->creator->full_name;
                return $item;
            })
            ->all();
        ;


        return response()->json(['relatedHostelInqueries' => $relatedHostelInqueries], HTTP_RESPONSE_OK);
    } // public function get_hostels_inqueries($id)

    public function hostel_inquery_destroy($hostel_inquery_id)  //
    {

//        $this->debToFile(print_r($hostel_inquery_id, true), '  HostelsController  - destroy $hostel_inquery_id::');

        try {
            $hostelInquery = HostelInquery::find($hostel_inquery_id);
            if ($hostelInquery == null) {
                return response()->json(['error_code' => 11, 'message' => 'Hostel Enquery # "' . $hostel_inquery_id . '" not found !', 'hostel_inquery' =>
                    null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();

            $hostelInquery->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'hostel_inquery' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }  // hostel_inquery_destroy


    public function hostel_enquery_set_status_action()  //
    {

        $request     = request();
        $requestData = $request->all();

        $hostel_inquery_id = ! empty($requestData['id']) ? $requestData['id'] : '';
        $new_status        = ! empty($requestData['status']) ? $requestData['status'] : '';

        try {
            $hostelInquery = HostelInquery::find($hostel_inquery_id);
            if ($hostelInquery == null) {
                return response()->json(['error_code' => 11, 'message' => 'Hostel Enquery # "' . $hostel_inquery_id . '" not found !', 'hostel_inquery' =>
                    null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            if ($hostelInquery->status == 'O' or $hostelInquery->status == 'C') {
                return response()->json(['error_code' => 11, 'message' => 'Status of this Hostel Enquery can not be changed !', 'hostelInquery' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }

            DB::beginTransaction();
            $hostelInquery->status     = $new_status;
            $hostelInquery->updated_at = Carbon::now(config('app.timezone'));
            $hostelInquery->save();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'hostelInquery' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }  // hostel_enquery_set_status_action


    //                 axios.post(window.API_BACKEND_VERSION_LINK + '/admin/hostel_enquery_update_info', {id: actionsHostelInquery.id, info: this.actionsHostelInquery.info}).then((response) => {
    public function hostel_enquery_update_info()  //
    {

        $request     = request();
        $requestData = $request->all();

        $hostel_inquery_id = ! empty($requestData['id']) ? $requestData['id'] : '';
        $info              = ! empty($requestData['info']) ? $requestData['info'] : '';

        try {
            $hostelInquery = HostelInquery::find($hostel_inquery_id);
            if ($hostelInquery == null) {
                return response()->json(['error_code' => 11, 'message' => 'Hostel Enquery # "' . $hostel_inquery_id . '" not found !', 'hostel_inquery' =>
                    null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }


            DB::beginTransaction();
            $hostelInquery->info       = $info;
            $hostelInquery->updated_at = Carbon::now(config('app.timezone'));
            $hostelInquery->save();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'hostelInquery' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }  // hostel_enquery_update_info

    // HOSTEL INQUERIES BLOCK END

    // HOSTEL IMAGES BLOCK START
    public function get_hostels_images($id)
    {
        $relatedHostelImages = HostelImage
            ::getByHostelId($id)
            ->orderBy('is_main', 'desc')
            ->orderBy('created_at', 'desc')
            ->get()
            ->map(function ($item) {
                $filenameData                     = HostelImage::setHostelImageImageProps($item->hostel_id, $item->filename, true);
//                $this->debToFile(print_r($filenameData, true), '  +++ $filenameData::');
                $item['filenameData'] = $filenameData;
                return $item;
            })
            ->all();

        return response()->json(['relatedHostelImages' => $relatedHostelImages], HTTP_RESPONSE_OK);
    } // public function get_hostels_images($id)

    public function hostel_image_destroy($hostel_image_id)  //
    {
        try {
            $hostelImage = HostelImage::find($hostel_image_id);
            if ($hostelImage == null) {
                return response()->json(['error_code' => 11, 'message' => 'Hostel Image # "' . $hostel_image_id . '" not found !', 'hostel_image' => null],
                    HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
            }
            DB::beginTransaction();

            $hostelImage->delete();
            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();

            return response()->json(['error_code' => 1, 'message' => $e->getMessage(), 'hostel_image' => null], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code' => 0, 'message' => ''], HTTP_RESPONSE_OK);
    }  // hostel_image_destroy


    public function hostel_image_store(Request $request)
    {
        $customerAvatarUploadedFile = $request->file('avatar');

        try {
            DB::beginTransaction();
            $requestData= $request->all();

            if ( $requestData['is_main'] ) { // only 1 HostelImage with is_main= true
                $relatedHostelImages = HostelImage
                    ::getByHostelId($requestData['hostel_id'])
                    ->get();
                foreach( $relatedHostelImages as $nextRelatedHostelImage ) {
                    $nextRelatedHostelImage->is_main= false;
                    $nextRelatedHostelImage->save();
                }
            } // if ( $requestData['is_main'] ) { // only 1 HostelImage with is_main= true

            $newHostelImage = new HostelImage();
            $newHostelImage->hostel_id = $requestData['hostel_id'];
            $newHostelImage->is_main   = $requestData['is_main'];
//            $newHostelImage->is_video  = $requestData['is_video'];
            $newHostelImage->info      = $requestData['info'];
            if ( ! empty($customerAvatarUploadedFile)) {
                $hostel_image_image     = HostelImage::checkValidImgName
                ( $requestData['avatar_filename'], with(new HostelImage)->getImgFilenameMaxLength(), true );


                $customer_avatar_file_path = $customerAvatarUploadedFile->getPathName();
//                $this->debToFile(print_r( $customer_avatar_file_path,true),' INSISRE 2 app/Http/Controllers/Admin/HostelImagesController.php store $customer_avatar_file_path::');
                $newHostelImage->filename    = $hostel_image_image;
            }

            $newHostelImage->save();

            if ( ! empty($hostel_image_image)) {
                $dest_avatar = 'public/' . HostelImage::getHostelImageImagePath($newHostelImage->hostel_id, $hostel_image_image);
                Storage::disk('local')->put($dest_avatar, File::get($customer_avatar_file_path));
                ImageOptimizer::optimize( storage_path().'/app/'.$dest_avatar, null );
            } // if ( !empty($hostel_image_image) ) {

//            $this->debToFile(print_r( $newCustomer->id,true),'  CustomersController:store  -000 $newCustomer->id::');
//            $this->debToFile(print_r( $newCustomer,true),'  CustomersController:store  -000 $newCustomer::');
//
            DB::commit();
        } catch (Exception $e) {
//
//            $this->debToFile(print_r( $e->getMessage(),true),'   -0 $e->getMessage()::');
//            $this->debToFile(print_r( $e,true),'   -1 $e::');
            DB::rollBack();
            return response()->json(['error_code'=> 1, 'message'=> $e->getMessage(), 'newHostelImage'=>null],HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        return response()->json(['error_code'=> 0, 'message'=> '', 'customerRow'=>$newHostelImage],HTTP_RESPONSE_OK);
    } // public function hostel_image_store($request)


    // HOSTEL IMAGES BLOCK END


}
