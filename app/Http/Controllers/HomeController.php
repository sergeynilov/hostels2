<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Settings;
use Config;
use App\MyAppModel;
use App\Customer;
use App\State;
use App\Facility;
use App\PaymentPackage;
use App\Region;
use App\Subregion;
use App\Hostel;
use App\HostelFacility;
use App\HostelImage;
use App\Location;
use App\ForumPost;
use App\HostelRoom;
use App\HostelInquery;
use Illuminate\Routing\Controller as BaseController;

use App\Http\Traits\FuncsTrait;
use App\User;
use App\library\CheckValueType;

use App\Category;
use JavaScript;


class HomeController extends BaseController
{
    use FuncsTrait;

    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index()
    {

//        $filename_path= '/app/user-avatars/-user-avatar-1/shawn_hadray.jpg';
//        Storage::delete($filename_path);

        //        $this->debToFile(print_r(-11, true), '  HomeController  -11 index::');
//        $this->addVariablesToJS(['foo' => 'bar', 'age' => 29]);
//        $site_name = Settings::getValue('site_name', CheckValueType::cvString, '');


        /*        $newHostelInqueriesCountByStatesArray=
                    HostelInquery
                        ::getByStatus('N')
                        ->select( "states.name as state_name", \DB::raw('count('.DB::getTablePrefix().'hostel_inqueries.id) as new_hostel_inqueries_count') )
                        ->join( "hostels", "hostels.id", '=', 'hostel_inqueries.hostel_id' )
                        ->join( "states", "states.id", '=', 'hostels.state_id' )
                        ->groupBy("states.name")
                        ->get();*/

        $settingsList = Settings::getSettingsList([], true);
        foreach ($settingsList as $next_key => $next_value) {
            $javaScriptVarsList['settings_' . $next_key] = $next_value;
        }

        $dashboard_home_url = $this->getDashboardHome();

        $current_locale = \App::getLocale();
        $site_name      = Settings::getValue('site_name', CheckValueType::cvString, '');
        $copyright_text = Settings::getValue('copyright_text', CheckValueType::cvString, '');
        $copyright_text = str_replace(["\r", "\n", "\t"], "", $copyright_text);

        $app_version = '1.x';
        if (file_exists(public_path('app_version.txt'))) {
            $app_version = File::get('app_version.txt');
        }

        $app_version = str_replace(["\r", "\n", "\t"], "", $app_version);

        $main_image_recommended_height = config('app.main_image_recommended_height');
        $main_image_recommended_width  = config('app.main_image_recommended_width');

        $text_color          = config('app.text_color');
        $disabled_text_color = config('app.disabled_text_color');


        $javaScriptVarsList['PUSHER_APP_KEY']                     = env('PUSHER_APP_KEY');
        $javaScriptVarsList['settings_js_moment_datetime_format'] = Config::get('app.js_moment_datetime_format', 'Do MMMM, YYYY h:mm');
        $javaScriptVarsList['settings_js_moment_date_format']     = Config::get('app.js_moment_date_format', 'Do MMMM, YYYY');
        $javaScriptVarsList['menuIconsArray']                     = Config::get('app.menuIconsArray', []);
        $javaScriptVarsList['API_VERSION']                        = Config::get('app.API_VERSION', '');
        $javaScriptVarsList['API_VERSION_LINK']                   = Config::get('app.API_VERSION_LINK', '');
        $javaScriptVarsList['app_url']                            = Config::get('app.url', '');
        $javaScriptVarsList['colorsList']                         = Config::get('app.colorsList', []);
        $javaScriptVarsList['API_BACKEND_VERSION_LINK']           = '/api';
        $javaScriptVarsList['API_ADMIN_VERSION_LINK']             = 'api/admin/';
        $javaScriptVarsList['GOOGLE_API_KEY']                     = Config::get('app.GOOGLE_API_KEY');
        $javaScriptVarsList['DASHBOARD_HOME_URL']                 = $dashboard_home_url;
        $javaScriptVarsList['SITE_NAME']                          = $site_name;


        $javaScriptVarsList['ACCESS_ROLE_ADMIN']       = ACCESS_ROLE_ADMIN;
        $javaScriptVarsList['ACCESS_ROLE_ADMIN_LABEL'] = ACCESS_ROLE_ADMIN_LABEL;

        $javaScriptVarsList['ACCESS_ROLE_MANAGER']       = ACCESS_ROLE_MANAGER;
        $javaScriptVarsList['ACCESS_ROLE_MANAGER_LABEL'] = ACCESS_ROLE_MANAGER_LABEL;

        $javaScriptVarsList['ACCESS_ROLE_CUSTOMER']       = ACCESS_ROLE_CUSTOMER;
        $javaScriptVarsList['ACCESS_ROLE_CUSTOMER_LABEL'] = ACCESS_ROLE_CUSTOMER_LABEL;

        $javaScriptVarsList['ACCESS_ROLE_CONTENT_EDITOR']       = ACCESS_ROLE_CONTENT_EDITOR;
        $javaScriptVarsList['ACCESS_ROLE_CONTENT_EDITOR_LABEL'] = ACCESS_ROLE_CONTENT_EDITOR_LABEL;

        $javaScriptVarsList['APP_VERSION']                   = $app_version;
        $javaScriptVarsList['IS_RUNNING_UNDER_DOCKER']       = $this->isRunningUnderDocker();
        $javaScriptVarsList['MAIN_IMAGE_RECOMMENDED_HEIGHT'] = $main_image_recommended_height;
        $javaScriptVarsList['MAIN_IMAGE_RECOMMENDED_WIDTH']  = $main_image_recommended_width;
        $javaScriptVarsList['TEXT_COLOR']                    = $text_color;
        $javaScriptVarsList['DISABLED_TEXT_COLOR']           = $disabled_text_color;
        $javaScriptVarsList['CURRENT_LOCALE']                = $current_locale;

        $javaScriptVarsList['current_remote_addr'] = ! empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $javaScriptVarsList['COPYRIGHT_TEXT']      = $copyright_text;
        JavaScript::put($javaScriptVarsList);
//        echo '<pre>$javaScriptVarsList::'.print_r($javaScriptVarsList,true).'</pre>';
//        die("-1 XXZ");
//        $site_name = Settings::getValue('site_name', CheckValueType::cvString, '');
        return view('index', []);

    }

    public function settings()
    {
        $request     = request();
        $requestData = $request->all();
        $retArray    = ['error_code' => 0, 'message' => ''];

        $this->debToFile(print_r($requestData, true), 'settings -01 $requestData::');
        foreach ($requestData as $next_param_name) {
            // settings:news_per_page                          backend_per_page
            if ($next_param_name == 'forum_posts_per_page') {
                $forum_posts_per_page             = Settings::getValue('forum_posts_per_page', CheckValueType::cvtInteger, 20);
                $retArray['forum_posts_per_page'] = $forum_posts_per_page;
            }

            if ($next_param_name == 'forum_threads_per_page') {
                $forum_threads_per_page             = Settings::getValue('forum_threads_per_page', CheckValueType::cvtInteger, 20);
                $retArray['forum_threads_per_page'] = $forum_threads_per_page;
            }

            if ($next_param_name == 'search_results_per_page') {
                $search_results_per_page             = Settings::getValue('search_results_per_page', CheckValueType::cvtInteger, 20);
                $retArray['search_results_per_page'] = $search_results_per_page;
            }

            if ($next_param_name == 'skinsArray') {
                $skinsArray = [];
                $tempSkins  = \Config::get('app.skinsArray', []);
                foreach ($tempSkins as $next_key => $next_value) {
                    $skinsArray[] = (object)['key' => $next_key, 'label' => $next_value];
                }
                $retArray['skinsArray'] = $skinsArray;
            }


            // PAYMENT_PACKAGE BLOCK BEGIN
            if ($next_param_name == 'activePaymentPackagesArray') {
                $retArray['activePaymentPackagesArray'] = PaymentPackage
                    ::getByActive(1)
                    ->get()
                    ->map(function ($item) {
                        $item['price_whole']    = floor($item->price);
                        $item['price_fraction'] = ($item->price - $item['price_whole']) * 100;

                        return $item;
                    })
                    ->all();

            }

            // PAYMENT_PACKAGE BLOCK END


            // FORUMS BLOCK BEGIN
            if ($next_param_name == 'forumPostActionValueArray') {
                $tempforumPostActionValueArray = ForumPost::getforumPostActionValueArray(true);
                $forumPostActionValueArray     = [];
                foreach ($tempforumPostActionValueArray as $next_key => $next_value) {
                    $forumPostActionValueArray[] = (object)$next_value;
                }
                $retArray['forumPostActionValueArray'] = $forumPostActionValueArray;
            }

            // FORUMS BLOCK END


            if ($next_param_name == 'customerAccountTypeValueArray') {
                $customerAccountTypeValueArray     = [];
                $tempCustomerAccountTypeValueArray = Customer::getCustomerAccountTypeValueArray(true);
                foreach ($tempCustomerAccountTypeValueArray as $next_key => $next_value) {
                    $customerAccountTypeValueArray[] = (object)$next_value;
                }
                $retArray['customerAccountTypeValueArray'] = $customerAccountTypeValueArray;
            }
            if ($next_param_name == 'customerStatusValueArray') {
                $tempCustomerStatusValueArray = Customer::getCustomerStatusValueArray(true);
                $customerStatusValueArray     = [];
                foreach ($tempCustomerStatusValueArray as $next_key => $next_value) {
                    $customerStatusValueArray[] = (object)$next_value;
                }
                $retArray['customerStatusValueArray'] = $customerStatusValueArray;
            }

            if ($next_param_name == 'customerSkinsArray') {
                $tempCustomerSkinsArray = Customer::getSkinsArray(true);
                $customerSkinsArray     = [];
                foreach ($tempCustomerSkinsArray as $next_key => $next_value) {
                    $customerSkinsArray[] = (object)$next_value;
                }
                $retArray['customerSkinsArray'] = $customerSkinsArray;
            }


            // CUSTOMERS BLOCK BEGIN
            if ($next_param_name == 'customers_active_count') { // N => New(Waiting activation), A=>Active, I=>Inactive
                $retArray['customers_active_count'] = Customer::getByStatus('A')->count();
            }
            if ($next_param_name == 'customers_inactive_count') {
                $retArray['customers_inactive_count'] = Customer::getByStatus('I')->count();
            }

            if ($next_param_name == 'customers_new_count') {
                $retArray['customers_new_count'] = Customer::getByStatus('N')->count();
            }

            if ($next_param_name == 'customers_all_count') {
                $retArray['customers_all_count'] = Customer::count();
            }
            // CUSTOMERS BLOCK END


            // HOSTELS BLOCK BEGIN
            if ($next_param_name == 'hostels_active_count') {
                $retArray['hostels_active_count'] = Hostel::getByStatus('A')->count();
            }
            if ($next_param_name == 'hostels_inactive_count') {
                $retArray['hostels_inactive_count'] = Hostel::getByStatus('I')->count();
            }

            if ($next_param_name == 'hostels_new_count') {
                $retArray['hostels_new_count'] = Hostel::getByStatus('N')->count();
            }

            if ($next_param_name == 'hostels_all_count') {
                $retArray['hostels_all_count'] = Hostel::count();
            }


            if ($next_param_name == 'new_hostel_inqueries_count') {
                $retArray['new_hostel_inqueries_count'] = HostelInquery::getByStatus('N')->count();
            }

            if ($next_param_name == 'accepted_hostel_inqueries_count') {
                $retArray['accepted_hostel_inqueries_count'] = HostelInquery::getByStatus('A')->count();
            }

            if ($next_param_name == 'newHostelInqueriesCountByStatesArray') {
                $retArray['newHostelInqueriesCountByStatesArray'] =
                    HostelInquery
                        ::getByStatus('N')
                        ->select("states.name as state_name", \DB::raw('count(' . DB::getTablePrefix() . 'hostel_inqueries.id) as new_hostel_inqueries_count'))
                        ->join("hostels", "hostels.id", '=', 'hostel_inqueries.hostel_id')
                        ->join("states", "states.id", '=', 'hostels.state_id')
                        ->groupBy("states.name")
                        ->get();
            }

            if ($next_param_name == 'hostelFacilitiesCountByFacilityIdArray') {
                $retArray['hostelFacilitiesCountByFacilityIdArray'] =
                    HostelFacility
                        ::select(
                            "facilities.id as facility_id",
                            "facilities.name as facility_name",
                            "facilities.descr as facility_descr",
                            \DB::raw('count(' . DB::getTablePrefix() . 'hostel_facilities.id) as hostel_facilities_count')
                        )
//                        ->join( "hostels", "hostels.id", '=', 'hostel_inqueries.hostel_id' )
                        ->join("facilities", "facilities.id", '=', 'hostel_facilities.facility_id')
                        ->groupBy("facilities.name")
                        ->groupBy("facilities.descr")
                        ->orderBy("hostel_facilities_count", 'desc')
                        ->take(12)
                        ->get();
            }

            if ($next_param_name == 'new_hostel_inqueries') {
                $retArray['new_hostel_inqueries'] = HostelInquery
                    ::getByStatus('N')
                    ->orderBy('start_date', 'desc')
                    ->orderBy('created_at', 'desc')
                    ->get();
            }

            if ($next_param_name == 'hostelImageIsMainValueArray') {
                $temphostelImageIsMainValueArray = HostelImage::gethostelImageIsMainValueArray(true);
                $hostelImageIsMainValueArray     = [];
                foreach ($temphostelImageIsMainValueArray as $next_key => $next_value) {
                    $hostelImageIsMainValueArray[] = (object)$next_value;
                }
                $retArray['hostelImageIsMainValueArray'] = $hostelImageIsMainValueArray;
            }
            if ($next_param_name == 'hostelImageIsVideoValueArray') {
                $temphostelImageIsVideoValueArray = HostelImage::gethostelImageIsVideoValueArray(true);
                $hostelImageIsVideoValueArray     = [];
                foreach ($temphostelImageIsVideoValueArray as $next_key => $next_value) {
                    $hostelImageIsVideoValueArray[] = (object)$next_value;
                }
                $retArray['hostelImageIsVideoValueArray'] = $hostelImageIsVideoValueArray;
            }

            if ($next_param_name == 'hostelFeatureValueArray') {
                $tempHostelFeatureValueArray = Hostel::getHostelFeatureValueArray(true);
                $hostelFeatureValueArray     = [];
                foreach ($tempHostelFeatureValueArray as $next_key => $next_value) {
                    $hostelFeatureValueArray[] = (object)$next_value;
                }
                $retArray['hostelFeatureValueArray'] = $hostelFeatureValueArray;
            }
            if ($next_param_name == 'hostelStatusValueArray') {
                $tempHostelStatusValueArray = Hostel::getHostelStatusValueArray(true);
                $hostelStatusValueArray     = [];
                foreach ($tempHostelStatusValueArray as $next_key => $next_value) {
                    $hostelStatusValueArray[] = (object)$next_value;
                }
                $retArray['hostelStatusValueArray'] = $hostelStatusValueArray;
            }

            if ($next_param_name == 'categoriesSelectionArray') {
                $categoriesSelectionArray     = [];
                $tempCategoriesSelectionArray = Category::getCategoriesSelectionArray();
                foreach ($tempCategoriesSelectionArray as $next_id => $next_name) {
                    $categoriesSelectionArray[] = [
                        'key'        => $next_id,
                        'label'      => $next_name,
                        'is_checked' => ($next_id < 10 ? true : false),
//                        'is_checked'=>false,
                    ];
                }

                $retArray['categoriesSelectionArray'] = $categoriesSelectionArray;
            }

            if ($next_param_name == 'facilitiesSelectionArray') {
                $tempFacilitiesSelectionArray = Facility::getFacilitiesSelectionArray();
                $facilitiesSelectionArray     = [];
                foreach ($tempFacilitiesSelectionArray as $next_id => $next_name) {
//                    if( $next_id > 5 ) continue;
                    $facilitiesSelectionArray[] = [
                        'key'   => $next_id,
                        'label' => $next_name,
//                        'is_checked'=>($next_id < -3 ? true: false), // to comment
//                        'is_checked'=>false,
                    ];
                }

//                        $this->debToFile(print_r($facilitiesSelectionArray, true), '  -6 $facilitiesSelectionArray::');
                $retArray['facilitiesSelectionArray'] = $facilitiesSelectionArray;
            }

            // hostelsSearchByFieldsArray', 'searchByFieldsOrderingArray

            if ($next_param_name == 'hostelInqueriesStatusValueArray') {
                $tempHostelInqueryStatusValueArray = HostelInquery::getHostelInqueryStatusValueArray(true);
                $hostelInqueriesStatusValueArray   = [];
                foreach ($tempHostelInqueryStatusValueArray as $next_key => $next_value) {
                    $hostelInqueriesStatusValueArray[] = (object)$next_value;
                }
                $retArray['hostelInqueriesStatusValueArray'] = $hostelInqueriesStatusValueArray;
            }

            if ($next_param_name == 'hostelsSearchByFieldsArray') {
                $tempHostelsSearchByFieldsArray = Hostel::getHostelsSearchByFieldsValueArray(true);
                $hostelsSearchByFieldsArray     = [];
                foreach ($tempHostelsSearchByFieldsArray as $next_key => $next_value) {
                    $hostelsSearchByFieldsArray[] = (object)$next_value;
                }
                $retArray['hostelsSearchByFieldsArray'] = $hostelsSearchByFieldsArray;
            }

            if ($next_param_name == 'searchByFieldsOrderingArray') {
                $tempSearchByFieldsOrderingArray = MyAppModel::getSearchByFieldsOrderingValueArray(true);
                $searchByFieldsOrderingArray     = [];
                foreach ($tempSearchByFieldsOrderingArray as $next_key => $next_value) {
                    $searchByFieldsOrderingArray[] = (object)$next_value;
                }
                $retArray['searchByFieldsOrderingArray'] = $searchByFieldsOrderingArray;
            }

            if ($next_param_name == 'facilitiesIdValueArray') {
                $facilitiesIdValueArray     = [];
                $tempFacilitiesIdValueArray = Facility::getFacilitiesSelectionArray();
                foreach ($tempFacilitiesIdValueArray as $next_id => $next_name) {
                    $facilitiesIdValueArray[] = [
                        'key'   => $next_id,
                        'label' => $next_name,
                    ];
                }
                $retArray['facilitiesIdValueArray'] = $facilitiesIdValueArray;
            }

            // HOSTELS BLOCK END

            // HOSTEL ROOMS BLOCK START
            if ($next_param_name == 'hostelRoomIsDormValueArray') {
                $hostelRoomIsDormValueArray     = [];
                $tempHostelRoomIsDormValueArray = HostelRoom::getHostel_RoomIs_DormValueArray();
                foreach ($tempHostelRoomIsDormValueArray as $next_id => $next_name) {
                    $hostelRoomIsDormValueArray[] = (object)$next_name;
                }
                $retArray['hostelRoomIsDormValueArray'] = $hostelRoomIsDormValueArray;
            }

            if ($next_param_name == 'hostelRoomIsPrivateValueArray') {
                $hostelRoomIsPrivateValueArray     = [];
                $tempHostelRoomIsPrivateValueArray = HostelRoom::getHostel_RoomIs_PrivateValueArray();
                foreach ($tempHostelRoomIsPrivateValueArray as $next_id => $next_name) {
                    $hostelRoomIsPrivateValueArray[] = (object)$next_name;
                }
                $retArray['hostelRoomIsPrivateValueArray'] = $hostelRoomIsPrivateValueArray;
            }

            if ($next_param_name == 'hostelRoomTypeValueArray') {
                $hostelRoomTypeValueArray     = [];
                $tempHostelRoomTypeValueArray = HostelRoom::getHostel_RoomTypeValueArray();
                foreach ($tempHostelRoomTypeValueArray as $next_id => $next_name) {
                    $hostelRoomTypeValueArray[] = (object)$next_name;
                }
                $retArray['hostelRoomTypeValueArray'] = $hostelRoomTypeValueArray;
            }
            // HOSTEL ROOMS BLOCK END


            if ($next_param_name == 'usersSelectionList') {
                $retArray['usersSelectionList'] = User::getUsersSelectionList();
            }

            if ($next_param_name == 'statesIdValueArray') {
                $statesIdValueArray     = [];
                $tempStatesIdValueArray = State::getStatesSelectionArray();
                foreach ($tempStatesIdValueArray as $next_key => $nextTempStatesIdValueArray) {
                    $statesIdValueArray[] = [
                        'key'   => $nextTempStatesIdValueArray->id,
                        'label' => $nextTempStatesIdValueArray->name . '(' . $nextTempStatesIdValueArray->code . ')',
                    ];
                }
                $retArray['statesIdValueArray'] = $statesIdValueArray;

            }

            if ($next_param_name == 'regionsSelectionList') {
                $retArray['regionsSelectionList'] = Region::getRegionsSelectionArray(null, 'A');  //
            }

            if ($next_param_name == 'shortUsersSelectionList') {
                $retArray['shortUsersSelectionList'] = User::getUsersSelectionList(true, ['short' => 1]);
            }
        }

        $this->debToFile(print_r($retArray, true), '  settings -2 $retArray::');

//        sleep(2);
        return response()->json($retArray, HTTP_RESPONSE_OK);
    }

    //    Route::get('get_location_by_postcode/{postal_code}', 'HomeController@get_location_by_postcode');
    public function get_location_by_postcode($postcode = '')
    {

        $locationRow = Location::getByPostcode($postcode)->first();

        if (empty($locationRow)) {
            return response()->json(['error_code' => 1, 'message' => 'Postal code ' . $postcode . ' not found !'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $current_state_label_id = '';
        $current_state_label    = '';

        $current_region_label_id = '';
        $current_region_label    = '';

        $current_subregion_label_id = '';
        $current_subregion_label    = '';

        $suburb    = $locationRow->suburb;

        $state = State::getByCode($locationRow->state)->first();
        if ( ! empty($state)) {
            $current_state_label_id = $state->id;
            $current_state_label    = $state->name;
        }

        $region = Region::getByName($locationRow->region)->first();
        if ( ! empty($region)) {
//            $this->debToFile(print_r($region, true), '  - 000 $region::');
            $current_region_label_id = $region->id;
            $current_region_label    = $region->name;
        }

        $subregion = Subregion::getByName($locationRow->subregion)->first();
        if ( ! empty($subregion)) {
            $current_subregion_label_id = $subregion->id;
            $current_subregion_label    = $subregion->name;
        }


        $subregionsList = Subregion
            ::getByStateId($current_state_label_id)
            ->getByRegionId($current_region_label_id)
            ->get()
            ->map(function ($item) {
                return ['key' => $item['id'], 'label' => $item['id'].'->'.$item['name']];
            })->all();

        $regionsList = Region
            ::getByStateId($current_state_label_id)
            ->getByActive('A')
            ->get()
            ->map(function ($item) {
                return ['key' => $item['id'], 'label' => $item['id'].'->'.$item['name']];
            })->all();


        return response()->json([
            'postcode'               => $postcode,
            'current_state_label_id' => $current_state_label_id,
            'current_state_label'    => $current_state_label,

            'current_region_label_id' => $current_region_label_id,
            'current_region_label'    => $current_region_label,
            'regionsList'             => $regionsList,

            'suburb'                  => $suburb,

            'current_subregion_label_id' => $current_subregion_label_id,
            'current_subregion_label'    => $current_subregion_label,
            'subregionsList'             => $subregionsList,
        ], HTTP_RESPONSE_OK);
    } // public function get_location_by_postcode($postcode = '')


    //    Route::get('get_regions_listing_by_state_id/{postal_code}', 'HomeController@get_regions_listing_by_state_id');
    public function get_regions_listing_by_state_id($state_id = '')
    {
        $stateRow = State::find($state_id);
        if (empty($stateRow)) {
            return response()->json(['error_code' => 1, 'message' => 'state # ' . $state_id . ' not found !'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }
        $current_region_label_id = '';
        $current_region_label    = '';
        $regionsList             = Region
            ::getByStateId($state_id)
            ->getByActive('A')
            ->get()
            ->map(function ($item) {
                return ['key' => $item['id'], 'label' => $item['id'].'->'.$item['name']];
            })->all();


        return response()->json([
            'state_id'                => $state_id,
            'current_region_label_id' => $current_region_label_id,
            'current_region_label'    => $current_region_label,
            'regionsList'             => $regionsList,
        ], HTTP_RESPONSE_OK);
    } // public function get_regions_listing_by_state_id($postcode = '')


    public function get_subregions_listing_by_region_id($region_id, $current_region_id = '')
    {
        $regionRow = Region::find($region_id);
        if (empty($regionRow)) {
            return response()->json(['error_code' => 1, 'message' => 'Region # ' . $region_id . ' not found !'], HTTP_RESPONSE_INTERNAL_SERVER_ERROR);
        }

        $current_subregion_label_id = '';
        $current_subregion_label    = '';

        $subregionsList = Subregion
            ::getByRegionId($region_id)
            ->get()
            ->map(function ($item) {
                return ['key' => $item['id'], 'label' => $item['id'].'->'.$item['name']];
            })->all();


        return response()->json([
            'region_id' => $region_id,

            'current_subregion_label_id' => $current_subregion_label_id,
            'current_subregion_label'    => $current_subregion_label,
            'subregionsList'             => $subregionsList,
        ], HTTP_RESPONSE_OK);

    }


    public function get_lists_data()
    {
        $request     = request();
        $requestData = $request->all();
        $retArray    = ['error_code' => 0, 'message' => ''];
        //                 this.$store.dispatch('get-list', ['states']);

        $this->debToFile(print_r($requestData, true), '  get_lists_data  -1 $requestData::');
        foreach ($requestData as $next_param_name) {

            if ($next_param_name == 'statesWithRegions') {
                $statesList = [];
                //     public static function getStatesSelectionArray($active= '', $order_by= 'name') :array {
                $tempStatesList = State::getStatesSelectionArray('A');
                $regionsList    = Region::getRegionsSelectionArray(null, 'A');
                foreach ($tempStatesList as $nextTempState) {
                    $regionsOfStateArray = [];
                    foreach ($regionsList as $nextRegion) {
//                        $this->debToFile(print_r( $nextRegion,true),'  TEXT  -2 $nextRegion::');
                        if ($nextRegion->state_id == $nextTempState->id) {
                            $related_common_hostels_count   = Hostel
                                ::getByRegionId($nextRegion->id)
                                ->getByStateId($nextTempState->id)
                                ->getByStatus('A')
                                ->count();
                            $related_featured_hostels_count = Hostel
                                ::getByRegionId($nextRegion->id)
                                ->getByStateId($nextTempState->id)
                                ->getByStatus('A')
                                ->getByFeature('F')
                                ->count();

                            $regionsOfStateArray[] = (object)[
                                'id'                             => $nextRegion->id,
                                'key'                            => $nextRegion->slug,
                                'state_id'                       => $nextRegion->state_id,
                                'name'                           => $nextRegion->name,
                                'label'                          => $nextRegion->name,
                                'active'                         => $nextRegion->active,
                                'slug'                           => $nextRegion->slug,
                                'related_common_hostels_count'   => $related_common_hostels_count,
                                'related_featured_hostels_count' => $related_featured_hostels_count,
                            ];

                        }
                    }
                    $statesList[] = (object)[
                        'id'                  => $nextTempState->id,
                        'code'                => $nextTempState->code,
                        'name'                => $nextTempState->name,
                        'slug'                => $nextTempState->slug,
                        'active'              => $nextTempState->active,
                        'regionsOfStateArray' => $regionsOfStateArray
                    ];
                } // foreach( $tempStatesList as $next_key=>$nextTempState ) {
                $retArray['statesWithRegionsList'] = $statesList;
            } // if( $next_param_name == 'statesWithRegions' ) {

            if ($next_param_name == 'states') {
//                $tempStatesList= State::getStatesSelectionArray();
//                $statesList= [];
//                foreach( $tempStatesList as $next_key=>$next$tempState ) {
//
//                }
//                $statesList[]=
                $retArray['statesList'] = State::getStatesSelectionArray();
            }
            if ($next_param_name == 'customerStatusValueArray') {

//                $retArray['customerStatusValueArray']= Customer::getCustomerStatusValueArray(false);
                $tempCustomerStatusValueArray = Customer::getCustomerStatusValueArray(true);
                $customerStatusValueArray     = [];
                foreach ($tempCustomerStatusValueArray as $next_key => $next_value) {
                    $customerStatusValueArray[] = (object)$next_value;
                }
                $retArray['customerStatusValueArray'] = $customerStatusValueArray;
            }
            if ($next_param_name == 'usersSelectionList') {
                $retArray['usersSelectionList'] = User::getUsersSelectionList();
            }
            if ($next_param_name == 'shortUsersSelectionList') {
                $retArray['shortUsersSelectionList'] = User::getUsersSelectionList(true, ['short' => 1]);
            }
        }

//        $this->debToFile(print_r($retArray, true), '  get_lists_data  -2 $retArray::');

        return response()->json($retArray, HTTP_RESPONSE_OK);
    } // public function get_lists_data()

    public function customers()
    {
        $request     = request();
        $requestData = $request->all();
//        $this->debToFile(print_r($requestData, true), '  customers  -11 $requestData::');

        $filter_status = ! empty($requestData['filter_status']) ? $requestData['filter_status'] : '';
        $customers     = Customer
            ::getByStatus($filter_status)
            ->get()
            ->map(function ($item) {
                return $item;
//                return [$item['field'] => $item['value']];
            })
            ->all();

//        $this->debToFile(print_r($requestData, true), '  customers  -11 $requestData::');
        return response()->json(['error_code' => 0, 'message' => '', 'customers' => $customers], HTTP_RESPONSE_OK);
    }


    public function customers_count()
    {
        $request     = request();
        $requestData = $request->all();
        $this->debToFile(print_r($requestData, true), '  customers  -11 $requestData::');

        //scopeGetByStatus
        $filter_status = ! empty($requestData['filter_status']) ? $requestData['filter_status'] : '';
        $customers     = Customer
            ::getByStatus($filter_status)
//            ->select( 'field', 'value' )
            ->count();


//        $this->debToFile(print_r($requestData, true), '  customers  -11 $requestData::');
        return response()->json(['error_code' => 0, 'message' => '', 'customers' => $customers], HTTP_RESPONSE_OK);
    } // public function customers_count()


}
