<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Traits\FuncsTrait;
use App\User;

class AuthController extends Controller
{

    use FuncsTrait;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
//        $this->debToFile(print_r( $credentials,true),'  login  -1 $credentials::');

        if ($token = $this->guard('api')->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }


    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard('api')->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $loggedUser= $this->guard()->user();

        $user_avatar_path= User::getUserAvatarPath($loggedUser->id, $loggedUser->avatar);
        $filenameData                     = User::setUserAvatarProps($loggedUser->id, $loggedUser->avatar, true);
        //            echo '<pre>$filenameData::'.print_r($filenameData,true).'</pre>';
//          $this->debToFile(print_r($filenameData, true), '  +++ $filenameData::');

//        $this->debToFile(print_r( $user_avatar_path,true),'  login  -2 $user_avatar_path::');
//        $this->debToFile(print_r( $token,true),'  login  -30 $token::');

        //                 $filenameData                     = User::setUserAvatarProps($item->user_id, $item->avatar, true);
        ////            echo '<pre>$filenameData::'.print_r($filenameData,true).'</pre>';
        //                $this->debToFile(print_r($filenameData, true), '  +++ $filenameData::');
        ////                $nextTempNews['filenameData']   = $filenameData;
        //
        ////                $this->debToFile(print_r($creator_avatar_path, true), '  - -1 $creator_avatar_path::');
        //
        //                                                            /*            $filenameData                     = News::setNewsImageProps($nextTempNews->id, $nextTempNews->image, true);
        ////            echo '<pre>$filenameData::'.print_r($filenameData,true).'</pre>';
        //            $filenameData['info']             = $nextTempNews->info;
        //            $this->debToFile(print_r($filenameData, true), '  +++ $filenameData::');
        //            $nextTempNews['filenameData']   = $filenameData;
        // */
        //                $item['filenameData'] = $filenameData;


        $usersGroups=  User::getUsersGroupsByUserId($loggedUser->id, false);
//        $this->debToFile(print_r( $usersGroups,true),'  login  -2 $usersGroups::');
        return response()->json([
            'access_token' => $token,
            'user'         => $loggedUser,
            'token_type'   => 'bearer',
            'user_avatar_path'   => $user_avatar_path,
            'filenameData' => $filenameData,
            'usersGroups'  => $usersGroups,
            'expires_in'   => $this->guard('api')->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return \Auth::Guard('api');
//        return Auth::guard();
    }

}
