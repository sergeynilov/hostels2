<?php

namespace App\Http\Traits;

//use File;
use Barryvdh\Debugbar\Facade as Debugbar;
use Carbon\Carbon;
use Config;
use Intervention\Image\Facades\Image as Image;
use Auth;
use DB;
use Twilio; // https://github.com/aloha/laravel-twilio
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\library\CheckValueType;

trait FuncsTrait
{
    protected $current_admin_template = 'defaultBS41Backend';
    protected $frontend_template_name = 'cardsBS41Frontend';
    protected $concat_str_max_length = 50;
    protected $m_concat_str_add_chars = '...';


    public static function isValidBool($val): bool
    {
        if (in_array($val, ["Y", "N"])) {
            return true;
        } else {
            return false;
        }
    }

    public static function isValidInteger($val): bool
    {
        if (preg_match('/^[1-9][0-9]*$/', $val)) {
            return true;
        } else {
            return false;
        }
    }

    public static function isValidFloat($val): bool
    {
        if (preg_match('/^[+-]?([0-9]*[.])?[0-9]+$/', $val)) {
            return true;
        } else {
            return false;
        }
    }

    protected function getDashboardHome(): string
    {
        return \App::make('url')->to('/');
    }


    public function getNiceFileSize($bytes, $binaryPrefix = true)
    {
        if ($binaryPrefix) {
            $unit = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
            if ($bytes == 0) {
                return '0 ' . $unit[0];
            }

            return @round($bytes / pow(1024, ($i = floor(log($bytes, 1024)))), 2) . ' ' . (isset($unit[$i]) ? $unit[$i] : 'B');
        } else {
            $unit = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
            if ($bytes == 0) {
                return '0 ' . $unit[0];
            }

            return @round($bytes / pow(1000, ($i = floor(log($bytes, 1000)))), 2) . ' ' . (isset($unit[$i]) ? $unit[$i] : 'B');
        }
    }

    public function getElasticsearchInfo($detail = null)
    {

        try {
            $elasticsearch_url = config('app.elasticsearch_url');
            if (empty($elasticsearch_url)) {
                return false;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $elasticsearch_url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, []);

            $output = curl_exec($ch);
            $output = json_decode($output);
            curl_close($ch);
            if (strtolower($detail) == 'version') {
                $version = ! empty($output->version->number) ? $output->version->number : false;

                return $version;
            }
        } catch (\Exception $e) {
            return '';
        }

        return $output;
    }

    public function getBackendTemplateName()
    {
        return $this->current_admin_template;
    }

    public function getFrontendTemplateName()
    {
        return $this->frontend_template_name;
    }


    public function concatStr(string $str, int $max_length = null, string $add_str = ' ...', $show_help = false, $strip_tags = true, $additive_code = ''):
    string {
        if ($strip_tags) {
            $str = strip_tags($str);
        }
        $ret_html = $this->limitChars($str, (! empty($max_length) ? $max_length : $this->concat_str_max_length), $add_str);
        if ($show_help and strlen($str) > $max_length) {
            $ret_html .= '<i class=" a_link fa bars" style="font-size:larger;" hidden ' . $additive_code . ' ></i>';
        }

        return $ret_html;
    }

    public function limitChars($str, $limit = 100, $end_char = null, $preserve_words = false)
    {
        $end_char = ($end_char === null) ? '&#8230;' : $end_char;

        $limit = (int)$limit;

        if (trim($str) === '' OR strlen($str) <= $limit) {
            return $str;
        }

        if ($limit <= 0) {
            return $end_char;
        }

        if ($preserve_words == false) {
            return rtrim(substr($str, 0, $limit)) . $end_char;
        }
        // TO FIX AND DELETE SPACE BELOW
        preg_match('/^.{' . ($limit - 1) . '}\S* /us', $str, $matches);

        return rtrim($matches[0]) . (strlen($matches[0]) == strlen($str) ? '' : $end_char);
    }

    /**
     * Limits a phrase to a given number of words.
     *
     * @param string   phrase to limit words of
     * @param integer  number of words to limit to
     * @param string   end character or entity
     *
     * @return  string
     */
    public function limitWords($str, $limit = 100, $end_char = null)
    {
        $limit    = (int)$limit;
        $end_char = ($end_char === null) ? '&#8230;' : $end_char;

        if (trim($str) === '') {
            return $str;
        }

        if ($limit <= 0) {
            return $end_char;
        }

        preg_match('/^\s*+(?:\S++\s*+){1,' . $limit . '}/u', $str, $matches);

        // Only attach the end character if the matched string is shorter
        // than the starting string.
        return rtrim($matches[0]) . (strlen($matches[0]) === strlen($str) ? '' : $end_char);
    }


    public function isDeveloperComp($check_debug = false)
    {
        if ($check_debug) {
            $debug = config('app.debug');
            if ( ! $debug) {
                return;
            }
        }

        if ( $this->isRunningUnderDocker() ) {
            return true;
        }

        $url = config('app.url');
        $pos = strpos($url, 'local-hostels2.com');
        if ( ! ($pos === false)) {
            return true;
        }

        return false;
    }

    public function getSpatieTagLocaledValue($value)
    {
        $spatie_tag_locale = config('app.spatie_tag_locale', 'en');
        $decodedValue      = (array)json_decode($value);
        if ( ! empty($decodedValue[$spatie_tag_locale])) {
            return $decodedValue[$spatie_tag_locale];
        }

        return '';
    }

    public function clearEmptyArrayItems($arr): array
    {
        if (empty($arr)) {
            return [];
        }
        foreach ($arr as $next_key => $next_value) {
            if (empty($next_value)) {
                unset($arr[$next_key]);
            }
        }

        return $arr;
    }

    public function concatArray($arr, $splitter = ',', $skip_empty = true, $skip_last_delimiter = true)
    {
        $ret_str = '';
        if ( ! is_array($arr) or empty($arr)) {
            return '';
        }
        $l              = count($arr);
        $nonempty_array = [];
        for ($i = 0; $i < $l; $i++) {
            $next_value = trim($arr[$i]);
            if (empty($next_value) and $skip_empty) {
                continue;
            }
            $nonempty_array[] = self::removeMore1Space($next_value);
        }

        $l = count($nonempty_array);
        for ($i = 0; $i < $l; $i++) {
            $next_value = trim($nonempty_array[$i]);
            $ret_str    .= $next_value . (($skip_last_delimiter and $i == $l - 1) ? '' : $splitter);
        }

        return $ret_str;
    }

    public function concatConditionalValues($valuesArray, $splitter = '', $default_value = '')
    {
        $ret         = '';
        $have_values = false;
//        echo '<pre>$valuesArray::'.print_r($valuesArray,true).'</pre>';
        foreach ($valuesArray as $next_key => $next_value) {
            if ($next_value['condition']) {
                $have_values = true;
                $ret         .= $next_value['value'] . $splitter;
            }
        }
        if (empty($have_values)) {
            $ret = $default_value;
        }
        $ret = self::trimRightSubString($ret, $splitter);

        return $ret;
    }

    public function removeMore1Space($str)
    {
        $res = preg_replace('/\s\s+/', ' ', $str); // This will be 'foo o' now

        return $res;
    }

    public function getRightSubstring(string $S, $count): string
    {
        return substr($S, strlen($S) - $count, $count);
    }

    public function trimRightSubString(string $s, string $substr): string
    {
        $res = preg_match('/(.*?)(' . preg_quote($substr, "/") . ')$/si', $s, $A);
        if ( ! empty($A[1])) {
            return $A[1];
        }

        return $s;
    }


    public function setArrayHeader(array $headersArray, array $dataArray): array
    {
//	    echo '<pre>setArrayHeader  $headersArray::'.print_r($headersArray,true).'</pre>';
        if (empty($headersArray) or ! is_array(($headersArray))) {
            return $dataArray;
        }
        $retArray = [];
        foreach ($headersArray as $next_header_key => $next_header_text) {
//		    echo '<pre>$next_header_text::'.print_r($next_header_text,true).'</pre>';
            $retArray[$next_header_key] = $next_header_text;
        }
        if (is_array($dataArray)) {
            foreach ($dataArray as $next_data_key => $next_data_text) {
                $retArray[$next_data_key] = $next_data_text;
            }
        }

        return $retArray;
    }


    protected function setFlashMessage(string $message_text, string $action_status = 'success', string $action_header = '')
    {
        \Session::flash('action_text', $message_text);
        \Session::flash('action_status', $action_status);
        if ( ! empty($action_header)) {
            \Session::flash('action_header', $action_header);
        }
    }

    public function getCFPriceFormat($value)
    {
        return number_format($value, 2, ',', '.');
    }


    public function getCFFormattedDate($date, $date_format = 'mysql', $output_format = ''): string
    {
        if (empty($date)) {
            return '';
        }
        $date_carbon_format = config('app.date_carbon_format');
        if ($date_format == 'mysql' /*and ! $this->isValidTimeStamp($date)*/) {
            $date_format = $this->getDateFormat("astext");
            $date        = Carbon::createFromTimestamp(strtotime($date))->format($date_format);

            return $date;
//            echo '<pre>-1 $date::'.print_r($date,true).'</pre>';
        }


        if ($this->isCFValidTimeStamp($date)) {
            if (strtolower($output_format) == 'astext') {
                $date_carbon_format_as_text = config('app.date_carbon_format_as_text', '%d %B, %Y');

                return Carbon::createFromTimestamp($date, Config::get('app.timezone'))->formatLocalized($date_carbon_format_as_text);
            }
            if (strtolower($output_format) == 'pickdate') {
                $date_carbon_format_as_pickdate = config('app.pickdate_format_submit');

                return Carbon::createFromTimestamp($date, Config::get('app.timezone'))->format($date_carbon_format_as_pickdate);
            }

            return Carbon::createFromTimestamp($date, Config::get('app.timezone'))->format($date_carbon_format);
        }
        $A = preg_split("/ /", $date);
        if (count($A) == 2) {
            $date = $A[0];
        }
//        echo '<pre>$date::'.print_r($date,true).'</pre>';
//        echo '<pre>$date_carbon_format::'.print_r($date_carbon_format,true).'</pre>';
//        die("-1 XXZ==");
        $a = Carbon::createFromFormat($date_carbon_format, $date);
        $b = $a->format(self::getCFDateFormat("astext"));

        return $a->format(self::getCFDateFormat("astext"));
    }

    public function getCFFormattedDateTime($datetime, $datetime_format = 'mysql'): string
    {
        if (empty($datetime)) {
            return '';
        }
//        if ($datetime_format == 'mysql' and ! $this->isValidTimeStamp($datetime_format)) {
        if ($datetime_format == 'mysql' and ! $this->isValidTimeStamp($datetime)) {
            $datetime_format = $this->getDateTimeFormat("astext");
            $ret             = Carbon::createFromTimestamp(strtotime($datetime))->format($datetime_format);

            return $ret;
        }
//        if ($this->isValidTimeStamp($datetime_format)) {
        if ($this->isValidTimeStamp($datetime)) {
            $datetime_format = $this->getDateTimeFormat("astext");
            $ret             = Carbon::createFromTimestamp($datetime)->format($datetime_format);

            return $ret;
        }

        return (string)$datetime;

//        $datetimetime_carbon_format = config('app.datetime_carbon_format');
//        $datetimetime_carbon_format_as_text = config('app.datetime_carbon_format_as_text', '%d %B, %Y %H:%M:%S');
//        if ( $this->isCFValidTimeStamp($datetime) ) {
//            if ( strtolower($output_format) == 'astext' ) {
//                return Carbon::createFromTimestamp($datetime, Config::get('app.timezone'))->formatLocalized($datetimetime_carbon_format_as_text);
//            }
//            return Carbon::createFromTimestamp($datetime, Config::get('app.timezone'))->format($datetimetime_carbon_format_as_text);
//        }
//        if ( gettype($datetime) === "object" ) {
//            if ( strtolower($output_format) == 'astext' ) {
//                return $datetime->formatLocalized($datetimetime_carbon_format_as_text);
//            }
//        }
//        $A = preg_split("/./", $datetime);
//        if (count($A) == 2) {
//            $datetime= $A[0];
//        }
//
//        $a= Carbon::createFromFormat($datetimetime_carbon_format, $datetime);
//        $b= $a->format(self::getCFDateFormat("astext"));
//        return $a->format(self::getCFDateFormat("astext"));
    }

    function isValidTimeStamp($timestamp)
    {
        return ((string)(int)$timestamp === (string)$timestamp)
               && ($timestamp <= PHP_INT_MAX)
               && ($timestamp >= ~PHP_INT_MAX);
    }

    public function getDateTimeFormat($format = '')
    {
        if (strtolower($format) == "numbers") {
            return 'Y-m-d H:i';
        }
        if (strtolower($format) == "astext") {
            return 'j F, Y g:i A';
        }

        return 'Y-m-d H:i';
    }

    public function getDateFormat($format = '')
    {
        if (strtolower($format) == "numbers") {
            return 'Y-m-d';
        }
        if (strtolower($format) == "astext") {
            return 'j F, Y';
        }

        return 'Y-m-d';
    }

    public function getCFDateTimeFormat($format = '')
    {
        if (strtolower($format) == "numbers") {
            return 'Y-m-d H:i';
        }
        if (strtolower($format) == "astext") {
            return 'j F, Y g:i A';
        }

        return 'Y-m-d H:i';
    }

    public function getCFDateFormat($format = '')
    {
        if (strtolower($format) == "numbers") {
            return 'Y-m-d';
        }
        if (strtolower($format) == "astext") {
            return 'j F, Y';
        }

        return 'Y-m-d';
    }

    function isCFValidTimeStamp($timestamp)
    {
        if (gettype($timestamp) == "object") {
            return false;
        }

        return ((string)(int)$timestamp === (string)$timestamp)
               && ($timestamp <= PHP_INT_MAX)
               && ($timestamp >= ~PHP_INT_MAX);
    }

    public function folderSize($dir)
    {
        $total_size = 0;
        $count      = 0;
        $dir_array  = scandir($dir);
        foreach ($dir_array as $key => $filename) {
            if ($filename != ".." && $filename != ".") {
                if (is_dir($dir . "/" . $filename)) {
                    $new_foldersize = foldersize($dir . "/" . $filename);
                    $total_size     = $total_size + $new_foldersize;
                } elseif (is_file($dir . "/" . $filename)) {
                    $total_size = $total_size + filesize($dir . "/" . $filename);
                    $count++;
                }
            }
        }

        return $total_size;
    }

    public function cFCreateDir(array $directoriesList = [], $mode = 0777)
    {
        foreach ($directoriesList as $dir) {
            if ( ! file_exists($dir)) {
                mkdir($dir, $mode);
            }
        }
    }


    public function cFWriteArrayToCsvFile(array $dataArray, string $filename, array $directoriesArray): int
    {
        self::cFCreateDir($directoriesArray);
        $path = $directoriesArray[count($directoriesArray) - 1];
        \Excel::create($filename, function ($excel) use ($dataArray) {
            $excel->sheet('file', function ($sheet) use ($dataArray) {
                $sheet->fromArray($dataArray);
            });
        })->store('csv', $path);

        return 1;
    }


    public function getCFImageShowSize(string $image_filename, int $orig_width, int $orig_height)
    {
        if ( ! file_exists($image_filename) or empty($image_filename) or is_dir($image_filename)) {
//            echo '<pre>!file_exists::'.print_r(-1,true).'</pre>';
            return;
        }
        try {
            $height = \Image::make($image_filename)->height();
            $width  = \Image::make($image_filename)->width();
        } catch (Exception $e) {
            return false;
        }
        $retArray                    = array('width' => 0, 'height' => 0, 'original_width' => 0, 'original_height' => 0);
        $retArray['original_width']  = $width;
        $retArray['original_height'] = $height;
        $retArray['width']           = $width;
        $retArray['height']          = $height;

        $ratio = round($width / $height, 3);

        if ($width > $orig_width) {
            $retArray['width']  = (int)($orig_width);
            $retArray['height'] = (int)($orig_width / $ratio);
            if ($retArray['width'] <= (int)$orig_width and $retArray['height'] <= (int)$orig_height) {
                return $retArray;
            }
            $width  = $retArray['width'];
            $height = $retArray['height'];
        }
        if ($height > $orig_height and ((int)($orig_height / $ratio)) <= $orig_width) {
            $retArray['width']  = (int)($orig_height * $ratio);
            $retArray['height'] = (int)($orig_height);

            return $retArray;
        }
        if ($height > $orig_height and ((int)($orig_height / $ratio)) > $orig_width) {
            $retArray['width']  = (int)($orig_height * $ratio);
            $retArray['height'] = (int)($retArray['width'] / $ratio);

            return $retArray;
        }

        return $retArray;
    }


    public function getCFImageProps(string $image_path, array $imagePropsArray = []): array
    {
        if ( ! file_exists($image_path)) {
            return [];
        }
        $imagesExtensionsArray = \Config::get('app.images_extensions', []);
        $extension             = self::getFilenameExtension($image_path);
        $file_width            = null;
        $file_height           = null;
        if (in_array($extension, $imagesExtensionsArray)) {
            $file_width  = Image::make($image_path)->width();
            $file_height = Image::make($image_path)->height();
            $file_size   = Image::make($image_path)->filesize();
        } else {
            $file_size = File::size($image_path);
        }
        $file_size_label       = self::getCFFileSizeAsString($file_size);
        $retArray              = [];
        $retArray['file_info'] = '<b>' . basename($image_path) . '</b>, ' . $file_size_label;


        foreach ($imagePropsArray as $nextImageProp => $nextImagePropValue) {
            $retArray[$nextImageProp] = $nextImagePropValue;
        }
        $retArray['file_size']       = $file_size;
        $retArray['file_size_label'] = $file_size_label;
        if (isset($file_width)) {
            $retArray['file_width'] = $file_width;
        }
        if (isset($file_height)) {
            $retArray['file_height'] = $file_height;
        }
//        echo '<pre>++ $retArray::'.print_r($retArray,true).'</pre>';
        if ( ! empty($retArray['file_width']) and ! empty($retArray['file_height'])) {
            $retArray['file_info'] .= ', ' . $retArray['file_width'] . 'x' . $retArray['file_height'];
        }

        return $retArray;
    }

    public function getFilenameBasename($file)
    {
        return File::name($file);
    }

    public function getFilenameExtension($file)
    {
        return File::extension($file);
    }


    public function getCFFileSizeAsString(string $file_size): string
    {
        if ((int)$file_size < 1024) {
            return $file_size . 'b';
        }
        if ((int)$file_size < 1024 * 1024) {
            return floor($file_size / 1024) . 'kb';
        }

        return floor($file_size / (1024 * 1024)) . 'mb';
    }


//    public function info($info)
//    { // TODO
//        if (class_exists("Debugbar")) {
//            Debugbar::info($info);
//        }
//    }
    function isRunningUnderDocker()
    {
        if (empty($_SERVER['HTTP_HOST'])) return false;
        $docker_host = '127.0.0.1:8084';
        $pos         = strpos($_SERVER['HTTP_HOST'], $docker_host);
        if ($pos === false) {
            return false;
        } else {
            return true;
        }
    }


    public function getSystemInfo()
    {
        $DB_CONNECTION = config('database.default');
        $connections   = config('database.connections');
        $database_name = ! empty($connections[$DB_CONNECTION]['database']) ? $connections[$DB_CONNECTION]['database'] : '';


        $pdo           = DB::connection()->getPdo();
        $db_version    = $pdo->query('select version()')->fetchColumn();
        $tables_prefix = DB::getTablePrefix();

//        $elasticsearch_version = $this->getElasticsearchInfo('version');


        $retArray['laravel_version'] = app()::VERSION;
        $retArray['php_version']     = phpversion();
        $retArray['debug']           = config('app.debug');
        $retArray['env']             = config('app.env');
        $retArray['db_connection']   = $DB_CONNECTION;
        $retArray['db_version']      = $db_version;
        $retArray['database_name']   = $database_name;
        $retArray['database_name']   = $database_name;
        $retArray['tables_prefix']   = $tables_prefix;
        ob_start();
        echo phpinfo();
        $retArray['phpinfo'] = ob_get_contents();
        ob_end_clean();

        return $retArray;
    }

    // http://stackoverflow.com/questions/36657629/how-to-use-traits-laravel-5-2

    public function isPositiveNumeric(int $str): bool
    {
        if (empty($str)) {
            return false;
        }

        return (is_numeric($str) && $str > 0 && $str == round($str));
    }


    public function replaceSpaces($S)
    {
        $Pattern = '/([\s])/xsi';
        $S       = preg_replace($Pattern, '&nbsp;', $S);

        return $S;
    }

    public function createDir(array $directoriesList = [], $mode = 0777)
    {
        foreach ($directoriesList as $dir) {
            if ( ! file_exists($dir)) {
                mkdir($dir, $mode);
            }
        }
    }

    public function deleteEmptyDirectory(string $directory_name)
    {
        if ( ! file_exists($directory_name) or ! is_dir($directory_name)) {
            return true;
        }
        $H = OpenDir($directory_name);
        while ($nextFile = readdir($H)) { // All files in dir
            if ($nextFile == "." or $nextFile == "..") {
                continue;
            }
            closedir($H);

            return false; // if there are files can not delete files
        }
        closedir($H);

        return rmdir($directory_name);
    }


    function deleteDirectory($dir)
    {
        if ( ! file_exists($dir)) {
            return true;
        }

        if ( ! is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if ( ! $this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($dir);
    }

    public function deleteDirectoryOld(string $directory_name)
    {
//        echo '<pre>$directory_name::'.print_r($directory_name,true).'</pre>';
        if ( ! file_exists($directory_name) or ! is_dir($directory_name)) {
            return true;
        }

        rmdir($directory_name);

//        File::deleteDirectory($directory_name);
        return;
//        echo '<pre>++++=$directory_name::'.print_r($directory_name,true).'</pre>';
        $H = OpenDir($directory_name);
        while ($nextFile = readdir($H)) { // All files in dir
            if ($nextFile == "." or $nextFile == "..") {
                continue;
            }
            if (is_dir($directory_name . DIRECTORY_SEPARATOR . $nextFile)) {
                $this->deleteDirectory($directory_name . DIRECTORY_SEPARATOR . $nextFile);
            } else {
                unlink($directory_name . DIRECTORY_SEPARATOR . $nextFile);
            }
        }
        closedir($H);

        return rmdir($directory_name);
    }

    public function pregSplit(string $splitter, string $string_items, bool $skip_empty = true, $to_lower = false): array
    {
        $retArray = [];
        $a        = preg_split(($splitter), $string_items);
        foreach ($a as $next_key => $next_value) {
            if ($skip_empty and ( ! isset($next_value) or empty($next_value))) {
                continue;
            }
            $retArray[] = ($to_lower ? strtolower(trim($next_value)) : trim($next_value));
        }

        return $retArray;
    }


    public function d($data, $file = null, $line = null)
    {
        $this->debToFile($data);

        return;

        $ret_str = '';

        if ( ! is_null($file)) {
            $ret_str .= 'file:' . $file . ', ';
        }

        if ( ! is_null($line)) {
            $ret_str .= 'line:' . $line . ', ';
        }
        if ( ! is_null($data)) {
            dump($ret_str . ' : ' . $data);
        }

        return;


        if (empty($data)) {
            return;
        }

        $get_type = $this->myGetType($data);
//        if ( $get_type== 'NULL' or $get_type== 'unknown type' ) return;
        echo '<pre>$data::' . print_r($data, true) . '</pre>';
        echo '<pre>gettype($data)::' . print_r(gettype($data), true) . '</pre>';
//
        dump($data);
    }

    function myGetType($var)
    {
        if (is_array($var)) {
            return "array";
        }
        if (is_bool($var)) {
            return "boolean";
        }
        if (is_float($var)) {
            return "float";
        }
        if (is_int($var)) {
            return "integer";
        }
        if (is_null($var)) {
            return "NULL";
        }
        if (is_numeric($var)) {
            return "numeric";
        }
        if (is_object($var)) {
            return "object";
        }
        if (is_resource($var)) {
            return "resource";
        }
        if (is_string($var)) {
            return "string";
        }

        return "unknown type";
    }


    public function makeStripTags(string $str)
    {
        return strip_tags($str);
    }

    protected function makeClearDoubledSpaces(string $str): string
    {
        return preg_replace("/(\s{2,})/ms", " ", $str);
    }

    protected function makeStripslashes(string $str): string
    {
        return stripslashes($str);
    }


    /* Submitting form string value must be worked out according to options of app */
    protected function workTextString($str)
    {
        if (is_string($str)) {
            $str = $this->makeStripTags($str);
        }
        if (is_string($str)) {
            $str = $this->makeStripslashes($str);
        }
        if (is_string($str)) {
            $str = $this->makeClearDoubledSpaces($str);
        }

        return is_string($str) ? trim($str) : '';
    }


    public function sendSMSMessageByTwilio(string $message_str, bool $show_site_name = true)
    {
//        $loggedUser = Auth::guard('api')->user();
        if (class_exists("Debugbar")) {
            \Debugbar::info('<pre>sendSMSMessageByTwilio  $message_str::' . print_r($message_str, true) . '</pre>');
        }
        \Log::info('<pre>sendSMSMessageByTwilio  $message_str::' . print_r($message_str, true) . '</pre>');

        return;

        $message = $message_str;
        if ($show_site_name) {
            $site_name = Settings::getValue('site_name', '');
            $message   = $site_name . " : '" . $message_str;
        }
        // +1 309-518-1423
        $default_phone = '+38 095-9180286'; //+1 309-518-1423

        /*        $newDebugging             = new Debugging();
                $newDebugging->user_id    = $loggedUser->id;

                $newDebugging->info       = $message;
                $newDebugging->type       = 'SMS';
                $newDebugging->save();*/
//        return;  // UNCOMMENT FOR REAL WORK

//        $current_error_reporting = error_reporting(E_ALL ^ E_WARNING);
        Twilio::message($default_phone, $message);
//        error_reporting($current_error_reporting);
//        error_reporting	22527
    } // public function sendSMSMessageByTwilio(string $message_str, bool $show_site_name= true )

    public function debToFile($contents, string $descr_text = '', bool $is_sql = false, string $file_name = '')
    {
        //return;
        try {
            if (empty($file_name)) {
                $file_name = storage_path() . '/logs/logging_deb.txt';
            }
//                $file_name = '/_wwwroot/lar/lprods/storage/logs/logging_deb.txt';
//                $fd = fopen($file_name, ($is_clear_text ? "w+" : "a+"));
            $fd = fopen($file_name, "a+");
            if (is_array($contents) == 'array') {
                $contents = print_r($contents, true);
            }
            if ($is_sql) {
//                fwrite($fd, '<pre>' . $descr_text . '::' . self::showFormattedSql($contents, false) . '<pre>' . chr(13));
            } else {
                fwrite($fd, '<pre>' . $descr_text . '::' . $contents . '</pre>' . chr(13));
            }
            //                     echo '<b><I>' . gettype($Var) . "</I>" . '&nbsp;$Var:=<font color= red>&nbsp;' . AppUtil::showFormattedSql($Var) . "</font></b></h5>";

            fclose($fd);

            return true;
        } catch (Exception $lException) {
            return false;
        }
    }


}
