<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\FuncsTrait;
use App\Category;

class CreateCategoryRequest extends FormRequest
{
    use FuncsTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $request= Request();
        return Category::getCategoryValidationRulesArray( $request->get('id') );

        //     public static function getCategoryValidationRulesArray($category_id= null) : array

        return [
            "name" => "required|min:3",
//            "email" => "required|email",
//            "phone" => "required|min:10|numeric",
//            "website" => "required|url"
        ];
        /* CREATE TABLE `categories` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`status` ENUM('N','A','I') NOT NULL COMMENT ' N=>New,  A=>Active, I=>Inactive' COLLATE 'utf8mb4_unicode_ci',
	`creator_id` INT(10) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL, */
    }
}
