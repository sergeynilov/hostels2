<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\FuncsTrait;
use App\User;

class PersonalRequest extends FormRequest
{
    use FuncsTrait;
    /**
     * Determine if the Customer is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $request= Request();
        return User::getUserValidationRulesArray( $request->get('id') );
    }
}
