<?php

namespace App\Http\Requests;

use App\Http\Traits\FuncsTrait;
use Illuminate\Foundation\Http\FormRequest;
use App\Customer;
class CustomerRegisterRequest extends FormRequest
{
    use FuncsTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request= Request();
        $requestData= $request->all();

        $this->debToFile(print_r( $requestData,true),'  getCustomerValidationRulesArray $requestData::');
        $customerValidationRulesArray= Customer::getCustomerValidationRulesArray( $request->get('id'), ['status'] );
//        $this->debToFile(print_r( $customerValidationRulesArray,true),'  getCustomerValidationRulesArray $customerValidationRulesArray::');
        return $customerValidationRulesArray;
    }
}
