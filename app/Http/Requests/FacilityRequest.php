<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Traits\FuncsTrait;
use App\Facility;

class FacilityRequest extends FormRequest
{
    use FuncsTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request= Request();
        return Facility::getFacilityValidationRulesArray( $request->get('id') );
    }
}
