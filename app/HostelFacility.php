<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class HostelFacility extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'hostel_facilities';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'facility_id', 'hostel_id'];

    public function Facility()
    {
        return $this->belongsTo('App\Facility');
    }
    public function scopeGetByFacilityId($query, $facility_id= null)
    {
        if (!empty($facility_id)) {
            if ( is_array($facility_id) ) {
                $query->whereIn(with(new HostelFacility)->getTable().'.facility_id', $facility_id);
            } else {
                $query->where(with(new HostelFacility)->getTable().'.facility_id', $facility_id);
            }
        }
        return $query;
    }


    public function hostel()
    {
        return $this->belongsTo('App\Hostel');
    }

    public function scopeGetByHostelId($query, $hostel_id= null)
    {
        if (!empty($hostel_id)) {
            if ( is_array($hostel_id) ) {
                $query->whereIn(with(new HostelFacility)->getTable().'.hostel_id', $hostel_id);
            } else {
                $query->where(with(new HostelFacility)->getTable().'.hostel_id', $hostel_id);
            }
        }
        return $query;
    }


}
