<?php

namespace App;
use DB;
use Cviebrock\EloquentSluggable\Sluggable;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class Forum extends MyAppModel
{
    use FuncsTrait;
    use Sluggable;
    protected $table      = 'forums';
    protected $primaryKey = 'id';
    public $timestamps    = false;


    protected $fillable = [ 'title', 'slug', 'description', 'creator_id', 'forum_category_id', 'published', 'views', 'updated_at' ];

    public function creator()
    {
        return $this->belongsTo('App\User','creator_id');
    }

    public function forumThreads()
    {
        return $this->hasMany('App\ForumThread', 'forum_id', 'id');
    }


    public function scopeGetByCreatorId($query, $creator_id= null)
    {
        if (!empty($creator_id)) {
            if ( is_array($creator_id) ) {
                $query->whereIn( with(new Forum)->getTable().'.creator_id', $creator_id );
            } else {
                $query->where( with(new Forum)->getTable() . '.creator_id', $creator_id );
            }
        }
        return $query;
    }


    public function scopeGetBySlug($query, $slug = null)
    {
        if (empty($slug)) {
            return $query;
        }
        return $query->where(with(new Forum)->getTable() . '.slug',  $slug);
    }

    public function scopeGetByTitle($query, $title = null, $partial = false)
    {
        if (empty($title)) {
            return $query;
        }
        return $query->where(with(new Forum)->getTable() . '.title', (! $partial ? '=' : 'like'), ($partial ? '%' : '') . $title . ($partial ? '%' : ''));
    }

    public function scopeGetByForumCategoryId($query, $forum_category_id= null)
    {
        if (!empty($forum_category_id)) {
            $query->where(with(new Forum)->getTable().'.forum_category_id', $forum_category_id);
        }
        return $query;
    }

    public function scopeGetByPublished($query, $published = null)
    {
        if (!isset($published) or strlen($published) == 0) {
            return $query;
        }
        return $query->where(with(new Forum)->getTable().'.published', $published);
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
