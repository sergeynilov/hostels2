<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;
use Illuminate\Validation\Rule;


class Category extends MyAppModel
{
    use FuncsTrait;

    protected $table = 'categories';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [ 'name' ];


    protected $casts = [
    ];


    protected static function boot() {
        parent::boot();
        static::deleting(function($category) {
        });

    }

    public function scopeGetByName($query, $name= null, $partial= false)
    {
        if (empty($name)) return $query;
        return $query->where(with(new Category)->getTable().'.name', (!$partial?'=':'like'), ($partial?'%':''). $name .($partial?'%':'') );
    }

    /* check if provided name is unique for categories.name field */
    public static function getSimilarCategoryByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Category::where( 'name', $name );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getCategoryValidationRulesArray($category_id= null) : array
    {
        $validationRulesArray = [
            'name' => [
                'required',
                'string',
                'max:100',
                Rule::unique(with(new Category)->getTable())->ignore($category_id),
            ],
        ];
        return $validationRulesArray;
    }

    public static function getCategoriesSelectionArray() :array {
        $categories = Category::orderBy('name','desc')->get();
        $categoriesSelectionArray= [];
        foreach( $categories as $nextCategory ) {
            $categoriesSelectionArray[$nextCategory->id]= $nextCategory->name;
        }
        return $categoriesSelectionArray;
    }

}
