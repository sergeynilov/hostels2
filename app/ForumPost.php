<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class ForumPost extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'forum_posts';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'body', 'user_id' , 'is_best_decision', 'forum_thread_id', 'updated_at'  ];
    private static $forumPostActionLabelValueArray = Array('Ed' => 'Edit', 'Del' => 'Delete', 'Sub' => 'Subscribe', 'Bkm' => 'Bookmark');

    public function forumThread()
    {
        return $this->belongsTo('App\ForumThread'/*, 'user_id'*/);
    }

    public function scopeGetByForumThreadId($query, $forum_thread_id= null)
    {
        if (!empty($forum_thread_id)) {
            if ( is_array($forum_thread_id) ) {
                $query->whereIn(with(new ForumPost)->getTable().'.forum_thread_id', $forum_thread_id);
            } else {
                $query->where(with(new ForumPost)->getTable().'.forum_thread_id', $forum_thread_id);
            }
        }
        return $query;
    }


    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }


    public function scopeGetByForumId($query, $forum_id = null, $alias= '')
    {
        if (empty($forum_id)) {
            return $query;
        }
        if (empty($alias)) {
            $alias= with(new ForumThread)->getTable();
        }
        return $query->where( $alias.'.forum_id', $forum_id );
    }


    public function scopeGetByUserId($query, $user_id = null)
    {
        if (empty($user_id)) {
            return $query;
        }
        return $query->where( 'user_id', $user_id );
    }


    public function scopeGetByIsBestDecision($query, $is_best_decision = null)
    {
        if (empty($is_best_decision)) {
            return $query;
        }
        return $query->where( 'is_best_decision', $is_best_decision );
    }


    public static function getForumPostActionValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$forumPostActionLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getForumPostActionLabel(string $published): string
    {
        if ( ! empty(self::$forumPostActionLabelValueArray[$published])) {
            return self::$forumPostActionLabelValueArray[$published];
        }
        return self::$forumPostActionLabelValueArray[0];
    }



    public static function getForumPostValidationRulesArray($forum_post_id = null): array
    {


        $validationRulesArray = [
            /*            'full_name'          => [
                            'required',
                            'string',
                            'max:100',
                        ],
                        'email'                => 'required|max:50|email',
                        'phone'                => 'required|max:255',
                        'website'              => 'required|max:100',
                        'info'           => 'required',
                        'start_date'           => 'required',
                        'request_callback'     => 'required|in:' . with(new ForumPost)->getValueLabelKeys(ForumPost::getRequestCallbackValueArray(false)),
                        'status'               => 'required|in:' . with(new ForumPost)->getValueLabelKeys(ForumPost::getForumPostStatusValueArray(false)),*/
        ];
        return $validationRulesArray;
    }


}
