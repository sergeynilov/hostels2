<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class UsersGroups extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'users_groups';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'user_id', 'group_id'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }
    public function scopeGetByUserId($query, $user_id= null)
    {
        if (!empty($user_id)) {
            if ( is_array($user_id) ) {
                $query->whereIn(with(new UsersGroups)->getTable().'.user_id', $user_id);
            } else {
                $query->where(with(new UsersGroups)->getTable().'.user_id', $user_id);
            }
        }
        return $query;
    }


    public function group()
    {
        return $this->belongsTo('App\Group');
    }
    public function scopeGetByGroupId($query, $group_id= null)
    {
        if (!empty($group_id)) {
            if ( is_array($group_id) ) {
                $query->whereIn(with(new UsersGroups)->getTable().'.group_id', $group_id);
            } else {
                $query->where(with(new UsersGroups)->getTable().'.group_id', $group_id);
            }
        }
        return $query;
    }


}
