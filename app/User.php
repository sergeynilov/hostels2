<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use App\UsersGroups;
use App\Group;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $userAvatarPropsArray = [];
    protected $avatar_filename_max_length = 255;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'status', 'account_type', 'first_name', 'last_name', 'phone', 'creator_id', 'avatar'
        /* CREATE TABLE `ad_users` (
        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
       `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
        `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('N','A','I') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' N => New(Waiting activation), A=>Active, I=>Inactive',
  `account_type` enum('I','B') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'I' COMMENT ' I=>Individual, B=>Business',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `verification_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `creator_id` int(10) unsigned DEFAULT NULL,
  `activated_at` datetime DEFAULT NULL,
  `avatar` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_users_username_unique` (`username`),
  UNIQUE KEY `ad_users_email_unique` (`email`),
  KEY `users_status_account_type_index` (`status`,`account_type`),
  KEY `users_creator_id_foreign` (`creator_id`),
  CONSTRAINT `ad_users_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `ad_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
 */
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static $uploads_user_avatars_dir = 'user-avatars/-user-avatar-';  // file:///_wwwroot/lar/hostels2/storage/app/public/user-avatars/-user-avatar-4/adam_lang.jpg

    protected static function boot() {
        parent::boot();
        static::deleting(function($user) {
            $hostel_avatar_image_path= User::getUserAvatarPath($user->id, $user->avatar);
            (with (new MyAppModel))->debToFile(print_r($hostel_avatar_image_path, true), ' User boot - $hostel_avatar_image_path::');
            MyAppModel::deleteFileByPath($hostel_avatar_image_path, true);
        });
    }


    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /* return data array by keys id/name based on filters/ordering... */
    public static function getUsersSelectionList( bool $key_return= true, array $filtersArray = [], string $order_by = 'name', string $order_direction = 'asc') : array
    {
        $usersList = User
            ::orderBy('username', 'asc')
            ->get();
        $resArray = [];
//        with(new User)->debToFile(print_r( $filtersArray,true),'  getUsersSelectionList  -2 $filtersArray::');
        foreach ($usersList as $nextUser) {
            if ($key_return) {
                $resArray[] = array('key' => $nextUser->id, 'label' => $nextUser->username . ( empty($filtersArray['short']) ? ' (' . $nextUser->email . ')' : '') );
            } else{
                $resArray[ $nextUser->id ]= $nextUser->username . ( empty($filtersArray['short']) ? ' (' . $nextUser->email . ')' : '');
            }
        }
        return $resArray;
    }


    public static function getUserValidationRulesArray() : array
    {

        $valid_nice_name_format= config('app.valid_nice_name_format');
        $additional_check_nice_name_validation_rule= 'regex:'.$valid_nice_name_format;
        $additional_name_validation_rule= 'check_user_unique_by_name';
        $additional_email_validation_rule= 'check_user_unique_by_email';

//        $valid_phone_format= config('app.valid_phone_format');
//        $additional_phone_validation_rule= 'regex:'.$valid_phone_format;
        $validationRulesArray = [
            'username'        => 'required|max:100|' . $additional_name_validation_rule,
            'email'           => 'required|email|max:100|' . $additional_email_validation_rule,
//            'status'          => 'required|'.$additional_check_nice_name_validation_rule,
            'status'          => 'required|in:' . with(new MyAppModel)->getValueLabelKeys(User::getNewsIsFeaturedValueArray(false)),
            'first_name'      => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'last_name'       => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'phone'           => 'nullable|max:255',
            'website'         => 'nullable|max:100',
            'notes'           => 'nullable',
        ];
        return $validationRulesArray;
    } // public static function getUserValidationRulesArray($user_id) : array

    public static function getUserRegisterValidationRulesArray() : array
    {

        $valid_nice_name_format= config('app.valid_nice_name_format');
        $additional_check_nice_name_validation_rule= 'regex:'.$valid_nice_name_format;
        $additional_name_validation_rule= 'check_user_unique_by_name';
        $additional_email_validation_rule= 'check_user_unique_by_email';

        $valid_phone_format= config('app.valid_phone_format');
//        $additional_phone_validation_rule= 'regex:'.$valid_phone_format;
        $validationRulesArray = [
            'username'            => 'required|max:255|' . $additional_name_validation_rule,
            'email'           => 'required|email|max:255|' . $additional_email_validation_rule,
            'password'        => 'required|min:6|max:15', // required|min:6|max:15|confirmed:register_password
            'password_2'      => 'required|min:6|max:15|same:password', // required|min:6|max:15|confirmed:register_password
            'first_name'      => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'last_name'       => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'phone'           => 'nullable|max:255',
            'website'         => 'nullable|max:100',
            'notes'           => 'nullable',
        ];
        return $validationRulesArray;
    } // public static function getUserRegisterValidationRulesArray($user_id) : array

    public function checkRolesAsText() {
        $arr= [];
//        echo '<pre>$this->id::'.print_r($this->id,true).'</pre>';
//        dd($this);
        if ( /*$this->hasRole(ACCESS_ROLE_ADMIN) */ 1 ) {
            $arr[]= ACCESS_ROLE_ADMIN_LABEL;
//            echo '-1';
        }
        if ( /* $this->hasRole(ACCESS_ROLE_MANAGER) */ 1 ) {
            $arr[]= ACCESS_ROLE_MANAGER_LABEL;
//            echo '-2';
        }
        if ( /*$this->hasRole(ACCESS_ROLE_CUSTOMER) */ 1 ) {
            $arr[]= ACCESS_ROLE_CUSTOMER_LABEL;
//            echo '-3';
        }
//        echo '<pre>$arr::'.print_r($arr,true).'</pre>';
//        die("-1 XXZ");
        return $this->concatArray($arr);
    }

    public function concatArray($arr, $splitter = ',', $skip_empty = true, $skip_last_delimiter = true)
    {
        $ret_str = '';
        if ( ! is_array($arr) or empty($arr)) {
            return '';
        }
        $l              = count($arr);
        $nonempty_array = [];
        for ($i = 0; $i < $l; $i++) {
            $next_value = trim($arr[$i]);
            if (empty($next_value) and $skip_empty) {
                continue;
            }
            $nonempty_array[] = self::removeMore1Space($next_value);
        }

        $l = count($nonempty_array);
        for ($i = 0; $i < $l; $i++) {
            $next_value = trim($nonempty_array[$i]);
            $ret_str    .= $next_value . (($skip_last_delimiter and $i == $l - 1) ? '' : $splitter);
        }

        return $ret_str;
    }

    public function removeMore1Space($str)
    {
        $res = preg_replace('/\s\s+/', ' ', $str); // This will be 'foo o' now

        return $res;
    }

    public function getAvatarFilenameMaxLength(): int
    {
        return $this->avatar_filename_max_length;
    }
    public static function getUserAvatarDir(int $user_avatar_id): string
    {
        return self::$uploads_user_avatars_dir . $user_avatar_id . '/';
    }


    public static function getUserAvatarPath(int $user_avatar_id, $avatar): string
    {
        with(new MyAppModel)->debToFile(print_r($avatar, true), '  - -2 $avatar::');

        if (empty($avatar)) {
            return '';
        }
//        return '/storage/app/' . self::$uploads_user_avatars_dir . $user_avatar_id . '/' . $avatar;
        return '/storage/' . self::$uploads_user_avatars_dir . $user_avatar_id . '/' . $avatar;
    }

    public static function setUserAvatarProps(int $user_id, string $avatar = null, bool $skip_non_existing_file = false): array
    {
        if (empty($avatar) and $skip_non_existing_file) {
            return [];
        }
        $file_was_found= true;
        $dir_path = self::$uploads_user_avatars_dir . '' . $user_id . '';
//        echo '<pre>$dir_path::'.print_r($dir_path,true).'</pre>';
        $file_full_path = $dir_path . '/' . $avatar;
//        echo '<pre>$file_full_path::'.print_r($file_full_path,true).'</pre>';
        $file_exists    = ( !empty($avatar) and Storage::disk('local')->exists('public/' . $file_full_path) );
//        echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
        if ( ! $file_exists) {
            if ($skip_non_existing_file) {
                return [];
            }
            $file_was_found= false;
            $file_full_path = config('app.empty_img_url');
        }

        $avatar_path = $file_full_path;
        if ($file_exists) {
            $avatar_url       = 'storage/app/' . $dir_path . '/' . $avatar;
            $avatarPropsArray = ['avatar' => $avatar, 'avatar_path' => $avatar_path, 'avatar_url' => '/storage/' . $dir_path . '/' . $avatar];
            $avatar_full_path = base_path() . '/storage/app/public/' . $avatar_path;
        } else {
            $avatarPropsArray = ['avatar' => $avatar, 'avatar_path' => $avatar_path, 'avatar_url' => $file_full_path];
            $avatar_full_path = base_path() . '/public/' . $avatar_path;
        }

        if ( ! empty($previewSizeArray['width'])) {
            $avatarPropsArray['preview_width']  = $previewSizeArray['width'];
            $avatarPropsArray['preview_height'] = $previewSizeArray['height'];
        }
        $userAvatarImgProps = with(new MyAppModel)->getCFImageProps($avatar_full_path, $avatarPropsArray);

        if ( !$file_was_found ) {
            $userAvatarImgProps['file_info'] = 'File "' . $avatar . '" not found ';
        }
        return $userAvatarImgProps;

    }


    /* get additional properties of cms_item photo : path, url, size etc... */
    public function getUserAvatarPropsAttribute(): array
    {
        return $this->userAvatarPropsArray;
    }

    /* set additional properties of cms_item photo : path, url, size etc... */
    public function setUserAvatarPropsAttribute(array $userAvatarPropsArray)
    {
        $this->userAvatarPropsArray = $userAvatarPropsArray;
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public static function getUsersGroupsByUserId(int $user_id, $show_group_title= false)
    {
        $groups_tb= with(new Group)->getTable();
        $users_groups_tb= with(new UsersGroups)->getTable();
        $usersGroupsList= UsersGroups::where( 'user_id', $user_id )
                                   ->leftJoin($groups_tb, $groups_tb.'.id', '=', $users_groups_tb.'.group_id')
                                   ->select( $groups_tb.".id as group_id", $groups_tb.".name as group_name" )
                                   ->get();
        if ( !$show_group_title ) return $usersGroupsList;
        $ret= '';
        foreach( $usersGroupsList as $nextUsersGroup ) {
            $ret.= $nextUsersGroup->group_name.', ';
        }
        $ret= with(new Group)->trimRightSubString($ret, ', ');
        return $ret;
    }


}
