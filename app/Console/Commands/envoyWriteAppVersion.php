<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Settings;
use App\Http\Traits\FuncsTrait;

class envoyWriteAppVersion extends Command
{
    use FuncsTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'envoy:write-app-version {app_version}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //   envoy run Hostels2Deploy  --lardeployer_password=mypass1112233 --app_version=0.105a
        $arguments = $this->arguments();
        echo '<pre>app/Console/Commands/envoyWriteAppVersion.php $arguments::'.print_r($arguments,true).'</pre>';
        if ( !empty($arguments['app_version']) ) {
            $app_version = $arguments['app_version'];

            echo '<pre>app/Console/Commands/envoyWriteAppVersion.php  $app_version::'.print_r($app_version,true).'</pre>';

/*            $appVersion = Settings::getSimilarSettingsByName('app_version');
            if ( empty($appVersion) ) {
                $appVersion= new Settings();
                $appVersion->name= "app_version";
            }
            $now_label= $this->getCFFormattedDateTime(time(), "astext");
            $appVersion->value= $app_version . ', on '. $now_label;
            $appVersion->save();*/

            file_put_contents( public_path('app_version.txt'), $app_version);
        }

        $ds = disk_total_space("/");
        $info= 'Server total space : '.$this->getNiceFileSize($ds);

        $ds = disk_free_space("/");
        $info.= ', ' . $this->getNiceFileSize($ds).' free, ';

//        echo '<pre>base_path()::'.print_r(base_path(),true).'</pre>';
        $ds = folderSize( base_path() );
        $info.= '. Application takes : ' . $this->getNiceFileSize($ds);
        echo $info;
    }


}

function folderSize($dir){
    $total_size = 0;
    $count = 0;
    $dir_array = scandir($dir);
    foreach($dir_array as $key=>$filename){
        if($filename!=".." && $filename!="."){
            if(is_dir($dir."/".$filename)){
                $new_foldersize = foldersize($dir."/".$filename);
                $total_size = $total_size+ $new_foldersize;
            }else if(is_file($dir."/".$filename)){
                $total_size = $total_size + filesize($dir."/".$filename);
                $count++;
            }
        }
    }
    return $total_size;
}

