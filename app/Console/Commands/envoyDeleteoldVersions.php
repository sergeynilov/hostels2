<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Command;
use App\Http\Traits\FuncsTrait;
use Symfony\Component\Process\Process;


class envoyDeleteoldVersions extends Command
{
    use FuncsTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'envoy:delete-old-versions {directory_to_clear}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {       // php artisan envoy:delete-old-versions  "/_wwwroot/lar/Hostels2/storage/app/public/hostels"

        $arguments = $this->arguments();
        if ( !empty($arguments['directory_to_clear']) ) {
            $directory_name = $arguments['directory_to_clear'];
        }
        $is_debug= true;
        //file:///_wwwroot/lar/Hostels2/storage/app/public/hostels
//        $files = Storage::disk('local')->directories( 'public/' . $directory_name);
        $directoriesList = File::directories($directory_name);

/*        if ($is_debug) {
            echo '<pre>==$directoriesList::' . print_r($directoriesList, true) . '</pre>';
        }*/

        $sortedDirectories = collect($directoriesList)
//            ->filter(function ($file) {
//                return in_array($file->getExtension(), ['png', 'gif', 'jpg']);
//            })
            ->sortBy(function ($dir) {
//                echo '<pre>-1 $file::'.print_r($file,true).'</pre>';
                return filemtime( $dir );
//                return $file->getCTime();
            })
//            ->map(function ($file) {
//                echo '<pre>-2 $file::'.print_r($file,true).'</pre>';
//                return $file->getBaseName();
//            })
            ;
        echo '<pre>+++ 222 $sortedDirectories::'.print_r($sortedDirectories,true).'</pre>';
        $leave_last_dirs= 5;
        $index= 1;
        foreach ($sortedDirectories as $next_directory) {
            echo '<pre>$index::'.print_r($index,true).'</pre>';
            if ( $index < count($sortedDirectories) - $leave_last_dirs ) {
                echo '<pre>CD AND DELETE $next_directory ::'.print_r($next_directory,true).'</pre>';

//                # chmod -R 775 {{ $release_number_dir }}/{{ $file }}

                $process = new Process(['cd ' . $next_directory]);
                $ret= $process->run();
                echo '<pre>CD $next_directory $ret::'.print_r($ret,true).'</pre>';
/*                $process = new Process(['chmod -R 777 ' . $next_directory]);
                $process->run();*/

                $process = new Process(['rm . -R -f']);
//                $process = new Process(['rm -R ' . $next_directory]);
                $ret2= $process->run();
                echo '<pre>rm -R  $next_directory $ret2::'.print_r($ret2,true).'</pre>';
//                $this->deleteDirectory($next_directory);
            }
            $index++;
//            break; // TO comment
        }
        //    public function deleteDirectory(string $directory_name)
        die("-1 XXZ");
        /*

    public
    function clearDirectoryByPeriod(
        string $directory_name,
        $hours
    ) {

        $is_debug                  = true;
        $deleted_files_count       = 0;
        $deleted_directories_count = 0;
        $exists                    = Storage::disk('local')->exists('public/' . $directory_name);
        if ($is_debug) {
            echo '<pre>$exists::' . print_r($exists, true) . '</pre>';
            echo '<pre>$directory_name::' . print_r($directory_name, true) . '</pre>';
        }
        if ( ! $exists) {
            return false;
        }

        $files = Storage::disk('local')->allFiles('public/' . $directory_name);
        if ($is_debug) {
            echo '<pre>$files::' . print_r($files, true) . '</pre>';
        }

        $now_dt = Carbon::now();
        foreach ($files as $next_file) {
            $next_file_time = Storage::disk('local')->lastModified($next_file);
            if ($is_debug) {
                echo '<pre>$next_file::' . print_r($next_file, true) . '</pre>';
                echo '<pre>??$next_file_time::' . print_r($next_file_time, true) . '</pre>';
                echo '<pre>222$next_file_time::' . print_r($this->getCFFormattedDateTime($next_file_time), true) . '</pre>';
            }


            $next_file_dt = Carbon::createFromTimestamp($next_file_time);
            $next_file_dt->addHours($hours);

            if ($now_dt->greaterThan($next_file_dt)) {    // the file is absolute
                $deleted_files_count++;
                if ($is_debug) {
                    echo '<pre>DELETE $next_file::' . print_r($next_file, true) . '</pre>';
                }
                Storage::disk('local')->delete($next_file);
            }
            echo 'next file <hr><hr>';
        }

        $directories = Storage::disk('local')->allDirectories('public/' . $directory_name);
        $is_debug    = true;
        foreach ($directories as $next_directory) {
            $next_directory_time = Storage::disk('local')->lastModified($next_directory);
            if ($is_debug) {
                echo '<pre>$next_directory::' . print_r($next_directory, true) . '</pre>';
                echo '<pre>??$next_directory_time::' . print_r($next_directory_time, true) . '</pre>';
                echo '<pre>222$next_directory_time::' . print_r($this->getCFFormattedDateTime($next_directory_time), true) . '</pre>';
            }
            $files                 = Storage::disk('local')->allFiles($next_directory);
            $directory_files_count = count($files);
            echo '<pre>$directory_files_count::' . print_r($directory_files_count, true) . '</pre>';


            $next_directory_dt = Carbon::createFromTimestamp($next_directory_time);
            $next_directory_dt->addHours($hours);

//            die("-1 XXZ");
//            if( $next_directory_dt->greaterThan($now_dt) and $directory_files_count == 0) {    // the directory is absolute and empty
            if ($now_dt->greaterThan($next_directory_dt) and $directory_files_count == 0) {    // the directory is absolute and empty
                $deleted_directories_count++;
                if ($is_debug) {
                    echo '<pre>DELETE $next_directory_time::' . print_r($next_directory_time, true) . '</pre>';
                }
                Storage::disk('local')->deleteDirectory($next_directory);
            }

            echo 'next dir <hr><hr>';
        }

        return ['deleted_files_count' => $deleted_files_count, 'deleted_directories_count' => $deleted_directories_count];
    }
 */
    }
}
