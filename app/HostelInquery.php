<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class HostelInquery extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'hostel_inqueries';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'hostel_id', 'email', 'full_name' , 'phone' , 'info' , 'start_date' , 'end_date' , 'request_callback', 'updated_at' ];

    private static $hostelInqueryStatusLabelValueArray = Array('N' => 'New', 'A' => 'Accepted', 'O' => 'Completed', 'C' => 'Cancelled');
    private static $hostelInqueryRequestCallbackLabelValueArray = Array('N' => 'New', 'A' => 'Accepted', 'O' => 'Completed', 'C' => 'Cancelled');

    public static function getHostelInqueryStatusValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$hostelInqueryStatusLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getHostelInqueryStatusLabel(string $status): string
    {
        if ( ! empty(self::$hostelInqueryStatusLabelValueArray[$status])) {
            return self::$hostelInqueryStatusLabelValueArray[$status];
        }
        return self::$hostelInqueryStatusLabelValueArray[0];
    }

    public static function getRequestCallbackValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$hostelInqueryRequestCallbackLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getRequestCallbackLabel(string $status): string
    {
        if ( ! empty(self::$hostelInqueryRequestCallbackLabelValueArray[$status])) {
            return self::$hostelInqueryRequestCallbackLabelValueArray[$status];
        }
        return self::$hostelInqueryRequestCallbackLabelValueArray[0];
    }

    public function hostel()
    {
        return $this->belongsTo('App\Hostel');
    }

    public function creator()
    {
        return $this->belongsTo('App\User','creator_id');
    }


    public function scopeGetByHostelId($query, $hostel_id= null)
    {
        if (!empty($hostel_id)) {
            if ( is_array($hostel_id) ) {
                $query->whereIn(with(new HostelInquery)->getTable().'.hostel_id', $hostel_id);
            } else {
                $query->where(with(new HostelInquery)->getTable().'.hostel_id', $hostel_id);
            }
        }
        return $query;
    }

    public function scopeGetByStatus($query, $status= null)
    {
        if (!empty($status)) {
            if ( is_array($status) ) {
                $query->whereIn(with(new HostelInquery)->getTable().'.status', $status);
            } else {
                $query->where(with(new HostelInquery)->getTable().'.status', $status);
            }
        }
        return $query;
    }

    public static function getHostelInqueryValidationRulesArray($hostel_inquery_id = null): array
    {

        //CREATE TABLE `ad_hostel_inqueries` (
//`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
//`hostel_id` INT(11) NOT NULL,
              //`email` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
                //`full_name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_unicode_ci',
             //`phone` VARCHAR(16) NOT NULL COLLATE 'utf8mb4_unicode_ci',
             //`info` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
        //`start_date` DATE NOT NULL,
        //`end_date` DATE NULL DEFAULT NULL,
        //`request_callback` ENUM('Y','N') NOT NULL DEFAULT 'N' COLLATE 'utf8mb4_unicode_ci',
        //`status` ENUM('N','E','P','C') NOT NULL DEFAULT 'N' COLLATE 'utf8mb4_unicode_ci',
//`created_at` DATETIME NOT NULL,

        $validationRulesArray = [
            'full_name'          => [
                'required',
                'string',
                'max:100',
            ],
            'email'                => 'required|max:50|email',
            'phone'                => 'required|max:255',
            'website'              => 'required|max:100',
            'info'           => 'required',
            'start_date'           => 'required',
            'request_callback'     => 'required|in:' . with(new HostelInquery)->getValueLabelKeys(HostelInquery::getRequestCallbackValueArray(false)),
            'status'               => 'required|in:' . with(new HostelInquery)->getValueLabelKeys(HostelInquery::getHostelInqueryStatusValueArray(false)),
        ];
        return $validationRulesArray;
    }


}
