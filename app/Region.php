<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class Region extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'regions';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'state_id', 'name', 'active' ];


    private static $RegionActiveLabelValueArray = Array('A' => 'Active', 'I' => 'Inactive');
    public static function getRegionActiveValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$RegionActiveLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }
    public static function getRegionActiveLabel(string $active): string
    {
        if ( ! empty(self::$RegionActiveLabelValueArray[$active])) {
            return self::$RegionActiveLabelValueArray[$active];
        }

        return self::$RegionActiveLabelValueArray[0];
    }



    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function scopeGetByActive($query, $active= null)
    {
        if (!empty($active)) {
            $query->where(with(new Region)->getTable().'.active', $active);
        }
        return $query;
    }

    public function scopeGetByName($query, $name= null, $partial= false)
    {
        if (empty($name)) return $query;
        return $query->where(with(new Region)->getTable().'.name', (!$partial?'=':'like'), ($partial?'%':''). $name .($partial?'%':'') );
    }


    public function scopeGetByStateId($query, $state_id= null)
    {
        if (!empty($state_id)) {
            if ( is_array($state_id) ) {
                $query->whereIn(with(new Region)->getTable().'.state_id', $state_id);
            } else {
                $query->where(with(new Region)->getTable().'.state_id', $state_id);
            }
        }
        return $query;
    }

    public static function getSimilarRegionByName( string $name, int $state_id, int $id= null, $return_count= false )
    {
        $quoteModel = Region::where( 'name', $name );
        $quoteModel = $quoteModel->where( 'state_id', '=' , $state_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }

        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getRegionsSelectionArray($state_id= '', $active= '', $order_by= 'name') :array {
        $regions = Region
            ::getByStateId($state_id)
            ->getByActive($active)
            ->orderBy($order_by,'asc')
            ->get();
        $regionsSelectionArray= [];
        foreach( $regions as $nextRegion ) {
            $regionsSelectionArray[]= (object)[ 'id'=> $nextRegion->id, 'state_id'=> $nextRegion->state_id, 'name'=>$nextRegion->name, 'slug'=>$nextRegion->slug , 'active'=>$nextRegion->active ];
        }
        return $regionsSelectionArray;
    }

}
