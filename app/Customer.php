<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\Rule;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;


class Customer extends User //MyAppModel
{
    use FuncsTrait;

    protected $table = 'users';
    protected $primaryKey = 'id';
//    protected $primaryKey = 'users.id';
    public $timestamps = false;

    protected $fillable = [
        'username',
        'account_type',
	    'email',
	    'status',
	    'phone',
	    'website',
	    'notes',
        'created_at',
        'updated_at'
    ];

    protected $pageContentAvatarPropsArray = [];
    protected $avatar_filename_max_length = 100;

    private static $customerAccountTypeLabelValueArray = Array('I' => 'Individual', 'B' => 'Business');
    private static $customerStatusLabelValueArray = Array('A' => 'Active', 'I' => 'Inactive', 'N' => 'New');

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('RoleCustomer', function (Builder $builder) {
            $builder->leftJoin( 'users_groups', 'users_groups.user_id', '=', 'users.id')
            ->select( with(new Customer)->getTable().'.*')
                ->where( 'users_groups.group_id', ACCESS_ROLE_CUSTOMER );

        });
    }

    public static function getCustomerAccountTypeValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$customerAccountTypeLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }

    public static function getCustomerAccountTypeLabel(string $account_type): string
    {
        if ( ! empty(self::$customerAccountTypeLabelValueArray[$account_type])) {
            return self::$customerAccountTypeLabelValueArray[$account_type];
        }

        return '';
    }

    public static function getCustomerStatusValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$customerStatusLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getCustomerStatusLabel(string $status): string
    {
        if ( ! empty(self::$customerStatusLabelValueArray[$status])) {
            return self::$customerStatusLabelValueArray[$status];
        }
        return self::$customerStatusLabelValueArray[0];
    }


    public static function getSkinsArray($key_return = true): array
    {
        $skinsArray=  \Config::get('app.skinsArray',[]);

        $resArray = [];
        foreach ($skinsArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getSkinsLabel(string $skin): string
    {
        if ( ! empty($skinsArray[$skin])) {
            return $skinsArray[$skin];
        }
        return !empty($skinsArray[0]) ? $skinsArray[0] : '';
    }

//

    public function scopeGetById($query, $id= null)
    {
        if (empty($id)) {
            return $query;
        }
        return $query->where(with(new Customer)->getTable().'.id', $id);
    }


    public function scopeGetByUsername($query, $username = null, $partial = false)
    {
        if (empty($username)) {
            return $query;
        }
        return $query->where(with(new Customer)->getTable() . '.username', (! $partial ? '=' : 'like'), ($partial ? '%' : '') . $username . ($partial ? '%' : ''));
    }

    public function scopeGetByStatus($query, $status = null)
    {
        if (empty($status)) {
            return $query;
        }
        return $query->where(with(new Customer)->getTable().'.status', $status);
    }


    public static function getCustomerValidationRulesArray($user_id= null, $skipFieldsArray= []) : array
    {

        $valid_nice_name_format= config('app.valid_nice_name_format');
        $additional_check_nice_name_validation_rule= 'regex:'.$valid_nice_name_format;
        $validationRulesArray = [
            'username' => [
                'required',
                'string',
                'max:100',
                Rule::unique(with(new User)->getTable())->ignore($user_id),
            ],
            'email' => [
                'required',
                'string',
                'max:100',
                Rule::unique(with(new User)->getTable())->ignore($user_id),
            ],
//            'status'          => 'required|in:' . with(new MyAppModel)->getValueLabelKeys(Customer::getCustomerStatusValueArray(false)),
            'first_name'      => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'last_name'       => 'nullable|max:50|'.$additional_check_nice_name_validation_rule,
            'phone'           => 'nullable|max:255',
            'website'         => 'nullable|max:100',
            'notes'           => 'nullable',
        ];
        foreach( $skipFieldsArray as $next_field_name ) {
            unset($validationRulesArray[$next_field_name]);
        }
//        with(new MyAppModel)->debToFile(print_r( $validationRulesArray,true),'  getCustomerValidationRulesArray $validationRulesArray::');
        return $validationRulesArray;
    } // public static function getCustomerValidationRulesArray($user_id) : array


    public static function getCustomersSelectionArray() :array {
        $customers = Customer::orderBy('username','asc')->get();
        $customersSelectionArray= [];
        foreach( $customers as $nextCustomer ) {
            $customersSelectionArray[$nextCustomer->id]= $nextCustomer->username;
        }
        return $customersSelectionArray;
    }

    public function getAvatarFilenameMaxLength(): int
    {
        return $this->avatar_filename_max_length;
    }

    /* get additional properties of cms_item image : path, url, size etc... */
    public function getCustomerAvatarPropsAttribute(): array
    {
        return $this->pageContentAvatarPropsArray;
    }


    /* set additional properties of cms_item image : path, url, size etc... */
    public function setCustomerAvatarPropsAttribute(array $pageContentAvatarPropsArray)
    {
        $this->pageContentAvatarPropsArray = $pageContentAvatarPropsArray;
    }



    /* check if provided source_url is unique for page_contents.source_url field */
    public static function getSimilarCustomerBySourceUrl( string $source_url, int $id= null, bool $return_count = false )
    {
        $quoteModel = Customer::where( 'source_url', $source_url );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }



}
