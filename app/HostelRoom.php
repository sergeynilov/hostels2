<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class HostelRoom extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'hostel_rooms';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'hostel_id', 'type', 'price', 'is_dorm', 'is_private'];
    private static $Hostel_RoomIs_DormValueArray = Array('Y' => 'Dorm', 'N' => 'Not Dorm');
    private static $Hostel_RoomIs_PrivateValueArray = Array('Y' => 'Private', 'N' => 'Not Private');
    private static $Hostel_RoomTypeLabelValueArray = [ ['key'=>1, 'label' => 'Private Room', 'is_dorm' => true, 'is_private' => false, 'is_required' => true],
                                                    ['key'=>4, 'label' => '4 Person Room', 'is_dorm' => false, 'is_private' => true, 'is_required' => true],
                                                    ['key'=>6, 'label' => '6 Person Room', 'is_dorm' => false, 'is_private' => false, 'is_required' => false],
                                                    ['key'=>8, 'label' => '8 Person Room', 'is_dorm' => false, 'is_private' => false, 'is_required' => false],
                                                    ['key'=>10, 'label' => '10 Person Room', 'is_dorm' => false, 'is_private' => false, 'is_required' => false],
                                                    ['key'=>12, 'label' => '12+ Person Room', 'is_dorm' => false, 'is_private' => false, 'is_required' => false]
];
    public static function getHostel_RoomIs_DormValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$Hostel_RoomIs_DormValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getHostel_RoomIs_DormLabel(string $is_dorm): string
    {
        if ( ! empty(self::$Hostel_RoomIs_DormValueArray[$is_dorm])) {
            return self::$Hostel_RoomIs_DormValueArray[$is_dorm];
        }
        return self::$Hostel_RoomIs_DormValueArray[0];
    }

/*    public static function getHostel_RoomIs_DormValueArray()
    {
        $ResArray = [];
        foreach ( with(new HostelRoom)->Hostel_RoomIs_DormValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }

    public static function getHostel_RoomIs_DormLabel($Type)
    {
        if (!empty(with(new HostelRoom)->Hostel_RoomIs_DormValueArray[$Type])) {
            return with(new HostelRoom)->Hostel_RoomIs_DormValueArray[$Type];
        }
        return '';
    }*/




    public static function getHostel_RoomIs_PrivateValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$Hostel_RoomIs_PrivateValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getHostel_RoomIs_PrivateLabel(string $is_private): string
    {
        if ( ! empty(self::$Hostel_RoomIs_PrivateValueArray[$is_private])) {
            return self::$Hostel_RoomIs_PrivateValueArray[$is_private];
        }
        return self::$Hostel_RoomIs_PrivateValueArray[0];
    }




/*    public static function getHostel_RoomTypeValueArray()
    {
        $ResArray = [];
        foreach (with(new HostelRoom)->Hostel_RoomTypeLabelValueArray as $Key => $Value) {
            $ResArray[] = array('key' => $Key, 'value' => $Value);
        }
        return $ResArray;
    }

    public static function getHostel_RoomTypeLabel($Type)
    {
        if (!empty(with(new HostelRoom)->Hostel_RoomTypeLabelValueArray[$Type])) {
            return with(new HostelRoom)->Hostel_RoomTypeLabelValueArray[$Type];
        }
        return '';
    }
  */
    public static function getHostel_RoomTypeValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$Hostel_RoomTypeLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $value['key'], 'label' => $value['label']];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getHostel_RoomTypeLabel(string $type): string
    {
        if ( ! empty(self::$Hostel_RoomTypeLabelValueArray[$type])) {
            return self::$Hostel_RoomTypeLabelValueArray[$type];
        }
        return self::$Hostel_RoomTypeLabelValueArray[0];
    }

    public function scopeGetByType($query, $type= null)
    {
        if (!empty($type)) {
            if ( is_array($type) ) {
                $query->whereIn(with(new HostelRoom)->getTable().'.type', $type);
            } else {
                $query->where(with(new HostelRoom)->getTable().'.type', $type);
            }
        }
        return $query;
    }


    public function hostel()
    {
        return $this->belongsTo('App\Hostel');
    }
    public function scopeGetByHostelId($query, $hostel_id= null)
    {
        if (!empty($hostel_id)) {
            if ( is_array($hostel_id) ) {
                $query->whereIn(with(new HostelRoom)->getTable().'.hostel_id', $hostel_id);
            } else {
                $query->where(with(new HostelRoom)->getTable().'.hostel_id', $hostel_id);
            }
        }
        return $query;
    }

/*    public function scopeGetByPriceFrom($query, $price= null)
    {
        if (!empty($price)) {
            $query->where(with(new HostelRoom)->getTable().'.price >= ', $price);
//            $query->where(with(new HostelRoom)->getTable().'.hostel_id', $hostel_id);
        }
        return $query;
    }

    public function scopeGetByPriceTill($query, $price= null)
    {
        if (!empty($price)) {
            $query->where(with(new HostelRoom)->getTable().'.price < ', $price);
//            $query->where(with(new HostelRoom)->getTable().'.hostel_id', $hostel_id);
        }
        return $query;
    }*/

    /*            if (!empty($filtersArray['sale_price_from'])) {
            $this->db->where($this->m_product_table . '.sale_price >= ' . $filtersArray['sale_price_from']);
        }
        if (!empty($filtersArray['sale_price_till'])) {
            $this->db->where($this->m_product_table . '.sale_price <= ' . $filtersArray['sale_price_till']);
        }
*/

/*    public static function getHostelValidationRulesArray($hostel_id= null) : array
    {
        $validationRulesArray = [
            'name' => [
                'required',
                'string',
                'max:50',
                Rule::unique(with(new Hostel)->getTable())->ignore($hostel_id),
            ],
            'descr'=>'required|max:255',
            'logo'=>'nullable|max:50',
        ];
        return $validationRulesArray;
    }*/


}
