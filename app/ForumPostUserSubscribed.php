<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;
use Illuminate\Validation\Rule;


class ForumPostUserSubscribed extends MyAppModel
{
    use FuncsTrait;

    protected $table = 'forum_post_user_subscribed';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [ 'user_id', 'forum_post_id' ];


    protected $casts = [
    ];

    protected static function boot() {
        parent::boot();
    }



    public static function getForumPostUserSubscribedValidationRulesArray() : array
    {
        $validationRulesArray = [
            'user_id'            => 'required|exists:'.( with(new User)->getTable() ).',id',
            'forum_post_id'      => 'required|exists:'.( with(new forumPost)->getTable() ).',id',
        ];
        return $validationRulesArray;
    }

    public function scopeGetByForumPostId($query, $forum_post_id= null)
    {
        if (!empty($forum_post_id)) {
            if ( is_array($forum_post_id) ) {
                $query->whereIn(with(new ForumPostUserSubscribed)->getTable().'.forum_post_id', $forum_post_id);
            } else {
                $query->where(with(new ForumPostUserSubscribed)->getTable().'.forum_post_id', $forum_post_id);
            }
        }
        return $query;
    }

}
