<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use DB;
use App\MyAppModel;
use Carbon\Carbon;
use Config;

use Illuminate\Support\Facades\File;
//use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\FuncsTrait;
use App\library\CheckValueType;

class News extends MyAppModel
{
    use Notifiable;
//    use Sluggable;
    use FuncsTrait;

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $table = 'news';

    protected $newsImagePropsArray = [];
    protected $img_filename_max_length = 100;

    protected static $uploads_news_dir = 'news/-news-';
    // file:///_wwwroot/lar/hostels2/storage/app/public/news/-news-14/1022319138.jpg
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'slug',
        'content',
        'content_shortly',
        'creator_id',
        'is_featured',
        'is_homepage',
        'is_top',
        'published',
        'image',
        'source_type',
        'source_url',  'updated_at'
    ];


    private static $newsIsFeaturedLabelValueArray = Array('1' => 'Is Featured', '0' => 'Is Not Featured');
    private static $newsIsHomepageLabelValueArray = Array('1' => 'Is Homepage', '0' => 'Is Not Homepage');
    private static $newsIsTopLabelValueArray = Array('1' => 'Is Top', '0' => 'Is Not Top');
    private static $newsPublishedLabelValueArray = Array('1' => 'Is Published', '0' => 'Is Not Published');

    protected $casts = [
    ];

//    public function newsImages()
//    {
//        return $this->hasMany('App\NewsImage');
//    }

    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($news) {
            $new_image_path = News::getNewsImagePath($news->id, $news->image, true);
            News::deleteFileByPath($new_image_path, true);

//            foreach ( $news->newsImages()->get() as $nextNewsImages ) {
//                $nextNewsImages->delete();
//            }
        });
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
//    public function sluggable()
//    {
//        return [
//            'slug' => [
//                'source' => 'title'
//            ]
//        ];
//    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'creator_id');
    }


    public function scopeGetByTitle($query, $title = null, $partial = false)
    {
        if (empty($title)) {
            return $query;
        }

        return $query->where(with(new News)->getTable() . '.title', (! $partial ? '=' : 'like'), ($partial ? '%' : '') . $title . ($partial ? '%' : ''));
    }


    public function scopeGetByPublished($query, $published = null)
    {
        if ( ! isset($published) or strlen($published) == 0) {
            return $query;
        }

        return $query->where(with(new News)->getTable() . '.published', $published);
    }

    public function scopeGetByIsFeatured($query, $is_featured = null)
    {
        if ( ! isset($is_featured) or strlen($is_featured) == 0) {
            return $query;
        }

        return $query->where(with(new News)->getTable() . '.is_featured', $is_featured);
    }

    public function scopeGetByIsHomepage($query, $is_homepage = null)
    {
        if ( ! isset($is_homepage) or strlen($is_homepage) == 0) {
            return $query;
        }

        return $query->where(with(new News)->getTable() . '.is_homepage', $is_homepage);
    }

    public function scopeGetByIsTop($query, $is_top = null)
    {
        if ( ! isset($is_top) or strlen($is_top) == 0) {
            return $query;
        }

        return $query->where(with(new News)->getTable() . '.is_top', $is_top);
    }


    public function scopeGetByCreatorId($query, $creator_id = null)
    {
        if ( ! empty($creator_id)) {
            if (is_array($creator_id)) {
                $query->whereIn(with(new News)->getTable() . '.creator_id', $creator_id);
            } else {
                $query->where(with(new News)->getTable() . '.creator_id', $creator_id);
            }
        }

        return $query;
    }

    public function scopeGetBySlug($query, $slug = null)
    {
        if (empty($slug)) {
            return $query;
        }

        return $query->where(with(new News)->getTable() . '.slug', $slug);
    }

    public static function getNewsIsFeaturedValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$newsIsFeaturedLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }

    public static function getNewsIsFeaturedLabel(string $is_featured): string
    {
        if ( ! empty(self::$newsIsFeaturedLabelValueArray[$is_featured])) {
            return self::$newsIsFeaturedLabelValueArray[$is_featured];
        }

        return self::$newsIsFeaturedLabelValueArray[0];
    }


    public static function getNewsIsHomepageValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$newsIsHomepageLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }

    public static function getNewsIsHomepageLabel(string $is_homepage): string
    {
        if ( ! empty(self::$newsIsHomepageLabelValueArray[$is_homepage])) {
            return self::$newsIsHomepageLabelValueArray[$is_homepage];
        }

        return self::$newsIsHomepageLabelValueArray[0];
    }


    public static function getNewsIsTopValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$newsIsTopLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }

    public static function getNewsIsTopLabel(string $is_top): string
    {
        if ( ! empty(self::$newsIsTopLabelValueArray[$is_top])) {
            return self::$newsIsTopLabelValueArray[$is_top];
        }

        return self::$newsIsTopLabelValueArray[0];
    }


    public static function getNewsPublishedValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$newsPublishedLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }

    public static function getNewsPublishedLabel(string $published): string
    {
        if ( ! empty(self::$newsPublishedLabelValueArray[$published])) {
            return self::$newsPublishedLabelValueArray[$published];
        }

        return self::$newsPublishedLabelValueArray[0];
    }

    public function getImgFilenameMaxLength(): int
    {
        return $this->img_filename_max_length;
    }


    public static function getNewsDir(int $news_id): string
    {
        return self::$uploads_news_dir . $news_id . '/';
    }

    public static function getNewsImagePath(int $news_id, $image): string
    {
        if (empty($image)) {
            return '';
        }

        return self::$uploads_news_dir . $news_id . '/' . $image;
    }


    public static function setNewsImageProps(int $news_id, string $image = null, bool $skip_non_existing_file = false): array
    {
//        echo '<pre>$news_id::'.print_r($news_id,true).'</pre>';
//        echo '<pre>setNewsImageProps  $image::'.print_r($image,true).'</pre>';
//        echo '<pre>$skip_non_existing_file::'.print_r($skip_non_existing_file,true).'</pre>';
        if (empty($image) and $skip_non_existing_file) {
            return [];
        }


        $dir_path       = self::$uploads_news_dir . '' . $news_id . '';
        $file_full_path = /*'/app/'.*/
            $dir_path . '/' . $image;
//        echo '<pre>!!!!$file_full_path::'.print_r($file_full_path,true).'</pre>';
        $file_exists = ( ! empty($image) and Storage::disk('local')->exists('public/' . $file_full_path));

//        echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
        if ( ! $file_exists) {
            if ($skip_non_existing_file) {
                return [];
            }
            $file_full_path = config('app.empty_img_url');       //file:///_wwwroot/lar/votes/public/images/emptyImg.png
//            echo '<pre>++ $file_full_path::'.print_r($file_full_path,true).'</pre>';
            $image = with(new News)->getFilenameBasename($file_full_path);
        }

        $image_path = $file_full_path;
        if ($file_exists) {
            $imagePropsArray = ['image' => $image, 'image_path' => $image_path, 'image_url' => Storage::url($file_full_path)];
            $image_full_path = ($image_path);
//            echo '<pre>-1 $image_full_path::'.print_r($image_full_path,true).'</pre>';
            $newsImgProps = with(new News)->getCFImageProps(base_path() . '/storage/app/public/' . $image_full_path, $imagePropsArray);
        } else {
            $newsImgProps = ['image' => $image, 'image_path' => $image_path, 'image_url' => ($file_full_path)];
        }


//        echo '<pre>$newsImgProps::'.print_r($newsImgProps,true).'</pre>';
        return $newsImgProps;

    }
    /*    public static function setNewsImageProps(int $news_id, string $image = null, bool $skip_non_existing_file = false): array
        {
            if (empty($image) and $skip_non_existing_file) {
                return [];
            }

            $dir_path = self::$uploads_news_dir . '' . $news_id . '';
            $file_full_path = $dir_path . '/' . $image;
    //        echo '<pre>$file_full_paths::'.print_r($file_full_path,true).'</pre>';
            $file_exists    = ( !empty($image) and Storage::disk('local')->exists('public/' . $file_full_path) );

            if ( ! $file_exists) {
                if ($skip_non_existing_file) {
                    return [];
                }
                $file_full_path = //  'public' .
                    config('app.empty_img_url');
                $image          = with(new News)->getFilenameBasename($file_full_path);
            }

            $image_path = $file_full_path;
            if ($file_exists) {
    //            $image_url       = 'storage/app/' . $dir_path . '/' . $image;
                // echo '<pre>storage_path( $dir_path . \'/\' . $image )::'.print_r(storage_path( $dir_path . '/' . $image ),true).'</pre>';
                $imagePropsArray = ['image' => $image, 'image_path' => $image_path, 'image_url' => storage_path( $dir_path . '/' . $image ) ];
                $image_full_path = base_path() . '/storage/app/public/' . $image_path;
            } else {
                $imagePropsArray = ['image' => $image, 'image_path' => $image_path, 'image_url' => $file_full_path];
                $image_full_path = base_path() . '/public/' . $image_path;
            }

            if ( ! empty($previewSizeArray['width'])) {
                $imagePropsArray['preview_width']  = $previewSizeArray['width'];
                $imagePropsArray['preview_height'] = $previewSizeArray['height'];
            }
            $newsImgProps = with(new News)->getCFImageProps($image_full_path, $imagePropsArray);

            return $newsImgProps;

        }*/

    /* get additional properties of cms_item image : path, url, size etc... */
    public function getNewsImagePropsAttribute(): array
    {
        return $this->newsImagePropsArray;
    }


    /* set additional properties of cms_item image : path, url, size etc... */
    public function setNewsImagePropsAttribute(array $newsImagePropsArray)
    {
        $this->newsImagePropsArray = $newsImagePropsArray;
    }


    /* check if provided source_url is unique for news.source_url field */
    public static function getSimilarNewsBySourceUrl(string $source_url, int $id = null, bool $return_count = false)
    {
        $quoteModel = News::where('source_url', $source_url);
        if ( ! empty($id)) {
            $quoteModel = $quoteModel->where('id', '!=', $id);
        }
        if ($return_count) {
            return $quoteModel->get()->count();
        }
        $retRow = $quoteModel->get();
        if (empty($retRow[0])) {
            return false;
        }

        return $retRow[0];
    }


    public static function getValidationRulesArray($news_id = null): array
    {
        $validationRulesArray = [
            'title' => [
                'required',
                'string',
                'max:255',
                Rule::unique(with(new News)->getTable())->ignore($news_id),
            ],

            'content'         => 'required',
            'content_shortly' => 'max:255',
            'is_featured'     => 'required|in:' . with(new News)->getValueLabelKeys(News::getNewsIsFeaturedValueArray(false)),
            'is_homepage'     => 'required|in:' . with(new News)->getValueLabelKeys(News::getNewsIsHomepageValueArray(false)),
            'is_top'          => 'required|in:' . with(new News)->getValueLabelKeys(News::getNewsIsTopValueArray(false)),
            'published'       => 'required|in:' . with(new News)->getValueLabelKeys(News::getNewsPublishedValueArray(false)),

            'source_type' => 'max:20',
            'source_url'  => 'max:255',
        ];

        return $validationRulesArray;
    }


}
