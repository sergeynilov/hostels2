<?php

namespace App;
use DB;
use Cviebrock\EloquentSluggable\Sluggable;

use App\MyAppModel;
use App\Http\Traits\FuncsTrait;
use Illuminate\Validation\Rule;

class Hostel extends MyAppModel
{
    use FuncsTrait;
    use Sluggable;
    protected $table      = 'hostels';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'uid', 'user_id', 'region_id', 'subregion_id', 'state_id', 'town', 'street_addr', 'postal_code', 'phone', 'email', 'name', 'feature', 'slug', 'short_descr', 'descr',
	'distance',	'website', 'is_priority_listing', 'is_homepage_spotlight', 'book_now_link',	'rating', 'status',	'payment_log_id', 'payment_expired_at',	'next_payment_at',
	'subscription_id', 'payment_gateway', 'view_count',	'about_us',	'cancellation_policy', 'must_read_information', 'updated_at' ];

    private static $hostelStatusLabelValueArray = ['A' => 'Active', 'I' => 'Inactive', 'N' => 'New'];
    private static $hostelFeatureLabelValueArray = ['F' => 'Feature', 'S' => 'Not Feature'];
    private static $hostelsSearchByFieldsArray = ['name' => 'Name', 'state_name' => 'State', 'region_name' => 'Region', 'price' => 'Price', 'rating' => 'Rating',
                                                  'feature' => 'Feature', ];
    /*      name
     state_id
    region_id
    price
    rating
    feature
 */

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public static function getHostelStatusValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$hostelStatusLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }

        return $resArray;
    }
    public static function getHostelStatusLabel(string $status): string
    {
        if ( ! empty(self::$hostelStatusLabelValueArray[$status])) {
            return self::$hostelStatusLabelValueArray[$status];
        }

        return self::$hostelStatusLabelValueArray[0];
    }


    public static function getHostelFeatureValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$hostelFeatureLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getHostelFeatureLabel(string $feature): string
    {
        if ( ! empty(self::$hostelFeatureLabelValueArray[$feature])) {
            return self::$hostelFeatureLabelValueArray[$feature];
        }

        return self::$hostelFeatureLabelValueArray[0];
    }




    public static function getHostelsSearchByFieldsValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$hostelsSearchByFieldsArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }
    public static function getHostelsSearchByFieldsLabel(string $feature): string
    {
        if ( ! empty(self::$hostelsSearchByFieldsArray[$feature])) {
            return self::$hostelsSearchByFieldsArray[$feature];
        }

        return self::$hostelsSearchByFieldsArray[0];
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function scopeGetByUserId($query, $user_id= null)
    {
        if (!empty($user_id)) {
            if ( is_array($user_id) ) {
                $query->whereIn(with(new Hostel)->getTable().'.user_id', $user_id);
            } else {
                $query->where(with(new Hostel)->getTable().'.user_id', $user_id);
            }
        }
        return $query;
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }
    public function scopeGetByRegionId($query, $region_id= null)
    {
        if (!empty($region_id)) {
            if ( is_array($region_id) ) {
                $query->whereIn(with(new Hostel)->getTable().'.region_id', $region_id);
            } else {
                $query->where(with(new Hostel)->getTable().'.region_id', $region_id);
            }
        }
        return $query;
    }

    public function subregion()
    {
        return $this->belongsTo('App\Subregion');
    }
    public function scopeGetBySubregionId($query, $subregion_id= null)
    {
        if (!empty($subregion_id)) {
            if ( is_array($subregion_id) ) {
                $query->whereIn(with(new Hostel)->getTable().'.subregion_id', $subregion_id);
            } else {
                $query->where(with(new Hostel)->getTable().'.subregion_id', $subregion_id);
            }
        }
        return $query;
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }
    public function scopeGetByStateId($query, $state_id= null)
    {
        if (!empty($state_id)) {
            if ( is_array($state_id) ) {
                $query->whereIn(with(new Hostel)->getTable().'.state_id', $state_id);
            } else {
                $query->where(with(new Hostel)->getTable().'.state_id', $state_id);
            }
        }
        return $query;
    }


    public function scopeGetByName($query, $name = null, $partial = false)
    {
        if (empty($name)) {
            return $query;
        }
        return $query->where(with(new Hostel)->getTable() . '.name', (! $partial ? '=' : 'like'), ($partial ? '%' : '') . $name . ($partial ? '%' : ''));
    }

    public function scopeGetBySlug($query, $slug = null)
    {
        if (empty($slug)) {
            return $query;
        }
        return $query->where(with(new Hostel)->getTable() . '.slug',  $slug);
    }


    public function scopeGetByStatus($query, $status= null)
    {
        if (!empty($status)) {
            if ( is_array($status) ) {
                $query->whereIn(with(new Hostel)->getTable().'.status', $status);
            } else {
                $query->where(with(new Hostel)->getTable().'.status', $status);
            }
        }
        return $query;
    }

    public function scopeGetByFeature($query, $feature= null)
    {
        if (!empty($feature)) {
            if ( is_array($feature) ) {
                $query->whereIn(with(new Hostel)->getTable().'.feature', $feature);
            } else {
                $query->where(with(new Hostel)->getTable().'.feature', $feature);
            }
        }
        return $query;
    }


    public function scopeNewHostelInqueriesCount($query, bool $new_hostel_inqueries_count= false)
    {
        if ( $new_hostel_inqueries_count ) {
            $query->havingRaw('new_hostel_inqueries_count > 0');
        }
        return $query;
    }

    public function scopeAcceptedHostelInqueriesCount($query, bool $accepted_hostel_inqueries_count= false)
    {
        if ( $accepted_hostel_inqueries_count ) {
            $query->havingRaw('accepted_hostel_inqueries_count > 0');
        }
        return $query;
    }

    public static function getSimilarHostelByName( string $name, int $state_id, int $id= null, $return_count= false )
    {
        $quoteModel = Hostel::where( 'name', $name );
        $quoteModel = $quoteModel->where( 'state_id', '=' , $state_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }

        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    /*     public function scopeGetByForumId($query, $forum_id = null, $alias= '')
    {
        if (empty($forum_id)) {
            return $query;
        }
        if (empty($alias)) {
            $alias= with(new ForumThread)->getTable();
        }
        return $query->where( $alias.'.forum_id', $forum_id );
    }
 */
    public function scopeGetByPriceFrom($query, $price= null, $alias= '')
    {
        if (empty($price)) {
            return $query;
        }
        if (empty($alias)) {
            $alias= with(new HostelRoom)->getTable();
        }
        return $query->whereRaw( $alias.'.price >= '.$price );
    }

    public function scopeGetByPriceTill($query, $price= null, $alias= '')
    {
        if (empty($price)) {
            return $query;
        }
        if (empty($alias)) {
            $alias= with(new HostelRoom)->getTable();
        }
        return $query->whereRaw( $alias.'.price <= '.$price );
    }


    public function scopeGetByRatingFrom($query, $rating= null, $alias= '')
    {
        if (empty($rating)) {
            return $query;
        }
        if (empty($alias)) {
            $alias= with(new Hostel)->getTable();
        }
        return $query->whereRaw( $alias.'.rating >= '.$rating );
    }

    public function scopeGetByRatingTill($query, $rating= null, $alias= '')
    {
        if (empty($rating)) {
            return $query;
        }
        if (empty($alias)) {
            $alias= with(new Hostel)->getTable();
        }
        return $query->whereRaw( $alias.'.rating <= '.$rating );
    }

    public function scopeGetByFacilityId($query, $facility_id= null, $alias= '')
    {
        if (empty($facility_id)) {
            return $query;
        }

        if (empty($alias)) {
            $alias= with(new HostelFacility())->getTable();
        }

        if (!empty($facility_id)) {
            if ( is_array($facility_id) ) {
                $query->whereIn($alias.'.facility_id', $facility_id);
            } else {
                $query->where($alias.'.facility_id', $facility_id);
            }
        }
        return $query;
    }


    public static function getHostelsSelectionArray($state_id= '', $order_by= 'name') :array {
        $hostels = Hostel
            ::getByStateId($state_id)
            ->orderBy($order_by,'asc')
            ->get();
        $hostelsSelectionArray= [];
        foreach( $hostels as $nextHostel ) {
            $hostelsSelectionArray[]= (object)[ 'id'=> $nextHostel->id, 'state_id'=> $nextHostel->state_id, 'name'=>$nextHostel->name, 'slug'=>$nextHostel->slug ];
        }
        return $hostelsSelectionArray;
    }

    /*
     *
     *
     name
     state_id
    region_id
    price
    rating
    feature
     *  CREATE TABLE `hostels` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`uid` VARCHAR(128) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`user_id` INT(11) NOT NULL,
	`region_id` INT(11) NULL DEFAULT NULL,
	`subregion_id` INT(11) NULL DEFAULT NULL,
	`state_id` INT(11) NOT NULL,
	`town` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`street_addr` VARCHAR(150) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`postal_code` VARCHAR(4) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`phone` VARCHAR(16) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`email` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`feature` ENUM('F','S') NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`slug` VARCHAR(150) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`short_descr` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`descr` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`distance` INT(11) NULL DEFAULT NULL,
	`website` VARCHAR(150) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`is_priority_listing` ENUM('Y','N') NOT NULL DEFAULT 'N' COLLATE 'utf8mb4_unicode_ci',
	`is_homepage_spotlight` ENUM('Y','N') NOT NULL DEFAULT 'N' COLLATE 'utf8mb4_unicode_ci',
	`book_now_link` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`rating` INT(11) NULL DEFAULT NULL,
	`status` ENUM('A','I','N') NOT NULL DEFAULT 'N' COLLATE 'utf8mb4_unicode_ci',
	`payment_log_id` VARCHAR(45) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`payment_expired_at` DATETIME NULL DEFAULT NULL,
	`next_payment_at` DATE NULL DEFAULT NULL,
	`subscription_id` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`payment_gateway` ENUM('P','E') NULL DEFAULT 'E' COLLATE 'utf8mb4_unicode_ci',
	`view_count` INT(11) NOT NULL DEFAULT '0',
	`about_us` TEXT NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`cancellation_policy` TEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`must_read_information` TEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`created_at` TIMESTAMP NULL DEFAULT NULL,
	`updated_at` TIMESTAMP NULL DEFAULT NULL,
	`modified_by` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name_UNIQUE` (`name`),
	UNIQUE INDEX `slug_UNIQUE` (`slug`),
	INDEX `fk_bp_status_postal_code_idx` (`status`, `postal_code`),
	INDEX `fk_bp_locations_idx` (`state_id`, `region_id`, `subregion_id`, `town`),
	INDEX `fk_bp_hostel_1_idx` (`user_id`),
	INDEX `fk_bp_hostel_1_idx2` (`region_id`),
	INDEX `fk_bp_hostel_subregion` (`subregion_id`),
	INDEX `fk_bp_hostel_2_idx` (`state_id`),
	INDEX `fk_bp_hostel_is_priority_listing` (`is_priority_listing`),
	INDEX `fk_bp_hostel_is_homepage_spotlight` (`is_homepage_spotlight`),
	INDEX `fk_bp_status_idx` (`status`),
	INDEX `fk_bp_hostel_subscription_id` (`subscription_id`),
	INDEX `fk_bp_hostel_1_idx1` (`modified_by`)
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
AUTO_INCREMENT=45
;
 */
    public static function getHostelValidationRulesArray($hostel_id= null) : array
    {
        $validationRulesArray = [
            'name' => [
                'required',
                'string',
                'max:50',
                Rule::unique(with(new Hostel)->getTable())->ignore($hostel_id),
            ],
            'short_descr'=>'required|max:255',
            'descr'=>'required',
//            'logo'=>'nullable|max:50',
        ];
        return $validationRulesArray;
    }

}
