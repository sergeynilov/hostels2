<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class PersonalHostelBookmark extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'personal_hostel_bookmarks';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'hostel_id', 'user_id' ];


    public function hostel()
    {
        return $this->belongsTo('App\Hostel');
    }
    public function scopeGetByHostelId($query, $hostel_id= null)
    {
        if (!empty($hostel_id)) {
            if ( is_array($hostel_id) ) {
                $query->whereIn(with(new PersonalHostelBookmark)->getTable().'.hostel_id', $hostel_id);
            } else {
                $query->where(with(new PersonalHostelBookmark)->getTable().'.hostel_id', $hostel_id);
            }
        }
        return $query;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function scopeGetByUserId($query, $user_id= null)
    {
        if (!empty($user_id)) {
            if ( is_array($user_id) ) {
                $query->whereIn(with(new PersonalHostelBookmark)->getTable().'.user_id', $user_id);
            } else {
                $query->where(with(new PersonalHostelBookmark)->getTable().'.user_id', $user_id);
            }
        }
        return $query;
    }

}
