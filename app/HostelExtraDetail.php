<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;
use Illuminate\Validation\Rule;


class HostelExtraDetail extends MyAppModel
{
    use FuncsTrait;

    protected $table = 'hostel_extra_details';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /* CREATE TABLE `ad_hostel_extra_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostel_id` int(11) NOT NULL,
  `num_beds` tinyint(3) unsigned DEFAULT NULL,
  `min_nights` tinyint(3) unsigned DEFAULT NULL,
  `max_nights` tinyint(3) unsigned DEFAULT NULL,
  `bedsheets` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `towels` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `parking` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `airport_train` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `luggage` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `coed_dorm` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `bathroom` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `credit_cards` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `nonsmoking_rooms` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `smoke_free` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `pets_allowed` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `service_animals` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `wheelchair` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `internet_computers` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `wireless_internet` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `checkout` enum('Y','N') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT ' Y=>Yes, N=>No',
  `reception_hours_start` int(11) NOT NULL DEFAULT '0',
  `reception_hours_end` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_bp_hostel_extra_details_hostel_id` (`hostel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
 */
    protected $fillable = [ 'name', 'descr', 'logo' ];


    protected $casts = [
    ];

    protected static function boot() {
        parent::boot();
        static::deleting(function($hostelExtraDetail) {
        });

    }



    public static function getHostelExtraDetailValidationRulesArray($hostelExtraDetail_id= null) : array
    {
        $validationRulesArray = [
//            'name' => [
//                'required',
//                'string',
//                'max:50',
//                Rule::unique(with(new HostelExtraDetail)->getTable())->ignore($hostelExtraDetail_id),
//            ],
//            'descr'=>'required|max:255',
//            'logo'=>'nullable|max:50',
        ];
        return $validationRulesArray;
    }

    public function scopeGetByHostelId($query, $hostel_id= null)
    {
        if (!empty($hostel_id)) {
            if ( is_array($hostel_id) ) {
                $query->whereIn(with(new HostelExtraDetail)->getTable().'.hostel_id', $hostel_id);
            } else {
                $query->where(with(new HostelExtraDetail)->getTable().'.hostel_id', $hostel_id);
            }
        }
        return $query;
    }


//    public static function getHostelExtraDetailsSelectionArray() {
//        $hostelExtraDetails = HostelExtraDetail
//            ::get()
//            ->first();
//        return $hostelExtraDetails;
//    }

}
