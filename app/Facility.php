<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;
use Illuminate\Validation\Rule;


class Facility extends MyAppModel
{
    use FuncsTrait;

    protected $table = 'facilities';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [ 'name', 'descr', 'logo' ];


    protected $casts = [
    ];

    protected static function boot() {
        parent::boot();
        static::deleting(function($facility) {
        });

    }

    public function scopeGetByName($query, $name= null, $partial= false)
    {
        if (empty($name)) return $query;
        return $query->where(with(new Facility)->getTable().'.name', (!$partial?'=':'like'), ($partial?'%':''). $name .($partial?'%':'') );
    }

    /* check if provided name is unique for facilities.name field */
    public static function getSimilarFacilityByName( string $name, int $id= null, bool $return_count = false )
    {
        $quoteModel = Facility::where( 'name', $name );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }
        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getFacilityValidationRulesArray($facility_id= null) : array
    {
        $validationRulesArray = [
            'name' => [
                'required',
                'string',
                'max:50',
                Rule::unique(with(new Facility)->getTable())->ignore($facility_id),
            ],
            'descr'=>'required|max:255',
            'logo'=>'nullable|max:50',
        ];
        return $validationRulesArray;
    }

    public static function getFacilitiesSelectionArray() :array {
        $facilities = Facility::orderBy('name','desc')->get();
        $facilitiesSelectionArray= [];
        foreach( $facilities as $nextFacility ) {
            $facilitiesSelectionArray[$nextFacility->id]= $nextFacility->name;
        }
        return $facilitiesSelectionArray;
    }

}
