<?php

namespace App;
use DB;
use App\MyAppModel;
use App\ForumPost;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Http\Traits\FuncsTrait;

class ForumThread extends MyAppModel
{
    use FuncsTrait;
    use Sluggable;
    protected $table      = 'forum_threads';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'title', 'creator_id', 'forum_id' , 'replies', 'is_salved', 'updated_at'  ];

    public function forum()
    {
        return $this->belongsTo('App\Forum', 'id');
    }

    public function latestForumPost()
    {
        return $this->hasOne('App\ForumPost')->latest();
    }

    public function forumPosts()
    {
        return $this->hasMany('App\ForumPost');

    }
    public function creator()
    {
        return $this->belongsTo('App\User','creator_id');
    }

    public function scopeGetByForumId($query, $forum_id= null)
    {
        if (!empty($forum_id)) {
            if ( is_array($forum_id) ) {
                $query->whereIn(with(new ForumThread)->getTable().'.forum_id', $forum_id);
            } else {
                $query->where(with(new ForumThread)->getTable().'.forum_id', $forum_id);
            }
        }
        return $query;
    }

    public function scopeGetByIsSalved($query, $is_salved= null)
    {
        if (!empty($is_salved)) {
            if ( is_array($is_salved) ) {
                $query->whereIn(with(new ForumThread)->getTable().'.is_salved', $is_salved);
            } else {
                $query->where(with(new ForumThread)->getTable().'.is_salved', $is_salved);
            }
        }
        return $query;
    }


    public function scopeGetBySlug($query, $slug = null)
    {
        if (empty($slug)) {
            return $query;
        }
        return $query->where(with(new ForumThread)->getTable() . '.slug',  $slug);
    }



    public static function getForumThreadValidationRulesArray($forum_thread_id = null): array
    {

        $validationRulesArray = [
            'title'          => [
                'required',
                'string',
                'max:255',
            ],
            'creator_id'   => 'nullable|integer|exists:' . (with(new User)->getTable()) . ',id',
            'forum_id'     => 'required|integer|exists:' . (with(new Forum)->getTable()) . ',id',
            'views'        => 'nullable|integer',
        ];
        return $validationRulesArray;
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


}
