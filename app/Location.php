<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class Location extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'location_data';
    protected $primaryKey = 'id';
    public $timestamps    = false;


    public function scopeGetByState($query, $state= null)
    {
        if (!empty($state)) {
            $query->where(with(new Location)->getTable().'.state', $state);
        }
        return $query;
    }

    public function scopeGetByRegion($query, $region= null)
    {
        if (!empty($region)) {
            $query->where(with(new Location)->getTable().'.region', $region);
        }
        return $query;
    }

    public function scopeGetBySubregion($query, $subregion= null)
    {
        if (!empty($subregion)) {
            $query->where(with(new Location)->getTable().'.subregion', $subregion);
        }
        return $query;
    }

    public function scopeGetBySuburb($query, $suburb= null)
    {
        if (!empty($suburb)) {
            $query->where(with(new Location)->getTable().'.suburb', $suburb);
        }
        return $query;
    }

    public function scopeGetByPostcode($query, $postcode= null)
    {
        if (!empty($postcode)) {
            $query->where(with(new Location)->getTable().'.postcode', $postcode);
        }
        return $query;
    }

}
