<?php

namespace App;
use DB;
use Cviebrock\EloquentSluggable\Sluggable;
use App\MyAppModel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\FuncsTrait;
use Illuminate\Validation\Rule;


class ForumCategory extends MyAppModel
{
    use FuncsTrait;
    use Sluggable;
    protected $table      = 'forum_categories';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected static $img_preview_width= 160;
    protected static $img_preview_height= 120;


    protected $forumCategoryPropsArray = [];
    protected $img_image_max_length = 255;

    protected static $uploads_forums_dir = 'forum_categories/-forum-category-';
    protected $fillable = [ 'title', 'description', 'creator_id' , 'published', 'image', 'updated_at' ];

    private static $forumCategoryPublishedLabelValueArray = Array('1' => 'Is Published', '0' => 'Is Not Published');

    protected $casts = [
    ];



    public function getImgImageMaxLength(): int
    {
        return $this->img_image_max_length;
    }


    public function creator()
    {
        return $this->belongsTo('App\User','creator_id');
    }


    protected static function boot() {
        parent::boot();
        static::deleting(function($forum) {
            $forum_image_path= ForumCategory::getForumCategoryImagePath($forum->forum_id, $forum->image);
            ForumCategory::deleteFileByPath($forum_image_path, true);
        });
    }


    public function scopeGetByPublished($query, $published = null)
    {
        if (!isset($published) or strlen($published) == 0) {
            return $query;
        }
        return $query->where(with(new ForumCategory)->getTable().'.published', $published);
    }


    public static function getForumCategoryDir(int $forum_id): string
    {
        return self::$uploads_forums_dir . $forum_id . '/';
    }

    public static function getForumCategoryImagePath(int $forum_id, $image): string
    {
        if (empty($image)) {
            return '';
        }
        return self::$uploads_forums_dir . $forum_id . '/' . $image;
    }


    //
    public static function setForumCategoryImageProps(int $forum_id, string $image = null, bool $skip_non_existing_file = false): array
    {
        if (empty($image) and $skip_non_existing_file) {
            return [];
        }
        $file_was_found= true;
        $dir_path = self::$uploads_forums_dir . '' . $forum_id . '';
//        echo '<pre>$dir_path::'.print_r($dir_path,true).'</pre>';
        $file_full_path = $dir_path . '/' . $image;
//        echo '<pre>$file_full_path::'.print_r($file_full_path,true).'</pre>';
        $file_exists    = ( !empty($image) and Storage::disk('local')->exists('public/' . $file_full_path) );
//        echo '<pre>$file_exists::'.print_r($file_exists,true).'</pre>';
        if ( ! $file_exists) {
            if ($skip_non_existing_file) {
                return [];
            }
            $file_was_found= false;
            $file_full_path = config('app.empty_img_url');
        }

        $image_path = $file_full_path;
        //        $image_path= public_path('storage/'.$dir_path . '/' . $image);
        //        $image_path= public_path('storage/app/'.$dir_path . '/' . $image);
        //        $image_path= 'storage/app/'.$dir_path . '/' . $image;
        //        $image_path= $file_full_path;
        // /_wwwroot/lar/Forums/storage/app/public/forums/-forum-1/tobe.png
        if ($file_exists) {
            $image_url       = 'storage/app/' . $dir_path . '/' . $image;
            $imagePropsArray = ['image' => $image, 'image_path' => $image_path, 'image_url' => '/storage/' . $dir_path . '/' . $image];
            $image_full_path = base_path() . '/storage/app/public/' . $image_path;
        } else {
            $imagePropsArray = ['image' => $image, 'image_path' => $image_path, 'image_url' => $file_full_path];
            $image_full_path = base_path() . '/public/' . $image_path;
        }

        if ( ! empty($previewSizeArray['width'])) {
            $imagePropsArray['preview_width']  = $previewSizeArray['width'];
            $imagePropsArray['preview_height'] = $previewSizeArray['height'];
        }
        $forumImgProps = with(new ForumCategory)->getCFImageProps($image_full_path, $imagePropsArray);

        if ( !$file_was_found ) {
            $forumImgProps['file_info'] = 'File "' . $image . '" not found ';
        }
        return $forumImgProps;

    }

    /* get additional properties of forum image : path, url, size etc... */
    public function getForumCategoryImagePropsAttribute(): array
    {
        return $this->forumCategoryPropsArray;
    }

    /* set additional properties of forum image : path, url, size etc... */
    public function setForumCategoryImagePropsAttribute(array $forumCategoryPropsArray)
    {
        $this->forumCategoryPropsArray = $forumCategoryPropsArray;
    }

    public static function getforumCategoryPublishedValueArray($key_return = true): array
    {
        $resArray = [];
        foreach (self::$forumCategoryPublishedLabelValueArray as $key => $value) {
            if ($key_return) {
                $resArray[] = ['key' => $key, 'label' => $value];
            } else {
                $resArray[$key] = $value;
            }
        }
        return $resArray;
    }

    public static function getforumCategoryPublishedLabel(string $published): string
    {
        if ( ! empty(self::$forumCategoryPublishedLabelValueArray[$published])) {
            return self::$forumCategoryPublishedLabelValueArray[$published];
        }
        return self::$forumCategoryPublishedLabelValueArray[0];
    }


    public static function getSimilarForumCategoryByImage( string $image, int $forum_id, int $id= null, $return_count= false )
    {
        $quoteModel = ForumCategory::where( 'image', $image );
        $quoteModel = $quoteModel->where( 'forum_id', '=' , $forum_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }

        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }


    //         return ForumCategory::getValidationRulesArray( $request->get('forum_id'), $request->get('id') );
    public static function getValidationRulesArray($forum_id): array
    {
        $additional_item_value_validation_rule= 'check_forum_unique_by_name:'.$forum_id.','.( !empty($forum_id)?$forum_id:'');
        /* id	smallint(5) unsigned Auto Increment
title	varchar(100)
description	mediumtext
creator_id	int(10) unsigned
published	tinyint(1) [0]
image	varchar(100) NULL
created_at	timestamp [CURRENT_TIMESTAMP]
updated_at	timestamp NULL */

        $validationRulesArray            = [
            'forum_id'      => 'required|exists:'.( with(new Forum)->getTable() ).',id',
            'name'             => 'required|max:255|' . $additional_item_value_validation_rule,
            'is_video'       => 'required|in:' . with(new ForumCategory)->getValueLabelKeys(ForumCategory::getForumCategoryIsCorrectValueArray(false)),
            'ordering'         => 'required|integer',
        ];

        return $validationRulesArray;
    }


    public function getImgPreviewWidth() : int
    {
        return self::$img_preview_width;
    }

    public function getImgPreviewHeight() : int
    {
        return self::$img_preview_height;
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
