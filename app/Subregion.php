<?php

namespace App;
use DB;
use App\MyAppModel;
use App\Http\Traits\FuncsTrait;

class Subregion extends MyAppModel
{
    use FuncsTrait;
    protected $table      = 'subregions';
    protected $primaryKey = 'id';
    public $timestamps    = false;

    protected $fillable = [ 'state_id', 'region_id', 'name' ];
/* 	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`state_id` INT(10) UNSIGNED NOT NULL,
	`region_id` INT(10) UNSIGNED NOT NULL,
	`name` VARCHAR(50) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`slug` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_unicode_ci', */


    public function state()
    {
        return $this->belongsTo('App\State');
    }
    public function scopeGetByStateId($query, $state_id= null)
    {
        if (!empty($state_id)) {
            if ( is_array($state_id) ) {
                $query->whereIn(with(new Subregion)->getTable().'.state_id', $state_id);
            } else {
                $query->where(with(new Subregion)->getTable().'.state_id', $state_id);
            }
        }
        return $query;
    }


    public function region()
    {
        return $this->belongsTo('App\Region');
    }
    public function scopeGetByRegionId($query, $region_id= null)
    {
        if (!empty($region_id)) {
            if ( is_array($region_id) ) {
                $query->whereIn(with(new Subregion)->getTable().'.region_id', $region_id);
            } else {
                $query->where(with(new Subregion)->getTable().'.region_id', $region_id);
            }
        }
        return $query;
    }

    public function scopeGetByName($query, $name= null, $partial= false)
    {
        if (empty($name)) return $query;
        return $query->where(with(new Subregion)->getTable().'.name', (!$partial?'=':'like'), ($partial?'%':''). $name .($partial?'%':'') );
    }


    public static function getSimilarSubregionByName( string $name, int $state_id, int $id= null, $return_count= false )
    {
        $quoteModel = Subregion::where( 'name', $name );
        $quoteModel = $quoteModel->where( 'state_id', '=' , $state_id );
        if ( !empty( $id ) ) {
            $quoteModel = $quoteModel->where( 'id', '!=' , $id );
        }

        if ( $return_count ) {
            return $quoteModel->get()->count();
        }
        $retRow= $quoteModel->get();
        if ( empty($retRow[0]) ) return false;
        return $retRow[0];
    }

    public static function getSubregionsSelectionArray($state_id= '', $order_by= 'name') :array {
        $regions = Subregion
            ::getByStateId($state_id)
            ->orderBy($order_by,'asc')
            ->get();
        $regionsSelectionArray= [];
        foreach( $regions as $nextSubregion ) {
            $regionsSelectionArray[]= (object)[ 'id'=> $nextSubregion->id, 'state_id'=> $nextSubregion->state_id, 'name'=>$nextSubregion->name, 'slug'=>$nextSubregion->slug ];
        }
        return $regionsSelectionArray;
    }

}
