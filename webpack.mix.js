const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//
let current_dashboard_template= 'BS4'



mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/' + current_dashboard_template + '/app.scss', 'public/css/' + current_dashboard_template);
mix.copy('node_modules/font-awesome/fonts', 'public/fonts');






// alert( 'resources/sass/' + current_dashboard_template + '/app.scss' )
/*

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/' + current_dashboard_template + '/app.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/sass/' + current_dashboard_template + '/style_lg.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/sass/' + current_dashboard_template + '/style_md.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/sass/' + current_dashboard_template + '/style_sm.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/sass/' + current_dashboard_template + '/style_xs_320.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/sass/' + current_dashboard_template + '/style_xs_480.scss', 'public/css/' + current_dashboard_template)
    .sass('resources/sass/' + current_dashboard_template + '/style_xs_600.scss', 'public/css/' + current_dashboard_template);

mix.copy('node_modules/font-awesome/fonts', 'public/fonts');
*/
